#include "stdafx.h"
#include "..\Headers\Map_Static.h"
#include "Management.h"
#include "Defines.h"
_USING(Client)

CMap_Static::CMap_Static(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CMap_Static::CMap_Static(const CMap_Static & rhs)
	: CGameObject(rhs)
{
}

HRESULT CMap_Static::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CMap_Static::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	m_isInstancing = false;
	return NOERROR;
}

_int CMap_Static::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
		
	return _int();
}

_int CMap_Static::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	Compute_CameraDistance(&vPosition);

	if (m_fCameraDistance > 7000.f)
		return 0;

	_matrix		matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	if (vPosition.y < -100.f && matView._42 > -100.f)
		return 0;

	if (!m_pFrustumCom->Culling_Frustum(&m_vLocalPos, m_matLocalInv, m_Max))
		return 0;

	if (m_isInstancing)
		m_pRendererCom->Add_Instancing(m_pMeshCom, m_pTransformCom->Get_Matrix());
	else
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;

	if (m_fCameraDistance < 8000.f)
		m_pRendererCom->Add_CubeGroup(this);

	return _int();
}

void CMap_Static::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);


	m_pColliderCom->Render_Collider();
}

void CMap_Static::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);
	
	pEffect->SetVector("g_vLightPos", &vLightPos);
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matW");
	pEffect->SetMatrix("g_matVP", VP);

	pEffect->CommitChanges();
	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		
		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();
}

void CMap_Static::Render_CubeMap(_matrix * View, _matrix * Proj)
{
	LPD3DXEFFECT		pEffect = m_pRendererCom->GetCubeEffectHandle()->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	_matrix matWVP = m_pTransformCom->Get_Matrix() * (*View) * (*Proj);

	pEffect->SetMatrix("g_matWVP", &matWVP);

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();
	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(i);
		if (nullptr == pSubSet)
			return;

		pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);

	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CMap_Static::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;
	Instancing_Check();



	m_iOtherOption = OtherOption;


	//if (6 == m_iOtherOption)
	//{
	//	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Map_Static_FarmHouse_NAVI");
	//	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
	//		_MSG_BOX("111");
	//}
	//else
	//if (3 == m_iOtherOption)
	//{
	//	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Map_Static_Basement_NAVI");
	//	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
	//		_MSG_BOX("111");
	//}
	//else
	//{
	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, Key);
	Add_Component(L"Com_Mesh", m_pMeshCom);
	//}


	if (7 != m_iOtherOption && 8 != m_iOtherOption && 6 != m_iOtherOption && 5 != m_iOtherOption && 4 != m_iOtherOption && 3 != m_iOtherOption)
	{
		m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, m_pMeshCom->Get_LocalTransform(), nullptr, m_pTransformCom->Get_Matrix_Pointer()));
		if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
			_MSG_BOX("111");
	}


	//m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, m_pMeshCom->Get_LocalTransform(), nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	//Add_Component(L"Com_Collider", m_pColliderCom);

	//m_pColliderCom = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	//if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
	//	return E_FAIL;


	if (9 == m_iOtherOption)
	{
		if (FAILED(m_pColliderCom->Ready_HullMesh(SCENE_STAGE, Key)))
			_MSG_BOX("111");
		CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_LAY, this);
	}
	else if (8 == m_iOtherOption)
	{
		m_fRad = 1500.f;
		_matrix			matLocalTransform;
		D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);

		//For.Com_Collider
		m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
		if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
			_MSG_BOX("111");
		if (FAILED(m_pColliderCom->Ready_HullMesh(SCENE_STAGE, Key)))
			_MSG_BOX("111");

		CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_WOOD, this);
	}
	else if (7 == m_iOtherOption)
	{
		m_fRad = 1500.f;
		_matrix			matLocalTransform;
		D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);

		//For.Com_Collider
		m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
		if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
			_MSG_BOX("111");

		if (FAILED(m_pColliderCom->Ready_HullMesh(SCENE_STAGE, Key)))
			_MSG_BOX("111");
		if (FAILED(m_pColliderCom->Ready_RealMesh(m_pMeshCom)))
			_MSG_BOX("111");
		CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_EXIT, this);
	}
	else if (6 == m_iOtherOption)
	{
		m_fRad = 2400.f;
		_matrix			matLocalTransform;
		D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);
		//For.Com_Collider
		m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
		if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
			_MSG_BOX("111");

		if (FAILED(m_pColliderCom->Ready_HullMesh(SCENE_STAGE, Key)))
			_MSG_BOX("111");
		if (FAILED(m_pColliderCom->Ready_NaviMesh(SCENE_STAGE, Key)))
			_MSG_BOX("111");

		CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_HOUSE, this);
	}
	else if (5 == m_iOtherOption)
	{
		m_fRad = 2400.f;
		_matrix			matLocalTransform;
		D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);
		//For.Com_Collider
		m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
		if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
			_MSG_BOX("111");

		if (FAILED(m_pColliderCom->Ready_NaviMesh(SCENE_STAGE, Key)))
			_MSG_BOX("111");

		CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_HUT, this);
	}
	else if (4 == m_iOtherOption)
	{
		m_fRad = 2400.f;
		_matrix			matLocalTransform;
		D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);
		//For.Com_Collider
		m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
		if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
			_MSG_BOX("111");

		if (FAILED(m_pColliderCom->Ready_NaviMesh(SCENE_STAGE, Key)))
			_MSG_BOX("111");

		CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_TERRAIN, this);
	}
	else if (3 == m_iOtherOption)
	{
		m_fRad = 2400.f;
		_matrix			matLocalTransform;
		D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);
		//For.Com_Collider
		m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
		if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
			_MSG_BOX("111");


		if (FAILED(m_pColliderCom->Ready_HullMesh(SCENE_STAGE, Key)))
			_MSG_BOX("111");
		if (FAILED(m_pColliderCom->Ready_NaviMesh(SCENE_STAGE, Key)))
			_MSG_BOX("111");

		CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_UNDER, this);
	}
	//else if (2 == m_iOtherOption)
	//{
	//	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_LAY, this);
	//	m_pColliderCom->Ready_HullMesh(SCENE_STAGE,Key);
	//}

	_vec3 vMax = m_pMeshCom->GetMax();
	_vec3 vMin = m_pMeshCom->GetMin();
	_vec3 vResult = (vMax - vMin) * 0.52f;
	
	m_Max = max(vResult.x, max(vResult.y, vResult.z));

	m_matLocalInv = m_pMeshCom->Get_LocalTransform();

	m_matLocalInv = m_matLocalInv * matWorld;
	m_vLocalPos = *(_vec3*)&m_matLocalInv.m[3][0];
	D3DXMatrixInverse(&m_matLocalInv, nullptr, &m_matLocalInv);
}

_matrix CMap_Static::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CMap_Static::Get_Key()
{
	return m_Key.c_str();
}

_int CMap_Static::Get_OtherOption()
{
	return m_iOtherOption;
}

HRESULT CMap_Static::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CMap_Static::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetTexture("g_MetalicTexture", pSubSet->MeshTexture.pMetallicTexture);
	pEffect->SetTexture("g_RoughnessTexture", pSubSet->MeshTexture.pRoughnessTexture);


	return NOERROR;
}

void CMap_Static::Instancing_Check()
{
	if (m_Key == L"Map_Static_SideFence_Wall" ||
		m_Key == L"Map_Static_SideFence" ||
		m_Key == L"Map_Static_Hay_Square" ||
		m_Key == L"Map_Static_Hay_Round" ||
		m_Key == L"Map_Static_TireStack_01" ||
		m_Key == L"Map_Static_TireStack_02" ||
		m_Key == L"Map_Static_TireStack_03" ||
		m_Key == L"Map_Static_WoodWall_1m" ||
		m_Key == L"Map_Static_WoodWall_2m_01" ||
		m_Key == L"Map_Static_WoodWall_2m_02" ||
		m_Key == L"Map_Static_WoodWall_2m_Jump" ||
		m_Key == L"Map_Static_WoodWall_4m" ||
		m_Key == L"Map_Static_WoodWall_Corner" ||
		m_Key == L"Map_Static_WoodWall_T")
		m_isInstancing = true;
}

CMap_Static * CMap_Static::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CMap_Static*	pInstance = new CMap_Static(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CMap_Static Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CMap_Static::Clone_GameObject()
{
	CMap_Static*	pInstance = new CMap_Static(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CMap_Static Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CMap_Static::Free()
{
	if (9 == m_iOtherOption)
		CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_LAY, this);
	else if (8 == m_iOtherOption)
		CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_WOOD, this);
	else if (7 == m_iOtherOption)
		CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_EXIT, this);
	else if (6 == m_iOtherOption)
		CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_HOUSE, this);
	else if (5 == m_iOtherOption)
		CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_HUT, this);
	else if (4 == m_iOtherOption)
		CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_TERRAIN, this);
	else if (3 == m_iOtherOption)
		CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_UNDER, this);

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	
	Safe_Release(m_pFrustumCom);


	//if(0!=m_iOtherOption)

	Safe_Release(m_pColliderCom);


	CGameObject::Free();
}
