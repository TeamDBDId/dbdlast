#include "stdafx.h"
#include "..\Headers\Action_HealingSelf.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"

_USING(Client)

CAction_HealingSelf::CAction_HealingSelf()
{
}

HRESULT CAction_HealingSelf::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	pCamper->Set_State(AC::HealingSelf);
	m_iState = AC::HealingSelf;
	m_bIsFinished = false;
	return NOERROR;
}

_int CAction_HealingSelf::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	if (!KEYMGR->MousePressing(1))
		return END_ACTION;

	return UPDATE_ACTION;
}

void CAction_HealingSelf::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);
	pCamper->SetCurCondition(pCamper->GetOldCondition());

	if (m_bIsFinished)
		pCamper->SetCurCondition(CCamper::HEALTHY);
}

void CAction_HealingSelf::Send_ServerData()
{
}

void CAction_HealingSelf::Free()
{
	CAction::Free();
}
