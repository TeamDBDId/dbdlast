#include "stdafx.h"
#include "..\Headers\Action_MoveLerp.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Plank.h"
#include "Slasher.h"
#include "Closet.h"
#include "Action_Manager.h"
#include "Action_HideCloset.h"
#include "Action_UnhideCloset.h"
#include "Action_Generator.h"
#include "Action_UnlockExit.h"
#include "Action_LootChest.h"
#include "Action_PassWindow.h"
#include "Action_CleansTotem.h"
#include "Action_PullDownPlank.h"
#include "Action_JumpPlank.h"
#include "Action_HealCamper.h"
#include "Action_WindowVault.h"
#include "Action_Destroy_Pallet.h"
#include "Action_Search_Locker.h"
#include "Action_Damage_Generator.h"
#include "Action_HookIn.h"
#include "Action_HookBeing.h"

_USING(Client)

CAction_MoveLerp::CAction_MoveLerp()
{
}

HRESULT CAction_MoveLerp::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_pGameObject->GetID() & CAMPER)
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->IsLockKey(true);
	}
	else
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->IsLockKey(true);
	}

	m_bIsPlaying = true;
	
	CTransform* pTransform = (CTransform*)m_pTargetOfInteraction->Get_ComponentPointer(L"Com_Transform");
	_vec3 vTargetPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);
	
	CTransform* pMyTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	_vec3 vMyPos = *pMyTransform->Get_StateInfo(CTransform::STATE_POSITION);
	m_vDistPos.y = vMyPos.y;

	if ((m_pTargetOfInteraction->GetID() & TOTEM) && (m_pTargetOfInteraction->GetID() & TOTEM))
		D3DXVec3Normalize(&m_vLook, &(vTargetPos - vMyPos));
	else if (m_pTargetOfInteraction->GetID() & EXITDOOR)
	{
		_vec3 vLook = vTargetPos - vMyPos;
		vLook.y = vMyPos.y;
		D3DXVec3Normalize(&vLook, &vLook);
		m_vLook = vLook;
	}
	else
		D3DXVec3Normalize(&m_vLook, &(vTargetPos - m_vDistPos));

	D3DXVec3Cross(&m_vRight, &_vec3(0.f, 1.f, 0.f), &m_vLook);
	D3DXVec3Normalize(&m_vRight, &m_vRight);

	return NOERROR;
}

_int CAction_MoveLerp::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

	_vec3 vPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);
	_vec3 vLook = *pTransform->Get_StateInfo(CTransform::STATE_LOOK);

	_vec3 vCurDirection = vPos - m_vDistPos;
	_vec3 vNCurDir;

	D3DXVec3Normalize(&vLook, &vLook);
	D3DXVec3Normalize(&vNCurDir, &vCurDirection);

	_float fAngle = acosf(vLook * vNCurDir);

	if (D3DXVec3Length(&vCurDirection) < 0.05f
		&& fAngle <= D3DXToRadian(5.5f))
		return END_ACTION;

	_bool bNotMove = false;
	if (!(m_pTargetOfInteraction->GetID() & TOTEM) && !(m_pTargetOfInteraction->GetID() & PLANK) && !(m_pTargetOfInteraction->GetID() & CAMPER))
		bNotMove = true;
	
	pTransform->Go_ToTarget_ChangeDirection(&m_vLook, fTimeDelta, bNotMove);

	return UPDATE_ACTION;
}

void CAction_MoveLerp::End_Action()
{
	m_bIsPlaying = false;

	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	
	//pTransform->Set_StateInfo(CTransform::STATE_RIGHT, &m_vRight);
	//pTransform->Set_StateInfo(CTransform::STATE_LOOK, &m_vLook);

	pTransform->Go_ToTarget_ChangeDirection(&m_vLook, 0.005f);

	if (!(m_pTargetOfInteraction->GetID() & TOTEM) && !(m_pTargetOfInteraction->GetID() & PLANK) && !(m_pTargetOfInteraction->GetID() & CAMPER))
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &m_vDistPos);

	_uint ID = m_pTargetOfInteraction->GetID();

	if (m_pGameObject->GetID() & CAMPER)
	{
		if (ID & CAMPER)
		{
			if (CCamper::HOOKED == ((CCamper*)m_pTargetOfInteraction)->GetCurCondition())
			{
				CAction_HookBeing* pAction_HookBeing = (CAction_HookBeing*)m_pGameObject->Find_Action(L"Action_HookBeing");
				pAction_HookBeing->SetCamper(m_pTargetOfInteraction);
				m_pGameObject->Set_Action(L"Action_HookBeing", 3.f);
			}
			else
			{
				CAction_HealCamper* pAction_HealCamper = (CAction_HealCamper*)m_pGameObject->Find_Action(L"Action_HealCamper");
				pAction_HealCamper->SetCamper(m_pTargetOfInteraction);
				m_pGameObject->Set_Action(L"Action_HealCamper", 100.f);
			}
		}
		else if (ID & HATCH)
		{

		}
		if(((CCamper*)m_pGameObject)->GetCurCondition() == CCamper::HEALTHY || ((CCamper*)m_pGameObject)->GetCurCondition() == CCamper::INJURED)
		{
			if (ID & CLOSET)
			{
				CCamper* pCamper = (CCamper*)m_pGameObject;
				if (pCamper->IsColl())
				{
					CAction_UnhideCloset* pAction_UnhideCloset = (CAction_UnhideCloset*)m_pGameObject->Find_Action(L"Action_UnhideCloset");
					pAction_UnhideCloset->SetCloset(m_pTargetOfInteraction);
					if (pAction_UnhideCloset->m_bIsPlaying)
						return;

					CAction_HideCloset* pAction_HideCloset = (CAction_HideCloset*)m_pGameObject->Find_Action(L"Action_HideCloset");
					pAction_HideCloset->SetCloset(m_pTargetOfInteraction);
					m_pGameObject->Set_Action(L"Action_HideCloset", 2.f);
				}
			}
			else if (ID & PLANK)
			{
				if (((CPlank*)m_pTargetOfInteraction)->GetState() == CPlank::StandIdle)
				{
					CAction_PullDownPlank* pAction = (CAction_PullDownPlank*)m_pGameObject->Find_Action(L"Action_PullDownPlank");
					pAction->SetPlank(m_pTargetOfInteraction);
					m_pGameObject->Set_Action(L"Action_PullDownPlank", 2.f);
				}
				else
				{
					CAction_JumpPlank* pAction = (CAction_JumpPlank*)m_pGameObject->Find_Action(L"Action_JumpPlank");
					pAction->SetPlank(m_pTargetOfInteraction);
					m_pGameObject->Set_Action(L"Action_JumpPlank", 2.f);
				}
			}
			else if (ID & WINDOW)
			{
				m_pGameObject->Set_Action(L"Action_PassWindow", 1.f);
			}
			else if (ID & CHEST)
			{
				CAction_LootChest* pAction = (CAction_LootChest*)m_pGameObject->Find_Action(L"Action_LootChest");
				pAction->SetChest(m_pTargetOfInteraction);
				m_pGameObject->Set_Action(L"Action_LootChest", 100.f);
			}
			else if (ID & GENERATOR)
			{
				CAction_Generator* pAction = (CAction_Generator*)m_pGameObject->Find_Action(L"Action_Generator");
				pAction->SetGenerator(m_pTargetOfInteraction);
				m_pGameObject->Set_Action(L"Action_Generator", 200.f);
			}
			else if (ID & EXITDOOR)
			{
				CAction_UnlockExit* pAction = (CAction_UnlockExit*)m_pGameObject->Find_Action(L"Action_UnlockExit");
				pAction->SetExitDoor(m_pTargetOfInteraction);
				m_pGameObject->Set_Action(L"Action_UnlockExit", 100.f);
			}
			else if (ID & TOTEM)
			{
				CAction_CleansTotem* pAction = (CAction_CleansTotem*)m_pGameObject->Find_Action(L"Action_CleansTotem");
				pAction->SetTotem(m_pTargetOfInteraction);
				m_pGameObject->Set_Action(L"Action_CleansTotem", 100.f);
			}
		}
	}
	else if (m_pGameObject->GetID() & SLASHER)
	{
		if (ID & CLOSET)
		{
			CAction_SearchLocker* pAction = (CAction_SearchLocker*)m_pGameObject->Find_Action(L"Action_SearchLocker");
			pAction->SetCloset(m_pTargetOfInteraction);
			m_pGameObject->Set_Action(L"Action_SearchLocker", 100.f);
		}
		else if (ID & HOOK)
		{
			CAction_HookIn* pAction = (CAction_HookIn*)m_pGameObject->Find_Action(L"Action_HookIn");
			pAction->SetHook(m_pTargetOfInteraction);
			m_pGameObject->Set_Action(L"Action_HookIn", 5.f);
		}
		else if (ID & PLANK)
		{
			CAction_Destroy_Pallet* pAction = (CAction_Destroy_Pallet*)m_pGameObject->Find_Action(L"Action_Destroy_Pallet");
			pAction->SetPalnk(m_pTargetOfInteraction);
			m_pGameObject->Set_Action(L"Action_Destroy_Pallet", 2.f);
		}
		else if (ID & WINDOW)
		{
			m_pGameObject->Set_Action(L"Action_WindowVault", 3.f);
		}
		else if (ID & GENERATOR)
		{
			CAction_Damage_Generator* pAction = (CAction_Damage_Generator*)m_pGameObject->Find_Action(L"Action_Damage_Generator");
			pAction->SetGenerator(m_pTargetOfInteraction);
			m_pGameObject->Set_Action(L"Action_Damage_Generator", 3.f);
		}
	}
}

void CAction_MoveLerp::Send_ServerData()
{
}

void CAction_MoveLerp::Free()
{
	CAction::Free();
}
