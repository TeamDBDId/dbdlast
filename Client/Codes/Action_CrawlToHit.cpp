#include "stdafx.h"
#include "..\Headers\Action_CrawlToHit.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"

_USING(Client)

CAction_CrawlToHit::CAction_CrawlToHit()
{
}

HRESULT CAction_CrawlToHit::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (!m_bIsInit)
		SetVectorPos();

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(true);
	m_bIsPlaying = true;
	m_fIndex = 0.f;

	CTransform* pCamperTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	m_vOriginPos = *pCamperTransform->Get_StateInfo(CTransform::STATE_POSITION);

	CManagement* pManagement = CManagement::GetInstance();
	CTransform* pSlasherTransform = (CTransform*)pManagement->Get_ComponentPointer(SCENE_STAGE, L"Layer_Slasher", L"Com_Transform");
	
	_vec3 vCamperLook = *pCamperTransform->Get_StateInfo(CTransform::STATE_LOOK);
	_vec3 vSlasherLook = *pSlasherTransform->Get_StateInfo(CTransform::STATE_LOOK);

	_float fAngle = D3DXToDegree(acosf(D3DXVec3Dot(&vCamperLook, &vSlasherLook)));

	if (fAngle >= 90.f)
	{
		pCamper->Set_State(AC::HitToCrawlBk);
		m_iState = AC::HitToCrawlBk;
	}
	else
	{
		pCamper->Set_State(AC::HitToCrawlFk);
		m_iState = AC::HitToCrawlFk;
	}

	return NOERROR;
}

_int CAction_CrawlToHit::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

		m_fIndex += fTimeDelta * 30.f;

	size_t iIndex = (size_t)m_fIndex;

	if (m_iState == AC::HitToCrawlBk)
	{
		if (m_vecPosBK.size() <= iIndex)
			return END_ACTION;

		if (m_fIndex >= 71.f)
			return END_ACTION;

		_vec3 vLocalPos = m_vecPosBK[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

		return UPDATE_ACTION;
	}
	else
	{
		if (m_fIndex >= 99.f)
			return END_ACTION;

		if (m_vecPosFT.size() <= iIndex)
			return END_ACTION;

		_vec3 vLocalPos = m_vecPosFT[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

		return UPDATE_ACTION;
	}

	return UPDATE_ACTION;
}

void CAction_CrawlToHit::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->Set_State(AC::Idle);
	pCamper->IsLockKey(false);
	//pCamper->SetCurCondition(CCamper::DYING);
	pCamper->SetEnergy(pCamper->GetDyingEnergy());
	pCamper->SetMaxEnergyAndMaxHeal();
}

void CAction_CrawlToHit::Send_ServerData()
{
}

void CAction_CrawlToHit::SetVectorPos()
{
	m_vecPosBK.reserve(71);

	m_vecPosBK.push_back(_vec3(3.333f, 0.f, 2.906f));
	m_vecPosBK.push_back(_vec3(3.086f, 0.f, 2.691f));
	m_vecPosBK.push_back(_vec3(2.796f, 0.f, 1.975f));
	m_vecPosBK.push_back(_vec3(3.044f, 0.f, 3.6f));
	m_vecPosBK.push_back(_vec3(5.317f, 0.f, 6.548f));
	m_vecPosBK.push_back(_vec3(7.734f, 0.f, 1.884f));
	m_vecPosBK.push_back(_vec3(8.593f, 0.f, 11.27f));
	m_vecPosBK.push_back(_vec3(8.225f, 0.f, 15.443f));
	m_vecPosBK.push_back(_vec3(7.857f, 0.f, 18.913f));
	m_vecPosBK.push_back(_vec3(9.211f, 0.f, 23.861f));
	m_vecPosBK.push_back(_vec3(12.35f, 0.f, 24.818f));
	m_vecPosBK.push_back(_vec3(5.189f, 0.f, 22.089f));
	m_vecPosBK.push_back(_vec3(-4.551f, 0.f, 14.578f));
	m_vecPosBK.push_back(_vec3(-11.348f, 0.f, 5.658f));
	m_vecPosBK.push_back(_vec3(-17.354f, 0.f, -4.216f));
	m_vecPosBK.push_back(_vec3(-20.236f, 0.f, -14.486f));
	m_vecPosBK.push_back(_vec3(-21.293f, 0.f, -25.188f));
	m_vecPosBK.push_back(_vec3(-22.761f, 0.f, -35.184f));
	m_vecPosBK.push_back(_vec3(-29.192f, 0.f, -44.042f));
	m_vecPosBK.push_back(_vec3(-33.977f, 0.f, -50.185f));
	m_vecPosBK.push_back(_vec3(-35.735f, 0.f, -52.643f));
	m_vecPosBK.push_back(_vec3(-37.08f, 0.f, -54.134f));
	m_vecPosBK.push_back(_vec3(-39.387f, 0.f, -56.163f));
	m_vecPosBK.push_back(_vec3(-43.175f, 0.f, -57.161f));
	m_vecPosBK.push_back(_vec3(-46.2f, 0.f, -56.164f));
	m_vecPosBK.push_back(_vec3(-49.072f, 0.f, -54.768f));
	m_vecPosBK.push_back(_vec3(-51.385f, 0.f, -53.514f));
	m_vecPosBK.push_back(_vec3(-53.226f, 0.f, -52.604f));
	m_vecPosBK.push_back(_vec3(-54.911f, 0.f, -52.086f));
	m_vecPosBK.push_back(_vec3(-56.277f, 0.f, -52.195f));
	m_vecPosBK.push_back(_vec3(-57.321f, 0.f, -52.413f));
	m_vecPosBK.push_back(_vec3(-58.033f, 0.f, -52.687f));
	m_vecPosBK.push_back(_vec3(-58.508f, 0.f, -53.249f));
	m_vecPosBK.push_back(_vec3(-58.775f, 0.f, -53.894f));
	m_vecPosBK.push_back(_vec3(-58.677f, 0.f, -54.261f));
	m_vecPosBK.push_back(_vec3(-58.488f, 0.f, -54.578f));
	m_vecPosBK.push_back(_vec3(-58.225f, 0.f, -54.922f));
	m_vecPosBK.push_back(_vec3(-58.194f, 0.f, -55.024f));
	m_vecPosBK.push_back(_vec3(-58.274f, 0.f, -54.92f));
	m_vecPosBK.push_back(_vec3(-58.357f, 0.f, -54.807f));
	m_vecPosBK.push_back(_vec3(-58.338f, 0.f, -54.836f));
	m_vecPosBK.push_back(_vec3(-58.261f, 0.f, -54.931f));
	m_vecPosBK.push_back(_vec3(-58.148f, 0.f, -55.035f));
	m_vecPosBK.push_back(_vec3(-58.003f, 0.f, -55.148f));
	m_vecPosBK.push_back(_vec3(-57.832f, 0.f, -55.268f));
	m_vecPosBK.push_back(_vec3(-57.637f, 0.f, -55.393f));
	m_vecPosBK.push_back(_vec3(-57.424f, 0.f, -55.522f));
	m_vecPosBK.push_back(_vec3(-57.197f, 0.f, -55.653f));
	m_vecPosBK.push_back(_vec3(-56.959f, 0.f, -55.784f));
	m_vecPosBK.push_back(_vec3(-56.716f, 0.f, -55.914f));
	m_vecPosBK.push_back(_vec3(-56.472f, 0.f, -56.042f));
	m_vecPosBK.push_back(_vec3(-56.23f, 0.f, -56.165f));
	m_vecPosBK.push_back(_vec3(-55.995f, 0.f, -56.281f));
	m_vecPosBK.push_back(_vec3(-55.772f, 0.f, -56.391f));
	m_vecPosBK.push_back(_vec3(-55.565f, 0.f, -56.49f));
	m_vecPosBK.push_back(_vec3(-55.378f, 0.f, -56.579f));
	m_vecPosBK.push_back(_vec3(-55.215f, 0.f, -56.656f));
	m_vecPosBK.push_back(_vec3(-55.081f, 0.f, -56.718f));
	m_vecPosBK.push_back(_vec3(-54.98f, 0.f, -56.765f));
	m_vecPosBK.push_back(_vec3(-54.916f, 0.f, -56.794f));
	m_vecPosBK.push_back(_vec3(-54.894f, 0.f, -56.804f));
	m_vecPosBK.push_back(_vec3(-54.894f, 0.f, -56.804f));
	m_vecPosBK.push_back(_vec3(-54.894f, 0.f, -56.804f));
	m_vecPosBK.push_back(_vec3(-54.894f, 0.f, -56.804f));
	m_vecPosBK.push_back(_vec3(-54.894f, 0.f, -56.804f));
	m_vecPosBK.push_back(_vec3(-54.894f, 0.f, -56.804f));
	m_vecPosBK.push_back(_vec3(-54.894f, 0.f, -56.804f));
	m_vecPosBK.push_back(_vec3(-54.894f, 0.f, -56.804f));
	m_vecPosBK.push_back(_vec3(-54.894f, 0.f, -56.804f));
	m_vecPosBK.push_back(_vec3(-54.894f, 0.f, -56.804f));
	m_vecPosBK.push_back(_vec3(-54.894f, 0.f, -56.804f));

	m_vecPosFT.reserve(99);

	m_vecPosFT.push_back(_vec3(-3.333f, 0.f, 2.906f));
	m_vecPosFT.push_back(_vec3(-1.701f, 0.f, 2.775f));
	m_vecPosFT.push_back(_vec3(-0.152f, 0.f, 13.181f));
	m_vecPosFT.push_back(_vec3(-0.299f, 0.f, 22.135f));
	m_vecPosFT.push_back(_vec3(-0.571f, 0.f, 28.063f));
	m_vecPosFT.push_back(_vec3(-2.171f, 0.f, 33.634f));
	m_vecPosFT.push_back(_vec3(-2.062f, 0.f, 36.357f));
	m_vecPosFT.push_back(_vec3(-2.047f, 0.f, 39.207f));
	m_vecPosFT.push_back(_vec3(-2.669f, 0.f, 42.437f));
	m_vecPosFT.push_back(_vec3(-3.242f, 0.f, 45.59f));
	m_vecPosFT.push_back(_vec3(-3.485f, 0.f, 49.604f));
	m_vecPosFT.push_back(_vec3(-3.608f, 0.f, 52.611f));
	m_vecPosFT.push_back(_vec3(-3.904f, 0.f, 57.121f));
	m_vecPosFT.push_back(_vec3(-4.156f, 0.f, 60.692f));
	m_vecPosFT.push_back(_vec3(-4.701f, 0.f, 66.555f));
	m_vecPosFT.push_back(_vec3(-5.592f, 0.f, 74.045f));
	m_vecPosFT.push_back(_vec3(-6.198f, 0.f, 78.168f));
	m_vecPosFT.push_back(_vec3(-7.213f, 0.f, 85.671f));
	m_vecPosFT.push_back(_vec3(-6.082f, 0.f, 94.29f));
	m_vecPosFT.push_back(_vec3(-5.591f, 0.f, 98.481f));
	m_vecPosFT.push_back(_vec3(-4.889f, 0.f, 105.889f));
	m_vecPosFT.push_back(_vec3(-5.714f, 0.f, 109.15f));
	m_vecPosFT.push_back(_vec3(-7.522f, 0.f, 114.531f));
	m_vecPosFT.push_back(_vec3(-8.975f, 0.f, 121.172f));
	m_vecPosFT.push_back(_vec3(-9.51f, 0.f, 122.331f));
	m_vecPosFT.push_back(_vec3(-8.864f, 0.f, 123.77f));
	m_vecPosFT.push_back(_vec3(-9.133f, 0.f, 123.808f));
	m_vecPosFT.push_back(_vec3(-9.271f, 0.f, 123.123f));
	m_vecPosFT.push_back(_vec3(-10.031f, 0.f, 121.01f));
	m_vecPosFT.push_back(_vec3(-10.382f, 0.f, 121.132f));
	m_vecPosFT.push_back(_vec3(-10.599f, 0.f, 122.616f));
	m_vecPosFT.push_back(_vec3(-12.114f, 0.f, 123.774f));
	m_vecPosFT.push_back(_vec3(-13.129f, 0.f, 123.567f));
	m_vecPosFT.push_back(_vec3(-13.663f, 0.f, 123.043f));
	m_vecPosFT.push_back(_vec3(-14.407f, 0.f, 122.27f));
	m_vecPosFT.push_back(_vec3(-14.579f, 0.f, 121.874f));
	m_vecPosFT.push_back(_vec3(-14.03f, 0.f, 121.463f));
	m_vecPosFT.push_back(_vec3(-13.19f, 0.f, 121.239f));
	m_vecPosFT.push_back(_vec3(-12.719f, 0.f, 121.122f));
	m_vecPosFT.push_back(_vec3(-11.893f, 0.f, 121.429f));
	m_vecPosFT.push_back(_vec3(-11.559f, 0.f, 121.342f));
	m_vecPosFT.push_back(_vec3(-11.566f, 0.f, 121.346f));
	m_vecPosFT.push_back(_vec3(-11.6f, 0.f, 121.391f));
	m_vecPosFT.push_back(_vec3(-11.602f, 0.f, 121.39f));
	m_vecPosFT.push_back(_vec3(-11.617f, 0.f, 121.409f));
	m_vecPosFT.push_back(_vec3(-11.609f, 0.f, 121.41f));
	m_vecPosFT.push_back(_vec3(-11.604f, 0.f, 121.413f));
	m_vecPosFT.push_back(_vec3(-11.591f, 0.f, 121.434f));
	m_vecPosFT.push_back(_vec3(-11.596f, 0.f, 121.435f));
	m_vecPosFT.push_back(_vec3(-11.656f, 0.f, 121.38f));
	m_vecPosFT.push_back(_vec3(-11.661f, 0.f, 121.367f));
	m_vecPosFT.push_back(_vec3(-11.663f, 0.f, 121.365f));
	m_vecPosFT.push_back(_vec3(-11.665f, 0.f, 121.362f));
	m_vecPosFT.push_back(_vec3(-11.667f, 0.f, 121.36f));
	m_vecPosFT.push_back(_vec3(-11.666f, 0.f, 121.361f));
	m_vecPosFT.push_back(_vec3(-11.663f, 0.f, 121.358f));
	m_vecPosFT.push_back(_vec3(-11.674f, 0.f, 121.334f));
	m_vecPosFT.push_back(_vec3(-11.654f, 0.f, 121.337f));
	m_vecPosFT.push_back(_vec3(-11.575f, 0.f, 121.335f));
	m_vecPosFT.push_back(_vec3(-11.454f, 0.f, 121.329f));
	m_vecPosFT.push_back(_vec3(-11.293f, 0.f, 121.32f));
	m_vecPosFT.push_back(_vec3(-11.094f, 0.f, 121.308f));
	m_vecPosFT.push_back(_vec3(-10.86f, 0.f, 121.292f));
	m_vecPosFT.push_back(_vec3(-10.594f, 0.f, 121.273f));
	m_vecPosFT.push_back(_vec3(-10.296f, 0.f, 121.252f));
	m_vecPosFT.push_back(_vec3(-9.969f, 0.f, 121.228f));
	m_vecPosFT.push_back(_vec3(-9.617f, 0.f, 121.202f));
	m_vecPosFT.push_back(_vec3(-9.24f, 0.f, 121.174f));
	m_vecPosFT.push_back(_vec3(-8.841f, 0.f, 121.144f));
	m_vecPosFT.push_back(_vec3(-8.422f, 0.f, 121.111f));
	m_vecPosFT.push_back(_vec3(-7.986f, 0.f, 121.078f));
	m_vecPosFT.push_back(_vec3(-7.535f, 0.f, 121.042f));
	m_vecPosFT.push_back(_vec3(-7.07f, 0.f, 121.006f));
	m_vecPosFT.push_back(_vec3(-6.595f, 0.f, 120.969f));
	m_vecPosFT.push_back(_vec3(-6.111f, 0.f, 120.93f));
	m_vecPosFT.push_back(_vec3(-5.621f, 0.f, 120.891f));
	m_vecPosFT.push_back(_vec3(-5.126f, 0.f, 120.852f));
	m_vecPosFT.push_back(_vec3(-4.63f, 0.f, 120.812f));
	m_vecPosFT.push_back(_vec3(-4.134f, 0.f, 120.772f));
	m_vecPosFT.push_back(_vec3(-3.64f, 0.f, 120.733f));
	m_vecPosFT.push_back(_vec3(-3.151f, 0.f, 120.693f));
	m_vecPosFT.push_back(_vec3(-2.649f, 0.f, 120.654f));
	m_vecPosFT.push_back(_vec3(-2.197f, 0.f, 120.616f));
	m_vecPosFT.push_back(_vec3(-1.735f, 0.f, 120.578f));
	m_vecPosFT.push_back(_vec3(-1.288f, 0.f, 120.542f));
	m_vecPosFT.push_back(_vec3(-0.856f, 0.f, 120.507f));
	m_vecPosFT.push_back(_vec3(-0.442f, 0.f, 120.473f));
	m_vecPosFT.push_back(_vec3(-0.049f, 0.f, 120.441f));
	m_vecPosFT.push_back(_vec3(-0.322f, 0.f, 120.411f));
	m_vecPosFT.push_back(_vec3(-0.668f, 0.f, 120.382f));
	m_vecPosFT.push_back(_vec3(-0.987f, 0.f, 120.356f));
	m_vecPosFT.push_back(_vec3(-1.277f, 0.f, 120.332f));
	m_vecPosFT.push_back(_vec3(-1.535f, 0.f, 120.311f));
	m_vecPosFT.push_back(_vec3(-1.76f, 0.f, 120.293f));
	m_vecPosFT.push_back(_vec3(-1.949f, 0.f, 120.277f));
	m_vecPosFT.push_back(_vec3(-2.1f, 0.f, 120.265f));
	m_vecPosFT.push_back(_vec3(-2.21f, 0.f, 120.255f));
	m_vecPosFT.push_back(_vec3(-2.278f, 0.f, 120.25f));
	m_vecPosFT.push_back(_vec3(-2.301f, 0.f, 120.248f));

	m_bIsInit = true;
}

void CAction_CrawlToHit::Free()
{
	CAction::Free();
}
