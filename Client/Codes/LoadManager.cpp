#include "stdafx.h"
#include "Defines.h"
#include "..\Headers\LoadManager.h"
#include "fstream"
#include "Management.h"
#include "Map_Static.h"
#include "Map_Navi.h"
#include "Grass.h"
#include "Tree.h"
#include "LoadManager.h"
#include "Generator.h"
#include "Closet.h"
#include "Plank.h"
#include "MeatHook.h"
#include "Hatch.h"
#include "ExitDoor.h"
#include "Chest.h"
#include "Sky.h"
#include "Terrain.h"
#include "Totem.h"
#include "Barrel.h"
#include "Lamp_01.h"
#include "Lamp_02.h"
#include "Light_01.h"
#include "Light_02.h"
#include "CollisionBox.h"
#include "MeshTexture.h"
#include "UI_Texture.h"
#include "UI_GameState.h"
#include "UI_Interaction.h"
#include "Camper.h"
#include "Slasher.h"
#include "Camera_Camper.h"
#include "Camera_Slasher.h"
#include "Moon.h"
#include "Machete.h"
#include "Spider.h"
#include "UI_Logo.h"
#include "UI_RoleSelection.h"
#include "UI_OverlayMenu.h"
#include "CustomLight.h"


_USING(Client)
_IMPLEMENT_SINGLETON(CLoadManager)

CLoadManager::CLoadManager()
{
}

void CLoadManager::LoadMapData(_uint Stage)
{
 	GET_INSTANCE_MANAGEMENT;

	wifstream fin;

	_tchar Path[256] = L"";
	swprintf_s(Path, L"../Bin/Data/Map/Stage%d.txt", Stage+1);
	fin.open(Path);

	_tchar szBuf[256] = L"";
	_matrix matWorld;
	_int OtherOption = 0;
	while (true)
	{
		for (int i = 0; i < 16; i++)
		{
			fin.getline(szBuf, 256, '|');
			_float temp = (_float)_wtof(szBuf);
			memcpy((_float*)&matWorld + i, &temp, sizeof(_float));
		}

		fin.getline(szBuf, 256, '|');
		wstring Name = szBuf;
		wstring* ObjName = nullptr;
		for (auto& iter : m_vecObjectName)
		{
			if (*iter == Name)
				ObjName = iter;
		}
		if (ObjName == nullptr)
		{
			ObjName = new wstring;
			*ObjName = Name;
			m_vecObjectName.push_back(ObjName);
		}
		fin.getline(szBuf, 256);
		OtherOption = _wtoi(szBuf);
		if (fin.eof())
			break;

		CGameObject* pGameObject = nullptr;
		const _tchar* LayerName = L"";

		_int ObjectNum = Find_ObjectName(Name);
		if (ObjectNum == 0)
		{
			pManagement->Add_GameObjectToLayer(L"GameObject_StaticMap", SCENE_STAGE, L"Layer_Map_Static", &pGameObject);
		}
		else if (ObjectNum == 1)
			pManagement->Add_GameObjectToLayer(L"GameObject_NaviMap", SCENE_STAGE, L"Layer_Map_Navi", &pGameObject);
		else
		{
			pManagement->Add_GameObjectToLayer((*ObjName).c_str(), SCENE_STAGE, L"Layer_GameObject", &pGameObject);
			if(ObjectNum!=9)
				pGameObject->SetID(ObjectNum);
		}
		
		

		if (ObjectNum == 0 && (OtherOption != 9&& OtherOption != 6 && OtherOption != 5 && OtherOption != 4 && OtherOption != 3))
			pGameObject->Set_MapIndex(1000);
		else
			Setup_Map_Index(pGameObject, matWorld);


		pGameObject->Set_Base(Name.c_str(), matWorld, OtherOption);
	}
	Safe_Release(pManagement);
}

void CLoadManager::Setup_Map_Index(CGameObject * pGameObject, const _matrix& matWorld)
{
	_vec3 vPosition = { matWorld._41, matWorld._42, matWorld._43 };

	_int iIndex = Compute_Map_Index(vPosition);


	if (0<=iIndex  && iIndex < 196)
		pGameObject->Set_MapIndex(iIndex);
}

_int CLoadManager::Compute_Map_Index(const _vec3 & vPosition)
{

	_int IndexX = (_int)(vPosition.x / _MAP_INTERVAL);
	_int IndexZ = (_int)(vPosition.z / _MAP_INTERVAL);

	return (IndexX * 14) + IndexZ;
}

_int CLoadManager::Find_ObjectName(wstring Key)
{
	_int ObjName = -1;
	if (wstring::npos != Key.find(L"Map_Static", 0))
		ObjName = 0;
	else if (wstring::npos != Key.find(L"Map_Navi", 0))
		ObjName = 1;
	else if (wstring::npos != Key.find(L"GameObject_Map_Closet", 0))
		ObjName = CLOSET;
	else if (wstring::npos != Key.find(L"GameObject_Map_Generator", 0))
		ObjName = GENERATOR;
	else if (wstring::npos != Key.find(L"GameObject_Map_MeatHook", 0))
		ObjName = HOOK;
	else if (wstring::npos != Key.find(L"GameObject_Map_Plank", 0))
		ObjName = PLANK;
	else if (wstring::npos != Key.find(L"GameObject_Map_Chest", 0))
		ObjName = CHEST;
	else if (wstring::npos != Key.find(L"GameObject_Map_ExitDoor", 0))
		ObjName = EXITDOOR;
	else if (wstring::npos != Key.find(L"GameObject_Map_Hatch", 0))
		ObjName = HATCH;
	else if (wstring::npos != Key.find(L"GameObject_Map_CollisionBox", 0))
		ObjName = WINDOW;
	else if (wstring::npos != Key.find(L"GameObject_Map_Totem", 0))
		ObjName = TOTEM;
	else if (wstring::npos != Key.find(L"GameObject_Map", 0))
		ObjName = 9;

	return ObjName;
}

HRESULT CLoadManager::Add_ReplacedName()
{
	CMeshTexture* pMeshTexture = GET_INSTANCE(CMeshTexture);
	pMeshTexture->AddRef();

	//WoodWall
	pMeshTexture->Add_ReplacedName(L"WoodPlank04_CAO.tga", L"T_SM_Wood_Wall_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"WoodPlank06_CAO.tga", L"T_SM_Wood_Wall_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rust04_CAO.tga", L"T_SM_Wood_Wall_CAO.tga");
	//WoodFence
	pMeshTexture->Add_ReplacedName(L"T_DriftWoodPlank01_N.tga", L"FlatNormal.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rope01_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rope01_CAO.tga", L"Black.tga");
	pMeshTexture->Add_ReplacedName(L"T_DriftWoodPlank01_CAO.tga", L"T_WoodFence03_CAO.tga");
	//Tree
	pMeshTexture->Add_ReplacedName(L"T_treeLeaf01_N.tga", L"FlatNormal.tga");
	//Trash
	pMeshTexture->Add_ReplacedName(L"T_Rubber_CAO.tga", L"T_SM_Trash01.tga");
	pMeshTexture->Add_ReplacedName(L"T_CarCrusher_MetalPlate05_CAO.tga", L"T_SM_Trash01.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rust01_CAO.tga", L"T_SM_Trash01.tga");
	//TireStack
	pMeshTexture->Add_ReplacedName(L"T_SM_BK_TireStacks_N.tga", L"T_FlatNormal_01.tga");
	//SideFence
	pMeshTexture->Add_ReplacedName(L"T_Rust01_CAO.tga", L"T_Fence01_Rust01_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rust01_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_Brick01_CAO.tga", L"T_DefaultA_CAO.tga");
	//RockWall
	pMeshTexture->Add_ReplacedName(L"T_Rock01_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rock01_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rock01_CAO.tga", L"T_RockWall01_CAO.tga");
	//Others
	//Small Stones
	pMeshTexture->Add_ReplacedName(L"T_Concrete05_N.tga", L"T_FlatNormal_01.tga");
	//Mechine
		//Transporter
	pMeshTexture->Add_ReplacedName(L"T_MetalBlack01_CAO.tga", L"T_Transport_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rust04_CAO.tga", L"T_Transport_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rust05_CAO.tga", L"T_Transport_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_BusParts_CAO.tga", L"T_Transport_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_SM_BK_Tire01_CAO.tga", L"T_Transport_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_StairsRusty01_CAO.tga", L"T_Transport_CAO.tga");
		//PickupTruck
	pMeshTexture->Add_ReplacedName(L"T_CarCrusher_MetalPlate05_CAO.tga", L"SM_PickupTruck_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rubber_CAO.tga", L"SM_PickupTruck_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_glass_CAO.tga", L"SM_PickupTruck_01.tga");
		//OldTractor
	pMeshTexture->Add_ReplacedName(L"T_Trackter01_N.tga", L"T_FlatNormal_01.tga");
		//FarmMachine
	pMeshTexture->Add_ReplacedName(L"T_BurgundyRust_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_ManureSpreaderWheel01_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_MetalBlack01_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rust04_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rust05_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_BurgundyRust_CAO.tga", L"T_ManureSpreader_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_ManureSpreaderWheel01_CAO.tga", L"T_ManureSpreader_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_MetalBlack01_CAO.tga", L"T_ManureSpreader_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rust04_CAO.tga", L"T_ManureSpreader_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rust05_CAO.tga", L"T_ManureSpreader_CAO.tga");
		//LogPile
	pMeshTexture->Add_ReplacedName(L"T_tree03_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_treeRing01_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_treeRing01_CAO.tga", L"T_LogPile_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_tree03_CAO.tga", L"T_LogPile_CAO.tga");
		//LumberPile
	pMeshTexture->Add_ReplacedName(L"T_tree02_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_tree03_D_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_treeRing01_D_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_tree02_CAO.tga", L"T_DefaultA_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_tree03_D_CAO.tga", L"T_DefaultA_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_treeRing01_D_CAO.tga", L"T_DefaultA_CAO.tga");
		//SlaughterTree
	pMeshTexture->Add_ReplacedName(L"T_tree02_D_D_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_tree02_CAO.tga", L"T_Slaughtertree01_CAO.tga");
		//Hut
	pMeshTexture->Add_ReplacedName(L"T_MetalPlates01_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_ShackPlate01_N.tga", L"T_FlatNormal_01.tga");
	pMeshTexture->Add_ReplacedName(L"T_DriftWoodPlank01_N.tga", L"FlatNormal.tga");
	pMeshTexture->Add_ReplacedName(L"T_WoodPlank07_N.tga", L"FlatNormal.tga");
	pMeshTexture->Add_ReplacedName(L"T_ShackPlate01_CAO.tga", L"T_DefaultA_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_DriftWoodPlank01_CAO.tga", L"T_DefaultA_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_WoodPlank07_CAO.tga", L"T_DefaultA_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_WoodPlank07_M.tga", L"T_DriftWoodPlank01_M.tga");
	pMeshTexture->Add_ReplacedName(L"T_MetalPlates01_D.tga", L"T_MetalPlates01_CAO.tga");
		//Hay	
	pMeshTexture->Add_ReplacedName(L"T_Haybale01_N.tga", L"T_FlatNormal_01.tga");
		//FarmHouse
	pMeshTexture->Add_ReplacedName(L"WoodPlank05_CAO.tga", L"T_FarmFloor_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Concrete05_CAO.tga", L"T_FarmFloor_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Miscellaneous_Farm02_CAO.tga", L"T_FarmFences_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"WoodPlank06_CAO.tga", L"T_FarmFloor_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_WoodPlankWhite_CAO.tga", L"T_Farm_miscellaneous_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_FactoryFloor01_CAO.tga", L"T_FarmFloor_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_WhiteWall_CAO.tga", L"T_Farm_miscellaneous_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_FactoryRoof01_CAO.tga", L"T_FarmRoof_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"WoodPlank04_CAO.tga", L"T_FarmBase_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_FarmCelling01_CAO.tga", L"T_FarmBase_CAO.tga");
		//ExitBuilding
	pMeshTexture->Add_ReplacedName(L"T_Concrete03_CAO.tga", L"T_ExitFloor01_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Escape_Ground_CAO.tga", L"T_Escape_Ground_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Brick01_CAO.tga", L"T_DefaultA_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Miscellaneous_01_CAO.tga", L"T_DefaultA_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Miscellaneous_ExitBuilding_CAO.tga", L"T_Miscellaneous_ExitBuilding_D.tga");
		//DeadCow
	pMeshTexture->Add_ReplacedName(L"T_SlaughterHook01_CAO.tga", L"DeadCow02_N.tga");
	pMeshTexture->Add_ReplacedName(L"T_SlaughterHook02_CAO.tga", L"DeadCow02_N.tga");
		//Basement
	pMeshTexture->Add_ReplacedName(L"T_Brick02_CAO.tga", L"T_GasStation_CAO_03.tga");
	pMeshTexture->Add_ReplacedName(L"WoodPlank07_D_CAO.tga", L"T_Basement_Wood_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"WoodPlank05_D_CAO.tga", L"T_Basement_Wood_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rust04_D_CAO.tga", L"T_Basement_Brick_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Concrete03_D_CAO.tga", L"T_Basement_Concrete_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"WoodPlank06_CAO.tga", L"T_Basement_Wall_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"WoodPlank04_CAO.tga", L"T_Basement_Wall_CAO.tga");
	pMeshTexture->Add_ReplacedName(L"T_Rust05_CAO.tga", L"Black.tga");
	Safe_Release(pMeshTexture);
	return NOERROR;
}

HRESULT CLoadManager::Prototype_S1_GameObject()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	// For.GameObject_UI_Texture
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_UI_Texture", CUI_Texture::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Terrain
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Terrain", CTerrain::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Sky
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Sky", CSky::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Static
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_StaticMap", CMap_Static::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Navi
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_NaviMap", CMap_Navi::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Grass
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Grass", CGrass::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Tree
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Tree", CTree::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Closet
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Closet", CCloset::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Generator
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Generator", CGenerator::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Hook
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_MeatHook", CMeatHook::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Plank
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Plank", CPlank::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Hatch
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Hatch", CHatch::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_ExitDoor
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_ExitDoor", CExitDoor::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Chest
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Chest", CChest::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Totem
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Totem", CTotem::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Barrel
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Barrel", CBarrel::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Light_01
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Light_01", CLight_01::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Light_02
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Light_02", CLight_02::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Lamp_01
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Lamp_01", CLamp_01::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_Lamp_02
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_Lamp_02", CLamp_02::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Map_CollisionBox
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Map_CollisionBox", CCollisionBox::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_UI_GameState
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_UI_GameState", CUI_GameState::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_UI_Interaction
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_UI_Interaction", CUI_Interaction::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.GameObject_Spider
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Spider", CSpider::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.GameObject_Camper
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Camper", CCamper::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Slasher
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Slasher", CSlasher::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Camera_Camper
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Camera_Camper", CCamera_Camper::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Camera_Slasher
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Camera_Slasher", CCamera_Slasher::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Moon
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Moon", CMoon::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Machete
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Machete", CMachete::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_Machete
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_CustomLight", CCustomLight::Create(m_pGraphic_Device))))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CLoadManager::Prototype_S1_StaticMesh()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	CMesh_Static* pMesh_Static = nullptr;

	// For.Component_Shader_Terrain
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Terrain", CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Terrain.fx"))))
		return E_FAIL;
	// For.Component_Shader_Mesh
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Shader_Mesh", CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Mesh.fx"))))
		return E_FAIL;
	// For.Map_Static_LightCone
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_LightCone", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/EffectMesh/LightCone/", L"LightCone.x"))))
		return E_FAIL;
	// For.Map_Static_FarmHouse
   	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_FarmHouse", pMesh_Static = CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/FarmHouse/", L"FarmHouse.x"))))
		return E_FAIL;
	// ForMap_Static_FarmHouse_Floor
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_FarmHouse_Floor", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/FarmHouse/", L"FarmHouse_Floor.x"))))
		return E_FAIL;
	// For.Mesh_Grass_1
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_1", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_1.x"))))
		return E_FAIL;
	// For.Mesh_Grass_2
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_2", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_2.x"))))
		return E_FAIL;
	// For.Mesh_Grass_3
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_3", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_3.x"))))
		return E_FAIL;
	// For.Mesh_Grass_4
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_4", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_4.x"))))
		return E_FAIL;
	// For.Mesh_Grass_5
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_5", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_5.x"))))
		return E_FAIL;
	// For.Mesh_Grass_6
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_6", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_6.x"))))
		return E_FAIL;
	// For.Mesh_Grass_7
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_7", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_7.x"))))
		return E_FAIL;
	// For.Mesh_Grass_8
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_8", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_8.x"))))
		return E_FAIL;
	// For.Mesh_Grass_9
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_9", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_9.x"))))
		return E_FAIL;
	// For.Mesh_Grass_10
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_10", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_10.x"))))
		return E_FAIL;
	// For.Mesh_Grass_11
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_11", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_11.x"))))
		return E_FAIL;
	// For.Mesh_Grass_12
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_12", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_12.x"))))
		return E_FAIL;
	// For.Mesh_Grass_13
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_13", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_13.x"))))
		return E_FAIL;
	// For.Mesh_Grass_14
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_14", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_14.x"))))
		return E_FAIL;
	// For.Mesh_Grass_15
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_15", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_15.x"))))
		return E_FAIL;
	// For.Mesh_Grass_16
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_16", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_16.x"))))
		return E_FAIL;
	// For.Mesh_Grass_17
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_17", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_17.x"))))
		return E_FAIL;
	// For.Mesh_Grass_18
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_18", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_18.x"))))
		return E_FAIL;
	// For.Mesh_Grass_19
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_19", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_19.x"))))
		return E_FAIL;
	// For.Mesh_Grass_20
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Grass_20", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Grass/", L"Grass_20.x"))))
		return E_FAIL;
	// For.Map_Static_ExitBuilding
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_ExitBuilding", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/ExitBuilding/", L"ExitBuilding.x"))))
		return E_FAIL;
	// For.Map_Static_ExitBuilding_Ground
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_ExitBuilding_Ground", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/ExitBuilding/", L"ExitBuilding_Ground.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_1.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_2.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_03
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_03", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_3.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_04
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_04", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_4.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_05
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_05", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_5.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_06
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_06", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_6.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_07
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_07", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_7.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_08
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_08", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_8.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_09
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_09", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_9.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_10
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_10", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_10.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_11
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_11", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_11.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_12
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_12", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_12.x"))))
		return E_FAIL;
	// For.Map_Static_Rock13
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_13", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_13.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_14
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_14", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_14.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_15
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_15", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_15.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_16
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_16", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_16.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_17
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_17", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_17.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_18
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_18", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_18.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_19
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_19", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_19.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_20
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_20", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_20.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_21
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_21", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_21.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_22
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_22", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_22.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_23
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_23", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_23.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_24
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_24", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_24.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_25
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_25", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_25.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_26
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_26", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_26.x"))))
		return E_FAIL;
	// For.Map_Static_Rock_27
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_27", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Rock/", L"Rock_27.x"))))
		return E_FAIL;
	// For.Mesh_Tree_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Tree_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Tree/", L"Tree_01.x"))))
		return E_FAIL;
	// For.Mesh_Tree_03
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Tree_03", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Tree/", L"Tree_03.x"))))
		return E_FAIL;
	// For.Map_Static_Basement
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Basement", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Basement/", L"Basement.x"))))
		return E_FAIL;
	// For.Map_Navi_Basement_Navi
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Navi_Basement_Navi", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Basement/", L"Basement_Navi.x"))))
		return E_FAIL;
	// For.Map_Static_Basement_Wall_2m
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Basement_Wall_2m", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Basement/", L"Basement_Wall_2m.x"))))
		return E_FAIL;
	// For.Map_Static_Basement_Wall_4m
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Basement_Wall_4m", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Basement/", L"Basement_Wall_4m.x"))))
		return E_FAIL;
	// For.Map_Static_Basement_HookBase
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Basement_HookBase", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Basement/", L"Basement_HookBase.x"))))
		return E_FAIL;
	// For.Map_Static_WoodFence_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodFence_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/WoodFence/", L"WoodFence_01.x"))))
		return E_FAIL;
	// For.Map_Static_WoodFence_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodFence_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/WoodFence/", L"WoodFence_02.x"))))
		return E_FAIL;
	// For.Map_Static_WoodFence_03
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodFence_03", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/WoodFence/", L"WoodFence_03.x"))))
		return E_FAIL;
	// For.Map_Static_WoodWall_1m
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_1m", pMesh_Static = CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/WoodWall/", L"WoodWall_1m.x"))))
		return E_FAIL;
	// For.Map_Static_WoodWall_2m_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_2m_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/WoodWall/", L"WoodWall_2m_01.x"))))
		return E_FAIL;
	// For.Map_Static_WoodWall_2m_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_2m_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/WoodWall/", L"WoodWall_2m_02.x"))))
		return E_FAIL;
	// For.Map_Static_WoodWall_2m_Jump
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_2m_Jump", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/WoodWall/", L"WoodWall_2m_Jump.x"))))
		return E_FAIL;
	// For.Map_Static_WoodWall_4m
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_4m", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/WoodWall/", L"WoodWall_4m.x"))))
		return E_FAIL;
	// For.Map_Static_WoodWall_Corner
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_Corner", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/WoodWall/", L"WoodWall_Corner.x"))))
		return E_FAIL;
	// For.Map_Static_WoodWall_T
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_T", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/WoodWall/", L"WoodWall_T.x"))))
		return E_FAIL;
	// For.Map_Static_TrashPile_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_TrashPile_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/TrashPile/", L"TrashPile_03.x"))))
		return E_FAIL;
	// For.Map_Static_TrashPile_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_TrashPile_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/TrashPile/", L"TrashPile_05.x"))))
		return E_FAIL;
	// For.Map_Static_Trash_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Trash_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Trash/", L"Trash_01.x"))))
		return E_FAIL;
	// For.Map_Static_TireStack_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_TireStack_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/TireStack/", L"TireStack_01.x"))))
		return E_FAIL;
	// For.Map_Static_TireStack_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_TireStack_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/TireStack/", L"TireStack_02.x"))))
		return E_FAIL;
	// For.Map_Static_TireStack_03
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_TireStack_03", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/TireStack/", L"TireStack_03.x"))))
		return E_FAIL;
	// For.Map_Static_Terrain_FarmHouse
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Terrain_FarmHouse", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Terrain/", L"Terrain_FarmHouse.x"))))
		return E_FAIL;
	// For.Map_Static_SideFence
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_SideFence", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/SideFence/", L"SideFence.x"))))
		return E_FAIL;
	// For.Map_Static_SideFence_Wall
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_SideFence_Wall", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/SideFence/", L"SideFence_Wall.x"))))
		return E_FAIL;
	// For.Map_Static_RockWall_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_RockWall_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/RockWall/", L"RockWall_01.x"))))
		return E_FAIL;
	// For.Map_Static_RockWall_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_RockWall_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/RockWall/", L"RockWall_02.x"))))
		return E_FAIL;
	// For.Map_Static_RockWall_03
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_RockWall_03", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/RockWall/", L"RockWall_03.x"))))
		return E_FAIL;
	// For.Map_Static_RockWall_Jump
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_RockWall_Jump", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/RockWall/", L"RockWall_Jump.x"))))
		return E_FAIL;
	// For.Map_Static_Guts
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Guts", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Others/", L"Guts.x"))))
		return E_FAIL;
	// For.Map_Static_LogPile
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_LogPile", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Others/", L"LogPile.x"))))
		return E_FAIL;
	// For.Map_Static_LumberPile
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_LumberPile", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Others/", L"LumberPile.x"))))
		return E_FAIL;
	// For.Map_Static_FarmMachine
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_FarmMachine", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Mechine/FarmMachine/", L"FarmMachine.x"))))
		return E_FAIL;
	// For.Map_Static_OldTractor
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_OldTractor", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Mechine/OldTractor/", L"OldTractor.x"))))
		return E_FAIL;
	// For.Map_Static_PickupTruck
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_PickupTruck", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Mechine/PickupTruck/", L"PickupTruck.x"))))
		return E_FAIL;
	// For.Map_Static_SmallStones_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_SmallStones_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/SmallStones/", L"SmallStones_01.x"))))
		return E_FAIL;
	// For.Map_Static_SmallStones_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_SmallStones_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/SmallStones/", L"SmallStones_02.x"))))
		return E_FAIL;
	// For.Map_Static_SmallStones_03
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_SmallStones_03", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/SmallStones/", L"SmallStones_03.x"))))
		return E_FAIL;
	// For.Map_Static_MeatHanger_4way
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_MeatHanger_4way", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/InteractableObjects/", L"MeatHanger_4way.x"))))
		return E_FAIL;
	// For.Mesh_Totem
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Totem", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/InteractableObjects/Totem/", L"Totem.x"))))
		return E_FAIL;
	// For.Map_Static_Hut_Floor
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hut_Floor", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Hut/", L"Hut_Floor.x"))))
		return E_FAIL;
	// For.Map_Static_Hut_Roof
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hut_Roof", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Hut/", L"Hut_Roof.x"))))
		return E_FAIL;
	// For.Map_Static_Hut_Wall_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hut_Wall_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Hut/", L"Hut_Wall_01.x"))))
		return E_FAIL;
	// For.Map_Static_Hut_Hut_Wall_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hut_Wall_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Hut/", L"Hut_Wall_02.x"))))
		return E_FAIL;
	// For.Map_Static_Hut_WoodPile
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hut_WoodPile", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Hut/", L"Hut_WoodPile.x"))))
		return E_FAIL;
	// For.Map_Static_Hay_Round
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hay_Round", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Hay/", L"Hay_Round.x"))))
		return E_FAIL;
	// For.Map_Static_Hay_Square
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hay_Square", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Hay/", L"Hay_Square.x"))))
		return E_FAIL;
	// For.Map_Static_DeadCow_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_DeadCow_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/DeadCow/", L"DeadCow_01.x"))))
		return E_FAIL;
	// For.Map_Static_DeadCow_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_DeadCow_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/DeadCow/", L"DeadCow_02.x"))))
		return E_FAIL;
	// For.Map_Static_DeadCow_03
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_DeadCow_03", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/DeadCow/", L"DeadCow_03.x"))))
		return E_FAIL;
	// For.Map_Static_DeadCow_04
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_DeadCow_04", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/DeadCow/", L"DeadCow_04.x"))))
		return E_FAIL;
	// For.Map_Static_DeadCow_Hook_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_DeadCow_Hook_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/DeadCow/", L"DeadCow_Hook_01.x"))))
		return E_FAIL;
	// For.Map_Static_DeadCow_Hook_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_DeadCow_Hook_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/DeadCow/", L"DeadCow_Hook_02.x"))))
		return E_FAIL;
	// For.Map_Static_DeadCow_Hook_03
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_DeadCow_Hook_03", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/DeadCow/", L"DeadCow_Hook_03.x"))))
		return E_FAIL;
	// For.Map_Static_Crate_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Crate_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Crate/", L"Crate_01.x"))))
		return E_FAIL;
	// For.Map_Static_Crate_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Crate_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Crate/", L"Crate_02.x"))))
		return E_FAIL;
	// For.Map_Static_Crate_03
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Crate_03", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Crate/", L"Crate_03.x"))))
		return E_FAIL;
	// For.Map_Static_Crate_04
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Crate_04", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Crate/", L"Crate_04.x"))))
		return E_FAIL;
	// For.Map_Static_SlaughterTree
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_SlaughterTree", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Others/", L"SlaughterTree.x"))))
		return E_FAIL;
	// For.Mesh_Barrel
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Barrel", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Barrel/", L"Barrel.x"))))
		return E_FAIL;
	// For.Mesh_Light_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Light_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Light/", L"Light_01.x"))))
		return E_FAIL;
	// For.Mesh_Light_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Light_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Light/", L"Light_03.x"))))
		return E_FAIL;
	// For.Mesh_Lamp_01
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Lamp_01", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Light/", L"Lamp_01.x"))))
		return E_FAIL;
	// For.Mesh_Lamp_02
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Lamp_02", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Light/", L"Lamp_02.x"))))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}


HRESULT CLoadManager::Prototype_S1_DynamicMesh()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	// For.Mesh_Closet
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Closet", CMesh_Dynamic::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Animation/Closet/", L"Closet.x"))))
		return E_FAIL;
	// For.Mesh_Generator
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Generator", CMesh_Dynamic::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Animation/Generator/", L"Generator.x"))))
		return E_FAIL;
	// For.Mesh_MeatHook
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_MeatHook", CMesh_Dynamic::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Animation/MeatHook/", L"MeatHook.x"))))
		return E_FAIL;
	// For.Mesh_Plank
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Plank", CMesh_Dynamic::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Animation/Plank/", L"Plank.x"))))
		return E_FAIL;
	// For.Mesh_Hatch
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Hatch", CMesh_Dynamic::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Animation/Hatch/", L"Hatch.x"))))
		return E_FAIL;
	// For.Mesh_ExitDoor
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_ExitDoor", CMesh_Dynamic::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Animation/ExitDoor/", L"ExitDoor.x"))))
		return E_FAIL;
	// For.Mesh_Chest
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Chest", CMesh_Dynamic::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Animation/Chest/", L"Chest.x"))))
		return E_FAIL;
	// For.Mesh_Spider
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Spider", CMesh_Dynamic::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Animation/Spider/", L"Spider.x"))))
		return E_FAIL;
	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CLoadManager::Prototype_S1_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Component_Buffer_CubeTex
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_CubeTex", CBuffer_CubeTex::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.Component_Buffer_ViewPort
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_ViewPort", CBuffer_ViewPort::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.Component_Texture_Light01E
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Texture_Light01E", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Etc/T_Light01_E.tga"))))
		return E_FAIL;

	//// For.Component_Texture_Light01E
	//if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Texture_NEONL", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Etc/T_NeonLight_EMask.tga"))))
	//	return E_FAIL;

	// For.Component_Texture_Light
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Texture_Light", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Light/LightTexture.png"))))
		return E_FAIL;

	// For.Component_Texture_Moon
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Texture_Moon", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Light/moon_diffuse.tga"))))
		return E_FAIL;

	// For.Component_Texture_Terrain
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Texture_Terrain", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Terrain/T_DirtTerrain_BC.tga"))))
		return E_FAIL;

	// For.Component_Texture_Sky
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Texture_Sky", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_CUBE, L"../Bin/Resources/Textures/SkyBox/Burger%d.dds", 4))))
		return E_FAIL;

	// For.Component_Picking
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Picking", CPicking::Create(m_pGraphic_Device, g_hWnd))))
		return E_FAIL;

	// For.Componet_Collider_Box
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Collider_Box", CCollider::Create(m_pGraphic_Device, CCollider::TYPE_BOX))))
		return E_FAIL;

	// For.Componet_Collider_Sphere
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Collider_Sphere", CCollider::Create(m_pGraphic_Device, CCollider::TYPE_SPHERE))))
		return E_FAIL;

	// For.Component_Navigation
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Navigation", CNavigation::Create(m_pGraphic_Device, L"../Bin/Data/Navi.dat"))))
		return E_FAIL;

	// For.Component_Shader_Sky
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Shader_Sky", CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Sky.fx"))))
		return E_FAIL;

	// For.Component_Shader_Light
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Shader_LightCircle", CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_LightCircle.fx"))))
		return E_FAIL;

	// For.Component_Frustum
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Frustum", CFrustum::Create(m_pGraphic_Device))))
		return E_FAIL;

	//// For.Component_Mesh_Camper_Adam
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Mesh_Camper_Adam", CMesh_Dynamic::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/DynamicMesh/Camper/Adam/", L"Adam.x"))))
		return E_FAIL;

	// For.Component_Mesh_Slasher_Wraith
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Mesh_Slasher_Wraith", CMesh_Dynamic::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/DynamicMesh/Slasher/", L"Wraith.x"))))
		return E_FAIL;

	// For.Component_Mesh_Weapon_Machete
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Mesh_Weapon_Machete", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/DynamicMesh/Weapon/Machete/", L"Machete.x"))))
		return E_FAIL;

	// For.Component_Texture_FlashLight
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Texture_FlashLight", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Light/FlashLight.tga"))))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CLoadManager::Prototype_S1_Texture()
{
 	if(FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/PlayersStatus/", L"PlayerStatus_Idle.tga")))return E_FAIL;
	if(FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/PlayersStatus/", L"PlayerStatus_Escape.tga")))return E_FAIL;
	if(FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/PlayersStatus/", L"PlayerStatus_Hurted.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/PlayersStatus/", L"PlayerStatus_Dying.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/PlayersStatus/", L"PlayerStatus_Hooked.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/PlayersStatus/", L"PlayerStatus_UnConnected.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/PlayersStatus/", L"PlayerStatus_Dead1.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/PlayersStatus/", L"PlayerStatus_Dead2.tga")))return E_FAIL;

	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/HudBasic/", L"Hud_Line.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/HudBasic/", L"Hud_HatchIcon.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/HudBasic/", L"Hud_GeneratorIcon.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/ProgressBar/", L"ProgressBar_Base.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/ProgressBar/", L"ProgressBar_Yellow.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/ProgressBar/", L"ProgressBar_Blue.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/ProgressBar/", L"ProgressBar_Red.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/ProgressBar/", L"ProgressBar_Texture_Hand.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/ProgressBar/", L"ProgressBar_Texture_MedicKit.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/ProgressBar/", L"ProgressBar_Texture_ToolBox.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/SkillCheck/", L"SkillCheck_Base.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/SkillCheck/", L"SkillCheck_Full.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/SkillCheck/", L"SkillCheck_NoneFull.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/SkillCheck/", L"SkillCheck_GridLine.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/SkillCheck/", L"SkillCheck_SpaceBar.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/SkillCheck/", L"SkillCheck_End.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/SkillCheck/", L"Button_Mouse.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Ingame/SkillCheck/", L"Button_Keyboard.tga")))return E_FAIL;
	

	// Yup's Textures
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/Etc/", L"BurnedEmbers.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/Etc/", L"DissolveGradient.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/Etc/", L"DissolveSpider.tga")))return E_FAIL;

	return NOERROR;
}

HRESULT CLoadManager::Prototype_S1_CollMesh()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Crate_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Crate_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Crate_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Crate_02_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Crate_03_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Crate_03_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Crate_04_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Crate_04_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Trash_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Trash_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hay_Square_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Hay_Square_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hay_Round_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Hay_Round_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodFence_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodFence_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodFence_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodFence_02_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodFence_03_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodFence_03_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_06_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Rock_06_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_16_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Rock_16_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_17_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Rock_17_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_1m_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_1m_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_2m_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_2m_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_2m_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_2m_02_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_2m_Jump_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_2m_Jump_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_Corner_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_Corner_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_T_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_T_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_4m_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_4m_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Tree_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Tree_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Tree_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Tree_02_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Tree_03_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Tree_03_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Barrel_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Barrel_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hut_Wall_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Hut_Wall_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hut_Wall_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Hut_Wall_02_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Basement_Wall_2m_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Basement_Wall_2m_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Basement_Wall_4m_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Basement_Wall_4m_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_TireStack_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"TireStack_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_TireStack_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"TireStack_02_HULL.x"))))
		return E_FAIL;
  	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_TireStack_03_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"TireStack_03_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_RockWall_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"RockWall_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_RockWall_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"RockWall_02_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_RockWall_03_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"RockWall_03_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_RockWall_Jump_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"RockWall_Jump_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_SideFence_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"SideFence_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_SideFence_Wall_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"SideFence_Wall_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_LogPile_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"LogPile_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_LumberPile_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"LumberPile_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_OldTractor_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"OldTractor_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_PickupTruck_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"PickupTruck_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_FarmMachine_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"FarmMachine_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Transporter_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Transporter_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Closet_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Closet_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Generator_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Generator_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_MeatHook_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"MeatHook_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Plank_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Plank_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Plank2_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Plank2_HULL.x"))))
		return E_FAIL;
	//if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Hatch", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Animation/Hatch/", L"Hatch.x"))))
	//	return E_FAIL;
	//if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_ExitDoor", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Animation/ExitDoor/", L"ExitDoor.x"))))
	//	return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Chest_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Chest_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_ExitBuilding_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"ExitBuilding_HULL.x"))))
		return E_FAIL;
	//if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_FarmHouse_Navi", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"FarmHouse_Navi.x"))))
	//	return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_FarmHouse_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"FarmHouse_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_FarmHouse_NAVI", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"FarmHouse_NAVI.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hut_Floor_NAVI", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Hut_Floor_NAVI.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Terrain_FarmHouse_NAVI", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Terrain_FarmHouse_NAVI.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Basement_NAVI", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Basement_NAVI.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Basement_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Basement_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_ExitDoor_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"ExitDoor_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_ExitBuilding_Ground_NAVI", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"ExitBuilding_Ground_NAVI.x"))))
		return E_FAIL;
	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CLoadManager::Prototype_L_GameObject()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	
	// For.GameObject_UI_Logo
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_UI_Logo", CUI_Logo::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.GameObject_UI_Lobby
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_UI_Lobby", CUI_RoleSelection::Create(m_pGraphic_Device))))
		return E_FAIL;
	// For.GameObject_UI_OverlayMenu
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_UI_OverlayMenu", CUI_OverlayMenu::Create(m_pGraphic_Device))))
		return E_FAIL;
	
	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CLoadManager::Prototype_L_Texture()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Logo/", L"DBD_Logo.tga")))return E_FAIL;

	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby1/", L"Icon_Camper.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby1/", L"Icon_Slasher.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby1/", L"Lobby_Panel.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby1/", L"Lobby_PanelBlend.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby1/", L"Lobby_Panel_Frame.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby1/", L"Lobby1_Background.tga")))return E_FAIL;

	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"Camper_Small.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"Slasher_Small.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"PanelScreen.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"Lobby2_Background_1.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"Panel_Bottom.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"UI_Icon_Panel.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"UI_Change_Icon.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"UI_Charicter_Icon.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"GoBack_Icon.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"GoBack_Panel.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"GoBack_Arrow.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"GoBack_Smoke.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"Ready_Icon.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"Ready_Panel.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"Ready_Arrow.tga")))return E_FAIL;

	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"UI_Start_Icon_1.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"UI_Start_Icon_2.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"UI_Start_Icon_3.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"UI_Start_Icon_4.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"UI_Start_Icon_5.tga")))return E_FAIL;
	if (FAILED(GET_INSTANCE(CMeshTexture)->AddTextureEX(m_pGraphic_Device, L"../Bin/Resources/Textures/UI/Lobby/Lobby2/", L"UI_Start_Icon.tga")))return E_FAIL;
	Safe_Release(pManagement);
	return NOERROR;
}


void CLoadManager::Free()
{
	for (auto& iter : m_vecObjectName)
		Safe_Delete(iter);
	m_vecObjectName.clear();
	Safe_Release(m_pGraphic_Device);
}
