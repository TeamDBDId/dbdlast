#include "stdafx.h"
#include "..\Headers\Action_BeingKilledBySpider.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "MeatHook.h"

_USING(Client)

CAction_BeingKilledBySpider::CAction_BeingKilledBySpider()
{
}

HRESULT CAction_BeingKilledBySpider::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);

	pCamper->Set_State(AC::BeingKilledBySpiderIN);
	m_iState = AC::BeingKilledBySpiderIN;
	m_pHook->SetState(CMeatHook::SpiderStabIN);

	return NOERROR;
}

_int CAction_BeingKilledBySpider::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	CCamper* pCamper = (CCamper*)m_pGameObject;
	if (m_iState == AC::BeingKilledBySpiderOut && pMeshCom->IsOverTime(0.3f))
	{
		return END_ACTION;
	}
	else if (m_iState == AC::BeingKilledBySpiderLoop && pMeshCom->IsOverTime(0.3f))
	{
		pCamper->Set_State(AC::BeingKilledBySpiderOut);
		m_iState = AC::BeingKilledBySpiderOut;
		m_pHook->SetState(CMeatHook::SpiderStabOut);
	}
	else if (m_iState == AC::BeingKilledBySpiderIN && pMeshCom->IsOverTime(0.3f))
	{
		pCamper->Set_State(AC::BeingKilledBySpiderLoop);
		m_iState = AC::BeingKilledBySpiderLoop;
		m_pHook->SetState(CMeatHook::SpiderStabLoop);
	}

	return UPDATE_ACTION;
}

void CAction_BeingKilledBySpider::End_Action()
{
	m_bIsPlaying = false;
	m_pHook = nullptr;
}

void CAction_BeingKilledBySpider::Send_ServerData()
{
}

void CAction_BeingKilledBySpider::Free()
{
	CAction::Free();
}
