#include "stdafx.h"
#include "Management.h"
#include "UI_Texture.h"
#include "UI_OverlayMenu.h"

_USING(Client);


CUI_OverlayMenu::CUI_OverlayMenu(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_OverlayMenu::CUI_OverlayMenu(const CUI_OverlayMenu & rhs)
	: CGameObject(rhs)
	, m_iMaxPlayer(rhs.m_iMaxPlayer)

{
}


HRESULT CUI_OverlayMenu::Ready_Prototype()
{
	_uint				m_CurButton = NONE;
	_uint				m_OldButton = NONE;
	return NOERROR;
}

HRESULT CUI_OverlayMenu::Ready_GameObject()
{

	Set_Background();
	Set_SideButton();
	Set_OtherButton();
	Set_StartIcon();
	return NOERROR;
}

_int CUI_OverlayMenu::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;

	Server_Check();
	Update_Job();
	Ready_Check();
	Mouse_Check();
	Lobby_Check();
	return _int();
}

_int CUI_OverlayMenu::LastUpdate_GameObject(const _float & fTimeDelta)
{
	return _int();
}

void CUI_OverlayMenu::Set_Background()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture;
	_vec2 vPos = { 0.0523451f,0.459679f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"PanelScreen.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_FadeIn(0.5f);
	m_UITexture.insert({ L"PanelScreen_0", pTexture });

	vPos = { 0.0739831f,0.493054f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Lobby2_Background_1.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_FadeIn(0.3f);
	m_UITexture.insert({ L"PanelScreen_1", pTexture });
	
	vPos = { 0.5, 0.970944f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Panel_Bottom.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_FadeIn(0.3f);
	m_UITexture.insert({ L"PanelScreen_2", pTexture });
	

	Safe_Release(pManagement);

}

void CUI_OverlayMenu::Update_Job()
{
	if (m_OldJobState != m_JobState)
	{
		if (m_JobState == false)
			Set_Camper();
		else
			Set_Slasher();

		m_OldJobState = m_JobState;
	}
}

void CUI_OverlayMenu::Set_Camper()
{
	_int  CamperCheck = 0;
	_bool Number[4] = { false,false,false,false };

	if (!lserver_data.GameStart)
	{
		for (int i = 0; i < 5; ++i)
		{
			if (lserver_data.playerdat[i].bConnect
				&&lserver_data.Number[i] < 5
				&& lserver_data.Number[i] >= 1)
			{
				CamperCheck++;
				Number[lserver_data.Number[i] - 1] = true;
			}
		}
		if (CamperCheck < 4)
		{
			for (int i = 0; i < 4; ++i)
			{
				if (!Number[i])
				{
					exPlayerNumber = i + 1;
					lobby_data.Packet = i + 2;
					break;
				}
			}
		}
		else
			return;
	}
	else
		return;
}

void CUI_OverlayMenu::Set_Slasher()
{
	_bool bSlasher = false;

	if (!lserver_data.GameStart)
	{
		for (int i = 0; i < 5; ++i)
		{
			if (5 == lserver_data.playerdat[i].Number)
				bSlasher = true;
		}
		if (!bSlasher)
		{
			lobby_data.Packet = 1;
			exPlayerNumber = 5;
		}
	}
}

void CUI_OverlayMenu::Button_L2_Change_Job(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"CJ_Panel"));
		pTexture->Set_Alpha(1.f);
		_vec2 vPos = pTexture->Get_Pos();
		_vec2 vScale = pTexture->Get_Scale();
		pTexture = Find_UITexture(wstring(L"CJ_Icon"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"CJ_Change_Icon"));
		pTexture->Set_Alpha(1.f);

		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Lobby_Panel_Frame.tga"));
		pTexture->Set_Pos(_vec2(vPos.x, vPos.y));
		pTexture->Set_Scale(vScale*0.9f, false);
		pTexture->IsButtonFrame(true);
		pTexture->Set_FadeIn(0.5f);
		m_UITexture.insert({ L"CJ_Frame", pTexture });

		wstring Tex = L"";
		if (m_JobState)
			Tex = L"생존자로 변경";
		else
			Tex = L"살인마로 변경";

		vPos.x += Tex.length()*15.f + g_iBackCX* 0.01f;

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(L"Black.tga");
		pTexture->Set_Scale(_vec2((_float)Tex.length() * g_iBackCX* 0.015f, 0.04f*g_iBackCY), false);
		pTexture->Set_Pos(_vec2(vPos.x, vPos.y));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"CJ_TexBase", pTexture });

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_Font(Tex, _vec2(15.f, 17.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 0);
		pTexture->Set_Pos(_vec2(vPos.x, vPos.y));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"CJ_Info", pTexture });

		Safe_Release(pManagement);
	}
	else if (State == UnTouch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"CJ_Panel"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"CJ_Icon"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"CJ_Change_Icon"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"CJ_Frame"));
		pTexture->Set_FadeOut(0.5f);
		pTexture->Set_DeadTime(0.5f);
		Delete_UI_Texture(wstring(L"CJ_Frame"));
		pTexture = Find_UITexture(wstring(L"CJ_TexBase"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"CJ_TexBase"));
		pTexture = Find_UITexture(wstring(L"CJ_Info"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"CJ_Info"));
	}
	else if (State == Click)
	{
		m_JobState = !m_JobState;
		Button_L2_Change_Job(UnTouch);
		m_CurButton = NONE;
		m_OldButton = NONE;
	}

}

void CUI_OverlayMenu::Button_L2_Change_Charicter(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"CC_Panel"));
		pTexture->Set_Alpha(1.f);
		_vec2 vPos = pTexture->Get_Pos();
		_vec2 vScale = pTexture->Get_Scale();
		pTexture = Find_UITexture(wstring(L"CC_Icon"));
		pTexture->Set_Alpha(1.f);

		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Lobby_Panel_Frame.tga"));
		pTexture->Set_Pos(_vec2(vPos.x, vPos.y));
		pTexture->Set_Scale(vScale*0.9f, false);
		pTexture->IsButtonFrame(true);
		pTexture->Set_FadeIn(0.5f);
		m_UITexture.insert({ L"CC_Frame", pTexture });

		wstring Tex = L"";
		if (m_JobState)
			Tex = L"살인마 선택";
		else
			Tex = L"생존자 선택";

		vPos.x += Tex.length()*15.f + g_iBackCX* 0.01f;

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_TexName(L"Black.tga");
		pTexture->Set_Scale(_vec2((_float)Tex.length() * g_iBackCX* 0.015f, 0.04f*g_iBackCY), false);
		pTexture->Set_Pos(_vec2(vPos.x, vPos.y));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"CC_TexBase", pTexture });

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
		pTexture->Set_Font(Tex, _vec2(15.f, 17.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 0);
		pTexture->Set_Pos(_vec2(vPos.x, vPos.y));
		pTexture->Set_FadeIn(0.2f);
		m_UITexture.insert({ L"CC_Info", pTexture });

		Safe_Release(pManagement);
	}
	else if (State == UnTouch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"CC_Panel"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"CC_Icon"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"CC_Frame"));
		pTexture->Set_FadeOut(0.5f);
		pTexture->Set_DeadTime(0.5f);
		Delete_UI_Texture(wstring(L"CC_Frame"));
		pTexture = Find_UITexture(wstring(L"CC_TexBase"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"CC_TexBase"));
		pTexture = Find_UITexture(wstring(L"CC_Info"));
		pTexture->Set_FadeOut(0.2f);
		pTexture->Set_DeadTime(0.2f);
		Delete_UI_Texture(wstring(L"CC_Info"));
	}
	else if (State == Click)
	{
	}
}

void CUI_OverlayMenu::Button_L2_Go_Back(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"GB_Panel"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"GB_Icon"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"GB_Text"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"GB_Arrow"));
		pTexture->Set_Alpha(1.f);

	}
	else if (State == UnTouch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"GB_Panel"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"GB_Icon"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"GB_Text"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"GB_Arrow"));
		pTexture->Set_Alpha(0.5f);
	}
	else if (State == Click)
	{
		m_iClickButton = Go_Back;
		Delete_All();
	}
}

void CUI_OverlayMenu::Button_L2_Ready(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"R_Panel"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"R_Icon"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"R_Text"));
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(wstring(L"R_Arrow"));
		pTexture->Set_Alpha(1.f);

	}
	else if (State == UnTouch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(wstring(L"R_Panel"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"R_Icon"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"R_Text"));
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(wstring(L"R_Arrow"));
		pTexture->Set_Alpha(0.5f);
	}
	else if (State == Click)
	{
		m_IsReady = !m_IsReady;
		_int MyNumber = lserver_data.Number[client_data.client_imei];

		if (MyNumber > 0 && MyNumber <= 5)
			lobby_data.Ready = m_IsReady;
	}
}


CUI_OverlayMenu * CUI_OverlayMenu::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_OverlayMenu*	pInstance = new CUI_OverlayMenu(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_OverlayMenu Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_OverlayMenu::Clone_GameObject()
{
	CUI_OverlayMenu*	pInstance = new CUI_OverlayMenu(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_OverlayMenu Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}


void CUI_OverlayMenu::Ready_Check()
{

	for (_uint i = 1; i <= 5; i++)
	{
		_tchar TextureTag[256] = L"";
		swprintf_s(TextureTag, L"S_%d", i);
		if (m_iMaxPlayer >= i)
		{
			CUI_Texture* pUITexture = nullptr;
			pUITexture = Find_UITexture((wstring)TextureTag);
			pUITexture->IsRendering(true);
			if (i <= m_iCurReadyPlayer)
				pUITexture->IsColor(true);
			else
				pUITexture->IsColor(false);
		}
		else
			Find_UITexture((wstring)TextureTag)->IsRendering(false);
	}
}

void CUI_OverlayMenu::Mouse_Check()
{
	CUI_Texture* pTexture = nullptr;
	pTexture = Find_UITexture(wstring(L"CC_Panel"));
	if (pTexture->IsTouch())
	{
		m_CurButton = CHANGE_CHARICTER;
		return;
	}
	pTexture = Find_UITexture(wstring(L"CJ_Panel"));
	if (pTexture->IsTouch())
	{
		m_CurButton = CHANGE_JOB;
		return;
	}
	pTexture = Find_UITexture(wstring(L"GB_Panel"));
	if (pTexture->IsTouch())
	{
		m_CurButton = BACK;
		return;
	}
	pTexture = Find_UITexture(wstring(L"R_Panel"));
	if (pTexture->IsTouch())
	{
		m_CurButton = READY;
		return;
	}
	m_CurButton = NONE;
}

void CUI_OverlayMenu::Lobby_Check()
{
	CUI_Texture* pTexture = Find_UITexture(wstring(L"CJ_Icon"));
	if (m_JobState)
		pTexture->Set_TexName(wstring(L"Camper_Small.tga"));
	else
		pTexture->Set_TexName(wstring(L"Slasher_Small.tga"));

	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));

	if (m_CurButton != m_OldButton)
	{
		if (m_CurButton == CHANGE_JOB)
			Button_L2_Change_Job(Touch);
		else if (m_CurButton == CHANGE_CHARICTER)
			Button_L2_Change_Charicter(Touch);
		else if (m_CurButton == BACK)
			Button_L2_Go_Back(Touch);
		else if (m_CurButton == READY)
			Button_L2_Ready(Touch);
		if (m_OldButton == CHANGE_JOB)
			Button_L2_Change_Job(UnTouch);
		else if (m_OldButton == CHANGE_CHARICTER)
			Button_L2_Change_Charicter(UnTouch);
		else if (m_OldButton == BACK)
			Button_L2_Go_Back(UnTouch);
		else if (m_OldButton == READY)
			Button_L2_Ready(UnTouch);
		m_OldButton = m_CurButton;
	}

	if (KEYMGR->MouseDown(0))
	{
		if (m_CurButton == CHANGE_JOB)
			Button_L2_Change_Job(Click);
		else if (m_CurButton == CHANGE_CHARICTER)
			Button_L2_Change_Charicter(Click);
		else if(m_CurButton == BACK)
			Button_L2_Go_Back(Click);
		else if (m_CurButton == READY)
			Button_L2_Ready(Click);
	}
}

void CUI_OverlayMenu::Server_Check()
{
	_int PlayerNum = 0;
	_int ReadyNum = 0;
	for (int i = 0; i < 5; i++)
	{
		if (lserver_data.playerdat[i].bConnect)
			PlayerNum++;
		if (lserver_data.playerdat[i].Ready)
			ReadyNum;
	}
}

void CUI_OverlayMenu::Set_SideButton()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture;

	_vec2 vPos = { 0.063f, 0.103f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Icon_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	pTexture->Set_Alpha(0.4f);
	m_UITexture.insert({ L"CC_Panel_Shadow", pTexture });

	vPos += { 0.005f, -0.01f};
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Icon_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	m_UITexture.insert({ L"CC_Panel", pTexture });

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Charicter_Icon.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	m_UITexture.insert({ L"CC_Icon", pTexture });

	vPos = { 0.063f, 0.25f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Icon_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	pTexture->Set_Alpha(0.4f);
	m_UITexture.insert({ L"CJ_Panel_Shadow", pTexture });

	vPos += { 0.005f, -0.01f};
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Icon_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.8f));
	m_UITexture.insert({ L"CJ_Panel", pTexture });

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Camper_Small.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	m_UITexture.insert({ L"CJ_Icon", pTexture });

	vPos = { 0.046747f, 0.195673f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Change_Icon.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.5f, 0.5f));
	m_UITexture.insert({ L"CJ_Change_Icon", pTexture });

	Safe_Release(pManagement);
}

void CUI_OverlayMenu::Set_StartIcon()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture = nullptr;

	_vec2 vPos = { 0.885137f, 0.833541f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Start_Icon_1.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	m_UITexture.insert({ L"S_1", pTexture });

	vPos = { 0.897278f, 0.829982f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Start_Icon_2.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	m_UITexture.insert({ L"S_2", pTexture });

	vPos = { 0.910985f, 0.825878f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Start_Icon_3.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	m_UITexture.insert({ L"S_3", pTexture });

	vPos = { 0.922624f, 0.833617f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Start_Icon_4.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	m_UITexture.insert({ L"S_4", pTexture });

	vPos = { 0.904958f, 0.82927f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Start_Icon_5.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	m_UITexture.insert({ L"S_5", pTexture });
	Safe_Release(pManagement);
}

void CUI_OverlayMenu::Set_OtherButton()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture = nullptr;
	_vec2 vPos = { 0.0931096f, 0.92f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"GoBack_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.56f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"GB_Panel", pTexture });
	 
	vPos = { 0.0892697f, 0.919537f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"GoBack_Icon.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"GB_Icon", pTexture });

	vPos = { 0.115088f, 0.919537f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"뒤로가기 [ESC]"),_vec2(12.f,16.f),D3DXCOLOR(1.f,1.f,1.f,1.f));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"GB_Text", pTexture });

	vPos = { 0.0574244f, 0.918603f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"GoBack_Arrow.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"GB_Arrow", pTexture });

	vPos = { 0.873996f, 0.91f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ready_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.68f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"R_Panel", pTexture });

	vPos = { 0.870136f, 0.910521f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ready_Icon.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"R_Icon", pTexture });

	vPos = { 0.897147f, 0.913747f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"준비"), _vec2(22.f, 25.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"R_Text", pTexture });

	vPos = { 0.937298f, 0.912069f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ready_Arrow.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.6f, 0.6f));
	m_UITexture.insert({ L"R_Arrow", pTexture });
	pTexture->Set_Alpha(0.5f);
	
	Safe_Release(pManagement);
}

CUI_Texture * CUI_OverlayMenu::Find_UITexture(wstring UIName)
{
	auto& iter = m_UITexture.find(UIName);
	if (iter == m_UITexture.end())
		return nullptr;
	return iter->second;
}

void CUI_OverlayMenu::Delete_UI_Texture(wstring UIName)
{
	auto& iter = m_UITexture.find(wstring(UIName));
	m_UITexture.erase(iter);
}

void CUI_OverlayMenu::Delete_All()
{
	for (auto& iter : m_UITexture)
	{
		iter.second->Set_DeadTime(0.3f);
		iter.second->Set_FadeOut(0.3f);
	}
	m_UITexture.clear();
}

void CUI_OverlayMenu::Free()
{
	CGameObject::Free();
}
