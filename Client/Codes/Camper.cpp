#include "stdafx.h"
#include "..\Headers\Camper.h"
#include "Management.h"
#include "Light_Manager.h"
#include "Camera_Camper.h"
#include "CustomLight.h"
#include "Slasher.h"
#include "Action_BeingHeal.h"
#include "Action_HealCamper.h"
#include "Action_HealingSelf.h"
#include "Action_HookBeing.h"
#include "Action_BeingKilledBySpider.h"
#include "Action_Generator.h"
#include "Action_JumpInHatch.h"
#include "Action_LootChest.h"
#include "Action_CleansTotem.h"
#include "Action_PickItemOnGround.h"
#include "Action_Sabotage.h"
#include "Action_UnlockExit.h"
#include "Action_HideCloset.h"
#include "Action_JumpPlank.h"
#include "Action_PassWindow.h"
#include "Action_PullDownPlank.h"
#include "Action_UnhideCloset.h"
#include "Action_CrawlToHit.h"
#include "Action_HookFree.h"
#include "Action_MoveLerp.h"
#include "Action_Occupied.h"
#include "Action_StunDrop.h"
#include "Action_Grab_Generic_Fast.h"
#include "Action_Grab_Obstacles.h"
#include "Action_Search_Locker.h"
#include "Action_Drop.h"
#include "Action_PickUp.h"
#include "Action_HookIn.h"
#include "EffectManager.h"
#include "Action_SpiderReaction.h"
#include "Action_HealingSelf.h"
#include "Totem.h"
#include "Generator.h"
#include "Chest.h"
#include "ExitDoor.h"

_USING(Client)

CCamper::CCamper(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CCamper::CCamper(const CCamper & rhs)
	: CGameObject(rhs)
{

}

HRESULT CCamper::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CCamper::Ready_GameObject()
{
	ZeroMemory(&camper_data, sizeof(CamperData));
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (FAILED(Ready_Actions()))
		return E_FAIL;

	m_pTransformCom->SetUp_Speed(450.f, D3DXToRadian(90.0f));
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(3200.f, 0.f, 3200.f));
	m_eCurState = AC::Idle;
	m_pMeshCom->Set_AnimationSet(AC::Idle);

	m_fEnergy = 0.f;
	m_fDyingEnergy = 240.f;
	m_fHookEnergy = 120.f;
	m_fMaxEnergy = 240.f;

	m_fHeal = 0.f;
	m_fMaxHeal = 0.f;
	
	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_PLAYER, this);

	m_eObjectID += CAMPER;
	//m_pCustomLight = CCustomLight::Create(m_pGraphic_Device, _vec3(3200.f, 100.f, 3200.f), D3DLIGHT_SPOT);
	//if (nullptr == m_pCustomLight)
	//	return E_FAIL;

	////m_pCustomLight->Set_Diffuse(D3DXCOLOR(1.f, 0.2f, 0.2f, 1.f));
	//m_pCustomLight->Set_Range(3000.f);
	//m_pCustomLight->Set_Direction(_vec3(0.f, -1.f, 0.f));
	//m_pCustomLight->Set_Theta(D3DXToRadian(12.5f));
	//m_pCustomLight->Set_Phi(D3DXToRadian(17.5f));

	//m_pCustomLight = CCustomLight::Create(m_pGraphic_Device, _vec3(3200.f, 100.f, 3200.f), D3DLIGHT_POINT);
	//if (nullptr == m_pCustomLight)
	//	return E_FAIL;
	//m_pCustomLight->Set_Diffuse(D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	//m_pCustomLight->Set_Range(2000.f);
	//
	////Add_ShadowCubeGroup
	//GET_INSTANCE_MANAGEMENTR(E_FAIL);

	//for (auto pObj : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Map_Static"))
	//{
	//	m_pCustomLight->Add_ShadowCubeGroup(pObj);
	//}
	//for (auto pObj : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_GameObject"))
	//{
	//	m_pCustomLight->Add_ShadowCubeGroup(pObj);
	//}

	//CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	//if (nullptr == pLight_Manager)
	//	return E_FAIL;

	//pLight_Manager->AddRef();

	//pLight_Manager->Ready_ShadowMap(m_pCustomLight->GetLight());

	//Safe_Release(pLight_Manager);

	//Safe_Release(pManagement);

	if (FAILED(m_pGraphic_Device->CreateTexture(128, 128, 1, 0, D3DFMT_A32B32G32R32F, D3DPOOL_MANAGED, &m_pSkinTextures, nullptr)))
		return E_FAIL;

	return NOERROR;
}

_int CCamper::Update_GameObject(const _float & fTimeDelta)
{
	Test();
	Init_Camera();
	Collision_Check();
	Key_Input(fTimeDelta);
	State_Check();
	MoveSpeed_Check();
	Compute_Map_Index();

	m_iCollState = 0;
	m_fTimeDelta = fTimeDelta;
	m_pTargetOfInteraction = nullptr;

	/*	cout <<m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->x << " ";
	cout << m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->y << " ";
	cout << m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->z << "        ";
	cout << m_iIndex << endl; */

	Update_Actions(fTimeDelta);
	return _int();
}

_int CCamper::LastUpdate_GameObject(const _float & fTimeDelta)
{
	CommunicationWithServer();
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CCamper::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	if (!m_bPenetration)
	{
		m_pMeshCom->Play_Animation(m_fTimeDelta);
		LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
		if (nullptr == pEffect)
			return;

		pEffect->AddRef();

		_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

		pEffect->Begin(nullptr, 0);

		pEffect->BeginPass(2);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{
				m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

				D3DLOCKED_RECT lock_Rect = { 0, };
				if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, D3DLOCK_NOSYSLOCK | D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE)))
					return;

				memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

				m_pSkinTextures->UnlockRect(0);

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		pEffect->EndPass();
		pEffect->End();

		m_pColliderCom->Render_Collider();

		Safe_Release(pEffect);
	}
	else
	{
		m_pMeshCom->Play_Animation(m_fTimeDelta);

		LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
		if (nullptr == pEffect)
			return;

		pEffect->AddRef();

		_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

		pEffect->Begin(nullptr, 0);

		pEffect->BeginPass(4);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{
				m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

				D3DLOCKED_RECT lock_Rect = { 0, };
				if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
					return;

				memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

				m_pSkinTextures->UnlockRect(0);

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		pEffect->EndPass();

		pEffect->BeginPass(5);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		pEffect->EndPass();
		pEffect->End();

		m_pColliderCom->Render_Collider();
		Safe_Release(pEffect);
	}
}

void CCamper::Render_ShadowMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	//if (nullptr == m_pMeshCom ||
	//	nullptr == pEffect)
	//	return;

	//pEffect->Begin(nullptr, 0);

	//pEffect->BeginPass(1);

	//pEffect->SetVector("g_vLightPos", &vLightPos);
	//m_pTransformCom->SetUp_OnShader(pEffect, "g_matW");
	//pEffect->SetMatrix("g_matVP", VP);

	//pEffect->CommitChanges();

	//_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	//for (size_t i = 0; i < dwNumMeshContainer; i++)
	//{
	//	const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

	//	for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
	//	{
	//		m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

	//		D3DLOCKED_RECT lock_Rect = { 0, };
	//		if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
	//			return;

	//		memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

	//		m_pSkinTextures->UnlockRect(0);

	//		pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

	//		pEffect->CommitChanges();

	//		m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
	//	}

	//}

	//pEffect->EndPass();
	//pEffect->End();
}

_int CCamper::Do_Coll(CGameObject * _pObj, const _vec3& _vPos)
{
	m_pTargetOfInteraction = _pObj;
	m_vTargetPos = _vPos;

	return _int();
}

void CCamper::Test()
{
	if (m_eCurState != m_eOldState)
	{
		//cout << (_uint)m_eCurState << endl;
	}
}

void CCamper::SetMaxEnergyAndMaxHeal()
{
	if (m_eCurCondition == INJURED)
	{
		m_fMaxHeal = 30.f;
	}
	else if (m_eCurCondition == DYING)
	{
		m_fMaxEnergy = 240.f;
		m_fMaxHeal = 30.f;
	}
	else if (m_eCurCondition == HOOKED)
	{
		m_fMaxEnergy = 120.f;
	}
	else
		return;
}

void CCamper::Check_InteractionOfObject(CGameObject * pGameObject)
{
	if (m_iIndex != exPlayerNumber)
		return;

	if (m_bIsCarried)
		return;

	if (m_eCurState == AC::ClosetIdle && KEYMGR->KeyDown(DIK_SPACE))
	{
		CAction_HideCloset* pAction_HideCloset = (CAction_HideCloset*)Find_Action(L"Action_HideCloset");
		if (pAction_HideCloset->m_bIsPlaying)
			return;

		Set_Action(L"Action_UnhideCloset", 2.f);
		return;
	}

	if (m_bIsLockKey)
		return;

	if (m_pTargetOfInteraction == nullptr)
		return;

	_uint eID = m_pTargetOfInteraction->GetID();
 	if (eID & SLASHER)
		return;

	if (m_eCurCondition != HEALTHY && m_eCurCondition != INJURED)
		return;

	if (KEYMGR->MouseDown(0))
	{
		if (eID & CLOSET || eID & HATCH || eID & PLANK || eID & WINDOW)
			return;
		else if (eID & CAMPER)
		{
			CCamper* pCamper = (CCamper*)m_pTargetOfInteraction;
			CONDITION ConditionOfCamper = pCamper->GetCurCondition();
			//if (ConditionOfCamper != INJURED || ConditionOfCamper != DYING || ConditionOfCamper != HOOKED)
			//	return;

			CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
			pAction_MoveLerp->SetPos(m_vTargetPos);
			pAction_MoveLerp->SetTargetOfInteraction(m_pTargetOfInteraction);
			Set_Action(L"Action_MoveLerp", 0.2f);
			return;
		}
		else if (eID & HOOK)
		{
			return;
		}
		else if (eID & CHEST || eID & TOTEM || eID & EXITDOOR || eID & GENERATOR)
		{
			_float fProgressTime = m_pTargetOfInteraction->Get_ProgressTime();
			_float fMaxProgressTime = m_pTargetOfInteraction->Get_MaxProgressTime();
			
			if (fProgressTime >= fMaxProgressTime)
				return;
			
			CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
			pAction_MoveLerp->SetPos(m_vTargetPos);
			pAction_MoveLerp->SetTargetOfInteraction(m_pTargetOfInteraction);
			Set_Action(L"Action_MoveLerp", 0.2f);
			return;
		}

	}
	else if (KEYMGR->MouseDown(1))
	{
		if (eID & CAMPER || eID & CLOSET || eID & HATCH || eID & PLANK || eID & WINDOW || eID & CHEST || eID & EXITDOOR || eID & TOTEM)
			return;
		else if (eID & HOOK)
		{
			return;
		}
		else if (eID & GENERATOR)
		{
			return;
		}
		else
		{
			Set_Action(L"Action_HealingSelf", 100.f);
			return;
		}
	} 
	else if (KEYMGR->KeyDown(DIK_SPACE))
	{
		if (eID & CLOSET || eID & PLANK || eID & WINDOW)
		{
			CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
			pAction_MoveLerp->SetPos(m_vTargetPos);
			pAction_MoveLerp->SetTargetOfInteraction(m_pTargetOfInteraction);
			Set_Action(L"Action_MoveLerp", 0.2f);
			return;
		}
	}

}

void CCamper::Init_Camera()
{
	if (!LateInit && exPlayerNumber == m_iIndex)
	{
		LateInit = true;

		CManagement* pManagement = CManagement::GetInstance();
		if (nullptr == pManagement)
			return;

		pManagement->AddRef();

		((CCamera_Camper*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Camera", 0))->Set_OrbitMatrix();

		Safe_Release(pManagement);
	}
}

void CCamper::CommunicationWithServer()
{
	if (exPlayerNumber == m_iIndex)	// ����
	{
		//camper_data.Character =

		camper_data.bLive = true;
		camper_data.bConnect = true;

		camper_data.vRight = *m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT);
		camper_data.vUp = *m_pTransformCom->Get_StateInfo(CTransform::STATE_UP);
		camper_data.vLook = *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
		camper_data.vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

		camper_data.iState = (_int)m_eCurState;
		camper_data.iCondition = m_eCurCondition;
		camper_data.iHookStage = m_eHookStage;
		//Test();
		if (m_eObjectID == server_data.Slasher.SecondInterationObject)
		{
			_tchar ActionTag[150] = L"";
			switch (server_data.Slasher.SecondInterationObjAnimation)
			{
			case PLAYERATTACK:
				Check_Attacked();
				return;
				break;
			case PICK_UP:
				m_isColl = false;
				lstrcpy(ActionTag, L"Action_PickUp");
				break;
			case HOOK_IN:
				m_bIsCarried = false;
				lstrcpy(ActionTag, L"Action_HookIn");
				break;
			case DROP:
				lstrcpy(ActionTag, L"Action_Drop");
				break;
			case GRAB_LOCKER:
				m_isColl = false;
				lstrcpy(ActionTag, L"Action_SearchLocker");
				break;
			case GRAB_OBSTACLES:
				m_isColl = false;
				Assert_EndAllAction();
				lstrcpy(ActionTag, L"Action_Grab_Obstacles");
				break;
			case GRAB_GENERIC_FAST:
				m_isColl = false;
				Assert_EndAllAction();
				lstrcpy(ActionTag, L"Action_Grab_Generic_Fast");
				break;
			default:
				break;
			}

			Set_Action(ActionTag, 100.f);
		}
		else
		{
			for (int i = 0; i < 4; ++i)
			{
				if (i == (exPlayerNumber - 1))
				{
					if (server_data.Campers[i].SecondInterationObject != 0
						|| server_data.Campers[i].SecondInterationObjAnimation != 0)
					{
						camper_data.SecondInterationObject = 0;
						camper_data.SecondInterationObjAnimation = 0;
						continue;
					}
				}

				if (m_eObjectID == server_data.Campers[i].SecondInterationObject)
				{
					_tchar ActionTag[150] = L"";

					switch (server_data.Campers[i].SecondInterationObjAnimation)
					{
					case HOOK_BEING:
						Assert_EndAllAction();
						lstrcpy(ActionTag, L"Action_HookBeing");
						break;
					case BEING_HEAL:
						Assert_EndAllAction();
						lstrcpy(ActionTag, L"Action_BeingHeal");
						break;
					case END_HEAL:
						Assert_EndAllAction();
						break;
					case ClOSET_OCCUPIED:
						lstrcpy(ActionTag, L"Action_ClosetOccupied");
						break;
					default:
						break;
					}
					Set_Action(ActionTag, 100.f);
				}
			}
		}
	}
	else
	{
		_vec3 vRight = server_data.Campers[m_iIndex - 1].vRight;
		_vec3 vUp = server_data.Campers[m_iIndex - 1].vUp;
		_vec3 vLook = server_data.Campers[m_iIndex - 1].vLook;
		_vec3 vPos = server_data.Campers[m_iIndex - 1].vPos;

		m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

		m_eCurState = (AC::ANIMATION_CAMPER)server_data.Campers[m_iIndex - 1].iState;
		m_eCurCondition = (CONDITION)server_data.Campers[m_iIndex - 1].iCondition;
		m_eHookStage = (HOOKSTAGE)server_data.Campers[m_iIndex - 1].iHookStage;

		if (exPlayerNumber != 5)
		{
			if (CONDITION::DYING == m_eCurCondition ||
				CONDITION::HOOKED == m_eCurCondition)
			{
				m_bPenetration = true;
				m_Color = { 1.f, 0.2f, 0.2f, 1.f };
			}
			else
			{
				m_bPenetration = false;
			}

			if (CONDITION::HOOKED == server_data.Campers[exPlayerNumber - 1].iCondition)
			{
				m_bPenetration = true;
				m_Color = { 0.8f, 0.8f, 0.1f, 1.f };
			}
			else
			{
				m_bPenetration = false;
			}
		}
		
	}
}

void CCamper::Compute_Map_Index()
{
	const _vec3* pPos = m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	_int IndexX = (_int)(pPos->x / _MAP_INTERVAL);
	_int IndexZ = (_int)(pPos->z / _MAP_INTERVAL);

	m_iMapIndex = (IndexX * 14) + IndexZ;

}

void CCamper::Collision_Check()
{
	//if (exPlayerNumber == 5)
	//{
	//	if (m_eCurState == AC::CrawlFT)
	//		m_isColl = true;
	//}
}

void CCamper::Check_OtherCamperState()
{
	CManagement* pManagement = CManagement::GetInstance();
	list<CGameObject*>* pObjList = pManagement->Get_ObjList(SCENE_STAGE, L"Layer_Camper");

	if (pObjList == nullptr)
		return;

	_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	
	for (auto pObj : *pObjList)
	{
		if (pObj->GetID() == m_eObjectID)
			continue;

		CTransform* pOtherCamperTransform = (CTransform*)pObj->Get_ComponentPointer(L"Com_Transform");
		_vec3 vOtherCamperPos = *pOtherCamperTransform->Get_StateInfo(CTransform::STATE_POSITION);

		_float fDist = D3DXVec3Length(&(vMyPos - vOtherCamperPos));
		if (fDist < 200.f)
		{
			CCamper* pOtherCamper = (CCamper*)pObj;
			AC::ANIMATION_CAMPER eOtherCamperState = pOtherCamper->GetState();
			
			switch (eOtherCamperState)
			{
			case AC::HealCamper:
				Set_Action(L"Action_BeingHeal", 50.f);
				break;
			case AC::HookBeingCamperIn:
				Set_Action(L"Action_hookBeing", 50.f);
				break;
			case AC::ClosetFull_Entering:
				Set_Action(L"Action_ClosetOccupied", 50.f);
				break;
			}
		}
	}
}

void CCamper::Check_SlasherCamperState()
{
	CManagement* pManagement = CManagement::GetInstance();
	CTransform* pSlasherTransform = (CTransform*)pManagement->Get_ComponentPointer(SCENE_STAGE, L"Layer_Slasher", L"Com_Transform", 0);
	_vec3 vSlasherPos = *pSlasherTransform->Get_StateInfo(CTransform::STATE_POSITION);
	
	_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	_float fDist = D3DXVec3Length(&(vMyPos - vSlasherPos));
	list<_float> CamperDistList;

	if (fDist < 200.f)
	{
		list<CGameObject*>* pObjList = pManagement->Get_ObjList(SCENE_STAGE, L"Com_Transform");

		if (pObjList == nullptr)
			return;

		for (auto pObj : *pObjList)
		{
			CTransform* pCamperTransform = (CTransform*)pObj->Get_ComponentPointer(L"Com_Transform");
			_vec3 vCamperPos = *pCamperTransform->Get_StateInfo(CTransform::STATE_POSITION);
		
			_float fCamperDist = D3DXVec3Length(&(vCamperPos - vSlasherPos));
			if (fCamperDist < 200.f)
				CamperDistList.push_back(fCamperDist);
		}

		if (CamperDistList.size() < 1)
			return;

		CamperDistList.sort();
		if (fabs(fDist - CamperDistList.front()) > 1.f)
			return;

		CSlasher* pSlasher = (CSlasher*)pManagement->Get_ObjList(SCENE_STAGE, L"Layer_Slasher")->back();

		AW::ANIMATION_SLASHER_WRAITH eSlasherState = pSlasher->Get_State();

		switch (eSlasherState)
		{
		case AW::TW_PcikUp_IN:
			Set_Action(L"Action_PickUp", 100.f);
			break;
		case AW::TW_Hook_In:
			Set_Action(L"Action_HookIn", 100.f);
			break;
		case AW::TW_Drop:
			Set_Action(L"Action_Drop", 100.f);
			break;
		case AW::TT_SearchLocker:
			Set_Action(L"Action_SearchLocker", 100.f);
			break;
		case AW::TT_Grab_Obstacles_BK:
		case AW::TT_Grab_Obstacles_FT:
			Set_Action(L"Action_Grab_Obstacles", 100.f);
			break;
		case AW::TT_Grab_Generic_Fast:
			Set_Action(L"Action_Grab_Generic_Fast", 100.f);
			break;
		}
		


	}


}


HRESULT CCamper::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Mesh
	m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Camper_Adam", (void*)CMesh_Dynamic::CAMPER);
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	m_pMatCamera = m_pMeshCom->Find_Frame("joint_TorsoC_01");

	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, 150.f, 150.f, 150.f);
	matLocalTransform._42 = 75.f;

	m_pColliderCom = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
		return E_FAIL;

	m_pRHandMatrix = m_pMeshCom->Find_Frame("joint_CamperAttach_01");

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CCamper::Ready_Actions()
{
	//CamperInteractions;
	CAction* pAction = new CAction_BeingHeal;
	m_mapActions.insert(MAPACTION::value_type(L"Action_BeingHeal", pAction));

	pAction = new CAction_HealCamper;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HealCamper", pAction));

	pAction = new CAction_HealingSelf;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HealingSelf", pAction));

	pAction = new CAction_HookBeing;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HookBeing", pAction));

	pAction = new CAction_ClosetOccupied;
	m_mapActions.insert(MAPACTION::value_type(L"Action_ClosetOccupied", pAction));
	// ObjectInteractions
	pAction = new CAction_BeingKilledBySpider;
	m_mapActions.insert(MAPACTION::value_type(L"Action_BeingKilledBySpider", pAction));

	pAction = new CAction_Generator;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Generator", pAction));

	pAction = new CAction_JumpInHatch;
	m_mapActions.insert(MAPACTION::value_type(L"Action_JumpInHatch", pAction));

	pAction = new CAction_LootChest;
	m_mapActions.insert(MAPACTION::value_type(L"Action_LootChest", pAction));

	pAction = new CAction_CleansTotem;
	m_mapActions.insert(MAPACTION::value_type(L"Action_CleansTotem", pAction));

	pAction = new CAction_PickItemOnGround;
	m_mapActions.insert(MAPACTION::value_type(L"Action_PickItemOnGround", pAction));

	pAction = new CAction_Sabotage;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Sabotage", pAction));

	pAction = new CAction_UnlockExit;
	m_mapActions.insert(MAPACTION::value_type(L"Action_UnlockExit", pAction));

	pAction = new CAction_SpiderReaction;
	m_mapActions.insert(MAPACTION::value_type(L"Action_SpiderReaction", pAction));

	// ObastcleInteractions
	pAction = new CAction_HideCloset;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HideCloset", pAction));

	pAction = new CAction_JumpPlank;
	m_mapActions.insert(MAPACTION::value_type(L"Action_JumpPlank", pAction));

	pAction = new CAction_PassWindow;
	m_mapActions.insert(MAPACTION::value_type(L"Action_PassWindow", pAction));

	pAction = new CAction_PullDownPlank;
	m_mapActions.insert(MAPACTION::value_type(L"Action_PullDownPlank", pAction));

	pAction = new CAction_UnhideCloset;
	m_mapActions.insert(MAPACTION::value_type(L"Action_UnhideCloset", pAction));

	// SlasherInteractions
	pAction = new CAction_CrawlToHit;
	m_mapActions.insert(MAPACTION::value_type(L"Action_CrawlToHit", pAction));

	pAction = new CAction_HookFree;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HookFree", pAction));

	pAction = new CAction_Drop;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Drop", pAction));

	pAction = new CAction_Grab_Generic_Fast;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Grab_Generic_Fast", pAction));

	pAction = new CAction_Grab_Obstacles;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Grab_Obstacles", pAction));

	pAction = new CAction_HookIn;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HookIn", pAction));

	pAction = new CAction_PickUp;
	m_mapActions.insert(MAPACTION::value_type(L"Action_PickUp", pAction));

	pAction = new CAction_SearchLocker;
	m_mapActions.insert(MAPACTION::value_type(L"Action_SearchLocker", pAction));

	pAction = new CAction_StunDrop;
	m_mapActions.insert(MAPACTION::value_type(L"Action_StunDrop", pAction));

	// public
	pAction = new CAction_MoveLerp;
	m_mapActions.insert(MAPACTION::value_type(L"Action_MoveLerp", pAction));

	return NOERROR;
}

HRESULT CCamper::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED * pMeshContainer, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetVector("g_DiffuseColor", (_vec4*)&m_Color);

	return NOERROR;
}

void CCamper::Key_Input(const _float& fTimeDelta)
{
	if (exPlayerNumber != m_iIndex)
		return;

	Check_InteractionOfObject(nullptr);

	if (m_bIsCarried)
	{
		IsCarried();
		return;
	}

	if (m_bIsLockKey)
		return;

	_int	iCount = 0;
	_vec3	vPosAcc = { 0.f, 0.f, 0.f };
	_matrix	matCam;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matCam);
	D3DXMatrixInverse(&matCam, nullptr, &matCam);
	_vec3 vRight = (_vec3)&matCam.m[CTransform::STATE_RIGHT][0];
	_vec3 vLook = (_vec3)&matCam.m[CTransform::STATE_LOOK][0];

	if (!m_bIsLockKey && KEYMGR->KeyPressing(DIK_W))
	{
		iCount++;
		D3DXVec3Normalize(&vLook, &vLook);
		vPosAcc += vLook;
	}
	if (!m_bIsLockKey && KEYMGR->KeyPressing(DIK_S))
	{
		iCount++;
		D3DXVec3Normalize(&vLook, &vLook);
		vPosAcc += -vLook;
	}
	if (!m_bIsLockKey && KEYMGR->KeyPressing(DIK_A))
	{
		iCount++;
		D3DXVec3Normalize(&vRight, &vRight);
		vPosAcc += -vRight;
	}
	if (!m_bIsLockKey && KEYMGR->KeyPressing(DIK_D))
	{
		iCount++;
		D3DXVec3Normalize(&vRight, &vRight);
		vPosAcc += vRight;
	}

	if (!m_bIsLockKey && KEYMGR->KeyPressing(DIK_LCONTROL))
	{
		if (KEYMGR->KeyPressing(DIK_W) || KEYMGR->KeyPressing(DIK_A) || KEYMGR->KeyPressing(DIK_S) || KEYMGR->KeyPressing(DIK_D))
		{
			vPosAcc = (vPosAcc / (_float)iCount);
			D3DXVec3Normalize(&vPosAcc, &vPosAcc);
			m_pTransformCom->Go_ToTarget_ChangeDirection(&vPosAcc, fTimeDelta * 0.3f * m_fMoveSpeed);

			if (m_eCurCondition == HEALTHY)
				m_eCurState = AC::CrouchWalk;
			else if (m_eCurCondition == INJURED)
				m_eCurState = AC::Injured_CrouchWalk;
		}
		else
		{
			if (m_eCurCondition == HEALTHY)
				m_eCurState = AC::CrouchStand;
			else if (m_eCurCondition == INJURED)
				m_eCurState = AC::Injured_CrouchIdle;
		}
	}
	else if (!m_bIsLockKey && KEYMGR->KeyPressing(DIK_LSHIFT))
	{
		if (m_eCurCondition != DYING && KEYMGR->KeyPressing(DIK_W) || KEYMGR->KeyPressing(DIK_A) || KEYMGR->KeyPressing(DIK_S) || KEYMGR->KeyPressing(DIK_D))
		{
			vPosAcc = (vPosAcc / (_float)iCount);
			D3DXVec3Normalize(&vPosAcc, &vPosAcc);
			m_pTransformCom->Go_ToTarget_ChangeDirection(&vPosAcc, fTimeDelta * m_fMoveSpeed);

			if (m_eCurCondition == HEALTHY)
				m_eCurState = AC::Run;
			else if (m_eCurCondition == INJURED)
				m_eCurState = AC::Injured_Run;
			else if (m_eCurCondition == DYING)
				m_eCurState = AC::CrawlFT;
		}
	}
	else if(!m_bIsLockKey)
	{
		if (m_eCurCondition == HEALTHY)
			m_eCurState = AC::Walk;
		else if (m_eCurCondition == INJURED)
			m_eCurState = AC::Injured_Walk;
		else if (m_eCurCondition == DYING)
			m_eCurState = AC::CrawlFT;

		if (KEYMGR->KeyPressing(DIK_W) || KEYMGR->KeyPressing(DIK_A) || KEYMGR->KeyPressing(DIK_S) || KEYMGR->KeyPressing(DIK_D))
		{
			vPosAcc = (vPosAcc / (_float)iCount);
			D3DXVec3Normalize(&vPosAcc, &vPosAcc);
			m_pTransformCom->Go_ToTarget_ChangeDirection(&vPosAcc, fTimeDelta * 0.5f * m_fMoveSpeed);
		}
		else
		{
			if (m_eCurCondition == HEALTHY)
				m_eCurState = AC::Idle;
			else if (m_eCurCondition == INJURED)
				m_eCurState = AC::Injured_Idle;
			else if (m_eCurCondition == DYING)
				m_eCurState = AC::CrawlFT;
		
		}
	}

	if (KEYMGR->KeyDown(DIK_B))
		EFFMGR->Make_Effect(CEffectManager::E_Generator,*m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

}

void CCamper::State_Check()
{
	if (m_eCurState == m_eOldState)
		return;

	if (m_eCurState == AC::ClosetHideFast || m_eOldState == AC::ClosetHideFast)
		m_pMeshCom->Set_NoBleningAnimationSet(m_eCurState);
	else
		m_pMeshCom->Set_AnimationSet(m_eCurState);


	m_eOldState = m_eCurState;
}

void CCamper::Check_Attacked()
{
	if (m_eCurCondition != HEALTHY && m_eCurCondition != INJURED
		&& m_eCurCondition != SPECIAL && m_eCurCondition != OBSTACLE)
		return;

	if (m_bIsCarried)
		return;

	if (AC::ClosetIdle == m_eCurState)
		return;

	server_data.Slasher.SecondInterationObject = 0;
	server_data.Slasher.SecondInterationObjAnimation = 0;

	Set_Action(L"Action_CrawlToHit", 10.f);

	switch (m_eCurCondition)
	{
	case HEALTHY:
		m_eCurCondition = INJURED;
		break;
	case INJURED:
		m_eCurCondition = DYING;
		break;
	case SPECIAL:
	{
		if (m_eOldCondition == HEALTHY)
			m_eCurCondition = INJURED;
		else if (m_eOldCondition == INJURED)
			m_eCurCondition = DYING;
	}
		break;
	case OBSTACLE:
	{
		if (m_eOldCondition == HEALTHY)
			m_eCurCondition = INJURED;
		else if (m_eOldCondition == INJURED)
			m_eCurCondition = DYING;
	}
	break;
	}
}

void CCamper::MoveSpeed_Check()
{
	switch (m_eCurCondition)
	{
	case HEALTHY:
		m_bPlayAnimation = true;
		m_fMoveSpeed = 1.f;
		break;
	case INJURED:
		m_bPlayAnimation = true;
		m_fMoveSpeed = 1.f;
		break;
	case DYING:
		m_fMoveSpeed = 0.1f;
		break;
	default:
		m_bPlayAnimation = true;
		m_fMoveSpeed = 1.f;
		break;
	}
}

void CCamper::IsCarried()
{
	m_isColl = false;
	CManagement* pManagement = CManagement::GetInstance();
	CTransform* pTransform = (CTransform*)pManagement->Get_ComponentPointer(SCENE_STAGE, L"Layer_Slasher", L"Com_Transform");
	CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
	
	_matrix matSlasher = pTransform->Get_Matrix();
	_vec3 vRight = -*pTransform->Get_StateInfo(CTransform::STATE_RIGHT) * 5.f;
	matSlasher.m[3][0] += vRight.x;
	matSlasher.m[3][1] += vRight.y;
	matSlasher.m[3][2] += vRight.z;

	m_pTransformCom->Set_Matrix(matSlasher);
	m_pTransformCom->Rotation_Axis_Angle(D3DXToRadian(180.f), /*pTransform->Get_StateInfo(CTransform::STATE_UP)*/&_vec3(0,1,0));
}

CCamper * CCamper::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCamper*	pInstance = new CCamper(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CCamper Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CCamper::Clone_GameObject()
{
	CCamper*	pInstance = new CCamper(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CCamper Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CCamper::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_PLAYER, this);

	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	Safe_Release(m_pColliderCom);

	Safe_Release(m_pCustomLight);

	CGameObject::Free();
}