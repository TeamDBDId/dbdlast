#include "stdafx.h"
#include "..\Headers\Totem.h"
#include "Management.h"
#include "Defines.h"
#include "MeshTexture.h"

_USING(Client)

CTotem::CTotem(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CTotem::CTotem(const CTotem & rhs)
	: CGameObject(rhs)
{
}

HRESULT CTotem::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CTotem::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_fProgressTime = 0.f;
	m_fMaxProgressTime = 14.f;
	return NOERROR;
}

_int CTotem::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
	{
		m_bDissolve = true;
	}
	if (m_bDissolve)
	{
		m_fDissolveTime += fTimeDelta;
	}
	if (m_fDissolveTime >= 2.5f)
		return DEAD_OBJ;

	ComunicateWithServer();
	return _int();
}

_int CTotem::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	Compute_CameraDistance(&vPosition);

	if (m_fCameraDistance > 10000.f)
		return 0;

	_matrix		matLocal, matWorld;
	matWorld = m_pTransformCom->Get_Matrix();
	matLocal = m_pMeshCom->Get_LocalTransform();

	matWorld = matLocal * matWorld;
	vPosition = *(_vec3*)&matWorld.m[3][0];
	D3DXMatrixInverse(&matWorld, nullptr, &matWorld);

	if (!m_pFrustumCom->Culling_Frustum(&vPosition, matWorld, m_Max))
		return 0;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CTotem::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);


	m_pColliderCom->Render_Collider();
}

void CTotem::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	_uint iPass = 0;
	if (!m_bDissolve)
		iPass = 0;
	else
		iPass = 8;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(iPass);

	pEffect->SetVector("g_vLightPos", &vLightPos);
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matW");
	pEffect->SetMatrix("g_matVP", VP);

	pEffect->CommitChanges();
	for (size_t i = 0; i < dwNumMaterials; i++)
	{

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();
}

void CTotem::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;


	m_fRad = 120.f;
	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);



	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Mesh_Totem");
	Add_Component(L"Com_Mesh", m_pMeshCom);

	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_Collider", m_pColliderCom);

	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_TOTEM, this);

	_vec3 vMax = m_pMeshCom->GetMax();
	_vec3 vMin = m_pMeshCom->GetMin();
	_vec3 vResult = (vMax - vMin) * 0.52f;

	m_Max = max(vResult.x, max(vResult.y, vResult.z));
}

_matrix CTotem::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CTotem::Get_Key()
{
	return m_Key.c_str();
}

_int CTotem::Get_OtherOption()
{
	return 0;
}

HRESULT CTotem::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CTotem::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);

	if (m_bDissolve)
	{
		pEffect->SetTexture("g_DissolveTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"DissolveSpider.tga")));
		pEffect->SetTexture("g_DissolveEffect", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"BurnedEmbers.tga")));
		pEffect->SetFloat("g_fTimeAcc", (m_fDissolveTime / 2.5f));
	}

	return NOERROR;
}

void CTotem::ComunicateWithServer()
{
	m_fProgressTime = server_data.Game_Data.Totem[m_eObjectID - TOTEM];

	if (m_fProgressTime > m_fMaxProgressTime)
		m_isDead = true;
}

CTotem * CTotem::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CTotem*	pInstance = new CTotem(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CTotem Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CTotem::Clone_GameObject()
{
	CTotem*	pInstance = new CTotem(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CArt_Static Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CTotem::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_TOTEM, this);

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
