#include "stdafx.h"
#include "..\Headers\Action_PickUp.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Slasher.h"

_USING(Client)

CAction_PickUp::CAction_PickUp()
{
}

HRESULT CAction_PickUp::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	if (m_pGameObject->GetID() & SLASHER)
		m_pSlasher = (CSlasher*)m_pGameObject;
	else
	{
		m_pCamper = (CCamper*)m_pGameObject;
		m_pSlasher = nullptr;
	}

	if (m_pSlasher == nullptr)	// camper
	{
		m_pCamper = (CCamper*)m_pGameObject;
		m_pCamper->IsLockKey(true);
		m_pCamper->Set_State(AC::TT_PickUp_In);
		m_iState = AC::TT_PickUp_In;
		m_pCamper->SetColl(false);
	}
	else
	{
		m_pSlasher = (CSlasher*)m_pGameObject;
		m_pSlasher->IsLockKey(true);
		m_pSlasher->Set_State(AW::TW_PcikUp_IN);
		m_iState = AW::TW_PcikUp_IN;
		m_pCamper->SetColl(false);
	}
	m_bIsPlaying = true;

	return NOERROR;
}

_int CAction_PickUp::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay > 0.0f)
		m_fDelay -= fTimeDelta;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	if (m_pSlasher == nullptr)
	{
		if (m_iState == AC::TT_PickUp_In)
		{
			CTransform* pCamperTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
			_vec3 vCamperDir[4];
			CTransform* pSlasherTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
			_vec3 vSlasherDir[4];
			
			for (_int i = 0; i < 4; ++i)
			{
				vCamperDir[i] = *pCamperTransform->Get_StateInfo(CTransform::STATE(i));
				vSlasherDir[i] = *pSlasherTransform->Get_StateInfo(CTransform::STATE(i));
				
				D3DXVec3Lerp(&vCamperDir[i], &vCamperDir[i], &vSlasherDir[i], fTimeDelta * 5.f);
				pCamperTransform->Set_StateInfo(CTransform::STATE(i), &vCamperDir[i]);
			}

			if (pMeshCom->IsOverTime(0.28f))
			{
				m_pCamper->Set_State(AC::TT_PickUp);
				m_iState = AC::TT_PickUp;
			}
		}
		else if (m_iState == AC::TT_PickUp)
		{
			m_pCamper->SetCarry(true);
			if (pMeshCom->IsOverTime(0.28f))
			{
				m_pCamper->Set_State(AC::TT_Carry_Idle);
				return END_ACTION;
			}
		}
	}
	else
	{
		if (m_iState == AW::TW_PcikUp_IN)
		{
			if (pMeshCom->IsOverTime(0.28f))
			{
				m_pSlasher->Set_State(AW::TW_PickUp);
				m_iState = AW::TW_PickUp;
				m_pSlasher->Set_Carry(true);
			}
		}
		else if (m_iState == AW::TW_PickUp)
		{
			if (pMeshCom->IsOverTime(0.28f))
			{
				m_pSlasher->Set_State(AW::TW_PickUp);
				return END_ACTION;
			}
		}
	}

	if (m_pSlasher != nullptr)
		Send_ServerData();

	return UPDATE_ACTION;
}

void CAction_PickUp::End_Action()
{
	m_fDelay = 1.f;
	m_bIsPlaying = false;
	if (m_pSlasher == nullptr)
	{
		m_pCamper->IsLockKey(false);
		m_pCamper->SetColl(false);
	}
	else
	{
		m_pSlasher->IsLockKey(false);
		m_pSlasher->Set_CarriedCamper(m_pCamper);
	}

	m_pCamper = nullptr;
	m_pSlasher = nullptr;
}

void CAction_PickUp::Send_ServerData()
{
	slasher_data.SecondInterationObject = m_pCamper->GetID();
	slasher_data.SecondInterationObjAnimation = PICK_UP;
}

void CAction_PickUp::Free()
{
	CAction::Free();
}
