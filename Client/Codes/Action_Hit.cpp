#include "stdafx.h"
#include "..\Headers\Action_Hit.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Slasher.h"
#include "Generator.h"

_USING(Client)

CAction_Hit::CAction_Hit()
{
}

HRESULT CAction_Hit::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	m_fIndex = 0.f;
	m_fAccTimeDelta = 1.f;

	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	m_bIsPlaying = true;
	pSlasher->IsLockKey(true);
	pSlasher->Set_State(AW::TW_Attack_In_FPV);
	pSlasher->Set_WeaponColl(false);

	m_iState = AW::TW_Attack_In_FPV;

	return NOERROR;
}

_int CAction_Hit::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay > 0.0f)
		m_fDelay -= fTimeDelta;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;
	((CSlasher*)m_pGameObject)->RotateY(fTimeDelta);

	_int iIndex = (int)m_fIndex;
	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

	if (m_iState == AW::TW_Attack_In_FPV)
	{
		m_fAccTimeDelta = 1.5f;

		if (pMeshCom->IsOverTime(0.25f) || !KEYMGR->MousePressing(0))
		{
			pSlasher->Set_WeaponColl(true);
			pSlasher->Set_State(AW::TW_Attack_Swing_FPV);
			m_iState = AW::TW_Attack_Swing_FPV;
		}
	}
	else if (m_iState == AW::TW_Attack_Swing_FPV)
	{
		m_fAccTimeDelta = 1.5f;

		if (pMeshCom->IsOverTime(0.3f))
		{
			pSlasher->Set_WeaponColl(false);
			pSlasher->Set_State(AW::TW_Attack_Miss_Out_FPV);
			m_iState = AW::TW_Attack_Miss_Out_FPV;
		}
	}
	else if (m_iState == AW::TW_Attack_Miss_Out_FPV)
	{
		m_fAccTimeDelta = 0.1f;

		if (pMeshCom->IsOverTime(0.01f))
		{
			pSlasher->Set_State(AW::TW_Idle);
			m_iState = AW::TW_Idle;
			return END_ACTION;
		}
	}

	if (KEYMGR->KeyPressing(DIK_W))
		pTransform->Go_Straight(fTimeDelta * m_fAccTimeDelta);
	if (KEYMGR->KeyPressing(DIK_S))
		pTransform->BackWard(fTimeDelta * m_fAccTimeDelta);
	if (KEYMGR->KeyPressing(DIK_A))
		pTransform->Go_Left(fTimeDelta * m_fAccTimeDelta);
	if (KEYMGR->KeyPressing(DIK_D))
		pTransform->Go_Right(fTimeDelta * m_fAccTimeDelta);

	return UPDATE_ACTION;
}

void CAction_Hit::End_Action()
{
	m_fDelay = 1.f;
	m_bIsPlaying = false;
	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	pSlasher->Set_WeaponColl(false);
	pSlasher->IsLockKey(false);
}

void CAction_Hit::Send_ServerData()
{
}

void CAction_Hit::Free()
{
	CAction::Free();
}
