#include "stdafx.h"
#include "Generator.h"
#include "Management.h"
#include "Light_Manager.h"
#include "CustomLight.h"
#include "Slasher.h"

_USING(Client)

CGenerator::CGenerator(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CGenerator::CGenerator(const CGenerator & rhs)
	: CGameObject(rhs)
{

}


HRESULT CGenerator::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CGenerator::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (FAILED(m_pGraphic_Device->CreateTexture(128, 128, 1, 0, D3DFMT_A32B32G32R32F, D3DPOOL_MANAGED, &m_pSkinTextures, nullptr)))
		return E_FAIL;

	if (exPlayerNumber == 5)
		m_bPenetration = true;

	m_fProgressTime = 0.f;
	m_fMaxProgressTime = 80.f;

	return NOERROR;
}

_int CGenerator::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	ComunicateWithServer();

	Progress_Check();

	m_fDeltaTime = fTimeDelta;
	State_Check();

	if (exPlayerNumber == 5)
	{
		if (m_fProgressTime > m_fMaxProgressTime)
			m_bPenetration = false;
		else
			m_bPenetration = true;
	}

	//
	if (m_fProgressTime > 0.5f)
	{
		swprintf_s(sztt, sizeof(sztt) / sizeof(wchar_t), L"%f", m_fProgressTime);

		//SetWindowText(g_hWnd, sztt);
	}
	Penetration_Check();

	return _int();
}

_int CGenerator::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (!m_bPenetration)
	{
		if (m_fCameraDistance > 6000.f)
			return 0;
	}

	_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	_matrix matWorld = m_pTransformCom->Get_Matrix();
	vPosition.y += 240.f;
	matWorld._42 = vPosition.y;
	D3DXMatrixInverse(&matWorld, nullptr, &matWorld);
	if (m_pFrustumCom->Culling_Frustum(&vPosition, matWorld, 240.f))
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;
	}

	return _int();
}

void CGenerator::Render_GameObject()
{
	if (!m_bPenetration)
	{
		if (nullptr == m_pMeshCom ||
			nullptr == m_pShaderCom)
			return;

		m_pMeshCom->Play_Animation(m_fDeltaTime);

		LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
		if (nullptr == pEffect)
			return;

		pEffect->AddRef();

		_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

		pEffect->Begin(nullptr, 0);

		pEffect->BeginPass(2);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{
				m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

				D3DLOCKED_RECT lock_Rect = { 0, };
				if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
					return;

				memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

				m_pSkinTextures->UnlockRect(0);

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		pEffect->EndPass();
		pEffect->End();

		Safe_Release(pEffect);
	}
	else
	{
		if (nullptr == m_pMeshCom ||
			nullptr == m_pShaderCom)
			return;

		m_pMeshCom->Play_Animation(m_fDeltaTime);

		LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
		if (nullptr == pEffect)
			return;

		pEffect->AddRef();

		_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

		pEffect->Begin(nullptr, 0);

		pEffect->BeginPass(4);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{
				m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

				D3DLOCKED_RECT lock_Rect = { 0, };
				if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, D3DLOCK_NOSYSLOCK | D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE)))
					return;

				memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

				m_pSkinTextures->UnlockRect(0);

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		pEffect->EndPass();

		pEffect->BeginPass(5);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		pEffect->EndPass();
		pEffect->End();

		/////////////////////


		Safe_Release(pEffect);
	}
}

void CGenerator::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(1);

	pEffect->SetVector("g_vLightPos", &vLightPos);
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matW");
	pEffect->SetMatrix("g_matVP", VP);

	pEffect->CommitChanges();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CGenerator::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;


	_vec3 vFront = *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
	D3DXVec3Normalize(&vFront, &vFront);
	vFront *= m_fRad;

	_vec3 vRight = *m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT);
	D3DXVec3Normalize(&vRight, &vRight);
	vRight *= m_fRad;



	m_vPosArr = new _vec3[4];
	m_vPosArr[DIR::FRONT] = vFront;
	m_vPosArr[DIR::BACK] = -vFront;
	m_vPosArr[DIR::LEFT] = -vRight;
	m_vPosArr[DIR::RIGHT] = vRight;


	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_GENER, this);

	_matrix matLocal;
	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	_vec3 vLocalPos = { 45.f, 470.f, -50.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[0] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider0", m_pLightCollider[0]);

	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	vLocalPos = { -50.f, 470.f, -50.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[1] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider1", m_pLightCollider[1]);

	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	vLocalPos = { 40.f, 435.f, -100.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[2] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider2", m_pLightCollider[2]);

	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	vLocalPos = { -40.f, 435.f, -100.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[3] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider3", m_pLightCollider[3]);

	for (int i = 0; i < 4; ++i)
	{
		_vec3 vCenter = m_pLightCollider[i]->Get_Center();

		if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomLight[i])))
			return;

		m_pCustomLight[i]->Add_Light();
		m_pCustomLight[i]->AddRef();

		m_pCustomLight[i]->Set_Position(vCenter);

		m_pCustomLight[i]->Set_Range(60.f);
		m_pCustomLight[i]->Set_Diffuse(D3DXCOLOR(5.f, 5.f, 5.f, 1.f));
		//AddShadowMap
		m_pCustomLight[i]->SetRender(false);

		CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
		if (nullptr == pLight_Manager)
			return;

		pLight_Manager->AddRef();

		pLight_Manager->Ready_ShadowMap(m_pCustomLight[i]->GetLight());

		Safe_Release(pLight_Manager);
	}


	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	vLocalPos = { 45.f, 380.f, -75.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pColliderSpotCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider0", m_pColliderSpotCom);

	if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomSpotLight)))
		return;

	m_pCustomSpotLight->Add_Light();
	m_pCustomSpotLight->AddRef();

	m_pCustomSpotLight->Set_Position(m_pColliderSpotCom->Get_Center());
	m_pCustomSpotLight->Set_Type(D3DLIGHT_SPOT);

	m_pCustomSpotLight->Set_Diffuse(D3DXCOLOR(3.6f, 3.6f, 3.5f, 1.f));
	m_pCustomSpotLight->Set_Range(400.f);
	m_pCustomSpotLight->Set_Theta(D3DXToRadian(21.5f));
	m_pCustomSpotLight->Set_Phi(D3DXToRadian(27.5f));

	m_pCustomSpotLight->Set_Direction(_vec3(0.f, -1.f, 0.f));
	m_pCustomSpotLight->SetRender(false);

	//Add_ShadowCubeGroup
	GET_INSTANCE_MANAGEMENT;

	for (auto pObj : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Map_Static"))
	{
		m_pCustomSpotLight->Add_ShadowCubeGroup(pObj);
	}
	for (auto pObj : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_GameObject"))
	{
		m_pCustomSpotLight->Add_ShadowCubeGroup(pObj);
	}

	CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return;

	pLight_Manager->AddRef();

	pLight_Manager->Ready_ShadowMap(m_pCustomSpotLight->GetLight());

	Safe_Release(pLight_Manager);

	Safe_Release(pManagement);
}

_matrix CGenerator::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CGenerator::Get_Key()
{
	return m_Key.c_str();
}

_int CGenerator::Get_OtherOption()
{
	return m_iOtherOption;
}

void CGenerator::State_Check()
{
	if (m_CurState == m_OldState)
		return;
	m_pMeshCom->Set_AnimationSet(m_CurState);
	m_OldState = m_CurState;
}

void CGenerator::Progress_Check()
{
	if (m_fProgressTime >= m_fMaxProgressTime)
	{
		m_CurState = On;
		for (int i = 0; i < 4; ++i)
			m_pCustomLight[i]->SetRender(true);
		
		m_pCustomSpotLight->SetRender(true);
	}
	else if (m_CurState == Failure)
	{
		m_CurState = Idle;
		Progress_Check();
	}
	else if (m_fProgressTime >= 60.f)
		m_CurState = PistonsIdle;
	else if (m_fProgressTime >= 40.f)
		m_CurState = PistonsIdleS3;
	else if (m_fProgressTime >= 20.f)
		m_CurState = PistonsIdleS2;
	else if (m_fProgressTime > 0.f)
		m_CurState = PistonsIdleS1;
	else if (m_fProgressTime == 0.f)
		m_CurState = Idle;
}

void CGenerator::ComunicateWithServer()
{
	if (server_data.Game_Data.GRP[m_eObjectID - GENERATOR] > 50.f)
	{
		int a = 10;
	}
	m_fProgressTime = server_data.Game_Data.GRP[m_eObjectID - GENERATOR];
}

void CGenerator::Penetration_Check()
{
	if (5 != exPlayerNumber)
		return;

	if (m_fProgressTime >= 80.f)
	{
		m_bPenetration = false;
		return;
	}
		
	GET_INSTANCE_MANAGEMENT;

	CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
	if (nullptr == pSlasher)
	{
		Safe_Release(pManagement);
		return;
	}

	if (nullptr == pSlasher->Get_CarriedCamper())		
		m_bPenetration = true;		
	else
		m_bPenetration = false;

	Safe_Release(pManagement);
}

HRESULT CGenerator::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.CMesh_Static
	m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_Generator");
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	m_fRad = 90.f;
	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);
	matLocalTransform._42 = m_fRad*0.5f;
	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_Collider", m_pColliderCom);
	if (FAILED(m_pColliderCom->Ready_HullMesh(SCENE_STAGE, L"Mesh_Generator")))
		_MSG_BOX("111");

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CGenerator::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);

	return NOERROR;
}


CGenerator * CGenerator::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CGenerator*	pInstance = new CGenerator(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CGenerator Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CGenerator::Clone_GameObject()
{
	CGenerator*	pInstance = new CGenerator(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CGenerator Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CGenerator::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_GENER, this);

	for (int i = 0; i < 4; ++i)
	{
		Safe_Release(m_pLightCollider[i]);
		Safe_Release(m_pCustomLight[i]);
	}
	
	Safe_Release(m_pColliderSpotCom);
	Safe_Release(m_pCustomSpotLight);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
