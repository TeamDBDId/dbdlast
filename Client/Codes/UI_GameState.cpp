#include "stdafx.h"
#include "..\Headers\UI_GameState.h"
#include "Management.h"
#include "UI_Texture.h"
#include "Camper.h"

_USING(Client);




CUI_GameState::CUI_GameState(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_GameState::CUI_GameState(const CUI_GameState & rhs)
	: CGameObject(rhs)
{
}


HRESULT CUI_GameState::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CUI_GameState::Ready_GameObject()
{
	Set_CamperStateUI();
	Set_BasicUI();
	Set_ExitState();
	return NOERROR;
}

_int CUI_GameState::Update_GameObject(const _float & fTimeDelta)
{
	Update_Generator();
	Update_CamperState();
	return _int();
}

_int CUI_GameState::LastUpdate_GameObject(const _float & fTimeDelta)
{
	return _int();
}

HRESULT CUI_GameState::Set_CamperStateUI()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	_vec2 vPos = { 0.07f, 0.91f };

	for (int i = 0; i < m_iCamperNum; i++)
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_pCamperState[i])))
			return E_FAIL;
		m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Idle.tga"));
		m_pCamperState[i]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
		m_pCamperState[i]->Set_Scale(_vec2(0.6f, 0.6f));
		vPos.x += 0.055f;
	}
	Safe_Release(pManagement);
	
	return NOERROR;
}

HRESULT CUI_GameState::Set_ExitState()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	_vec2 vPos = { 0.09f, 0.808f };

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_pExitState[0])))
		return E_FAIL;
	m_pExitState[0]->Set_TexName(wstring(L"Hud_GeneratorIcon.tga"));
	m_pExitState[0]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_pExitState[0]->Set_Scale(_vec2(0.6f, 0.6f));

	vPos = { 0.0546f, 0.816f };

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_pExitState[1])))
		return E_FAIL;
	m_pExitState[1]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_pExitState[1]->Set_Font(L"5", _vec2(25.f,35.f), D3DXCOLOR(1.f,1.f,1.f,1.f));
	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CUI_GameState::Set_BasicUI()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	_vec2 vPos = { 0.15f, 0.86f };

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_BaseUI)))
		return E_FAIL;
	m_BaseUI->Set_TexName(wstring(L"Hud_Line.tga"));
	m_BaseUI->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_BaseUI->Set_Scale(_vec2(0.6f, 0.6f));

	Safe_Release(pManagement);
	return NOERROR;
}

void CUI_GameState::Update_Generator()
{
	_int LeaveGenerator = 5;
	for (size_t i = 0; i < 7; i++)
	{
		if (server_data.Game_Data.GRP[i] >= 80.f)
			LeaveGenerator--;
	}
	if (m_iLeaveGenerator == LeaveGenerator)
		return;
	GET_INSTANCE(CUIManager)->Set_GeneratorNum(LeaveGenerator);

	if (LeaveGenerator != 0)
	{
		_tchar Num[10] = L"";
		swprintf_s(Num, L"%d", 4);
		m_pExitState[1]->Set_Font(Num, _vec2(25.f, 35.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	}
	else
	{
		m_pExitState[0]->Set_Font(L"나가기", _vec2(25.f, 23.f), D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
		m_pExitState[1]->Set_Font(L"출구를 찾아", _vec2(10.f, 12.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		m_pExitState[0]->Set_Pos(_vec2(0.0743385f* g_iBackCX, 0.839946f* g_iBackCY));
		m_pExitState[1]->Set_Pos(_vec2(0.0698925f* g_iBackCX, 0.815174f* g_iBackCY));
		m_pExitState[1]->Set_Test();
	}

	m_iLeaveGenerator = LeaveGenerator;
}

void CUI_GameState::Update_CamperState()
{
	for (size_t i = 0; i < MAX_Client; i++)
	{
		if (!server_data.Campers[i].bConnect)
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_UnConnected.tga"));
		else if (server_data.Campers[i].iCondition == CCamper::HEALTHY)
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Idle.tga"));
		else if (server_data.Campers[i].iCondition == CCamper::INJURED)
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Hurted.tga"));
		else if(server_data.Campers[i].iCondition == CCamper::DYING)
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Dying.tga")); 
		else if (server_data.Campers[i].iCondition == CCamper::HOOKED)
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Hooked.tga"));
		else if (server_data.Campers[i].iCondition == CCamper::HOOKDEAD)
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Dead1.tga"));
		else if (server_data.Campers[i].iCondition == CCamper::BLOODDEAD)
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Dead2.tga"));
		else if (server_data.Campers[i].iCondition == CCamper::ESCAPE)
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Escape.tga"));
	}
}

CUI_GameState * CUI_GameState::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_GameState*	pInstance = new CUI_GameState(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_GameState Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_GameState::Clone_GameObject()
{
	CUI_GameState*	pInstance = new CUI_GameState(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_GameState Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CUI_GameState::Free()
{
	CGameObject::Free();
}
