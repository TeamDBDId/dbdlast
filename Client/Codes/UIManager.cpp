#include "stdafx.h"
#include "UIManager.h"

_USING(Client)
_IMPLEMENT_SINGLETON(CUIManager)

CUIManager::CUIManager()
{
}

void CUIManager::Add_Button(const wstring & TextureName, const wstring & TextName)
{
	if (m_pUIInteraction == nullptr)
		return;
	Add_Button(TextureName, TextName);
}

void CUIManager::Free()
{
	m_pUIInteraction = nullptr;
}
