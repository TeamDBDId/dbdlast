#include "stdafx.h"
#include "..\Headers\Slasher.h"
#include "Management.h"
#include "Light_Manager.h"
#include "Camera_Camper.h"
#include "Machete.h"
#include "CustomLight.h"
#include "Action_Drop.h"
#include "Action_Grab_Generic_Fast.h"
#include "Action_Grab_Obstacles.h"
#include "Action_HookIn.h"
#include "Action_PickUp.h"
#include "Action_Damage_Generator.h"
#include "Action_Destroy_Pallet.h"
#include "Action_Search_Locker.h"
#include "Action_Stun_Pallet.h"
#include "Action_StunDrop.h"
#include "Action_WindowVault.h"
#include "Action_Hit.h"
#include "Action_MoveLerp.h"
#include "Camper.h"
#include "Plank.h"

_USING(Client)

CSlasher::CSlasher(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CSlasher::CSlasher(const CSlasher & rhs)
	: CGameObject(rhs)
{
}

HRESULT CSlasher::Ready_Prototype()
{
	m_eObjectID = SLASHER;
	return NOERROR;
}

HRESULT CSlasher::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (FAILED(Ready_Action()))
		return E_FAIL;

	m_eCurState = AW::TW_Idle;
	m_pMeshCom->Set_AnimationSet(AW::TW_Idle);

	m_eObjectID = SLASHER;

	m_pTransformCom->SetUp_Speed(480.f, D3DXToRadian(360.0f));
	
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(3200.f, 0.f, 3200.f));
	//m_pTransformCom->Scaling(0.02f, 0.02f, 0.02f);

	if (FAILED(m_pGraphic_Device->CreateTexture(128, 128, 1, 0, D3DFMT_A32B32G32R32F, D3DPOOL_MANAGED, &m_pSkinTextures, nullptr)))
		return E_FAIL;

	m_pInput_Device = CInput_Device::GetInstance();
	m_pInput_Device->AddRef();
	m_bSkillUse = true;

	m_vecFPVAnim.reserve(16);
	m_vecFPVAnim.push_back(AW::TW_Attack_Bow_FPV);
	m_vecFPVAnim.push_back(AW::TW_Attack_In_FPV);
	m_vecFPVAnim.push_back(AW::TW_Attack_Miss_Out_FPV);
	m_vecFPVAnim.push_back(AW::TW_Attack_Swing_FPV);
	m_vecFPVAnim.push_back(AW::TW_Attack_Wipe_FPV);
	m_vecFPVAnim.push_back(AW::TW_Bell_In_FPV);
	m_vecFPVAnim.push_back(AW::TW_Bell_Loop_FPV);
	m_vecFPVAnim.push_back(AW::TW_Bell_Out_FPV);
	m_vecFPVAnim.push_back(AW::TW_Close_Hatch_FPV);
	m_vecFPVAnim.push_back(AW::TT_Carry_Attack_BOW_FPV);
	m_vecFPVAnim.push_back(AW::TT_Carry_Attack_IN_FPV);
	m_vecFPVAnim.push_back(AW::TT_Carry_Attack_Out_FPV);
	m_vecFPVAnim.push_back(AW::TT_Carry_Attack_Swing_FPV);
	m_vecFPVAnim.push_back(AW::TT_Grab_Generic_Fast_FPV);
	m_vecFPVAnim.push_back(AW::TT_Grab_Obstacles_BK_FPV);
	m_vecFPVAnim.push_back(AW::TT_Grab_Obstacles_FT_FPV);

	// Light
	m_pCustomLight = nullptr;

	if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomLight)))
		return E_FAIL;

	m_pCustomLight->Add_Light();
	m_pCustomLight->AddRef();

	m_pCustomLight->Set_Position(_vec3(3200.f, 300.f, 3200.f));
	m_pCustomLight->Set_Type(D3DLIGHT_SPOT);

	m_pCustomLight->Set_Diffuse(D3DXCOLOR(3.f, 0.2f, 0.2f, 1.f));
	//m_pCustomLight->Set_Ambient(D3DXCOLOR(1.f, 0.18f, 0.18f, 1.f));
	m_pCustomLight->Set_Range(1400.f);
	m_pCustomLight->Set_Theta(0.04f);
	m_pCustomLight->Set_Phi(0.1f);

	m_pCustomLight->Set_Direction(_vec3(0,1,0));
	m_pCustomLight->SetMovable(true);
	m_pCustomLight->SetRender(true);

	//AddShadowMap
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	m_pCustomLight->Add_ShadowCubeGroup(pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Map_Static").front());
	Safe_Release(pManagement);

	CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return E_FAIL;

	pLight_Manager->AddRef();

	pLight_Manager->Ready_ShadowMap(m_pCustomLight->GetLight());

	Safe_Release(pLight_Manager);

	Safe_Release(m_pInput_Device);
	if (5 == exPlayerNumber)
		m_pCustomLight->SetRender(false);

	return NOERROR;
}

_int CSlasher::Update_GameObject(const _float & fTimeDelta)
{
	Init_Camera();
	m_pMeshCom->Play_Animation(fTimeDelta);
	Key_Input(fTimeDelta);
	Test();
	State_Check();
	CommunicationWithServer();
	Compute_Map_Index();

	Cal_LightPos();
	Update_Actions(fTimeDelta);
	m_pTargetOfInteraction = nullptr;
	m_CollObjList.clear();
	return _int();
}

_int CSlasher::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CSlasher::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

  	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		if (!m_bRendHead && i == 0)
			continue;

		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, D3DLOCK_NOSYSLOCK | D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

_int CSlasher::Do_Coll(CGameObject * _pObj, const _vec3 & _vPos)
{
	if (0 == _pObj->GetID())
		return 0;

	m_CollObjList.push_back(_pObj);
	m_pTargetOfInteraction = _pObj;
	m_vTargetPos = _vPos;

	return _int();
}

void CSlasher::Set_WeaponColl(_bool bColl)
{
	if (nullptr == m_pWeapon)
		return;
	

	if (0 == slasher_data.iCharacter)
	{
		((CMachete*)m_pWeapon)->Set_CollState(bColl);
	}
}

void CSlasher::State_Check()
{
	if (m_eCurState == m_eOldState)
		return;

	m_pMeshCom->Set_AnimationSet(m_eCurState);
	
	m_eOldState = m_eCurState;
}

void CSlasher::Key_Input(const _float & fTimeDelta)
{
	if (exPlayerNumber != 5)
		return;

	Interact_Object();

	if (m_bIsLockKey)
		return;

	RotateY(fTimeDelta);

	if (KEYMGR->KeyPressing(DIK_W))
	{
		m_pTransformCom->Go_Straight(fTimeDelta);
		m_eCurState = AW::TW_RunFT;
	}
	if (KEYMGR->KeyPressing(DIK_S))
	{
		m_pTransformCom->BackWard(fTimeDelta);
		m_eCurState = AW::TW_RunBK;
	}
	if (KEYMGR->KeyPressing(DIK_A))
	{
		m_pTransformCom->Go_Left(fTimeDelta);
		m_eCurState = AW::TW_RunLT;
	}
	if (KEYMGR->KeyPressing(DIK_D))
	{
		m_pTransformCom->Go_Right(fTimeDelta);
		m_eCurState = AW::TW_RunRT;
	
	}

	if (!KEYMGR->KeyPressing(DIK_W) && !KEYMGR->KeyPressing(DIK_A)
		&& !KEYMGR->KeyPressing(DIK_S) && !KEYMGR->KeyPressing(DIK_D))
	{
		m_eCurState = AW::TW_Idle;
	}
		
	if (KEYMGR->KeyPressing(DIK_G))
		m_eCurState = AW::TW_Bell_Loop_FPV;

	if (m_bIsCarry)
		m_eCurState = AW::TT_Carry_Idle;
}

void CSlasher::CommunicationWithServer()
{
	if (!bGameServer)
		return;

	if (exPlayerNumber == 5)
	{
		//camper_data.Character =
		slasher_data.bConnect = true;

		slasher_data.vRight = *m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT);
		slasher_data.vUp = *m_pTransformCom->Get_StateInfo(CTransform::STATE_UP);
		slasher_data.vLook = *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
		slasher_data.vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

		_matrix matView;
		m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
		D3DXMatrixInverse(&matView, nullptr, &matView);

		memcpy(&slasher_data.vCamLook, &matView.m[2][0], sizeof(_vec3));

		if (Find_FPVAnim(m_eCurState))
			slasher_data.iState = ((_int)m_eCurState - 1);
		else
			slasher_data.iState = (_int)m_eCurState;

		if (server_data.Slasher.SecondInterationObject != 0
			|| server_data.Slasher.SecondInterationObjAnimation != 0)
		{
			slasher_data.SecondInterationObject = 0;
			slasher_data.SecondInterationObjAnimation = 0;
		}



	}
	else if(exPlayerNumber < 5)
	{
		_vec3 vRight = server_data.Slasher.vRight;
		_vec3 vUp = server_data.Slasher.vUp;
		_vec3 vLook = server_data.Slasher.vLook;
		_vec3 vPos = server_data.Slasher.vPos;

		m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

		m_eCurState = (AW::ANIMATION_SLASHER_WRAITH)server_data.Slasher.iState;
	}
}

void CSlasher::Compute_Map_Index()
{
	const _vec3* pPos = m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	_int IndexX = (_int)(pPos->x / _MAP_INTERVAL);
	_int IndexZ = (_int)(pPos->z / _MAP_INTERVAL);

	m_iMapIndex = (IndexX * 14) + IndexZ;
}

HRESULT CSlasher::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Fire_Noise");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Mesh
	m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Slasher_Wraith", (void*)CMesh_Dynamic::SLASHER);
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, 150.f, 150.f, 150.f);
	matLocalTransform._42 = 75.f;

	m_pColliderCom = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
		return E_FAIL;
	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_PLAYER, this);

	//m_pCamMatrix = m_pMeshCom->Find_Frame("jointCam_01");
	//m_pCamMatrix = m_pMeshCom->Find_Frame("joint_CamOff_01");
	m_pRHandMatrix = m_pMeshCom->Find_Frame("joint_WeaponRT_01");
	m_pLHandMatrix = m_pMeshCom->Find_Frame("joint_WeaponLT_01");
	m_pHeadMatrix = m_pMeshCom->Find_Frame("TW_Head03");
	

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CSlasher::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED * pMeshContainer, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);

	if (m_bSkillUse)
		m_pTextureCom->SetUp_OnShader(pEffect, "g_NoiseTexture");

	return NOERROR;
}

HRESULT CSlasher::Ready_Action()
{
	CAction* pAction = new CAction_Drop;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Drop", pAction));

	pAction = new CAction_Grab_Generic_Fast;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Grab_Generic_Fast", pAction));

	pAction = new CAction_Grab_Obstacles;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Grab_Obstacles", pAction));

	pAction = new CAction_HookIn;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HookIn", pAction));

	pAction = new CAction_PickUp;
	m_mapActions.insert(MAPACTION::value_type(L"Action_PickUp", pAction));

	pAction = new CAction_Damage_Generator;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Damage_Generator", pAction));

	pAction = new CAction_Destroy_Pallet;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Destroy_Pallet", pAction));

	pAction = new CAction_SearchLocker;
	m_mapActions.insert(MAPACTION::value_type(L"Action_SearchLocker", pAction));

	pAction = new CAction_Stun_Pallet;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Stun_Pallet", pAction));

	pAction = new CAction_StunDrop;
	m_mapActions.insert(MAPACTION::value_type(L"Action_StunDrop", pAction));

	pAction = new CAction_WindowVault;
	m_mapActions.insert(MAPACTION::value_type(L"Action_WindowVault", pAction));

	pAction = new CAction_MoveLerp;
	m_mapActions.insert(MAPACTION::value_type(L"Action_MoveLerp", pAction));

	pAction = new CAction_Hit;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Hit", pAction));

	return NOERROR;
}

void CSlasher::Init_Camera()
{
	if (!m_bLaitInit && exPlayerNumber == 5)
	{
		m_bLaitInit = true;

		CManagement* pManagement = CManagement::GetInstance();
		if (nullptr == pManagement)
			return;

		pManagement->AddRef();

		((CCamera_Camper*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Camera", 0))->Set_OrbitMatrix();

		Safe_Release(pManagement);
	}
}

void CSlasher::RotateY(const _float& fTimeDelta)
{
	_long	MouseMove = 0;

	if (MouseMove = m_pInput_Device->Get_DIMouseMove(CInput_Device::DIM_X))
	{
		m_pTransformCom->Rotation_Y(D3DXToRadian(MouseMove) * fTimeDelta);
	}
}

void CSlasher::Interact_Object()
{
	if (5 != exPlayerNumber)
		return;

	if (m_bIsLockKey)
		return;

	if (m_pTargetOfInteraction != nullptr)
	{
		if (m_pTargetOfInteraction->GetID() & PLANK)
		{
			if (m_pTargetOfInteraction->Get_ProgressTime() >= 0.01f &&
				m_pTargetOfInteraction->Get_ProgressTime() <= 1.488)
			{
				Set_Action(L"Action_Stun_Pallet", 10.f);
				return;
			}
		}
	}


	if (KEYMGR->MousePressing(0))
	{
		if (m_pTargetOfInteraction == nullptr)
		{
			Set_Action(L"Action_Hit", 10.f);
			return;
		}
		else if (m_pTargetOfInteraction != nullptr)
		{
			_uint eID = m_pTargetOfInteraction->GetID();
			if (eID & SLASHER || eID & CLOSET || eID & HATCH || eID & HOOK || eID & PLANK || eID & WINDOW || eID & CHEST || eID & EXITDOOR || eID & TOTEM)
			{
				Set_Action(L"Action_Hit", 10.f);
				return;
			}
			if (eID & GENERATOR)
			{
				if (m_bIsCarry)
					return;
				if (m_pTargetOfInteraction->Get_ProgressTime() < 0.01f)
					return;

				CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
				pAction_MoveLerp->SetPos(m_vTargetPos);
				pAction_MoveLerp->SetTargetOfInteraction(m_pTargetOfInteraction);
				Set_Action(L"Action_MoveLerp", 0.2f);
			}
			else if (eID & CAMPER)
			{
				CCamper* pCamper = (CCamper*)m_pTargetOfInteraction;
				CCamper::CONDITION ConditionOfCamper = pCamper->GetCurCondition();

				if (ConditionOfCamper == CCamper::SPECIAL)
				{
					if (m_bIsCarry)
						return;

					CAction_Grab_Generic_Fast* pAction_Grab_Generic_Fast = (CAction_Grab_Generic_Fast*)Find_Action(L"Action_Grab_Generic_Fast");
					pAction_Grab_Generic_Fast->SetCamper(m_pTargetOfInteraction);
					Set_Action(L"Action_Grab_Generic_Fast", 10.f);
					return;
				}
				else if (ConditionOfCamper == CCamper::OBSTACLE)
				{
					CCamper::CONDITION OldConditionOfCamper = pCamper->GetOldCondition();
					
					if (OldConditionOfCamper == CCamper::INJURED)
					{
						if (m_bIsCarry)
							return;

						CAction_Grab_Obstacles* pAction_Grab_Obstacles = (CAction_Grab_Obstacles*)Find_Action(L"Action_Grab_Obstacles");
						pAction_Grab_Obstacles->SetCamper(m_pTargetOfInteraction);
						Set_Action(L"Action_Grab_Obstacles", 10.f);
						return;
					}
					else
					{
						Set_Action(L"Action_Hit", 10.f);
						return;
					}
				}
				else
				{
					Set_Action(L"Action_Hit", 10.f);
					return;
				}
			}
		}
	}
	else if (KEYMGR->KeyPressing(DIK_SPACE))
	{
		if (m_pTargetOfInteraction == nullptr)
			return;
		else
		{
			_uint eID = m_pTargetOfInteraction->GetID();
			if (eID & SLASHER || eID & HATCH || eID & GENERATOR || eID & EXITDOOR || eID & CHEST || eID & TOTEM)
				return;
			else if (eID & CAMPER)
			{
				if (m_bIsCarry)
					return;

				CCamper* pCamper = (CCamper*)m_pTargetOfInteraction;
				CCamper::CONDITION CurConditionOfCamper = pCamper->GetCurCondition();
			
				if (CurConditionOfCamper != CCamper::DYING)
					return;
				else
				{
					CAction_PickUp* pAction_PickUp = (CAction_PickUp*)Find_Action(L"Action_PickUp");
					pAction_PickUp->SetCamper(m_pTargetOfInteraction);
					Set_Action(L"Action_PickUp", 10.f);
				}
			}
			else if (eID & CLOSET || eID & HOOK || eID & PLANK || eID & WINDOW)
			{
				if (eID & HOOK && !m_bIsCarry)
					return;

				if (eID & PLANK)
				{
					if(m_pTargetOfInteraction->Get_ProgressTime() < 1.488f)
						return;
				}

				CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
				pAction_MoveLerp->SetPos(m_vTargetPos);
				pAction_MoveLerp->SetTargetOfInteraction(m_pTargetOfInteraction);
				Set_Action(L"Action_MoveLerp", 0.2f);
			}
		}
	}
	else if (KEYMGR->KeyDown(DIK_R) && m_bIsCarry)
	{
		Set_Action(L"Action_Drop", 5.f);
		return;
	}
}

void CSlasher::Cal_LightPos()
{
	if (5 == exPlayerNumber)
		return;

	_matrix matWorldLight = *m_pHeadMatrix * m_pTransformCom->Get_Matrix();
	_vec3	vLightPos = *(_vec3*)&matWorldLight.m[3][0] + *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK) * 30.f;

	_vec3 vLightDir = _vec3(0.2f, -0.8f, 0.f);

	_vec3 vLookUp = _vec3(1, 0, 0);

	_vec3 vLook = *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
	vLook.y = 0.f;
	D3DXVec3Normalize(&vLook, &vLook);
	_float fAngle = acosf((vLook * vLookUp));
	_vec3	vLookUpY = vLookUp ^ vLook;

	if (vLookUpY.y < 0.f)
		fAngle = D3DXToRadian(360.f) - fAngle;

	_matrix matRot;
	D3DXMatrixRotationY(&matRot, fAngle);
	D3DXVec3TransformNormal(&vLightDir, &vLightDir, &matRot);

	D3DXVec3Normalize(&vLightDir, &vLightDir);
	m_pCustomLight->Set_Position(vLightPos);
	m_pCustomLight->Set_Direction(vLightDir);
}

_bool CSlasher::Find_FPVAnim(AW::ANIMATION_SLASHER_WRAITH Anim)
{
	for (auto& anim : m_vecFPVAnim)
	{
		if (anim == Anim)
			return true;
	}

	return false;
}

void CSlasher::Test()
{
	if (m_eCurState != m_eOldState)
	{
		//cout << (_uint)m_eCurState << endl;
	}
}

CSlasher * CSlasher::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CSlasher*	pInstance = new CSlasher(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CSlasher Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CSlasher::Clone_GameObject()
{
	CSlasher*	pInstance = new CSlasher(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CSlasher Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CSlasher::Free()
{
	m_vecFPVAnim.clear();

	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_PLAYER, this);

	Safe_Release(m_pCustomLight);
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pInput_Device);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pColliderCom);

	CGameObject::Free();
}