#include "stdafx.h"
#include "..\Headers\Action_SpiderReaction.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Input_Device.h"
#include "MeatHook.h"
#include "Action_BeingKilledBySpider.h"

_USING(Client)

CAction_SpiderReaction::CAction_SpiderReaction()
{
}

HRESULT CAction_SpiderReaction::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetCurCondition(CCamper::HOOKED);
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);
	m_fSpaceAcc = 0.f;
	m_fDelay = -1.f;
	CManagement* pManagement = CManagement::GetInstance();
	list<CGameObject*>* pObjList = pManagement->Get_ObjList(SCENE_STAGE, L"Layer_GameObject");

	CTransform* pCamperTransform = (CTransform*)pCamper->Get_ComponentPointer(L"Com_Transform");
	_vec3 vCamperPos = *pCamperTransform->Get_StateInfo(CTransform::STATE_POSITION);
	for (auto pHook : *pObjList)
	{
		if (pHook->GetID() & HOOK)
		{
			CTransform* pHookTransform = (CTransform*)pHook->Get_ComponentPointer(L"Com_Transform");
			_vec3 vHookPos = *pHookTransform->Get_StateInfo(CTransform::STATE_POSITION);

			_float fDist = D3DXVec3Length(&(vCamperPos - vHookPos));
			if (fDist <= 500.f)
			{
				m_pHook = (CMeatHook*)pHook;
				break;
			}
		}
	}

	CCamper::HOOKSTAGE HookStageOfCamper = pCamper->GetHookStage();

	if (HookStageOfCamper == CCamper::EscapeAttempts)
	{
		pCamper->SetEnergy(pCamper->GetHookedEnergy());
		pCamper->SetMaxEnergyAndMaxHeal();
		pCamper->Set_State(AC::TT_Hook_Out);
		m_iState = AC::TT_Hook_Out;
		m_pHook->SetState(CMeatHook::GetHookedOut);
	}
	else if (HookStageOfCamper == CCamper::Struggle)
	{
		pCamper->Set_State(AC::Spider_Reaction_In);
		m_iState = AC::Spider_Reaction_In;
		m_pHook->SetState(CMeatHook::SpiderReaction_IN);
	}

	return NOERROR;
}

_int CAction_SpiderReaction::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CCamper* pCamper = (CCamper*)m_pGameObject;

	pCamper->SetEnergy(pCamper->GetEnergy() - (fTimeDelta));

	CCamper::HOOKSTAGE HookStageOfCamper = pCamper->GetHookStage();
	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	if (HookStageOfCamper == CCamper::EscapeAttempts)
	{
		if (pCamper->GetEnergy() <= 60.f)
		{
			pCamper->SetHookStage(CCamper::Struggle);
			pCamper->Set_State(AC::Spider_Reaction_In);
			m_iState = AC::Spider_Reaction_In;
			m_pHook->SetState(CMeatHook::SpiderReaction_IN);
		}

		if (m_iState == AC::TT_Hook_Out && pMeshCom->IsOverTime(0.3f))
		{
			pCamper->Set_State(AC::HookedIdle);
			m_iState = AC::HookedIdle;
		}
		else if (m_iState == AC::TT_Hook_Out && KEYMGR->MousePressing(0))
			m_iState = AC::HookedIdle;
		
		if (m_iState == AC::HookedIdle)
		{
			if (m_fDelay >= 0.f)
				m_fDelay -= fTimeDelta;
			else
			{
				if (KEYMGR->MousePressing(CInput_Device::DIM_LBUTTON))
				{
					pCamper->Set_State(AC::HookedStruggle);
					m_pHook->SetState(CMeatHook::Struggle);
					m_fSpaceAcc += fTimeDelta;
				}
				else
				{
					pCamper->Set_State(AC::HookedIdle);
					m_pHook->SetState(CMeatHook::Idle);
					m_fSpaceAcc = 0.f;
				}
			}
		}
		
		if (m_fSpaceAcc >= 1.5f)
		{
			int iRand = rand() % 100;
			if (iRand > 95)
			{
				m_pHook->SetState(CMeatHook::HookedFree);
				pCamper->SetCurCondition(CCamper::INJURED);
				m_pGameObject->Set_Action(L"Action_HookFree", 5.f);
				return END_ACTION;
			}
			else
			{
				pCamper->SetEnergy(pCamper->GetEnergy() - 20.f);
				pCamper->Set_State(AC::HookedIdle);
				m_fSpaceAcc = 0.f;
				m_fDelay = 1.f;
			}
		}

	}
	else if (HookStageOfCamper == CCamper::Struggle)
	{
		if (m_iState == AC::Spider_Reaction_Out && pMeshCom->IsOverTime(0.3f))
		{
			pCamper->Set_State(AC::Spider_Struggle);
			m_iState = AC::Spider_Struggle;
			m_pHook->SetState(CMeatHook::Spider_Struggle);
		}
		else if (m_iState == AC::Spider_Reaction_Loop && pMeshCom->IsOverTime(0.3f))
		{
			pCamper->Set_State(AC::Spider_Reaction_Out);
			m_iState = AC::Spider_Reaction_Out;
			m_pHook->SetState(CMeatHook::SpiderReaction_OUT);
		}
		else if (m_iState == AC::Spider_Reaction_In && pMeshCom->IsOverTime(0.3f))
		{
			pCamper->Set_State(AC::Spider_Reaction_Loop);
			m_iState = AC::Spider_Reaction_Loop;
			m_pHook->SetState(CMeatHook::SpiderReaction_Loop);
		}

		if (m_iState == AC::Spider_Struggle)
		{
			m_fSpaceAcc += fTimeDelta;

			if (pCamper->GetEnergy() <= 0.f)
				pCamper->SetHookStage(CCamper::Sacrifice);

			if (KEYMGR->KeyDown(DIK_SPACE))
				m_fSpaceAcc = 0.f;

			if (m_fSpaceAcc > 1.f)
			{
				pCamper->Set_State(AC::Spider_StruggleToSacrifice);
				pCamper->SetHookStage(CCamper::Sacrifice);
				m_pHook->SetState(CMeatHook::SpiderStruggle2Sacrifice);
			}
		}
	}
	else
	{
		CAction_BeingKilledBySpider* pAction = (CAction_BeingKilledBySpider*)m_pGameObject->Find_Action(L"Action_BeingKilledBySpider");
		pAction->SetHook(m_pHook);
		m_pGameObject->Set_Action(L"Action_BeingKilledBySpider", 400.f);
		return END_ACTION;
	}


	return UPDATE_ACTION;
}

void CAction_SpiderReaction::End_Action()
{
	m_bIsPlaying = false;
	m_pHook = nullptr;
}

void CAction_SpiderReaction::Send_ServerData()
{
}

void CAction_SpiderReaction::Free()
{
	CAction::Free();
}
