#include "stdafx.h"
#include "..\Headers\Action_Grab_Obstacles.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Slasher.h"

_USING(Client)

CAction_Grab_Obstacles::CAction_Grab_Obstacles()
{
}

HRESULT CAction_Grab_Obstacles::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	if (!m_bIsInit)
		SetVectorPos();

	m_bIsPlaying = true;
	m_fIndex = 0.f;
	if (m_pGameObject->GetID() & SLASHER)
	{
		CTransform* pSlasherTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
		_vec3 vSlasherLook = *pSlasherTransform->Get_StateInfo(CTransform::STATE_LOOK);

		CTransform* pCamperTransform = (CTransform*)m_pCamper->Get_ComponentPointer(L"Com_Transform");
		_vec3 vCamperLook = *pCamperTransform->Get_StateInfo(CTransform::STATE_LOOK);

		_float fAngle = D3DXToDegree(acosf(D3DXVec3Dot(&vSlasherLook, &vCamperLook)));
		m_pCamper->SetColl(false);
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->IsLockKey(true);
		if (fAngle >= 90.f)
		{
			pSlasher->Set_State(AW::TT_Grab_Obstacles_BK_FPV);
			m_iState = AW::TT_Grab_Obstacles_BK_FPV;
		}
		else
		{
			pSlasher->Set_State(AW::TT_Grab_Obstacles_FT_FPV);
			m_iState = AW::TT_Grab_Obstacles_FT_FPV;
		}
	}
	else
	{
		CManagement* pManagement = CManagement::GetInstance();
		CTransform* pSlasherTransform = (CTransform*)pManagement->Get_ComponentPointer(SCENE_STAGE, L"Layer_Slasher", L"Com_Transform", 0);
		_vec3 vSlasherPos = *pSlasherTransform->Get_StateInfo(CTransform::STATE_POSITION);

		CTransform* pCamperTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
		_vec3 vCamperPos = *pCamperTransform->Get_StateInfo(CTransform::STATE_POSITION);

		_vec3 vDir = vSlasherPos - vCamperPos;
		D3DXVec3Normalize(&vDir, &vDir);
		_vec3 vCamperLook = *pCamperTransform->Get_StateInfo(CTransform::STATE_LOOK);

		_float fAngle = D3DXToDegree(acosf(D3DXVec3Dot(&vDir, &vCamperLook)));
		
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->SetColl(false);
		pCamper->IsLockKey(true);
		if (fAngle >= 90.f)
		{
			pCamper->Set_State(AC::TT_Grab_Obstacles_BK);
			m_iState = AC::TT_Grab_Obstacles_BK;
		}
		else
		{
			pCamper->Set_State(AC::TT_Grab_Obstacles_FT);
			m_iState = AC::TT_Grab_Obstacles_FT;
		}
		m_vOriginPos = vCamperPos;
	}
	Send_ServerData();
	return NOERROR;
}

_int CAction_Grab_Obstacles::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay > 0.0f)
		m_fDelay -= fTimeDelta;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;
	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	size_t iIndex = (size_t)m_fIndex;
	if (m_iState == AC::TT_Grab_Obstacles_FT)
	{
		if (pMeshCom->IsOverTime(0.3f))
		{
			CCamper* pCamper = (CCamper*)m_pGameObject;
			pCamper->Set_State(AC::TT_Carry_Idle);
		}

		if (m_fIndex >= 81.f)
			return END_ACTION;

		if (m_vecPosFT.size() <= iIndex)
			return END_ACTION;

		_vec3 vLocalPos = m_vecPosFT[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	}
	else if (m_iState == AC::TT_Grab_Obstacles_BK)
	{
		if (pMeshCom->IsOverTime(0.3f))
		{
			CCamper* pCamper = (CCamper*)m_pGameObject;
			pCamper->Set_State(AC::TT_Carry_Idle);
		}

		if (m_fIndex >= 81.f)
			return END_ACTION;

		if (m_vecPosBK.size() <= iIndex)
			return END_ACTION;

		_vec3 vLocalPos = m_vecPosBK[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	}
	else
	{
		if (pMeshCom->IsOverTime(0.3f))
		{
			CSlasher* pSlasher = (CSlasher*)m_pGameObject;
			pSlasher->Set_State(AW::TT_Carry_Idle);
			return END_ACTION;
		}
	}


	return UPDATE_ACTION;
}

void CAction_Grab_Obstacles::End_Action()
{
	m_fDelay = 1.f;
	m_bIsPlaying = false;

	if (m_pGameObject->GetID() & SLASHER)
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->Set_Carry(true);
		pSlasher->Set_CarriedCamper(m_pCamper);
		pSlasher->IsLockKey(false);
	}
	else
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->IsLockKey(false);
		pCamper->SetColl(false);
		pCamper->SetCarry(true);
	}

	m_pCamper = nullptr;
}

void CAction_Grab_Obstacles::Send_ServerData()
{
	if (m_pGameObject->GetID() & SLASHER)
	{
		slasher_data.SecondInterationObject = m_pCamper->GetID();
		slasher_data.SecondInterationObjAnimation = GRAB_OBSTACLES;
	}
}

void CAction_Grab_Obstacles::SetVectorPos()
{
	m_vecPosBK.reserve(81);

	m_vecPosBK.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.036f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.051f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.062f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.07f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.08f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.094f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.114f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.144f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.32f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.443f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.564f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.71f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.872f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -1.049f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -1.243f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -1.44f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -1.691f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -2.164f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -6.696f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -16.269f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -29.003f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -43.018f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -56.435f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -67.374f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -75.647f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -81.526f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.827f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -85.256f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -88.573f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -92.743f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -96.314f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -98.804f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.63f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.934f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.977f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.976f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.974f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.972f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.969f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.967f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.964f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.961f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.958f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.955f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.952f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.948f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.945f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.942f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.938f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.935f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.931f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.928f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.924f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.921f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.918f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.915f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.912f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.909f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.906f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.904f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.901f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.899f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.897f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.895f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.894f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.893f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.892f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.891f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.891f));

	m_vecPosFT.reserve(86);

	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.802f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 3.091f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 6.692f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 11.43f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 17.131f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 23.619f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 30.719f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 38.256f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 46.056f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 53.944f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 61.744f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 69.281f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 76.381f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 82.869f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 88.57f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 93.308f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 96.909f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 99.198f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));

	m_bIsInit = true;
}

void CAction_Grab_Obstacles::Free()
{
	CAction::Free();
}
