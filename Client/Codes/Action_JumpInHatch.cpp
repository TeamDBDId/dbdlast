#include "stdafx.h"
#include "..\Headers\Action_JumpInHatch.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"

_USING(Client)

CAction_JumpInHatch::CAction_JumpInHatch()
{
}

HRESULT CAction_JumpInHatch::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	m_bIsPlaying = true;

	pCamper->Set_State(AC::JumpInHatch);
	m_iState = AC::JumpInHatch;

	return NOERROR;
}

_int CAction_JumpInHatch::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (m_iState == AC::JumpInHatch && pMeshCom->IsOverTime(0.01f))
	{

	}

	return UPDATE_ACTION;
}

void CAction_JumpInHatch::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetCurCondition(pCamper->GetOldCondition());
	pCamper->IsLockKey(false);
}

void CAction_JumpInHatch::Send_ServerData()
{
}

void CAction_JumpInHatch::Free()
{
	CAction::Free();
}
