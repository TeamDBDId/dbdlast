#include "stdafx.h"
#include "..\Headers\Action_Damage_Generator.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Slasher.h"
#include "Generator.h"

_USING(Client)

CAction_Damage_Generator::CAction_Damage_Generator()
{
}

HRESULT CAction_Damage_Generator::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	m_fIndex = 0.f;

	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	m_bIsPlaying = true;
	pSlasher->IsLockKey(true);

	pSlasher->Set_State(AW::TW_Damage_Generator);

	return NOERROR;
}

_int CAction_Damage_Generator::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;

	_int iIndex = (int)m_fIndex;
	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (pMeshCom->IsOverTime(0.3f))
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->Set_State(AW::TW_Idle);
		
	}

	if (m_fIndex > 81.f)
		return END_ACTION;

	Send_ServerData();
	return UPDATE_ACTION;
}

void CAction_Damage_Generator::End_Action()
{
	m_bIsPlaying = false;
	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	pSlasher->IsLockKey(false);

	m_pGenerator = nullptr;
	Send_ServerData();
}

void CAction_Damage_Generator::Send_ServerData()
{
	if (m_pGenerator != nullptr)
		slasher_data.InterationObject = m_pGenerator->GetID();
	else
		slasher_data.InterationObject = 0;
}

void CAction_Damage_Generator::Free()
{
	CAction::Free();
}
