#include "stdafx.h"
#include "..\Headers\Action_Grab_Generic_Fast.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Slasher.h"

_USING(Client)

CAction_Grab_Generic_Fast::CAction_Grab_Generic_Fast()
{
}

HRESULT CAction_Grab_Generic_Fast::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	if (!m_bIsInit)
		SetVectorPos();

	m_bIsPlaying = true;
	m_fIndex = 0.f;

	if (m_pGameObject->GetID() & SLASHER)
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->IsLockKey(true);
		m_pCamper->SetColl(false);
		pSlasher->Set_State(AW::TT_Grab_Generic_Fast_FPV);
		m_iState = AW::TT_Grab_Generic_Fast_FPV;
	}
	else
	{
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
		m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);

		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->IsLockKey(true);
		pCamper->Set_State(AC::TT_Grab_Generic_Fast);
		m_iState = AC::TT_Grab_Generic_Fast;
		pCamper->SetColl(false);
	}

	Send_ServerData();
	return NOERROR;
}

_int CAction_Grab_Generic_Fast::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay > 0.0f)
		m_fDelay -= fTimeDelta;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	m_fIndex += fTimeDelta * 30.f;
	size_t iIndex = (size_t)m_fIndex;

	if (m_iState == AC::TT_Grab_Generic_Fast)
	{
		if (pMeshCom->IsOverTime(0.3f))
		{
			CCamper* pCamper = (CCamper*)m_pGameObject;
			pCamper->Set_State(AC::TT_Carry_Idle);
		}

		if (m_fIndex >= 81.f)
			return END_ACTION;

		if (m_vecCamperPos.size() <= iIndex)
			return END_ACTION;

		_vec3 vLocalPos = m_vecCamperPos[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	}
	else
	{
		if (pMeshCom->IsOverTime(0.3f))
		{
			CSlasher* pSlasher = (CSlasher*)m_pGameObject;
			pSlasher->Set_State(AW::TT_Carry_Idle);
			return END_ACTION;
		}
	}

	return UPDATE_ACTION;
}

void CAction_Grab_Generic_Fast::End_Action()
{
	m_bIsPlaying = false;
	m_fDelay = 1.f;
	if (m_pGameObject->GetID() & SLASHER)
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->Set_Carry(true);
		pSlasher->Set_CarriedCamper(m_pCamper);
		pSlasher->IsLockKey(false);
	}
	else
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->IsLockKey(false);
		pCamper->SetCarry(true);
	}

	m_pCamper = nullptr;
}

void CAction_Grab_Generic_Fast::Send_ServerData()
{
	if (m_pGameObject->GetID() & SLASHER)
	{
		slasher_data.SecondInterationObject = m_pCamper->GetID();
		slasher_data.SecondInterationObjAnimation = GRAB_GENERIC_FAST;
	}
}

void CAction_Grab_Generic_Fast::SetVectorPos()
{
	m_vecCamperPos.reserve(81);

	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.036f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.051f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.062f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.07f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.08f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.094f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.114f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.144f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.32f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.443f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.563f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.71f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.872f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.049f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.243f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.44f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.691f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.164f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -5.233f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -11.108f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -18.943f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -27.892f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -37.109f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -45.75f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -52.969f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -57.921f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -59.757f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));
	m_vecCamperPos.push_back(_vec3(-0.01f, 0.f, -59.669f));

	m_bIsInit = true;
}

void CAction_Grab_Generic_Fast::Free()
{
	CAction::Free();
}
