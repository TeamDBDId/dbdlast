#include "stdafx.h"
#include "..\Headers\Scene_Lobby.h"
#include "Management.h"
#include "Scene_Stage.h"
#include "Camper.h"
#include "UI_RoleSelection.h"
#include "UI_OverlayMenu.h"
_USING(Client)

CScene_Lobby::CScene_Lobby(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CScene(pGraphic_Device)
{
	lserver_data.GameStart = false;
	lserver_data.AllReady = false;
}

HRESULT CScene_Lobby::Ready_Scene()
{
	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;

	bLobbyServer = true;

	Lobbysock = CServerManager::GetInstance()->Ready_RobbyServer();

	hThread = (HANDLE)_beginthreadex(nullptr, 0, Lobby_Thread, 0, 0, nullptr);

	return NOERROR;
}

_int CScene_Lobby::Update_Scene(const _float & fTimeDelta)
{
	LobbyState_Check();

	return CScene::Update_Scene(fTimeDelta);
}

_int CScene_Lobby::LastUpdate_Scene(const _float & fTimeDelta)
{
	if (lserver_data.GameStart && lserver_data.WaittingTime == 0)
	{
 		exPlayerNumber = lserver_data.Number[client_data.client_imei];
		bUseServer = true;
		bLobbyServer = false;
		if (5 > lserver_data.playerdat[client_data.client_imei].Number)
		{
			camper_data.Character = lserver_data.playerdat[client_data.client_imei].character;
			//camper_data.Number = lserver_data.playerdat[client_data.client_imei].Number;
			//camper_data.iHP = 100;
			//camper_data.iState = 
		}
		else
		{
			slasher_data.iCharacter = lserver_data.playerdat[client_data.client_imei].character;
		}

		GET_INSTANCE_MANAGEMENTR(0);

		CScene_Stage*	pNewScene = CScene_Stage::Create(m_pGraphic_Device);
		if (nullptr == pNewScene)
			return - 1;

		if (FAILED(pManagement->SetUp_ScenePointer(pNewScene)))
			return -1;

		Safe_Release(pNewScene);
		Safe_Release(pManagement);

		return 0;
	}


	return CScene::LastUpdate_Scene(fTimeDelta);
}

void CScene_Lobby::Render_Scene()
{
	
}

size_t CScene_Lobby::Lobby_Thread(LPVOID lpParam)
{
	QueryPerformanceFrequency(&tickPerSecond);
	QueryPerformanceCounter(&startTick);
	while (bLobbyServer) {
		QueryPerformanceCounter(&endTick);
		dTime = (double)(endTick.QuadPart - startTick.QuadPart) / tickPerSecond.QuadPart;

		while (dTime >= 0.002000) {
			QueryPerformanceCounter(&startTick);
			dTime = 0;
			lobby_data.bConnect = true;
			
			CServerManager::GetInstance()->Data_ExchangeLobby(Lobbysock, lobby_data, client_data, lserver_data);
			sendPacket++;
		}
	}

	return 0;
}

HRESULT CScene_Lobby::Ready_Prototype_GameObject()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Lobby", SCENE_LOBBY, L"Layer_UI", &m_pCurUI)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Lobby::Ready_Prototype_Component()
{
	return NOERROR;
}

void CScene_Lobby::Update_Server()
{
}


CScene_Lobby * CScene_Lobby::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CScene_Lobby*	pInstance = new CScene_Lobby(pGraphic_Device);

	if (FAILED(pInstance->Ready_Scene()))
	{
		MessageBox(0, L"CScene_Lobby Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CScene_Lobby::Free()
{
	GET_INSTANCE_MANAGEMENT
	pManagement->Clear_Layers(SCENE_LOBBY);
	Safe_Release(pManagement);
	ZeroMemory(&lobby_data, sizeof(LobbyData));
	closesocket(Lobbysock);
	//_endthreadex(0);
	CloseHandle(hThread);

	CScene::Free();
}


void CScene_Lobby::LobbyState_Check()
{
	if (m_eCurLobbyState == RoleSelection)
	{
		_uint iClickState = ((CUI_RoleSelection*)m_pCurUI)->Get_ClickButton();
		if (iClickState == CUI_RoleSelection::NONE)
			return;
		m_pCurUI->SetDead();
		GET_INSTANCE_MANAGEMENT;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_OverlayMenu", SCENE_LOBBY, L"Layer_UI_Lobby", &m_pCurUI);
		m_eCurLobbyState = OverlayMenu;
		if (iClickState == CUI_RoleSelection::CAMPER)
			((CUI_OverlayMenu*)m_pCurUI)->Set_Job(false);
		else if(iClickState == CUI_RoleSelection::SLASHER)
			((CUI_OverlayMenu*)m_pCurUI)->Set_Job(true);

		Safe_Release(pManagement);
	}
	else if (m_eCurLobbyState == OverlayMenu)
	{
		_uint iClickState = ((CUI_OverlayMenu*)m_pCurUI)->Get_State();
		if (iClickState == CUI_OverlayMenu::NONE)
			return;

		if (iClickState == CUI_OverlayMenu::Go_Back)
		{
			m_pCurUI->SetDead();
			GET_INSTANCE_MANAGEMENT;
			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Lobby", SCENE_LOBBY, L"Layer_UI_Lobby", &m_pCurUI);
			m_eCurLobbyState = RoleSelection;
			Safe_Release(pManagement);
		}
	}
}

