#include "stdafx.h"
#include "..\Headers\Action_PickItemOnGround.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"

_USING(Client)

CAction_PickItemOnGround::CAction_PickItemOnGround()
{
}

HRESULT CAction_PickItemOnGround::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;

	pCamper->Set_State(AC::PickItemOnGround);
	m_iState = AC::PickItemOnGround;

	return NOERROR;
}

_int CAction_PickItemOnGround::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (m_iState == AC::PickItemOnGround && pMeshCom->IsOverTime(0.01f))
	{

	}

	return UPDATE_ACTION;
}

void CAction_PickItemOnGround::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);
}

void CAction_PickItemOnGround::Send_ServerData()
{
}

void CAction_PickItemOnGround::Free()
{
	CAction::Free();
}
