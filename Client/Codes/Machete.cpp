#include "stdafx.h"
#include "..\Headers\Machete.h"
#include "Management.h"
#include "Defines.h"
#include "Slasher.h"

_USING(Client)

CMachete::CMachete(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CMachete::CMachete(const CMachete & rhs)
	: CGameObject(rhs)
{
}

HRESULT CMachete::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CMachete::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	D3DXMatrixIdentity(&m_matParent);
	m_pTransformCom->Set_Parent(&m_matParent);
	Approach_Slasher();
	
	_float fX = D3DXToRadian(90.f);
	_float fY = D3DXToRadian(0.f);
	_float fZ = D3DXToRadian(0.f);

	m_pTransformCom->SetUp_RotateXYZ(&fX, &fY, &fZ);
	return NOERROR;

	m_isColl = false;
}

_int CMachete::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;

	return _int();
}

_int CMachete::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	if (nullptr != m_pRHandMatrix)
		m_matParent = *m_pRHandMatrix * m_pSlasherTransform->Get_Matrix();
	
		

	_vec3 vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	D3DXVec3TransformCoord(&vPos, &vPos, &m_matParent);

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CMachete::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(9);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	m_pColliderCom->Render_Collider();

	Safe_Release(pEffect);

}

void CMachete::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	pEffect->SetVector("g_vLightPos", &vLightPos);
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matW");
	pEffect->SetMatrix("g_matVP", VP);

	pEffect->CommitChanges();
	for (size_t i = 0; i < dwNumMaterials; i++)
	{

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();
}

_int CMachete::Do_Coll(CGameObject * _pObj, const _vec3 & _vPos)
{
	if (5 == exPlayerNumber)
	{
		if (_pObj->GetID() & CAMPER)
		{
			m_isColl = false;
			slasher_data.SecondInterationObject = _pObj->GetID();
			slasher_data.SecondInterationObjAnimation = PLAYERATTACK;
		}
	}

	return _int();
}

HRESULT CMachete::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	// For.Com_Mesh
	m_pMeshCom = (CMesh_Static*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Weapon_Machete");
	if (FAILED(Add_Component(L"Com_Mesh", m_pFrustumCom)))
		return E_FAIL;


	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, 100.f, 100.f, 100.f);
	matLocalTransform.m[3][2] = 40.f;

	m_pColliderCom = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, &m_matParent));
	if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
		return E_FAIL;

	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_WEAPON, this);

	if (5 == exPlayerNumber)
	{
		CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
		pSlasher->Set_Weapon(this);
	}
	m_isColl = false;
	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CMachete::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetTexture("g_MetalicTexture", pSubSet->MeshTexture.pMetallicTexture);
	pEffect->SetTexture("g_RoughnessTexture", pSubSet->MeshTexture.pRoughnessTexture);

	return NOERROR;
}

void CMachete::Approach_Slasher()
{
	CManagement* pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return;

	pManagement->AddRef();

	CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
	m_pRHandMatrix = pSlasher->Get_RHandMatrix();
	m_pSlasherTransform = (CTransform*)pSlasher->Get_ComponentPointer(L"Com_Transform");
	
	Safe_Release(pManagement);
}

CMachete * CMachete::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CMachete*	pInstance = new CMachete(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CMachete Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CMachete::Clone_GameObject()
{
	CMachete*	pInstance = new CMachete(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CArt_Static Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CMachete::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_WEAPON, this);

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
