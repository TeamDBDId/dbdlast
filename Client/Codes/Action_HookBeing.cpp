#include "stdafx.h"
#include "..\Headers\Action_HookBeing.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"

_USING(Client)

CAction_HookBeing::CAction_HookBeing()
{
}

HRESULT CAction_HookBeing::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;

	if (pCamper->GetCurCondition() != CCamper::HOOKED)
	{
		pCamper->Set_State(AC::HookBeingCamperIn);
		m_iState = AC::HookBeingCamperIn;
		pCamper->SetOldCondition(pCamper->GetCurCondition());
		pCamper->SetCurCondition(CCamper::SPECIAL);
		Send_ServerData();
	}
	else
	{
		pCamper->Set_State(AC::HookBeingRescuedIn);
		m_iState = AC::HookBeingRescuedIn;
	}

	return NOERROR;
}

_int CAction_HookBeing::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (m_iState == AC::HookBeingCamperIn)
	{
		if (pMeshCom->IsOverTime(0.3f))
		{
			((CCamper*)m_pGameObject)->Set_State(AC::HookBeingCamperEnd);
			m_iState = AC::HookBeingCamperEnd;
		}
	}
	else if (m_iState == AC::HookBeingCamperEnd)
	{
		if (pMeshCom->IsOverTime(0.3f))
		{
			((CCamper*)m_pGameObject)->Set_State(AC::Idle);
			return END_ACTION;
		}
	}

	if (m_iState == AC::HookBeingRescuedIn)
	{
		if (pMeshCom->IsOverTime(0.3f))
		{
			((CCamper*)m_pGameObject)->Set_State(AC::HookBeingRescuredEnd);
			m_iState = AC::HookBeingRescuredEnd;
		}
	}
	else if (m_iState == AC::HookBeingRescuredEnd)
	{
		if (pMeshCom->IsOverTime(0.3f))
		{
			((CCamper*)m_pGameObject)->Set_State(AC::Idle);
			return END_ACTION;
		}
	}

	return UPDATE_ACTION;
}

void CAction_HookBeing::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);

	if (m_iState == AC::HookBeingCamperEnd)
	{
		pCamper->SetCurCondition(pCamper->GetOldCondition());
	}
	else if (m_iState == AC::HookBeingRescuredEnd)
	{
		pCamper->SetCurCondition(CCamper::INJURED);
		pCamper->SetHookedEnergy();

		if (pCamper->GetEnergy() >= 60.f)
		{
			pCamper->SetHookStage(CCamper::Struggle);
		}
		else
			pCamper->SetHookStage(CCamper::Sacrifice);
	}
	else if (m_iState == AC::HookBeingRescuedIn)
	{
		m_pGameObject->Set_Action(L"Action_Spider_Reaction", 120.f);
	}


}

void CAction_HookBeing::Send_ServerData()
{
	camper_data.SecondInterationObject = m_pCamper->GetID();
	camper_data.SecondInterationObjAnimation = HOOK_BEING;
}

void CAction_HookBeing::Free()
{
	CAction::Free();
}
