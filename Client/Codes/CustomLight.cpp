#include "stdafx.h"
#include "..\Headers\CustomLight.h"
#include "Management.h"
#include "Light_Manager.h"

_USING(Client)

CCustomLight::CCustomLight(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CCustomLight::CCustomLight(const CCustomLight & rhs)
	: CGameObject(rhs)
	, m_LightInfo(rhs.m_LightInfo)
{
	
}

void CCustomLight::Render_GameObject()
{
	//if (nullptr == m_pLightCone)
	//	return;

	//LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	//if (nullptr == pEffect)
	//	return;

	//pEffect->AddRef();

	//if (FAILED(SetUp_ConstantTable(pEffect)))
	//	return;

	//pEffect->Begin(nullptr, 0);
	//pEffect->BeginPass(0);

	//m_pLightCone->Render_Mesh(0);

	//pEffect->EndPass();
	//pEffect->End();

	//Safe_Release(pEffect);
}

//void CCustomLight::Cal_CamperShadow(CCollider* pCollider)
//{
//	if (m_pLight->Get_LightInfo()->Type != D3DLIGHT_SPOT)
//		return;
//
//	CManagement* pManagement = CManagement::GetInstance();
//	if (nullptr == pManagement)
//		return;
//
//	pManagement->AddRef();
//
//	CCollider* pCircleCollider = nullptr;
//	_uint CollCheck = 0;
//	_float vRad = 0.f;
//	for (auto& pCamper : *pManagement->Get_ObjList(SCENE_STAGE, L"Layer_Camper"))
//	{
//		pCircleCollider = (CCollider*)pCamper->Get_ComponentPointer(L"Com_Collider");
//		if (pCollider->Collision_Sphere(pCircleCollider))
//		{
//			++CollCheck;
//			m_pLight->AddDynamicShadowList(pCamper);
//		}
//	}
//	if (0 == CollCheck)
//		m_pLight->UseDynamicShadow(false);
//	else
//		m_pLight->UseDynamicShadow(true);
//
//	Safe_Release(pManagement);
//}

void CCustomLight::Add_Light()
{
	CLight_Manager*	pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return;

	pLight_Manager->AddRef();

	m_pLight = pLight_Manager->Add_LightInfo2(m_pGraphic_Device, m_LightInfo);
	if (nullptr == m_pLight)
	{
		Safe_Release(pLight_Manager);
		return;
	}
	m_pLight->AddRef();

	Safe_Release(pLight_Manager);
}

_int CCustomLight::Update_GameObject(const _float & fTimeDelta)
{
	if (m_bMovable)
	{
		Set_Position(*m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		Set_Direction(*m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK));
	}
	return _int();
}

_int CCustomLight::LastUpdate_GameObject(const _float & fTimeDelta)
{
	//if (nullptr == m_pRendererCom)
	//	return -1;

	//if (FAILED(m_pRendererCom->Add_StemGroup(this)))
	//	return -1;

	return _int();
}

HRESULT CCustomLight::Ready_Light(LPDIRECT3DDEVICE9 pGraphic_Device, _vec3 vPosition, D3DLIGHTTYPE ltype)
{
	D3DLIGHT9 LightInfo;

	ZeroMemory(&LightInfo, sizeof(D3DLIGHT9));

	LightInfo.Type = ltype;
	LightInfo.Diffuse = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);
	LightInfo.Specular = LightInfo.Diffuse;
	LightInfo.Ambient = D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.f);

	LightInfo.Position = _vec3(0.f, -1.f, 0.f);
	LightInfo.Range = 1000.0f;
	LightInfo.Direction = _vec3(0.f,-1.f,0.f);
	//LightInfo.Attenuation0 = 0.01f;
	//LightInfo.Attenuation1 = 0.01f;
	//LightInfo.Attenuation2 = 0.01f;

	//LightInfo.Falloff = 1.f;
	LightInfo.Theta = D3DXToRadian(12.5f);
	LightInfo.Phi = D3DXToRadian(17.5f);

	m_LightInfo = LightInfo;

	return NOERROR;
}

HRESULT CCustomLight::Ready_GameObject()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	//Map_Static_LightCone
	m_pLightCone = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Map_Static_LightCone");
	if (FAILED(Add_Component(L"Com_Mesh", m_pLightCone)))
		return E_FAIL;
	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_LightCircle");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Texture
	m_pLightTexture = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Texture_FlashLight");
	if (FAILED(Add_Component(L"Com_Texture0", m_pLightTexture)))
		return E_FAIL;

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, (_vec3*)&m_LightInfo.Position);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &_vec3(0.f, -1.f, 0.f));

	return NOERROR;
}

HRESULT CCustomLight::SetUp_ConstantTable(LPD3DXEFFECT pEffect)
{
	_matrix	 matWVP, matWorld, matView, matProj;

	_matrix matOne;
	_float fScale = m_pLight->Get_LightInfo()->Range * 0.01f;
	D3DXMatrixScaling(&matOne, fScale * 0.5f, fScale, fScale * 0.5f);
	matWorld = m_pTransformCom->Get_Matrix();
	memcpy(&matOne.m[3], &matWorld.m[3], sizeof(_vec3));
	m_pTransformCom->Set_Matrix(matOne);
	matWorld.m[3][1] -= 200.f;

	pEffect->SetVector("g_vLightDiffuse", (_vec4*)&m_pLight->Get_LightInfo()->Diffuse);
	
	m_pTransformCom->Scaling(fScale, fScale, fScale);

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matWVP = matWorld * matView* matProj;
	pEffect->SetMatrix("g_matWVP", &matWVP);

	const SUBSETDESC* pSubSet = m_pLightCone->Get_SubSetDesc(0);
	if (nullptr == pSubSet)
		return E_FAIL;
	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);

	return NOERROR;
}

CCustomLight * CCustomLight::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCustomLight*	pInstance = new CCustomLight(pGraphic_Device);
	_vec3 vPosition = _vec3(0,0,0);
	D3DLIGHTTYPE ltype = D3DLIGHT_POINT;

	if (FAILED(pInstance->Ready_Light(pGraphic_Device, vPosition, ltype)))
	{
		MessageBox(0, L"CCustomLight Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CCustomLight::Free()
{
	Safe_Release(m_pLightCone);
	Safe_Release(m_pLightTexture);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pLight);
	CGameObject::Free();
}

CGameObject * CCustomLight::Clone_GameObject()
{
	CCustomLight*	pInstance = new CCustomLight(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CCustomLight Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}
