#include "stdafx.h"
#include "..\Headers\Action_HookIn.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "MeatHook.h"
#include "Slasher.h"
#include "Action_SpiderReaction.h"

_USING(Client)

CAction_HookIn::CAction_HookIn()
{
}

HRESULT CAction_HookIn::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	if (!m_bIsInit)
		SetVectorPos();
	
	m_fIndex = 0.f;
	if (m_pGameObject->GetID() & SLASHER)
	{
		m_pSlasher = (CSlasher*)m_pGameObject;
		m_pSlasher->Set_State(AW::TW_Hook_In);
		m_pSlasher->IsLockKey(true);
		m_pCamper = (CCamper*)m_pSlasher->Get_CarriedCamper();
		m_iState = AW::TW_Hook_In;
		m_pHook->SetState(CMeatHook::GetHookedIn);
	}
	else
	{
		m_pCamper = (CCamper*)m_pGameObject;
		m_pCamper->SetCurCondition(CCamper::HOOKED);
		m_pCamper->IsLockKey(true);
		m_pCamper->SetCarry(false);
		m_pCamper->Set_State(AC::TT_Hook_In);
		m_pSlasher = nullptr;
		m_iState = AC::TT_Hook_In;
	}
	m_bIsPlaying = true;

	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);

	if (m_pSlasher != nullptr)
		Send_ServerData();

	return NOERROR;
}

_int CAction_HookIn::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay > 0.0f)
		m_fDelay -= fTimeDelta;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;
	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (m_pSlasher == nullptr)
	{
		if (m_vecCamperPos.size() <= (size_t)m_fIndex)
			return END_ACTION;

		if (m_fIndex >= 46.f)
			return END_ACTION;

		if (m_iState == AC::TT_Hook_In && pMeshCom->IsOverTime(0.28f))
			return END_ACTION;
		
		_vec3 vLocalPos = m_vecCamperPos[(size_t)m_fIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
		m_pCamper->SetCarry(false);
	}
	else
	{
		if (m_vecSlasherPos.size() <= (size_t)m_fIndex)
			return END_ACTION;

		if (m_fIndex >= 46.f)
			return END_ACTION;

		if (m_iState == AW::TW_Hook_In && pMeshCom->IsOverTime(0.28f))
		{
			m_pSlasher->Set_State(AW::TW_Idle);
			return END_ACTION;
		}

		_vec3 vLocalPos = m_vecSlasherPos[(_int)m_fIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	}

	return UPDATE_ACTION;
}

void CAction_HookIn::End_Action()
{
	m_fDelay = 1.f;
	m_bIsPlaying = false;

	if (m_pSlasher == nullptr)
	{
		m_pCamper->SetCarry(false);
		m_pCamper->SetColl(true);
		m_pGameObject->Set_Action(L"Action_SpiderReaction", 200.f);
	}
	else
	{
		m_pSlasher->Set_Carry(false);
		m_pSlasher->Set_CarriedCamper(nullptr);
		m_pCamper->SetColl(true);
		m_pSlasher->IsLockKey(false);
	}

	m_pCamper = nullptr;
	m_pSlasher = nullptr;
	m_pHook = nullptr;
}

void CAction_HookIn::Send_ServerData()
{
	slasher_data.SecondInterationObject = m_pCamper->GetID();
	slasher_data.SecondInterationObjAnimation = HOOK_IN;
}

void CAction_HookIn::SetVectorPos()
{
	m_vecCamperPos.reserve(46);

	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 3.994f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 11.197f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 27.277f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 36.319f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 42.419f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 51.705f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 59.906f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 63.889f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 71.164f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 75.881f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 79.874f));


	m_vecSlasherPos.reserve(46);

	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -8.623f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -17.246f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -25.868f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.491f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.491f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.491f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.491f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.491f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.491f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.491f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.491f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.491f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.494f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.5f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.514f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.538f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.569f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.607f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.649f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.695f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.743f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.79f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.937f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.88f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.919f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.952f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.978f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.994f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -35.f));

	m_bIsInit = true;
}

void CAction_HookIn::Free()
{
	CAction::Free();
}
