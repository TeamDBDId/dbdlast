#include "stdafx.h"
#include "..\Headers\Action_UnlockExit.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "ExitDoor.h"

_USING(Client)

CAction_UnlockExit::CAction_UnlockExit()
{
}

HRESULT CAction_UnlockExit::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);

	pCamper->Set_State(AC::UnlockExit);
	m_iState = AC::UnlockExit;

	if (m_pExitDoor != nullptr)
		m_pExitDoor->SetState(CExitDoor::Opening);

	return NOERROR;
}

_int CAction_UnlockExit::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (!KEYMGR->MousePressing(0))
		return END_ACTION;

	if (m_pExitDoor->Get_ProgressTime() >= m_pExitDoor->Get_MaxProgressTime())
		return END_ACTION;

	if (m_iState == AC::UnlockExit && pMeshCom->IsOverTime(0.25f))
	{
		((CCamper*)m_pGameObject)->Set_State(AC::UnlockExitIdle);
		m_iState = AC::UnlockExitIdle;
	}

	 return UPDATE_ACTION;
}

void CAction_UnlockExit::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetCurCondition(pCamper->GetOldCondition());
	pCamper->Set_State(AC::Idle);
	pCamper->IsLockKey(false);
}

void CAction_UnlockExit::Send_ServerData()
{
}

void CAction_UnlockExit::Free()
{
	CAction::Free();
}
