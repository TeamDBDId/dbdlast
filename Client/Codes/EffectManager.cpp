#include "stdafx.h"
#include "EffectManager.h"
#include "Management.h"

#include "Effect_Generator.h"
#include "Effect_Smoke.h"
#include "Effect_Flash.h"

#include "Effect_Mist.h"
#include "Effect_Fire.h"


#include "Math_Manager.h"

_USING(Client)

_IMPLEMENT_SINGLETON(CEffectManager)

CEffectManager::CEffectManager()
{
	m_pManagement = CManagement::GetInstance();
	m_pManagement->AddRef();
}


HRESULT CEffectManager::Ready_Effect(LPDIRECT3DDEVICE9 _pGDevice)
{
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Generator", CEffect_Generator::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Smoke", CEffect_Smoke::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Flash", CEffect_Flash::Create(_pGDevice))))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Mist", CEffect_Mist::Create(_pGDevice))))
		return E_FAIL;

	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Fire", CEffect_Fire::Create(_pGDevice))))
		return E_FAIL;


	//미스트
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Mist_SubUV", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Mist/Mist_SubUV.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Mist_Cloud", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Mist/Mist_Cloud.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Mist_Noise", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Mist/Mist_Noise.tga"))))
		return E_FAIL;


	//제너레이터
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Generator_Flash", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Generator/Generator_Flash.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Generator_Flash_Grad", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Generator/Generator_Flash_Grad.png"))))
		return E_FAIL;
	//if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Generator_Particle", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Generator/Generator_Flash_Particle%d.png",4))))
	//	return E_FAIL;
	//if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Generator_Spark", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Generator/Generator_Flash_Spark.tga"))))
	//	return E_FAIL;

	//모닥불
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Fire_Tile0", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Fire/Fire_Tile0.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Fire_Tile1", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Fire/Fire_Tile1.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Fire_Candle", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Fire/Fire_Candle.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Fire_Noise", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Fire/Fire_Noise.png"))))
		return E_FAIL;
	
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Mesh_Cone", CMesh_Static::Create(_pGDevice, L"../Bin/Resources/Meshes/EffectMesh/Fire/", L"Fire_Cone.x"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Mesh_Ribbon0", CMesh_Static::Create(_pGDevice, L"../Bin/Resources/Meshes/EffectMesh/Fire/", L"Fire_Ribbon0.x"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Mesh_Ribbon1", CMesh_Static::Create(_pGDevice, L"../Bin/Resources/Meshes/EffectMesh/Fire/", L"Fire_Ribbon1.x"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Mesh_Ribbon2", CMesh_Static::Create(_pGDevice, L"../Bin/Resources/Meshes/EffectMesh/Fire/", L"Fire_Ribbon2.x"))))
		return E_FAIL;




	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Mist", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_Mist.fx"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Fire", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_Fire.fx"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Generator", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_Generator.fx"))))
		return E_FAIL;


	return NOERROR;
}

void CEffectManager::Make_Effect(const eEffect& _eEffect, const _vec3 & _vPos)
{
	switch (_eEffect)
	{
	case E_Fire:
		if (_vec3(0.f, 0.f, 0.f) == _vPos)
			_MSG_BOX("Fail Effect_Fire Pos")
		else
			Make_Fire(_vPos);
		break;
	case E_Mist:
		Make_Mist();
		break;
	case E_Generator:
		if (_vec3(0.f, 0.f, 0.f) == _vPos)
			_MSG_BOX("Fail Effect_Generator Pos")
		else
			Make_Generator(_vPos);
		break;
	//case E_BonFire:
	//	break;
	}


}

void CEffectManager::Make_Fire(const _vec3 & _vPos)
{

	CEffect_Fire* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_Fire", SCENE_STAGE, L"Layer_EFire", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Fire");
	pEffect->Set_Pos(_vPos);

}


void CEffectManager::Make_Mist()
{

	for (size_t i = 0; i < 100;++i)
	{
		CEffect_Mist* pEffect = nullptr;
		if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_Mist", SCENE_STAGE, L"Layer_EMist", (CGameObject**)&pEffect)))
			_MSG_BOX("Fail Make_Mist");
		pEffect->Set_Pos(_vec3(_float(Math_Manager::CalRandIntFromTo(0, 100)), 0.f, _float(Math_Manager::CalRandFloatFromTo(0, 100))));
	}
	
}

void CEffectManager::Make_Generator(const _vec3 & _vPos)
{
	_vec3 vPos = _vPos;
	vPos.y = 100.f;

	CEffect_Generator* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_Generator", SCENE_STAGE, L"Layer_EGenerator", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Generator");
	pEffect->Set_Pos(vPos);

}





void CEffectManager::Free()
{
	Safe_Release(m_pManagement);
}

