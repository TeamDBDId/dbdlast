#include "stdafx.h"
#include "..\Headers\Action_HookFree.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"

_USING(Client)

CAction_HookFree::CAction_HookFree()
{
}

HRESULT CAction_HookFree::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (!m_bIsInit)
		SetVectorPos();

	m_fIndex = 0.f;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);
	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);

	pCamper->Set_State(AC::HookedFree);
	m_iState = AC::HookedFree;

	return NOERROR;
}

_int CAction_HookFree::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CCamper* pCamper = (CCamper*)m_pGameObject;

	m_fIndex += fTimeDelta * 30.f;
	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	size_t iIndex = (size_t)m_fIndex;

	if (m_iState == AC::HookedFree && pMeshCom->IsOverTime(0.3f))
		pCamper->Set_State(AC::Injured_Idle);

	if (m_fIndex >= 74.f)
		return END_ACTION;

	if (m_vecPos.size() <= iIndex)
		return END_ACTION;

	_vec3 vLocalPos = m_vecPos[iIndex];
	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

	_matrix mat = pTransform->Get_Matrix();
	D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

	_vec3 vPos = m_vOriginPos + vLocalPos;
	pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	
	return UPDATE_ACTION;
}

void CAction_HookFree::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);
	pCamper->SetColl(true);
	pCamper->SetCurCondition(CCamper::INJURED);
	pCamper->SetHookedEnergy();
	pCamper->SetHeal(0.f);
}

void CAction_HookFree::Send_ServerData()
{
}

void CAction_HookFree::SetVectorPos()
{
	m_vecPos.reserve(74);

	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.002f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.007f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.013f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.018f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.02f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.023f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.026f));
	m_vecPos.push_back(_vec3(0.02f, 0.f, 0.191f));
	m_vecPos.push_back(_vec3(0.164f, 0.f, 1.71f));
	m_vecPos.push_back(_vec3(1.062f, 0.f, 11.214f));
	m_vecPos.push_back(_vec3(2.617f, 0.f, 27.689f));
	m_vecPos.push_back(_vec3(4.416f, 0.f, 46.731f));
	m_vecPos.push_back(_vec3(6.363f, 0.f, 67.352f));
	m_vecPos.push_back(_vec3(7.975f, 0.f, 84.425f));
	m_vecPos.push_back(_vec3(9.09f, 0.f, 96.228f));
	m_vecPos.push_back(_vec3(9.967f, 0.f, 105.509f));
	m_vecPos.push_back(_vec3(10.754f, 0.f, 113.845f));
	m_vecPos.push_back(_vec3(11.601f, 0.f, 122.812f));
	m_vecPos.push_back(_vec3(12.607f, 0.f, 133.463f));
	m_vecPos.push_back(_vec3(13.451f, 0.f, 142.4f));
	m_vecPos.push_back(_vec3(14.103f, 0.f, 149.311f));
	m_vecPos.push_back(_vec3(14.507f, 0.f, 153.584f));
	m_vecPos.push_back(_vec3(14.64f, 0.f, 154.994f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));
	m_vecPos.push_back(_vec3(14.659f, 0.f, 155.195f));

	m_bIsInit = true;
}

void CAction_HookFree::Free()
{
	CAction::Free();
}
