#include "stdafx.h"
#include "..\Headers\Lamp_01.h"
#include "Management.h"
#include "Defines.h"
#include "CustomLight.h"
#include "Light_Manager.h"

_USING(Client)

CLamp_01::CLamp_01(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CLamp_01::CLamp_01(const CLamp_01 & rhs)
	: CGameObject(rhs)
{
}

HRESULT CLamp_01::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CLamp_01::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	return NOERROR;
}

_int CLamp_01::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	return _int();
}

_int CLamp_01::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	Compute_CameraDistance(&vPosition);

	if (m_fCameraDistance > 10000.f)
		return 0;

	_matrix		matLocal, matWorld;
	matWorld = m_pTransformCom->Get_Matrix();
	matLocal = m_pMeshCom->Get_LocalTransform();

	matWorld = matLocal * matWorld;
	vPosition = *(_vec3*)&matWorld.m[3][0];
	D3DXMatrixInverse(&matWorld, nullptr, &matWorld);

	if (!m_pFrustumCom->Culling_Frustum(&vPosition, matWorld, m_Max))
		return 0;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	m_pCustomLight->LastUpdate_GameObject(fTimeDelta);
	return _int();
}

void CLamp_01::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);


	m_pColliderCom->Render_Collider();

	m_pLightCollider->Render_Collider();
}

void CLamp_01::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
}

void CLamp_01::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;

	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Mesh_Lamp_01");
	Add_Component(L"Com_Mesh", m_pMeshCom);

	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Box", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_AABB, m_pMeshCom->Get_LocalTransform(), nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_Collider", m_pColliderCom);

	_matrix matLocal;
	D3DXMatrixScaling(&matLocal, 1000.f, 1000.f, 1000.f);
	_vec3 vLocalPos = { 0.f, -36.f, -34.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_OBB, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider", m_pLightCollider);
	
	//D3DXVec3TransformCoord(&vLocalPos, &vLocalPos, m_pTransformCom->Get_Matrix_Pointer());

	_vec3 vCenter = m_pLightCollider->Get_Center();

	m_pCustomLight = nullptr;

	if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomLight)))
		return;

	m_pCustomLight->Add_Light();
	m_pCustomLight->AddRef();

	m_pCustomLight->Set_Position(vCenter);
	m_pCustomLight->Set_Type(D3DLIGHT_SPOT);

	//m_pCustomLight->Set_Diffuse(D3DXCOLOR(1.f, 0.2f, 0.2f, 1.f));
	m_pCustomLight->Set_Range(400.f);
	m_pCustomLight->Set_Diffuse(D3DXCOLOR(3.6f, 3.6f, 3.5f, 1.f));
	m_pCustomLight->Set_Direction(_vec3(0.f, -1.f, 0.f));
	m_pCustomLight->Set_Theta(D3DXToRadian(12.5f));
	m_pCustomLight->Set_Phi(D3DXToRadian(17.5f));

	//AddShadowMap
	GET_INSTANCE_MANAGEMENT;

	for (auto pObj : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Map_Static"))
	{
		m_pCustomLight->Add_ShadowCubeGroup(pObj);
	}
	for (auto pObj : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_GameObject"))
	{
		m_pCustomLight->Add_ShadowCubeGroup(pObj);
	}

	CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return;

	pLight_Manager->AddRef();

	pLight_Manager->Ready_ShadowMap(m_pCustomLight->GetLight());

	Safe_Release(pLight_Manager);

	_vec3 vMax = m_pMeshCom->GetMax();
	_vec3 vMin = m_pMeshCom->GetMin();
	_vec3 vResult = (vMax - vMin) * 0.52f;

	m_Max = max(vResult.x, max(vResult.y, vResult.z));

	Safe_Release(pManagement);
}

_matrix CLamp_01::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CLamp_01::Get_Key()
{
	return m_Key.c_str();
}

_int CLamp_01::Get_OtherOption()
{
	return 0;
}

HRESULT CLamp_01::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CLamp_01::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetTexture("g_MetalicTexture", pSubSet->MeshTexture.pMetallicTexture);
	pEffect->SetTexture("g_RoughnessTexture", pSubSet->MeshTexture.pRoughnessTexture);

	return NOERROR;
}

CLamp_01 * CLamp_01::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CLamp_01*	pInstance = new CLamp_01(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CLamp_01 Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CLamp_01::Clone_GameObject()
{
	CLamp_01*	pInstance = new CLamp_01(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CLamp_01 Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CLamp_01::Free()
{
	Safe_Release(m_pCustomLight);
	Safe_Release(m_pLightCollider);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
