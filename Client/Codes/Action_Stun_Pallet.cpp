#include "stdafx.h"
#include "..\Headers\Action_Stun_Pallet.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Slasher.h"
#include "Plank.h"

_USING(Client)

CAction_Stun_Pallet::CAction_Stun_Pallet()
{
}

HRESULT CAction_Stun_Pallet::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	m_fIndex = 0.f;

	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	m_bIsPlaying = true;
	pSlasher->IsLockKey(true);

	pSlasher->Set_State(AW::TT_Stun_Pallet);
	m_iState = AW::TT_Stun_Pallet;

	return NOERROR;
}

_int CAction_Stun_Pallet::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay > 0.0f)
		m_fDelay -= fTimeDelta;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;

	_int iIndex = (int)m_fIndex;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	CSlasher* pSlasher = (CSlasher*)m_pGameObject;

	if (m_iState == AW::TT_Stun_Pallet && pMeshCom->IsOverTime(0.3f))
	{
		pSlasher->Set_State(AW::TW_Idle);
		return END_ACTION;
	}

	return UPDATE_ACTION;
}

void CAction_Stun_Pallet::End_Action()
{
	m_fDelay = 1.f;
	m_bIsPlaying = false;
	CSlasher* pSlahser = (CSlasher*)m_pGameObject;
	pSlahser->IsLockKey(false);
}

void CAction_Stun_Pallet::Send_ServerData()
{
}

void CAction_Stun_Pallet::Free()
{
	CAction::Free();
}
