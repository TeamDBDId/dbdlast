#include "stdafx.h"
#include "..\Headers\Action_Destroy_Pallet.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Slasher.h"
#include "Plank.h"

_USING(Client)

CAction_Destroy_Pallet::CAction_Destroy_Pallet()
{
}

HRESULT CAction_Destroy_Pallet::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	m_fIndex = 0.f;

	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	m_bIsPlaying = true;
	pSlasher->IsLockKey(true);

	pSlasher->Set_State(AW::TW_Destroy_Pallet);
	m_iState = AW::TW_Destroy_Pallet;

	return NOERROR;
}

_int CAction_Destroy_Pallet::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (m_pPlank->Get_ProgressTime() >= m_pPlank->Get_MaxProgressTime())
	{
		if(!m_pPlank->Get_IsDead())
			slasher_data.InterationObject = m_pPlank->GetID();
	}
	if (pMeshCom->IsOverTime(0.2f))
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->Set_State(AW::TW_Idle);
	}

	if (m_fIndex > 81.f)
		return END_ACTION;

	return UPDATE_ACTION;
}

void CAction_Destroy_Pallet::End_Action()
{
	m_bIsPlaying = false;
	CSlasher* pSlahser = (CSlasher*)m_pGameObject;
	pSlahser->IsLockKey(false);
}

void CAction_Destroy_Pallet::Send_ServerData()
{
}

void CAction_Destroy_Pallet::Free()
{
	CAction::Free();
}
