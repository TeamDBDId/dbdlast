#include "stdafx.h"
#include "..\Headers\Action_BeingHeal.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"

_USING(Client)

CAction_BeingHeal::CAction_BeingHeal()
{
}

HRESULT CAction_BeingHeal::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	m_bIsFinsh = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;

	pCamper->IsLockKey(true);
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	
	pCamper->Set_State(AC::BeingHeal);
	
	return NOERROR;
}

_int CAction_BeingHeal::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	_uint HealPlayer = 3;

	for (_int i = 0; i < 4; ++i)
	{
		if (i == (exPlayerNumber - 1))
			continue;

		if (server_data.Campers[i].SecondInterationObject == m_pGameObject->GetID())
		{
			if(server_data.Campers[i].SecondInterationObjAnimation == BEING_HEAL)
				camper_data.fHeal += fTimeDelta;
			else if (server_data.Campers[i].SecondInterationObjAnimation == END_HEAL)
				return END_ACTION;
		}

		if (server_data.Campers[i].SecondInterationObject != m_pGameObject->GetID())
		{
			--HealPlayer;
		}
			
		if (camper_data.fHeal >= 20.f)
		{
			m_bIsFinsh = true;
			return END_ACTION;
		}
	}

	if (0 == HealPlayer)
		return END_ACTION;

	if (KEYMGR->KeyPressing(DIK_W) || KEYMGR->KeyPressing(DIK_A) || KEYMGR->KeyPressing(DIK_S) || KEYMGR->KeyPressing(DIK_D) || KEYMGR->KeyPressing(DIK_LSHIFT))
	{
		camper_data.Packet = END_HEAL;
		return END_ACTION;
	}

	return UPDATE_ACTION;
}

void CAction_BeingHeal::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);

	Send_ServerData();

	if (m_bIsFinsh)
	{
		if (pCamper->GetOldCondition() == CCamper::DYING)
		{
			pCamper->SetCurCondition(CCamper::INJURED);
			pCamper->SetMaxEnergyAndMaxHeal();
			pCamper->SetDyingEnergy();
		}
		else if (pCamper->GetOldCondition() == CCamper::INJURED)
		{
			pCamper->SetCurCondition(CCamper::HEALTHY);
		}
	}
	else
	{
		pCamper->SetCurCondition(pCamper->GetOldCondition());
	}

	m_pCamper = nullptr;
}

void CAction_BeingHeal::Send_ServerData()
{
}

void CAction_BeingHeal::Free()
{
	CAction::Free();
}
