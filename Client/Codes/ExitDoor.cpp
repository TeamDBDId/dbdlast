#include "stdafx.h"
#include "ExitDoor.h"
#include "Management.h"
#include "Light_Manager.h"
#include "CustomLight.h"

_USING(Client)

CExitDoor::CExitDoor(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CExitDoor::CExitDoor(const CExitDoor & rhs)
	: CGameObject(rhs)
{

}


HRESULT CExitDoor::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CExitDoor::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_CurState = Closed;
	m_pMeshCom->Set_AnimationSet(Closed);

	m_fProgressTime = 0.f;
	m_fMaxProgressTime = 20.f;

	if (FAILED(m_pGraphic_Device->CreateTexture(128, 128, 1, 0, D3DFMT_A32B32G32R32F, D3DPOOL_MANAGED, &m_pSkinTextures, nullptr)))
		return E_FAIL;

	return NOERROR;
}

_int CExitDoor::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;

	m_iCurAnimation = m_CurState;
	m_fDeltaTime = fTimeDelta;
	ComunicateWithServer();
	State_Check();
	return _int();
}

_int CExitDoor::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (m_fCameraDistance > 6000.f)
		return 0;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CExitDoor::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	m_pMeshCom->Play_Animation(m_fDeltaTime);

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();

	//m_pColliderCom->Render_Collider();
	//for (int i = 0; i < 3; ++i)
	//	m_pLightCollider[i]->Render_Collider();

	Safe_Release(pEffect);
}

void CExitDoor::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(1);

	pEffect->SetVector("g_vLightPos", &vLightPos);
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matW");
	pEffect->SetMatrix("g_matVP", VP);

	pEffect->CommitChanges();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, D3DLOCK_NOSYSLOCK | D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CExitDoor::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;

	const _matrix* pMat = m_pMeshCom->Find_Frame("joint_Crank_01");
	m_fRad = 120.f;
	_matrix			matLocalTransform;
	_vec3			vPos;
	D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);

	vPos = *(_vec3*)&(pMat->m[3][0]);
	vPos += -*m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK)*40.f;

	matLocalTransform._41 = vPos.x;
	matLocalTransform._42 = 0.f;
	matLocalTransform._43 = vPos.z;

	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
		_MSG_BOX("111");
	if (FAILED(m_pColliderCom->Ready_HullMesh(SCENE_STAGE, L"Mesh_ExitDoor")))
		_MSG_BOX("111");

	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_EXITDOOR, this);

	//
	_matrix matLocal;
	D3DXMatrixScaling(&matLocal, 10.f, 10.f, 10.f);
	_vec3 vLocalPos = { 45.f, 300.f, -50.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[0] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider0", m_pLightCollider[0]);

	D3DXMatrixScaling(&matLocal, 10.f, 10.f, 10.f);
	vLocalPos = { 50.f, 300.f, -50.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[1] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider1", m_pLightCollider[1]);

	D3DXMatrixScaling(&matLocal, 10.f, 10.f, 10.f);
	vLocalPos = { 55.f, 300.f, -50.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[2] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider2", m_pLightCollider[2]);

	for (int i = 0; i < 3; ++i)
	{
		_vec3 vCenter = m_pLightCollider[i]->Get_Center();

		if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomLight[i])))
			return;

		m_pCustomLight[i]->Add_Light();
		m_pCustomLight[i]->AddRef();

		m_pCustomLight[i]->Set_Position(vCenter);

		m_pCustomLight[i]->Set_Range(30.f);
		m_pCustomLight[i]->Set_Diffuse(D3DXCOLOR(3.f, 0.1f, 0.1f, 1.f));
		//AddShadowMap
		//m_pCustomLight[i]->SetRender(false);

		CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
		if (nullptr == pLight_Manager)
			return;

		pLight_Manager->AddRef();

		pLight_Manager->Ready_ShadowMap(m_pCustomLight[i]->GetLight());

		Safe_Release(pLight_Manager);
	}
}

_matrix CExitDoor::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CExitDoor::Get_Key()
{
	return m_Key.c_str();
}

_int CExitDoor::Get_OtherOption()
{
	return m_iOtherOption;
}

void CExitDoor::State_Check()
{
	if (m_CurState == m_OldState)
		return;
	m_pMeshCom->Set_AnimationSet(m_CurState);
	m_OldState = m_CurState;
}

void CExitDoor::ComunicateWithServer()
{

}

HRESULT CExitDoor::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.CMesh_Dynamic
	m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_ExitDoor");
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CExitDoor::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);

	return NOERROR;
}


CExitDoor * CExitDoor::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CExitDoor*	pInstance = new CExitDoor(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CExitDoor Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CExitDoor::Clone_GameObject()
{
	CExitDoor*	pInstance = new CExitDoor(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CExitDoor Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CExitDoor::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_EXITDOOR, this);

	for (int i = 0; i < 3; ++i)
	{
		Safe_Release(m_pLightCollider[i]);
		Safe_Release(m_pCustomLight[i]);
	}

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
