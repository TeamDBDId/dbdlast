#include "stdafx.h"
#include "..\Headers\Action_Sabotage.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"

_USING(Client)

CAction_Sabotage::CAction_Sabotage()
{
}

HRESULT CAction_Sabotage::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	pCamper->Set_State(AC::Sabotage_In);
	m_iState = AC::Sabotage_In;

	return NOERROR;
}

_int CAction_Sabotage::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (m_iState == AC::Sabotage_In && pMeshCom->IsOverTime(0.01f))
	{
		((CCamper*)m_pGameObject)->Set_State(AC::Sabotage_Loop);

		m_iState = AC::Sabotage_Loop;
	}

	return UPDATE_ACTION;
}

void CAction_Sabotage::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetCurCondition(pCamper->GetOldCondition());
	pCamper->IsLockKey(false);
}

void CAction_Sabotage::Send_ServerData()
{
}

void CAction_Sabotage::Free()
{
	CAction::Free();
}
