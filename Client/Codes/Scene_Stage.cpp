#include "stdafx.h"
#include "..\Headers\Scene_Stage.h"
#include "Management.h"
#include "Camera_Debug.h"
#include "Camera_Camper.h"
#include "Camera_Slasher.h"
#include "Light_Manager.h"
#include "Scene_Lobby.h"
#include "Camper.h"
#include "Slasher.h"
#include "LoadManager.h"
#include "Moon.h"
#include "Machete.h"
#include "UI_Interaction.h"
_USING(Client)

CScene_Stage::CScene_Stage(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CScene(pGraphic_Device)
{

}

HRESULT CScene_Stage::Ready_Scene()
{
	bGameServer = true;

	if (bUseServer)
	{
		Gamesock = CServerManager::GetInstance()->Ready_Server();

		hThread = (HANDLE)_beginthreadex(nullptr, 0, Game_Thread, 0, 0, nullptr);
	}

    if (FAILED(Ready_Prototype_Component()))
		return E_FAIL;

	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;

	if (FAILED(Ready_Layer_Camera(L"Layer_Camera")))
		return E_FAIL;

	if (FAILED(Ready_Layer_BackGround(L"Layer_BackGround")))
		return E_FAIL;


	if (FAILED(Ready_LightInfo()))
		return E_FAIL;

	GET_INSTANCE(CLoadManager)->LoadMapData(0);

	if (exPlayerNumber < 5)
	{
		if (FAILED(Ready_Layer_Camper(L"Layer_Camper", (_uint)exPlayerNumber)))
			return E_FAIL;
	}

	if (FAILED(Ready_Layer_Slasher(L"Layer_Slasher")))
		return E_FAIL;

	if (FAILED(Ready_UI()))
		return E_FAIL;
	bStartGameServer = true;

	return NOERROR;
}

_int CScene_Stage::Update_Scene(const _float & fTimeDelta)
{
	Check_Connection_OtherPlayers();


	return CScene::Update_Scene(fTimeDelta);
}

_int CScene_Stage::LastUpdate_Scene(const _float & fTimeDelta)
{
	CColl_Manager::GetInstance()->Coll(fTimeDelta);

	return CScene::LastUpdate_Scene(fTimeDelta);
}

void CScene_Stage::Render_Scene()
{

}

size_t CScene_Stage::Game_Thread(LPVOID lpParam)
{
	QueryPerformanceFrequency(&GtickPerSecond);
	QueryPerformanceCounter(&GstartTick);
	while (bGameServer)
	{
		QueryPerformanceCounter(&GendTick);
		dGTime = (double)(GendTick.QuadPart - GstartTick.QuadPart) / GtickPerSecond.QuadPart;

		while (dGTime >= 0.002000) {
			QueryPerformanceCounter(&GstartTick);
			dGTime = 0;
			if (exPlayerNumber < 5)
				camper_data.Number = exPlayerNumber;
			else
				slasher_data.Number = exPlayerNumber;
			CServerManager::GetInstance()->Data_Exchange(Gamesock, camper_data, client_data, server_data, slasher_data);
			sendPacket++;
		}
	}

	return 0;
}

HRESULT CScene_Stage::Ready_LightInfo()
{
	CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return E_FAIL;

	pLight_Manager->AddRef();

	D3DLIGHT9			LightInfo;
	ZeroMemory(&LightInfo, sizeof(D3DLIGHT9));

	LightInfo.Type = D3DLIGHT_DIRECTIONAL;
	LightInfo.Diffuse = D3DXCOLOR(0.15f, 0.15f, 0.15f, 1.f);
	LightInfo.Specular = D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.f);
	LightInfo.Ambient = D3DXCOLOR(0.13f, 0.13f, 0.13f, 1.f);
	//LightInfo.Diffuse = D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.f);
	//LightInfo.Specular = D3DXCOLOR(0.6f, 0.6f, 0.6f, 1.f);
	//LightInfo.Ambient = D3DXCOLOR(0.1f, 0.1f, 0.1f, 1.f);

	LightInfo.Direction = _vec3(1.f, -1.f, 1.f);

	if (FAILED(pLight_Manager->Add_LightInfo(m_pGraphic_Device, LightInfo)))
		return E_FAIL;

	//LightInfo.Type = D3DLIGHT_SPOT;
	//LightInfo.Diffuse = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);
	//LightInfo.Specular = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);
	//LightInfo.Ambient = D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.f);

	////LightInfo.Diffuse = D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.f);
	////LightInfo.Specular = D3DXCOLOR(0.6f, 0.6f, 0.6f, 1.f);
	////LightInfo.Ambient = D3DXCOLOR(0.1f, 0.1f, 0.1f, 1.f);

	//LightInfo.Position = _vec3(3200.f, 50.f, 3200.f);
	//LightInfo.Direction = _vec3(0.f, -1.f, 0.f);
	//LightInfo.Theta = D3DXToRadian(6.5f);
	//LightInfo.Phi = D3DXToRadian(10.5f);
	//LightInfo.Range = 1000.f;

	//if (FAILED(pLight_Manager->Add_LightInfo(m_pGraphic_Device, LightInfo)))
	//	return E_FAIL;

	//for (int i = 0; i < 2; ++i)

	//ZeroMemory(&LightInfo, sizeof(D3DLIGHT9));

	//LightInfo.Type = D3DLIGHT_POINT;
	//LightInfo.Diffuse = D3DXCOLOR(4.f, 4.f, 4.f, 1.f);
	//LightInfo.Specular = LightInfo.Diffuse;
	//LightInfo.Ambient = D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.f);

	//LightInfo.Position = _vec3(3500.f, 5.f, 3500.f);
	//LightInfo.Range = 3000.0f;

	//if (FAILED(pLight_Manager->Add_LightInfo(m_pGraphic_Device, LightInfo)))
	//	return E_FAIL;

	//ZeroMemory(&LightInfo, sizeof(D3DLIGHT9));

	//LightInfo.Type = D3DLIGHT_SPOT;
	//LightInfo.Diffuse = D3DXCOLOR(1.f, 0.2f, 0.2f, 1.f);
	//LightInfo.Specular = LightInfo.Diffuse;
	//LightInfo.Ambient = D3DXCOLOR(1.f, 0.1f, 0.1f, 1.f);
	//LightInfo.Attenuation0 = 0.0001f;
	//LightInfo.Attenuation1 = 0.0001f;
	//LightInfo.Attenuation2 = 0.0001f;

	//LightInfo.Position = _vec3(3500.f, 25.f, 3500.f);
	//LightInfo.Range = 10000.0f;
	//LightInfo.Falloff = 1.f;
	//LightInfo.Theta = D3DXToRadian(12.5f);
	//LightInfo.Phi = D3DXToRadian(17.5f);
	//LightInfo.Direction = _vec3(0.f, -1.f, 0.f);

	//if (FAILED(pLight_Manager->Add_LightInfo(m_pGraphic_Device, LightInfo)))
	//	return E_FAIL;

	Safe_Release(pLight_Manager);

	return NOERROR;
}

// 원형객체를 생성해 놓는다.
HRESULT CScene_Stage::Ready_Prototype_GameObject()
{
	return NOERROR;
}

HRESULT CScene_Stage::Ready_Prototype_Component()
{
	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Camera(const _tchar * pLayerTag)
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Camera_Debug
	CCamera_Debug*		pCameraObject = nullptr;
	CCamera_Camper*		pCameraCamper = nullptr;
	//CCamera_Slasher*	pCameraSlasher = nullptr;

	// 원형카메라를 복제해서 레이어에 추가할(실제 사용할)객체를 생성한다.레이어에 추가한다ㅏ.
	// 그렇게 복제된 객체를 건져온다.

	/*if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Debug", SCENE_STAGE, pLayerTag, (CGameObject**)&pCameraObject)))
		return E_FAIL;*/

	if (0 == exPlayerNumber)
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Debug", SCENE_STAGE, pLayerTag, (CGameObject**)&pCameraObject)))
			return E_FAIL;
	}
	else/* if (exPlayerNumber < 5 && exPlayerNumber > 0)*/
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Camper", SCENE_STAGE, pLayerTag, (CGameObject**)&pCameraCamper)))
			return E_FAIL;
	}
	//else
	//{
	//	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Slasher", SCENE_STAGE, pLayerTag, (CGameObject**)&pCameraSlasher)))
	//		return E_FAIL;
	//}

	CAMERADESC		CameraDesc;
	ZeroMemory(&CameraDesc, sizeof(CAMERADESC));
	CameraDesc.vEye = _vec3(3200.f, 20.f, 3200.f);
	CameraDesc.vAt = _vec3(0.f, 0.f, 0.f);
	CameraDesc.vAxisY = _vec3(0.f, 1.f, 0.f);
		
	PROJDESC		ProjDesc;
	ZeroMemory(&ProjDesc, sizeof(PROJDESC));
	ProjDesc.fFovY = D3DXToRadian(60.0f);
	ProjDesc.fAspect = _float(g_iBackCX) / g_iBackCY;
	ProjDesc.fNear = 5.f;
	ProjDesc.fFar = 10000.f;

	if (0 == exPlayerNumber)
	{
		if (FAILED(pCameraObject->SetUp_CameraProjDesc(CameraDesc, ProjDesc)))
			return E_FAIL;
	}
	else/* if (exPlayerNumber < 5 && exPlayerNumber > 0)*/
	{
		if (FAILED(pCameraCamper->SetUp_CameraProjDesc(CameraDesc, ProjDesc)))
			return E_FAIL;
	}
	//else
	//{
	//	if (FAILED(pCameraSlasher->SetUp_CameraProjDesc(CameraDesc, ProjDesc)))
	//		return E_FAIL;
	//}

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_BackGround(const _tchar * pLayerTag)
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Sky
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Sky", SCENE_STAGE, pLayerTag)))
		return E_FAIL;

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Moon", SCENE_STAGE, L"Layer_Moon")))
		return E_FAIL;

	//EFFMGR->Make_Effect(CEffectManager::E_Fire,_vec3(3200.f,500.f,3200.f));
	EFFMGR->Make_Effect(CEffectManager::E_Mist);
	
	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Camper(const _tchar * pLayerTag, _uint Index)
{
	CManagement*		pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return E_FAIL;

	pManagement->AddRef();

	// For.Player
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camper", SCENE_STAGE, pLayerTag)))
		return E_FAIL;

	((CCamper*)pManagement->Get_ObjectList(SCENE_STAGE, pLayerTag).back())->Set_Index(Index);
	((CCamper*)pManagement->Get_ObjectList(SCENE_STAGE, pLayerTag).back())->SetID(Index - 1);
	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Slasher(const _tchar * pLayerTag)
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Slasher", SCENE_STAGE, pLayerTag)))
		return E_FAIL;

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Machete", SCENE_STAGE, pLayerTag)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Prototype_CollMesh()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();


	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Crate_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Crate_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Crate_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Crate_02_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Crate_03_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Crate_03_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Crate_04_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Crate_04_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Trash_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Trash_01_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hay_Square_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Hay_Square_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hay_Round_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Hay_Round_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodFence_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodFence_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodFence_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodFence_02_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodFence_03_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodFence_03_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_06_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Rock_06_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_16_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Rock_16_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Rock_17_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Rock_17_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_1m_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_1m_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_2m_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_2m_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_2m_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_2m_02_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_2m_Jump_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_2m_Jump_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_Corner_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_Corner_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_T_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_T_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_WoodWall_4m_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"WoodWall_4m_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Tree_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Tree_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Tree_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Tree_02_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Tree_03_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Tree_03_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Barrel_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Barrel_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hut_Wall_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Hut_Wall_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hut_Wall_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Hut_Wall_02_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Basement_Wall_2m_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Basement_Wall_2m_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Basement_Wall_4m_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Basement_Wall_4m_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_TireStack_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"TireStack_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_TireStack_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"TireStack_02_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_TireStack_03_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"TireStack_03_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_RockWall_01_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"RockWall_01_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_RockWall_02_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"RockWall_02_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_RockWall_03_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"RockWall_03_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_RockWall_Jump_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"RockWall_Jump_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_SideFence_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"SideFence_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_SideFence_Wall_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"SideFence_Wall_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_LogPile_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"LogPile_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_LumberPile_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"LumberPile_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_OldTractor_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"OldTractor_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_PickupTruck_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"PickupTruck_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_FarmMachine_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"FarmMachine_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Transporter_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Transporter_HULL.x"))))
		return E_FAIL;


	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Closet_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Closet_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Generator_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Generator_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_MeatHook_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"MeatHook_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Plank_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Plank_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Plank2_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Plank2_HULL.x"))))
		return E_FAIL;
	//if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Hatch", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Animation/Hatch/", L"Hatch.x"))))
	//	return E_FAIL;
	//if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_ExitDoor", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Animation/ExitDoor/", L"ExitDoor.x"))))
	//	return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_Chest_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Chest_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_ExitBuilding_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"ExitBuilding_HULL.x"))))
		return E_FAIL;

	//if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_FarmHouse_Navi", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"FarmHouse_Navi.x"))))
	//	return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_FarmHouse_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"FarmHouse_HULL.x"))))
  		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_FarmHouse_NAVI", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"FarmHouse_NAVI.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Hut_Floor_NAVI", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Hut_Floor_NAVI.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Terrain_FarmHouse_NAVI", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Terrain_FarmHouse_NAVI.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Basement_NAVI", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Basement_NAVI.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_Basement_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"Basement_HULL.x"))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Mesh_ExitDoor_HULL", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"ExitDoor_HULL.x"))))
		return E_FAIL;
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Map_Static_ExitBuilding_Ground_NAVI", CMesh_Static::Create(m_pGraphic_Device, L"../Bin/Resources/Meshes/HullMesh/", L"ExitBuilding_Ground_NAVI.x"))))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_UI()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_GameState", SCENE_STAGE, L"Layer_UI_State")))
		return E_FAIL;


	CUI_Interaction* pUI_Interaction = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Interaction", SCENE_STAGE, L"Layer_UI_Interaction", (CGameObject**)&pUI_Interaction)))
		return E_FAIL;
	GET_INSTANCE(CUIManager)->Set_UIInteraction(pUI_Interaction);



	Safe_Release(pManagement);
	return NOERROR;
}

CScene_Stage * CScene_Stage::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CScene_Stage*	pInstance = new CScene_Stage(pGraphic_Device);

	if (FAILED(pInstance->Ready_Scene()))
	{
		MessageBox(0, L"CScene_Stage Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);		
	}
	return pInstance;
 }

void CScene_Stage::Check_Connection_OtherPlayers()
{
	if (exPlayerNumber < 5)
		bConnectedCamper[exPlayerNumber-1] = true;

	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return;

	pManagement->AddRef();

	for (int i = 0; i < 4; ++i)
	{
		if (server_data.Campers[i].bConnect && !bConnectedCamper[i])
		{
			bConnectedCamper[i] = true;
			Ready_Layer_Camper(L"Layer_Camper", i+1);
		}
		else if (!server_data.Campers[i].bConnect && bConnectedCamper[i])
		{
			bConnectedCamper[i] = false;
		}
	}

	Safe_Release(pManagement);
}

void CScene_Stage::Free()
{

	GET_INSTANCE_MANAGEMENT


	if (bUseServer)
	{
		closesocket(Gamesock);

		CloseHandle(hThread);
	}

	bGameServer = false;

	pManagement->Clear_Layers(SCENE_STAGE);

	Safe_Release(pManagement);

	//CScene_Lobby*	pNewScene = CScene_Lobby::Create(m_pGraphic_Device);
	//if (nullptr == pNewScene)
	//	return;

	//if (FAILED(pManagement->SetUp_ScenePointer(pNewScene)))
	//	return;

	//Safe_Release(pNewScene);

	CScene::Free();
}
