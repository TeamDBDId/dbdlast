#include "stdafx.h"
#include "Effect_Fire.h"
#include "Management.h"



#include "Math_Manager.h"
_USING(Client)

CEffect_Fire::CEffect_Fire(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_Fire::CEffect_Fire(const CEffect_Fire & _rhs)
	: CBaseEffect(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_Fire::Ready_Prototype()
{
	CBaseEffect::Ready_Prototype();

	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_Fire::Ready_GameObject()
{
	CBaseEffect::Ready_GameObject();


	if (FAILED(Ready_Component()))
		return E_FAIL;

	

	m_pTransformCom[0]->Scaling(80.f, 80.f, 0.f);
	m_pTransformCom[1]->Scaling(100.f, 500.f, 0.f);


	return NOERROR;
}

_int CEffect_Fire::Update_GameObject(const _float & _fTick)
{

	m_fTime += _fTick;
	if (6.f < m_fTime)
		m_fTime = 0.f;
	 



	return _int();
}

_int CEffect_Fire::LastUpdate_GameObject(const _float & _fTick)
{



	if (nullptr == m_pRendererCom)
		return -1;


	_matrix		matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	_vec3		vRight, vUp, vLook;

	for (size_t i = 0; i < 2; ++i)
	{
		vRight = *(_vec3*)&matView.m[0][0] * m_pTransformCom[i]->Get_Scale().x;
		//vUp = *(_vec3*)&matView.m[1][0] * m_pTransformCom[i]->Get_Scale().y;
		vLook = *(_vec3*)&matView.m[2][0] * m_pTransformCom[i]->Get_Scale().z;

		m_pTransformCom[i]->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
		//m_pTransformCom[i]->Set_StateInfo(CTransform::STATE_UP, &vUp);
		m_pTransformCom[i]->Set_StateInfo(CTransform::STATE_LOOK, &vLook);
	}

	



	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;



	return _int();
}

void CEffect_Fire::Render_GameObject()
{
	//for (size_t i = 0; i < 2; ++i)
		Render_Buffer(0);

	//Render_Buffer(1);


}

void CEffect_Fire::Set_Pos(const _vec3 & _vPos)
{
	
	m_pTransformCom[0]->Set_StateInfo(CTransform::STATE_POSITION, &_vPos);
	m_pTransformCom[1]->Set_StateInfo(CTransform::STATE_POSITION, &(_vPos+_vec3(0.f,250.f,0.f)));
}




void CEffect_Fire::Render_Buffer(const _uint & _iIdx)
{

	if (nullptr == m_pBufferCom)
		return;

	// 셰이더를 이용해서 그려. 
	LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();


	if (FAILED(SetUp_ContantTable(pEffect, _iIdx)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(_iIdx);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);

}

HRESULT CEffect_Fire::Ready_Component()
{
	CBaseEffect::Ready_Component(L"Component_Shader_Fire");


	CManagement* pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();


	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	m_pTransformCom[0] = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform0", m_pTransformCom[0])))
		return E_FAIL;
	m_pTransformCom[1] = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform1", m_pTransformCom[1])))
		return E_FAIL;


	m_pTextureCom[0] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Fire_Candle");
	if (FAILED(Add_Component(L"Com_Texture0", m_pTextureCom[0])))
		return E_FAIL;
	m_pTextureCom[1] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Fire_Noise");
	if (FAILED(Add_Component(L"Com_Texture1", m_pTextureCom[1])))
		return E_FAIL;

	m_pTextureCom[2] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Fire_Tile1");
	if (FAILED(Add_Component(L"Com_Texture2", m_pTextureCom[2])))
		return E_FAIL;
	//m_pTextureCom[3] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Fire_Noise");
	//if (FAILED(Add_Component(L"Com_Texture3", m_pTextureCom[3])))
	//	return E_FAIL;
	//m_pTextureCom[4] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Fire_Smoke");
	//if (FAILED(Add_Component(L"Com_Texture4", m_pTextureCom[4])))
	//	return E_FAIL;
	

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CEffect_Fire::SetUp_ContantTable(LPD3DXEFFECT _pEffect, const _uint& _iIdx)
{
	CBaseEffect::SetUp_ContantTable(_pEffect);

	
	m_pTransformCom[_iIdx]->SetUp_OnShader(_pEffect, "g_matWorld");


	if (0 == _iIdx)
	{
		m_pTextureCom[0]->SetUp_OnShader(_pEffect, "g_Tex2");
		m_pTextureCom[1]->SetUp_OnShader(_pEffect, "g_Tex1");
		m_pTextureCom[2]->SetUp_OnShader(_pEffect, "g_Tex0");

	}
	else
	{

	}
	//else
	//{
	//	m_pTextureCom[2]->SetUp_OnShader(_pEffect, "g_Tex0");
	//	m_pTextureCom[3]->SetUp_OnShader(_pEffect, "g_Tex1");
	//	m_pTextureCom[4]->SetUp_OnShader(_pEffect, "g_Tex2");
	//}
	

	_pEffect->SetFloat("g_fTime", m_fTime*0.33f);



	/*_pEffect->SetInt("g_iColumn", _uint(m_fTime*20.f) / 6);
	_pEffect->SetInt("g_iRow", _uint(m_fTime*20.f) % 6);*/

	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_Fire * CEffect_Fire::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_Fire*	pInst = new CEffect_Fire(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_Fire::Clone_GameObject()
{
	CEffect_Fire*	pInst = new CEffect_Fire(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("Fire Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_Fire::Free()
{
	Safe_Release(m_pBufferCom);

	for (size_t i = 0; i<TRANSCNT; ++i)
		Safe_Release(m_pTransformCom[i]);

	for (size_t i = 0; i<TEXCNT; ++i)
		Safe_Release(m_pTextureCom[i]);



	CBaseEffect::Free();
}
