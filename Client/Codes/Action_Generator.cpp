#include "stdafx.h"
#include "..\Headers\Action_Generator.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Generator.h"

_USING(Client)

CAction_Generator::CAction_Generator()
{
}

HRESULT CAction_Generator::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	pCamper->Set_State(AC::GeneratorActivation);
	m_iState = AC::GeneratorActivation;
	return NOERROR;
}

_int CAction_Generator::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	if (m_pGenerator->Get_ProgressTime() > m_pGenerator->Get_MaxProgressTime())
		return END_ACTION;

	if (camper_data.Packet == FAILED_SKILL_CHECK)
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->Set_State(AC::GeneratorFail);
		m_iState = AC::GeneratorFail;
	}

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	if (m_iState == AC::GeneratorFail && pMeshCom->IsOverTime(0.3f))
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->Set_State(AC::GeneratorActivation);
		m_iState = AC::GeneratorActivation;
	}

	if (!KEYMGR->MousePressing(0))
		return END_ACTION;

	Send_ServerData();
	return UPDATE_ACTION;
}

void CAction_Generator::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);
	m_pGenerator = nullptr;
	pCamper->SetCurCondition(pCamper->GetOldCondition());
	Send_ServerData();
}

void CAction_Generator::Send_ServerData()
{
	if (m_pGenerator != nullptr)
	{
		camper_data.InterationObject = m_pGenerator->GetID();
		camper_data.InterationObjAnimation = m_pGenerator->Get_CurAnimation();
	}
	else
		camper_data.InterationObject = 0;
}

void CAction_Generator::Free()
{
	CAction::Free();
}
