#include "stdafx.h"
#include "..\Headers\Action_PassWindow.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"

_USING(Client)

CAction_PassWindow::CAction_PassWindow()
{
}

HRESULT CAction_PassWindow::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::OBSTACLE);
	pCamper->IsLockKey(true);
	pCamper->SetColl(false);
 	m_bIsPlaying = true;
	m_fIndex = 0.f;

	if (!m_bIsInit)
		SetVectorPos();

	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);
	
	if (KEYMGR->KeyPressing(DIK_LSHIFT))
	{
		pCamper->Set_State(AC::WindowVault_Fast);
		m_iState = AC::WindowVault_Fast;
	}
	else
	{
		pCamper->Set_State(AC::WindowVault_Mid);
		m_iState = AC::WindowVault_Mid;
	}

	return NOERROR;
}

_int CAction_PassWindow::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	if (m_iState == AC::WindowVault_Mid)
		m_fIndex += fTimeDelta * 30.f;
	else
		m_fIndex += fTimeDelta * 30.f * 1.5f;

	size_t iIndex = (size_t)m_fIndex;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	
	if (pMeshCom->IsOverTime(0.3f))
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->Set_State(AC::Idle);
	}

	if (m_vecPos.size() <= iIndex)
		return END_ACTION;

	if (m_fIndex >= 61.f)
		return END_ACTION;

	_vec3 vLocalPos = m_vecPos[iIndex];
	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

	_matrix mat = pTransform->Get_Matrix();
	D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

	_vec3 vPos = m_vOriginPos + vLocalPos;
	pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

	return UPDATE_ACTION;
}

void CAction_PassWindow::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetCurCondition(pCamper->GetOldCondition());
	pCamper->Set_State(AC::Idle);
	pCamper->IsLockKey(false);
	pCamper->SetColl(true);
}

void CAction_PassWindow::Send_ServerData()
{
}

void CAction_PassWindow::SetVectorPos()
{
	m_vecPos.reserve(30);

	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 6.651f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 13.384f));
	m_vecPos.push_back(_vec3(-0.666f, 0.f, 20.076f));
	m_vecPos.push_back(_vec3(-2.022f, 0.f, 26.602f));
	m_vecPos.push_back(_vec3(-3.103f, 0.f, 32.838f));
	m_vecPos.push_back(_vec3(-3.718f, 0.f, 38.875f));
	m_vecPos.push_back(_vec3(-4.322f, 0.f, 44.887f));
	m_vecPos.push_back(_vec3(-4.915f, 0.f, 50.878f));
	m_vecPos.push_back(_vec3(-5.499f, 0.f, 56.848f));
	m_vecPos.push_back(_vec3(-6.074f, 0.f, 62.801f));
	m_vecPos.push_back(_vec3(-6.641f, 0.f, 68.737f));
	m_vecPos.push_back(_vec3(-7.201f, 0.f, 74.658f));
	m_vecPos.push_back(_vec3(-7.756f, 0.f, 80.568f));
	m_vecPos.push_back(_vec3(-8.305f, 0.f, 86.467f));
	m_vecPos.push_back(_vec3(-8.851f, 0.f, 92.358f));
	m_vecPos.push_back(_vec3(-9.393f, 0.f, 98.242f));
	m_vecPos.push_back(_vec3(-9.934f, 0.f, 104.121f));
	m_vecPos.push_back(_vec3(-10.473f, 0.f, 109.999f));
	m_vecPos.push_back(_vec3(-11.012f, 0.f, 115.876f));
	m_vecPos.push_back(_vec3(-11.551f, 0.f, 121.753f));
	m_vecPos.push_back(_vec3(-12.092f, 0.f, 127.634f));
	m_vecPos.push_back(_vec3(-12.635f, 0.f, 133.52f));
	m_vecPos.push_back(_vec3(-13.182f, 0.f, 139.413f));
	m_vecPos.push_back(_vec3(-13.733f, 0.f, 145.315f));
	m_vecPos.push_back(_vec3(-14.289f, 0.f, 151.229f));

	m_bIsInit = true;
}

void CAction_PassWindow::Free()
{
	CAction::Free();
}
