#include "stdafx.h"
#include "..\Headers\Action_Search_Locker.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Slasher.h"
#include "Camper.h"
#include "Closet.h"

_USING(Client)

CAction_SearchLocker::CAction_SearchLocker()
{
}

HRESULT CAction_SearchLocker::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	if (!m_bIsInit)
		SetVectorPos();

	m_fIndex = 0.f;
	m_bIsPlaying = true;

	if (m_pGameObject->GetID() & SLASHER)
	{
		m_pSlasher = (CSlasher*)m_pGameObject;
		m_pSlasher->IsLockKey(true);

		CManagement* pManagement = CManagement::GetInstance();
		list<CGameObject*>* pObjList = pManagement->Get_ObjList(SCENE_STAGE, L"Layer_Camper");

		if (pObjList != nullptr)
		{
			for (auto pObj : *pObjList)
			{
				CCamper* pCamper = (CCamper*)pObj;
				if (pCamper->GetState() == AC::ClosetIdle)
				{
					CTransform* pTransform = (CTransform*)pCamper->Get_ComponentPointer(L"Com_Transform");
					_vec3 vCamperPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);

					CTransform* pSlasherTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
					_vec3 vSlasherPos = *pSlasherTransform->Get_StateInfo(CTransform::STATE_POSITION);

					_float fDist = D3DXVec3Length(&(vCamperPos - vSlasherPos));
					if (fDist <= 150.f)
					{
						m_pSlasher->Set_State(AW::TT_Grab_Locker);
						m_pSlasher->SetColl(false);
						m_iState = AW::TT_Grab_Locker;
						m_pCloset->SetState(CCloset::DoorSlasherPickCamper);
						m_pCamper = pCamper;
						m_pCamper->SetColl(false);
						m_pSlasher->Set_CarriedCamper(pCamper);
						CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
						m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);
						Send_ServerData();
						return NOERROR;
					}
				}
			}
		}

		m_pSlasher->Set_State(AW::TT_SearchLocker);
		m_iState = AW::TT_SearchLocker;
		m_pCloset->SetState(CCloset::DoorOpenEmpty);

	}
	else
	{
		m_pCamper = (CCamper*)m_pGameObject;
		m_pSlasher = nullptr;
		m_pCamper->IsLockKey(true);
		m_pCamper->SetColl(false);
		m_pCamper->Set_State(AC::TT_Grab_Locker);
		m_iState = AC::TT_Grab_Locker;
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
		m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);
	}
	
	Send_ServerData();
	return NOERROR;
}

_int CAction_SearchLocker::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay > 0.f)
		m_fDelay -= fTimeDelta;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;
	size_t iIndex = (size_t)m_fIndex;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (m_pSlasher != nullptr)
	{
		if (m_iState == AW::TT_SearchLocker)
		{
			if (pMeshCom->IsOverTime(0.6f))
				m_pCloset->SetState(CCloset::DoorIdleClosed);

			if (pMeshCom->IsOverTime(0.01f))
				return END_ACTION;

			if (m_fIndex >= 71.f)
				return END_ACTION;
		}
		else if (m_iState == AW::TT_Grab_Locker)
		{
			if (pMeshCom->IsOverTime(0.3f))
			{
				m_pCloset->SetState(CCloset::DoorIdleClosed);
				CSlasher* pSlasher = (CSlasher*)m_pGameObject;
				pSlasher->Set_State(AW::TT_Carry_Idle);
			}

			if (m_fIndex >= 151.f)
				return END_ACTION;

			if (m_vecSlasherPos.size() <= iIndex)
				return END_ACTION;

			_vec3 vLocalPos = m_vecSlasherPos[iIndex];
			CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

			_matrix mat = pTransform->Get_Matrix();
			D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

			_vec3 vPos = m_vOriginPos + vLocalPos;
			pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
		}
	}
	else
	{
		if (m_iState == AC::TT_Grab_Locker && pMeshCom->IsOverTime(0.3f))
		{
			CCamper* pCamper = (CCamper*)m_pGameObject;
			pCamper->Set_State(AC::TT_Carry_Idle);
		}

		if (m_fIndex >= 151.f)
			return END_ACTION;

		if (m_vecCamperPos.size() <= iIndex)
			return END_ACTION;

		_vec3 vLocalPos = m_vecCamperPos[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	}

	Send_ServerData();
	return UPDATE_ACTION;
}

void CAction_SearchLocker::End_Action()
{
	m_bIsPlaying = false;
	if (m_pSlasher == nullptr)
	{
		m_pCamper->Set_State(AC::TT_Carry_Idle);
		m_pCamper->IsLockKey(false);
		m_pCamper->SetCarry(true);
		m_fDelay = 1.f;
	}
	else
	{
		m_pSlasher->IsLockKey(false);
		if (m_iState == AW::TT_Grab_Locker)
		{
			m_pSlasher->SetColl(true);
			m_pSlasher->Set_Carry(true);
			m_pSlasher->Set_CarriedCamper(m_pCamper);
			m_fDelay = 1.f;
		}
	}
	m_pCloset = nullptr;
	m_pCamper = nullptr;
	m_pSlasher = nullptr;

	Send_ServerData();
}

void CAction_SearchLocker::Send_ServerData()
{
	if (m_pCloset != nullptr)
	{
		slasher_data.InterationObject = m_pCloset->GetID();
		slasher_data.InterationObjAnimation = m_pCloset->Get_CurAnimation();
	}
	else
		slasher_data.InterationObject = 0;

	if (m_pSlasher != nullptr && m_pCamper != nullptr)
	{
		slasher_data.SecondInterationObject = m_pCamper->GetID();
		slasher_data.SecondInterationObjAnimation = GRAB_LOCKER;
	}
}

void CAction_SearchLocker::SetVectorPos()
{
	m_vecCamperPos.reserve(151);

	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.017f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.046f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.085f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.135f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.195f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.264f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.344f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.432f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.528f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.634f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.747f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.868f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.996f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 1.131f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 1.273f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 1.421f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 1.575f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 1.734f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 1.899f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.069f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.243f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.421f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.604f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.789f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.909f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.911f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.816f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.648f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.43f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.186f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 1.937f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 1.707f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 1.466f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 1.178f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.858f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.52f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.179f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.15f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.453f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.74f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.023f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.288f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.521f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.706f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.866f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.01f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.108f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.127f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.035f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.952f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.923f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -1.943f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.006f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.107f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.24f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.398f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.577f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.77f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.972f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -3.176f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -3.378f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -3.571f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -3.227f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -2.141f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, -0.733f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.57f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 1.287f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 1.938f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.692f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.546f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.169f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.448f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.626f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.727f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.778f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.805f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.833f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.89f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 2.999f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 3.189f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 3.484f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 3.91f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 4.494f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 5.261f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 7.956f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 11.324f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 15.191f));
	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 19.383f));
	m_vecCamperPos.push_back(_vec3(-1.762f, 0.f, 23.727f));
	m_vecCamperPos.push_back(_vec3(-3.128f, 0.f, 29.68f));
	m_vecCamperPos.push_back(_vec3(-2.996f, 0.f, 38.591f));
	m_vecCamperPos.push_back(_vec3(-2.846f, 0.f, 45.976f));
	m_vecCamperPos.push_back(_vec3(-2.77f, 0.f, 52.957f));
	m_vecCamperPos.push_back(_vec3(-2.914f, 0.f, 59.547f));
	m_vecCamperPos.push_back(_vec3(-3.468f, 0.f, 65.92f));
	m_vecCamperPos.push_back(_vec3(-5.332f, 0.f, 71.971f));
	m_vecCamperPos.push_back(_vec3(-8.898f, 0.f, 76.499f));
	m_vecCamperPos.push_back(_vec3(-13.054f, 0.f, 79.562f));
	m_vecCamperPos.push_back(_vec3(-14.621f, 0.f, 83.18f));
	m_vecCamperPos.push_back(_vec3(-13.44f, 0.f, 86.093f));
	m_vecCamperPos.push_back(_vec3(-10.541f, 0.f, 88.35f));
	m_vecCamperPos.push_back(_vec3(-6.836f, 0.f, 90.288f));
	m_vecCamperPos.push_back(_vec3(-0.874f, 0.f, 92.247f));
	m_vecCamperPos.push_back(_vec3(0.944f, 0.f, 96.086f));
	m_vecCamperPos.push_back(_vec3(1.854f, 0.f, 108.22f));
	m_vecCamperPos.push_back(_vec3(2.072f, 0.f, 121.751f));
	m_vecCamperPos.push_back(_vec3(0.172f, 0.f, 135.684f));
	m_vecCamperPos.push_back(_vec3(-4.455f, 0.f, 145.086f));
	m_vecCamperPos.push_back(_vec3(-10.202f, 0.f, 153.208f));
	m_vecCamperPos.push_back(_vec3(-15.461f, 0.f, 159.461f));
	m_vecCamperPos.push_back(_vec3(-20.442f, 0.f, 160.664f));
	m_vecCamperPos.push_back(_vec3(-24.8f, 0.f, 157.777f));
	m_vecCamperPos.push_back(_vec3(-26.648f, 0.f, 155.6f));
	m_vecCamperPos.push_back(_vec3(-27.502f, 0.f, 154.17f));
	m_vecCamperPos.push_back(_vec3(-27.618f, 0.f, 154.4f));
	m_vecCamperPos.push_back(_vec3(-26.445f, 0.f, 155.284f));
	m_vecCamperPos.push_back(_vec3(-24.355f, 0.f, 155.616f));
	m_vecCamperPos.push_back(_vec3(-22.989f, 0.f, 155.843f));
	m_vecCamperPos.push_back(_vec3(-22.597f, 0.f, 156.05f));
	m_vecCamperPos.push_back(_vec3(-22.322f, 0.f, 156.3f));
	m_vecCamperPos.push_back(_vec3(-22.139f, 0.f, 156.547f));
	m_vecCamperPos.push_back(_vec3(-22.025f, 0.f, 156.85f));
	m_vecCamperPos.push_back(_vec3(-21.956f, 0.f, 157.085f));
	m_vecCamperPos.push_back(_vec3(-21.975f, 0.f, 157.25f));
	m_vecCamperPos.push_back(_vec3(-22.03f, 0.f, 157.244f));
	m_vecCamperPos.push_back(_vec3(-22.117f, 0.f, 157.226f));
	m_vecCamperPos.push_back(_vec3(-22.233f, 0.f, 157.196f));
	m_vecCamperPos.push_back(_vec3(-22.377f, 0.f, 157.157f));
	m_vecCamperPos.push_back(_vec3(-22.543f, 0.f, 157.11f));
	m_vecCamperPos.push_back(_vec3(-22.73f, 0.f, 157.054f));
	m_vecCamperPos.push_back(_vec3(-22.933f, 0.f, 156.992f));
	m_vecCamperPos.push_back(_vec3(-23.151f, 0.f, 156.924f));
	m_vecCamperPos.push_back(_vec3(-23.38f, 0.f, 156.851f));
	m_vecCamperPos.push_back(_vec3(-23.616f, 0.f, 156.775f));
	m_vecCamperPos.push_back(_vec3(-23.857f, 0.f, 156.696f));
	m_vecCamperPos.push_back(_vec3(-24.099f, 0.f, 156.615f));
	m_vecCamperPos.push_back(_vec3(-24.34f, 0.f, 156.535f));
	m_vecCamperPos.push_back(_vec3(-24.577f, 0.f, 156.454f));
	m_vecCamperPos.push_back(_vec3(-24.805f, 0.f, 156.375f));
	m_vecCamperPos.push_back(_vec3(-25.023f, 0.f, 156.299f));
	m_vecCamperPos.push_back(_vec3(-25.226f, 0.f, 156.226f));
	m_vecCamperPos.push_back(_vec3(-25.413f, 0.f, 156.158f));
	m_vecCamperPos.push_back(_vec3(-25.579f, 0.f, 156.096f));
	m_vecCamperPos.push_back(_vec3(-25.723f, 0.f, 156.04f));
	m_vecCamperPos.push_back(_vec3(-25.839f, 0.f, 155.993f));
	m_vecCamperPos.push_back(_vec3(-25.927f, 0.f, 155.924f));
	m_vecCamperPos.push_back(_vec3(-25.981f, 0.f, 155.906f));
	m_vecCamperPos.push_back(_vec3(-26.f, 0.f, 155.9f));

	m_vecSlasherPos.reserve(151);

	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -4.255f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -8.483f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -12.697f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -16.911f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -21.139f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -25.394f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -26.691f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -34.043f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -38.465f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -44.607f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -50.491f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -55.943f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -60.791f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -64.862f));
	m_vecSlasherPos.push_back(_vec3(0.f, 0.f, -67.983f));
	m_vecSlasherPos.push_back(_vec3(0.009f, 0.f, -69.983f));
	m_vecSlasherPos.push_back(_vec3(0.009f, 0.f, -70.687f));
	m_vecSlasherPos.push_back(_vec3(1.439f, 0.f, -70.801f));
	m_vecSlasherPos.push_back(_vec3(4.848f, 0.f, -71.071f));
	m_vecSlasherPos.push_back(_vec3(8.917f, 0.f, -71.394f));
	m_vecSlasherPos.push_back(_vec3(12.326f, 0.f, -71.664f));
	m_vecSlasherPos.push_back(_vec3(13.755f, 0.f, -71.777f));
	m_vecSlasherPos.push_back(_vec3(13.245f, 0.f, -71.55f));
	m_vecSlasherPos.push_back(_vec3(12.015f, 0.f, -70.968f));
	m_vecSlasherPos.push_back(_vec3(10.513f, 0.f, -70.177f));
	m_vecSlasherPos.push_back(_vec3(9.189f, 0.f, -69.325f));
	m_vecSlasherPos.push_back(_vec3(8.491f, 0.f, -68.558f));
	m_vecSlasherPos.push_back(_vec3(8.413f, 0.f, -67.73f));
	m_vecSlasherPos.push_back(_vec3(8.737f, 0.f, -67.178f));
	m_vecSlasherPos.push_back(_vec3(9.35f, 0.f, -66.975f));
	m_vecSlasherPos.push_back(_vec3(10.138f, 0.f, -66.989f));
	m_vecSlasherPos.push_back(_vec3(10.944f, 0.f, -66.905f));
	m_vecSlasherPos.push_back(_vec3(11.611f, 0.f, -66.6f));
	m_vecSlasherPos.push_back(_vec3(12.f, 0.f, -66.042f));
	m_vecSlasherPos.push_back(_vec3(12.035f, 0.f, -65.2f));
	m_vecSlasherPos.push_back(_vec3(11.94f, 0.f, -64.133f));
	m_vecSlasherPos.push_back(_vec3(11.619f, 0.f, -62.872f));
	m_vecSlasherPos.push_back(_vec3(11.218f, 0.f, -61.636f));
	m_vecSlasherPos.push_back(_vec3(10.832f, 0.f, -60.633f));
	m_vecSlasherPos.push_back(_vec3(10.458f, 0.f, -59.924f));
	m_vecSlasherPos.push_back(_vec3(10.101f, 0.f, -59.428f));
	m_vecSlasherPos.push_back(_vec3(9.804f, 0.f, -59.068f));
	m_vecSlasherPos.push_back(_vec3(9.642f, 0.f, -58.813f));
	m_vecSlasherPos.push_back(_vec3(9.557f, 0.f, -58.708f));
	m_vecSlasherPos.push_back(_vec3(9.474f, 0.f, -58.728f));
	m_vecSlasherPos.push_back(_vec3(9.371f, 0.f, -58.831f));
	m_vecSlasherPos.push_back(_vec3(9.275f, 0.f, -59.044f));
	m_vecSlasherPos.push_back(_vec3(9.147f, 0.f, -59.406f));
	m_vecSlasherPos.push_back(_vec3(8.997f, 0.f, -59.889f));
	m_vecSlasherPos.push_back(_vec3(8.862f, 0.f, -60.464f));
	m_vecSlasherPos.push_back(_vec3(8.774f, 0.f, -61.096f));
	m_vecSlasherPos.push_back(_vec3(8.735f, 0.f, -61.751f));
	m_vecSlasherPos.push_back(_vec3(8.729f, 0.f, -62.376f));
	m_vecSlasherPos.push_back(_vec3(8.727f, 0.f, -62.936f));
	m_vecSlasherPos.push_back(_vec3(8.717f, 0.f, -63.37f));
	m_vecSlasherPos.push_back(_vec3(8.701f, 0.f, -63.651f));
	m_vecSlasherPos.push_back(_vec3(8.633f, 0.f, -63.803f));
	m_vecSlasherPos.push_back(_vec3(8.503f, 0.f, -63.852f));
	m_vecSlasherPos.push_back(_vec3(8.35f, 0.f, -63.829f));
	m_vecSlasherPos.push_back(_vec3(8.142f, 0.f, -63.778f));
	m_vecSlasherPos.push_back(_vec3(7.891f, 0.f, -63.722f));
	m_vecSlasherPos.push_back(_vec3(7.643f, 0.f, -63.668f));
	m_vecSlasherPos.push_back(_vec3(7.41f, 0.f, -63.612f));
	m_vecSlasherPos.push_back(_vec3(7.23f, 0.f, -63.573f));
	m_vecSlasherPos.push_back(_vec3(7.112f, 0.f, -63.534f));

	m_bIsInit = true;
}

void CAction_SearchLocker::Free()
{
	CAction::Free();
}
