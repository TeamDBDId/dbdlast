#include "stdafx.h"
#include "..\Headers\Item.h"
#include "Management.h"
#include "Camper.h"

_USING(Client)


CItem::CItem(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CItem::CItem(const CItem & rhs)
	: CGameObject(rhs)
{
}

void CItem::Ready_Item(ITEMTYPE eType, _float fDurability)
{
	CManagement* pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return;

	pManagement->AddRef();

	switch (eType)
	{
	case AIDKIT:
		//m_pMeshCom = (CMesh_Static*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Weapon_Machete");
		//if (FAILED(Add_Component(L"Com_Mesh", m_pFrustumCom)))
			return;
		break;
	case TOOLKIT:
		//m_pMeshCom = (CMesh_Static*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Weapon_Machete");
		//if (FAILED(Add_Component(L"Com_Mesh", m_pFrustumCom)))
			return;
		break;
	case FLASHLIGHT:
		//m_pMeshCom = (CMesh_Static*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Weapon_Machete");
		//if (FAILED(Add_Component(L"Com_Mesh", m_pFrustumCom)))
			return;
		break;
	default:
		break;
	}

	m_fDurability = fDurability;

	Safe_Release(pManagement);
}

HRESULT CItem::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CItem::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;


	return NOERROR;
}

_int CItem::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return DEAD_OBJ;

	return _int();
}

_int CItem::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	if (nullptr != m_pRHandMatrix)
		m_matParent = *m_pRHandMatrix * m_pCamperTransform->Get_Matrix();

	_vec3 vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	D3DXVec3TransformCoord(&vPos, &vPos, &m_matParent);


	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CItem::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CItem::Use_Item(_vec3 * vDirection)
{
	switch (m_eType)
	{
	case AIDKIT:

		break;
	case TOOLKIT:

		break;
	case FLASHLIGHT:

		break;
	default:
		break;
	}

	if (m_fDurability <= 0.f)
		m_isDead = true;
}

void CItem::Approach_Camper(_uint iPlayerNum)
{
	CManagement* pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return;

	pManagement->AddRef();

	for (auto& pCamper : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper"))
	{
		if ((pCamper->GetID() - CAMPER) == (iPlayerNum - 1))
		{
			//m_pRHandMatrix = pCamper->Get_RHandMatrix();
			//m_pCamperTransform = (CTransform*)pCamper->Get_ComponentPointer(L"Com_Transform");
		}
	}

	Safe_Release(pManagement);
}

HRESULT CItem::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CItem::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);


	return NOERROR;
}

CItem * CItem::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CItem*	pInstance = new CItem(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CItem Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CItem::Clone_GameObject()
{
	CItem*	pInstance = new CItem(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CItem Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CItem::Free()
{
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pFrustumCom);

	CGameObject::Free();
}
