#include "stdafx.h"
#include "..\Headers\Action_StunDrop.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"

_USING(Client)

CAction_StunDrop::CAction_StunDrop()
{
}

HRESULT CAction_StunDrop::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;

	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);

	pCamper->Set_State(AC::TT_Stun_Drop);
	m_iState = AC::TT_Stun_Drop;

	return NOERROR;
}

_int CAction_StunDrop::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (m_iState == AC::TT_Stun_Drop && pMeshCom->IsOverTime(0.01f))
	{
		return END_ACTION;
	}

	return UPDATE_ACTION;
}

void CAction_StunDrop::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);
}

void CAction_StunDrop::Send_ServerData()
{
}

void CAction_StunDrop::SetVectorPos()
{
	m_vecPos.reserve(33);

	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(2.012f, 0.f, -0.228f));
	m_vecPos.push_back(_vec3(7.472f, 0.f, -0.849f));
	m_vecPos.push_back(_vec3(15.518f, 0.f, -1.763f));
	m_vecPos.push_back(_vec3(25.289f, 0.f, -2.872f));
	m_vecPos.push_back(_vec3(35.921f, 0.f, -4.08f));
	m_vecPos.push_back(_vec3(46.554f, 0.f, -5.288f));
	m_vecPos.push_back(_vec3(56.325f, 0.f, -6.398f));
	m_vecPos.push_back(_vec3(64.371f, 0.f, -7.311f));
	m_vecPos.push_back(_vec3(69.831f, 0.f, -7.932f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecPos.push_back(_vec3(71.843f, 0.f, -8.16f));

	m_bIsInit = true;
}

void CAction_StunDrop::Free()
{
	CAction::Free();
}
