#include "stdafx.h"
#include "..\Headers\Action_HealCamper.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"

_USING(Client)

CAction_HealCamper::CAction_HealCamper()
{
}

HRESULT CAction_HealCamper::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;

	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	pCamper->Set_State(AC::HealCamper);
	m_iState = AC::HealCamper;

	Send_ServerData();
	return NOERROR;
}

_int CAction_HealCamper::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	if (server_data.Campers[m_pCamper->GetID() - CAMPER].Packet == END_HEAL)
		return END_ACTION;

	if (server_data.Campers[m_pCamper->GetID() - CAMPER].fHeal >= 20.f)
		return END_ACTION;

	if (!KEYMGR->MousePressing(0))
	{
		camper_data.SecondInterationObject = m_pCamper->GetID();
		camper_data.SecondInterationObjAnimation = END_HEAL;
		return END_ACTION;
	}

	Send_ServerData();
	return UPDATE_ACTION;
}

void CAction_HealCamper::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);
	pCamper->SetCurCondition(pCamper->GetOldCondition());
}

void CAction_HealCamper::Send_ServerData()
{
	if (m_pCamper != nullptr)
	{
		camper_data.SecondInterationObject = m_pCamper->GetID();
		camper_data.SecondInterationObjAnimation = BEING_HEAL;
	}
	else
		camper_data.InterationObject = 0;
}

void CAction_HealCamper::Free()
{
	CAction::Free();
}
