#include "stdafx.h"
#include "..\Headers\UI_Interaction.h"
#include "Management.h"
#include "UI_Texture.h"
#include "Generator.h"
#include "Math_Manager.h"
#include "Camper.h"
#include "Plank.h"
#include "Totem.h"
#include "Chest.h"
#include "Action_HideCloset.h"
#include "Slasher.h"
_USING(Client);




CUI_Interaction::CUI_Interaction(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_Interaction::CUI_Interaction(const CUI_Interaction & rhs)
	: CGameObject(rhs)
	, m_eSkillCheckState(rhs.m_eSkillCheckState)
	, m_fSkillCheckTime(rhs.m_fSkillCheckTime)
	, m_pPlayer(rhs.m_pPlayer)
	, m_pCollisionObjtect(rhs.m_pCollisionObjtect)
{
}


HRESULT CUI_Interaction::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CUI_Interaction::Ready_GameObject()
{
	Set_ProgressBar();
	m_fProgress = 0.f;
	m_PlayerNum = exPlayerNumber;
	return NOERROR;
}

_int CUI_Interaction::Update_GameObject(const _float & fTimeDelta)
{
	if (m_fSkillCheckTime != 0.f)
	{
		m_fSkillCheckTime -= fTimeDelta;
		if (m_fSkillCheckTime <= 0.f)
			m_fSkillCheckTime = 0.f;
	}
	Set_SkillCheck();
	Interact_Check();
	Update_SkillCheck(fTimeDelta);
	Update_AddButton_Camper();
	Update_ButtonPos();
	return _int();
}

_int CUI_Interaction::LastUpdate_GameObject(const _float & fTimeDelta)
{
	Check_CollisionObject();
	return _int();
}

void CUI_Interaction::Add_Button(wstring TextureName, wstring TextName)
{
	auto& iter = m_mapButton.find(TextureName);
	if (iter != m_mapButton.end())
		return;

	GET_INSTANCE_MANAGEMENT
	ButtonInfo Button;

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&Button.pButtonTexture)))
		return;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&Button.pButtonText)))
		return;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&Button.pMotionText)))
		return;


	if(TextureName == L"Space")
		Button.pButtonTexture->Set_TexName(L"SkillCheck_SpaceBar.tga");
	else if(TextureName == L"M1" || TextureName == L"M2")
		Button.pButtonTexture->Set_TexName(L"Button_Mouse.tga");
	else
		Button.pButtonTexture->Set_TexName(L"Button_Keyboard.tga");


	Button.pButtonTexture->Set_Scale(_vec2(0.6667f, 0.6667f));
	Button.pButtonText->Set_Font(TextureName, _vec2(10.f, 12.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	Button.pMotionText->Set_Font(TextName, _vec2(14.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	
	Button.pButtonTexture->Set_OneTimeRendering();
	Button.pMotionText->Set_OneTimeRendering();
	Button.pButtonText->Set_OneTimeRendering();
	
	m_mapButton.insert({ TextureName,Button });
	Safe_Release(pManagement);
}

void CUI_Interaction::Interact_Check()
{
	m_InteractionObject = camper_data.InterationObject;
	_uint iItem = camper_data.iItem;

	GET_INSTANCE_MANAGEMENT;
	CGameObject* pGameObject = pManagement->Get_GameObjectFromObjectID(SCENE_STAGE, L"Layer_GameObject", m_InteractionObject);
	Safe_Release(pManagement);
	
	
	_vec2 Start = { 0.42f* g_iBackCX, 0.827786f* g_iBackCY };
	m_ProgressBar[Bar]->IsRendering(true);
	m_ProgressBar[Base]->IsRendering(true);
	m_ProgressBar[Texture]->IsRendering(true);
	m_ProgressBar[Text]->IsRendering(true);
	if (m_InteractionObject & GENERATOR)
	{
		m_ProgressBar[Bar]->Set_UV(_vec2(pGameObject->Get_ProgressTime() / pGameObject->Get_MaxProgressTime(), 1.f));
		m_ProgressBar[Text]->Set_Font(wstring(L"수리"), _vec2(10.f, 12.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		if (iItem == 0)
			m_ProgressBar[Texture]->Set_TexName(L"ProgressBar_Texture_Hand.tga");
		IsSkillCheck();
	}
	else if (m_InteractionObject & TOTEM)
	{
		m_ProgressBar[Bar]->Set_UV(_vec2(pGameObject->Get_ProgressTime() / pGameObject->Get_MaxProgressTime(), 1.f));
		m_ProgressBar[Text]->Set_Font(wstring(L"토템 정화"), _vec2(10.f, 12.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		m_ProgressBar[Texture]->Set_TexName(L"ProgressBar_Texture_Hand.tga");
	}
	else if (m_InteractionObject & CHEST)
	{
		m_ProgressBar[Bar]->Set_UV(_vec2(pGameObject->Get_ProgressTime() / pGameObject->Get_MaxProgressTime(), 1.f));
		m_ProgressBar[Text]->Set_Font(wstring(L"찾기"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		m_ProgressBar[Texture]->Set_TexName(L"ProgressBar_Texture_Hand.tga");
	}
	else if (m_InteractionObject & CAMPER)
	{
		m_ProgressBar[Bar]->Set_UV(_vec2(dynamic_cast<CCamper*>(pGameObject)->GetHeal() / dynamic_cast<CCamper*>(pGameObject)->GetMaxHeal(), 1.f));
		m_ProgressBar[Text]->Set_Font(wstring(L"아무거나적어"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		if (iItem == 0)
			m_ProgressBar[Texture]->Set_TexName(L"ProgressBar_Texture_Hand.tga");
		IsSkillCheck();
	}
	else
	{
		m_ProgressBar[Base]->IsRendering(false);
		m_ProgressBar[Bar]->IsRendering(false);
		m_ProgressBar[Texture]->IsRendering(false);
		m_ProgressBar[Text]->IsRendering(false);
	}

}

void CUI_Interaction::Update_SkillCheck(const _float& fTimeDelta)
{
	if (m_eSkillCheckState != SC_Check)
		return;
	if (m_InteractionObject == 0)
	{
		m_eSkillCheckState = SC_NONE;
		camper_data.Packet = FAILED_SKILL_CHECK;//실패

		for (_int i = 0; i < SkillCheckEnd; i++)
		{
			camper_data.Packet = FAILED_SKILL_CHECK;
			m_SkillCheck[i]->Set_DeadTime(1.f);
			m_SkillCheck[i] = nullptr;
		}
		return;
	}

	_float fGridRotation = m_SkillCheck[S_GridLine]->Get_Rotation();
	
	if (KEYMGR->KeyDown(DIK_SPACE) || fGridRotation == 2.f*D3DX_PI)
	{
		m_eSkillCheckState = SC_NONE;
		m_fSkillCheckTime = 3.f;
	
		_float fStartAngle = m_SkillCheck[S_Full]->Get_Angle().x;
		if(fGridRotation < fStartAngle || fGridRotation > fStartAngle + D3DXToRadian(45.f))
			camper_data.Packet = FAILED_SKILL_CHECK;//실패
		else if (fGridRotation <= fStartAngle + D3DXToRadian(11.f))
			camper_data.Packet = 11;//대성공
		else
		{
			//성공
		}
		for (int i = 0; i < SkillCheckEnd; i++)
		{
			m_SkillCheck[i]->Set_DeadTime(1.f);
			m_SkillCheck[i] = nullptr;
		}
		return;
	}

	m_SkillCheck[S_GridLine]->Set_Rotation(fTimeDelta * 5.f);
}

void CUI_Interaction::Update_ButtonPos()
{
	_float m_StartPos = (0.5f - (m_mapButton.size()*0.05f)) * g_iBackCX;

	for (auto& iter : m_mapButton)
	{
		_float ButtonScaleX = iter.second.pButtonTexture->Get_Scale().x;
		_float MotionTextScaleX = iter.second.pMotionText->Get_Scale().x;
		_float ButtonPosX = m_StartPos + (ButtonScaleX*0.5f);
		_float TextPosX = ButtonPosX + (ButtonScaleX*0.5f) + (g_iBackCX * 0.001f) + (MotionTextScaleX * 0.5f);
		wstring TextureName = iter.second.pButtonTexture->Get_TexName();
		_float ButtonTexPosX = 0.f;
		if (TextureName == L"SkillCheck_SpaceBar.tga")
			ButtonTexPosX = -0.002f *g_iBackCX;
		else if (TextureName == L"SkillCheck_SpaceBar.tga")
			ButtonTexPosX = -0.002f *g_iBackCX;
		else
			ButtonTexPosX = -0.002f *g_iBackCX;

		iter.second.pButtonTexture->Set_Pos(_vec2(ButtonPosX, 0.92f *g_iBackCY));
		iter.second.pButtonText->Set_Pos(_vec2(ButtonPosX+ ButtonTexPosX, 0.915f *g_iBackCY));
		iter.second.pMotionText->Set_Pos(_vec2(TextPosX, 0.918f *g_iBackCY));

		m_StartPos = max(m_StartPos + 0.1f * g_iBackCX, (TextPosX + (MotionTextScaleX*0.5f) + (g_iBackCX * 0.005f)));
	}
	m_mapButton.clear();
}

void CUI_Interaction::Update_AddButton_Camper()
{
	if (m_PlayerNum >= 5)
		return;
	if (m_pPlayer == nullptr)
		return;

	if (m_pCollisionObjtect == nullptr)
	{
		if (m_InteractionObject & CLOSET)
		{
			if (((CCamper*)m_pPlayer)->GetState() == AC::ClosetIdle)
				Add_Button(L"Space", L"나가기");
		}
		return;
	}

	_int iCollisionObject = m_pCollisionObjtect->GetID();

	if (!(((CCamper*)m_pPlayer)->GetOldCondition() == CCamper::HEALTHY || ((CCamper*)m_pPlayer)->GetOldCondition() == CCamper::INJURED))
		return;

	if (iCollisionObject != m_InteractionObject)
	{
		if (iCollisionObject & GENERATOR)
		{
			if (GET_INSTANCE(CUIManager)->Get_GeneratorNum() <= 0)
				return;
			_float fProgressTime = m_pCollisionObjtect->Get_ProgressTime();
			_float fMaxProgressTime = m_pCollisionObjtect->Get_MaxProgressTime();
			if (fProgressTime >= fMaxProgressTime)
				return;
			Add_Button(L"M1", L"수리");
		}
		else if (iCollisionObject & CLOSET)
			Add_Button(L"Space", L"숨기");
		else if (iCollisionObject & TOTEM)
			Add_Button(L"M1", L"토템 정화");
		else if (iCollisionObject & CHEST)
		{
			_float fProgressTime = m_pCollisionObjtect->Get_ProgressTime();
			_float fMaxProgressTime = m_pCollisionObjtect->Get_MaxProgressTime();
			if (fProgressTime >= fMaxProgressTime)
				return;
			Add_Button(L"M1", L"찾기");
		}
		else if (iCollisionObject & PLANK)
		{
			if (m_pCollisionObjtect->Get_IsFall())
				Add_Button(L"Space", L"넘기");
			else
				Add_Button(L"Space", L"넘어뜨리기");
		}
		else if (iCollisionObject & WINDOW)
			Add_Button(L"Space", L"넘기");
	}




}

void CUI_Interaction::Update_AddButton_Slasher()
{
}

void CUI_Interaction::Check_CollisionObject()
{
	if (m_pPlayer == nullptr)
	{
		GET_INSTANCE_MANAGEMENT;
		if (m_PlayerNum == 5)
			m_pPlayer = pManagement->Get_GameObjectFromObjectID(SCENE_STAGE, L"Layer_Slasher", SLASHER);
		else
			m_pPlayer = pManagement->Get_GameObjectFromObjectID(SCENE_STAGE, L"Layer_Camper", CAMPER + (m_PlayerNum - 1));
		Safe_Release(pManagement);
	}

	if (m_pPlayer == nullptr)
		return;

	m_pCollisionObjtect = nullptr;
	if (m_PlayerNum == 5);
		//m_pCollisionObjtect = ((CSlasher*)pPlayer)->
	else
		m_pCollisionObjtect = ((CCamper*)m_pPlayer)->GetTargetOfInteraction();
}

HRESULT CUI_Interaction::Set_ProgressBar()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	for (_uint i = 0; i < ProgressBarEnd; i++)
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_ProgressBar[i])))
			return E_FAIL;
		m_ProgressBar[i]->IsRendering(false);
	}

	_vec2 vPos = { 0.5f, 0.85f };
	m_ProgressBar[Base]->Set_TexName(wstring(L"ProgressBar_Base.tga"));
	m_ProgressBar[Base]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_ProgressBar[Base]->Set_Scale(_vec2(0.6667f, 0.6667f));

	vPos = { 0.4938f, 0.85f };
	m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Red.tga"));
	m_ProgressBar[Bar]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_ProgressBar[Bar]->Set_Scale(_vec2(0.6667f, 0.5f));
	m_ProgressBar[Bar]->Set_UV(_vec2(0.3f, 1.f));

	vPos = { 0.4076f, 0.8358f };
	m_ProgressBar[Texture]->Set_TexName(wstring(L"ProgressBar_Texture_Hand.tga"));
	m_ProgressBar[Texture]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_ProgressBar[Texture]->Set_Scale(_vec2(0.2f, 0.2f));

	vPos = { 0.42931f, 0.827786f };
	m_ProgressBar[Text]->Set_Font(wstring(L"수리"), _vec2(10.f, 12.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	m_ProgressBar[Text]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CUI_Interaction::Set_SkillCheck()
{
	if (m_eSkillCheckState != SC_Ready || m_fSkillCheckTime != 0.f)
		return NOERROR;
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	for (_uint i = 0; i < SkillCheckEnd; i++)
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_SkillCheck[i])))
			return E_FAIL;
		m_SkillCheck[i]->IsRendering(true);
	}

	_float fStartRadian = Math_Manager::CalRandFloatFromTo((3.1415926f * 0.5f), ((2.f*3.1415926f) - D3DXToRadian(50.f)));
	_vec2 vPos = { 0.5f, 0.5f };
	m_SkillCheck[S_Base]->Set_TexName(wstring(L"SkillCheck_Base.tga"));
	m_SkillCheck[S_Base]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_SkillCheck[S_Base]->Set_Scale(_vec2(0.6667f, 0.6667f));

	m_SkillCheck[S_Full]->Set_TexName(wstring(L"SkillCheck_Full.tga"));
	m_SkillCheck[S_Full]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_SkillCheck[S_Full]->Set_Scale(_vec2(0.6667f, 0.6667f));
	m_SkillCheck[S_Full]->Set_Angle(_vec2(fStartRadian, fStartRadian + D3DXToRadian(11.f)));

	m_SkillCheck[S_NoneFull]->Set_TexName(wstring(L"SkillCheck_NoneFull.tga"));
	m_SkillCheck[S_NoneFull]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_SkillCheck[S_NoneFull]->Set_Scale(_vec2(0.6667f, 0.6667f));
	m_SkillCheck[S_NoneFull]->Set_Angle(_vec2(fStartRadian + D3DXToRadian(11.f), fStartRadian + D3DXToRadian(45.f)));

	m_SkillCheck[S_End]->Set_TexName(wstring(L"SkillCheck_End.tga"));
	m_SkillCheck[S_End]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_SkillCheck[S_End]->Set_Scale(_vec2(0.6667f, 0.6667f));
	m_SkillCheck[S_End]->Set_Rotation(fStartRadian + D3DXToRadian(45.f));

	m_SkillCheck[S_GridLine]->Set_TexName(wstring(L"SkillCheck_GridLine.tga"));
	m_SkillCheck[S_GridLine]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_SkillCheck[S_GridLine]->Set_Scale(_vec2(0.6667f, 0.6667f));

	vPos = { 0.5f, 0.5058f };
	m_SkillCheck[S_SpaceBar]->Set_TexName(wstring(L"SkillCheck_SpaceBar.tga"));
	m_SkillCheck[S_SpaceBar]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_SkillCheck[S_SpaceBar]->Set_Scale(_vec2(0.6667f, 0.6667f));

	vPos = { 0.5f, 0.503284f };
	m_SkillCheck[S_Text]->Set_Font(wstring(L"Space"), _vec2(10.f, 14.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	m_SkillCheck[S_Text]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));


	Safe_Release(pManagement);
	m_eSkillCheckState = SC_Check;

	return NOERROR;
}

_bool CUI_Interaction::IsSkillCheck()
{
	if (m_eSkillCheckState != SC_NONE)
		return false;
	if (m_fSkillCheckTime != 0.f)
		return false;

	_int Random = Math_Manager::CalRandIntFromTo(0, 30);

	if (Random == 1)
	{
		m_eSkillCheckState = SC_Ready;
		m_fSkillCheckTime = 1.f;
		return true;
	}
	return false;
}

CUI_Interaction * CUI_Interaction::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_Interaction*	pInstance = new CUI_Interaction(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_Interaction Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_Interaction::Clone_GameObject()
{
	CUI_Interaction*	pInstance = new CUI_Interaction(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_Interaction Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CUI_Interaction::Free()
{
	CGameObject::Free();
}
