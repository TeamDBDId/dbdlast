#include "stdafx.h"
#include "..\Headers\Action_WindowVault.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Slasher.h"

_USING(Client)

CAction_WindowVault::CAction_WindowVault()
{
}

HRESULT CAction_WindowVault::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	pSlasher->IsLockKey(true);
	m_bIsPlaying = true;
	m_fIndex = 0.f;

	if (!m_bIsInit)
		SetVectorPos();

	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);

	pSlasher->Set_State(AW::TT_WindowVault);
	m_iState = AW::TT_WindowVault;
	

	return NOERROR;
}

_int CAction_WindowVault::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;

	size_t iIndex = (size_t)m_fIndex;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (pMeshCom->IsOverTime(0.3f))
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->Set_State(AW::TW_Idle);
	}

	if (m_vecPos.size() <= iIndex)
		return END_ACTION;

	if (m_fIndex >= 45.f)
		return END_ACTION;

	_vec3 vLocalPos = m_vecPos[iIndex];
	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

	_matrix mat = pTransform->Get_Matrix();
	D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

	_vec3 vPos = m_vOriginPos + vLocalPos;
	pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

	return UPDATE_ACTION;
}

void CAction_WindowVault::End_Action()
{
	m_bIsPlaying = false;
	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	pSlasher->IsLockKey(false);
}

void CAction_WindowVault::Send_ServerData()
{
}

void CAction_WindowVault::SetVectorPos()
{
	m_vecPos.reserve(45);

	m_vecPos.push_back(_vec3(0.f, 0.f, -44.636f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 36.989f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 31.189f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 26.235f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 21.706f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 17.041f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 12.262f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 7.272f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 1.457f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 4.262f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 9.321f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 14.127f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 19.191f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 24.202f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 28.841f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 33.345f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 37.953f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 42.885f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 48.626f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 55.183f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 60.974f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 64.731f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 66.949f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 68.429f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 70.873f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 74.679f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 78.812f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 83.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 87.025f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 90.456f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 92.999f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 94.992f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 96.872f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 99.086f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 101.726f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 104.578f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 107.83f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 111.075f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 114.842f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 119.591f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 125.14f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 131.536f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 138.252f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 145.128f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 152.596f));

	m_bIsInit = true;
}

void CAction_WindowVault::Free()
{
	CAction::Free();
}
