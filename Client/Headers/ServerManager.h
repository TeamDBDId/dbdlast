#pragma once

#include "Base.h"
#include "Defines.h"

_BEGIN(Client)
class CServerManager : public CBase
{
	_DECLARE_SINGLETON(CServerManager)
private:
	explicit CServerManager();
	~CServerManager() = default;
public:
	void ErrorHandling(char *message);
	SOCKET Ready_RobbyServer();
	SOCKET Ready_Server();
	void Data_ExchangeLobby(SOCKET sock, LobbyData &lPlayer, Client_Data &cd, LServer_Data &lserver_data);
	void Data_Exchange(SOCKET sock, CamperData &player, Client_Data &cd, Server_Data &server_data, SlasherData slasher = SlasherData());
public:
	SOCKETINFO dataBuf;
	SOCKETINFOGAME GdataBuf;
	int sendBytes = 0;
	int recvBytes = 0;
	int flags = 0;
	WSAEVENT event;
	WSAOVERLAPPED overlapped;
	Client_Data *cdData;
	Server_Data *spData;

	LServer_Data* rspData;

	virtual void Free() override;
};

_END