#pragma once

#include "Defines.h"
#include "Camera.h"

_BEGIN(Engine)
class CFrustum;
class CCollider;
_END

_BEGIN(Client)
class CCamera_Camper final : public CCamera
{
private:
	explicit CCamera_Camper(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCamera_Camper(const CCamera_Camper& rhs);
	virtual ~CCamera_Camper() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	void	Set_OrbitMatrix();
public:
	_bool	m_bIsControl = false;
private:
	CFrustum*		m_pFrustumCom = nullptr;
private:
	const _matrix*	m_PlayerRoot = nullptr;
	const _matrix*	m_PlayerMat = nullptr;
	_float			m_fDistance = 0.f;
	_float			m_fSpeed = 0.f;
	POINT			m_ptMouse;
	_vec3			m_vCamPos = _vec3();
	_float			m_MoveX = 0;
	_float			m_MoveY = 0;
	_bool			m_bStart = false;
private:
	HRESULT Ready_Component();
	void	Start_Camera(const _float& fTimeDelta);
	_bool	CheckCollision(_vec3* pOut);
public:
	static CCamera_Camper* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
protected:
	virtual void Free();
};

_END