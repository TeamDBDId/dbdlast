#pragma once
#include "GameObject.h"

_BEGIN(Client)

class CUI_Texture;
class CUI_OverlayMenu : public CGameObject
{
public:
	enum ButtonState { Touch, UnTouch, Click };
	enum Lobby2Button { NONE, BACK, READY, CHANGE_JOB, CHANGE_CHARICTER};
	enum LobbyState{ None, Go_Back, Ready };
private:
	explicit CUI_OverlayMenu(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_OverlayMenu(const CUI_OverlayMenu& rhs);
	virtual ~CUI_OverlayMenu() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);

	_uint Get_State() { return m_iClickButton; }
	void Set_Job(_bool Job) { m_JobState = Job; m_OldJobState = !m_JobState; }
private:
	void Ready_Check();
	void Mouse_Check();
	void Lobby_Check();
	void Server_Check();

	void Set_SideButton();
	void Set_StartIcon();
	void Set_OtherButton();
	void Set_Background();

	void Update_Job();
	void Set_Camper();
	void Set_Slasher();


	void Button_L2_Change_Job(ButtonState State);
	void Button_L2_Change_Charicter(ButtonState State);
	void Button_L2_Go_Back(ButtonState State);
	void Button_L2_Ready(ButtonState State);

	CUI_Texture* Find_UITexture(wstring UIName);
	void Delete_UI_Texture(wstring UIName);
	void Delete_All();
private:
	map<wstring, CUI_Texture*> m_UITexture;

	_uint				m_iClickButton = NONE;
	_bool				m_JobState = false;
	_bool				m_OldJobState = false;
	
	_uint				m_CurButton = NONE;
	_uint				m_OldButton = NONE;

	_uint				m_iMaxPlayer = 1;
	_uint				m_iCurReadyPlayer = 0;

	_bool				m_IsReady = false;

public:
	static CUI_OverlayMenu* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END