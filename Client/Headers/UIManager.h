#pragma once
#include "Base.h"
#include "GameObject.h"

_BEGIN(Client)

class CUI_Interaction;
class CUIManager : public CBase
{
	_DECLARE_SINGLETON(CUIManager)
private:
	explicit CUIManager();
	virtual ~CUIManager() = default;

public:
	void Set_UIInteraction(CUI_Interaction* pUIInteraction) { m_pUIInteraction = pUIInteraction; }
	void Add_Button(const wstring& TextureName, const wstring& TextName);
	void Set_GeneratorNum(_uint Num) { m_iGeneratorNum = Num; }
	_uint Get_GeneratorNum() { return m_iGeneratorNum; }
private:
	CUI_Interaction* m_pUIInteraction = nullptr;
	_uint m_iGeneratorNum = 5;

private:
	virtual void Free() override;

};
_END