#pragma once

#include "Defines.h"
#include "GameObject.h"

_BEGIN(Engine)

class CRenderer;
class CShader;
class CManagement;
_END

_BEGIN(Client)
class CBaseEffect : public CGameObject
{


protected:
	explicit CBaseEffect(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CBaseEffect(const CBaseEffect& _rhs);
	virtual ~CBaseEffect() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();

public:
	virtual void Set_Pos(const _vec3& _vPos) = 0;
protected:
	void PlaySound(const _tchar* pLayerTag);
protected:
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;



	
	_float				m_fTime = 0.f;
	_float				m_fLifeTime = 0.f;
	

	
protected:
	virtual HRESULT Ready_Component(const _tchar* _shader);
	virtual HRESULT SetUp_ContantTable(LPD3DXEFFECT _pEffect);
	virtual void Free();	

};

_END