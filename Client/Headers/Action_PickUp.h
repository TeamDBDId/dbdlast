#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CCamper;
class CSlasher;
class CAction_PickUp final : public CAction
{
public:
	explicit CAction_PickUp();
	virtual ~CAction_PickUp() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
	void SetCamper(CGameObject* pCamper) { m_pCamper = (CCamper*)pCamper; }
private:
	_float	m_fDelay = 0.f;
	_int m_iState = 0;
	_bool m_bIsInit = false;
	CCamper*		m_pCamper = nullptr;
	CSlasher*		m_pSlasher = nullptr;
protected:
	virtual void Free();
};

_END