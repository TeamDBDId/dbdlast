#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "Transform.h"

_BEGIN(Engine)
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CMesh_Static;
class CFrustum;
_END

_BEGIN(Client)
class CItem : public CGameObject
{
	enum ITEMTYPE { AIDKIT, TOOLKIT, FLASHLIGHT, ITEMEND };
private:
	explicit CItem(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CItem(const CItem& rhs);
	virtual ~CItem() = default;
public:
	void Ready_Item(ITEMTYPE eType, _float fDurability);
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
public:
	void Use_Item(_vec3* vDirection = nullptr);
	void Approach_Camper(_uint iPlayerNum);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Static*		m_pMeshCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	const _matrix*		m_pRHandMatrix = nullptr;
	_matrix				m_matParent;
	CTransform*			m_pCamperTransform = nullptr;
	_uint	 m_iPlayerNum = 0;
	ITEMTYPE m_eType = ITEMEND;
	_float	 m_fDurability = 0.f;
public:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint& iAttributeID);
public:
	static CItem* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();
};

_END
