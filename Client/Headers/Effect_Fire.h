#pragma once

#include "Defines.h"
#include "BaseEffect.h"

_BEGIN(Engine)
class CTransform;
class CTexture;
class CBuffer_RcTex;
//class CMesh_Static;
_END

_BEGIN(Client)
class CEffect_Fire final : public CBaseEffect
{
private:
	explicit CEffect_Fire(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CEffect_Fire(const CEffect_Fire& _rhs);
	virtual ~CEffect_Fire() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();

public:
	virtual void Set_Pos(const _vec3& _vPos);


private:
	const _uint			TRANSCNT = 2;
	CTransform*			m_pTransformCom[2] = {};
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	

	const _uint			TEXCNT = 3;
	CTexture*			m_pTextureCom[3] = {};
private:
	void Render_Buffer(const _uint& _iIdx);
private:
	virtual HRESULT Ready_Component(); // 이 객체안에서 사용할 컴포넌트들을 원형객체로부터 복사(공유) 해서 온다.
	virtual HRESULT SetUp_ContantTable(LPD3DXEFFECT _pEffect,const _uint& _iIdx);
public:
	static CEffect_Fire* Create(LPDIRECT3DDEVICE9 _pGDevice);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END