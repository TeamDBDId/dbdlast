#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "Transform.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
class CShader;
_END

_BEGIN(Client)

class CUIButton :
	public CGameObject
{
public:
	explicit CUIButton(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUIButton(const CUIButton& rhs);
	virtual ~CUIButton() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
public:
	void SetInfo(_float fX, _float fY, _float SizeX, _float SizeY, SCENEID eID, vector<_tchar*> vecTexTag
		, _uint ShaderPass = 0, void(*funcClickEvent)() = NULL, void(*funcHomingEvent)() = NULL, void(*funcCustomEvent)() = NULL);

	void SetInfo(_float fX, _float fY, _float SizeX, _float SizeY, SCENEID eID, _tchar* TextureTag
		, _uint ShaderPass = 0, void(*funcClickEvent)() = NULL, void(*funcHomingEvent)() = NULL, void(*funcCustomEvent)() = NULL);

	void SetFloor(_uint floor) { m_UIDist = floor; }
	void SetOriginalSize(_float Value);

	void ChangePass(_uint ShaderPass) { m_iPass = ShaderPass; }
	void ChangeSize(_float SizeX, _float SizeY) { m_fSizeX = SizeX; m_fSizeY = SizeY; }
	void ChangePosition(_float fX, _float fY) { m_fX = fX; m_fY = fY; }

	void AddClickEvent(void(*funcClickEvent)());
	void AddHomingEvent(void(*funcHomingEvent)());
	void AddCustomEvent(void(*funcCustomEvent)());
	void AddIdleEvent(void(*funcIdleEvent)());

	void DeleteClickEvent() { bClickEvent = false; ClickEvent = NULL; }
	void DeleteHomingEvent() { bHomingEvent = false; HomingEvent = NULL; }
	void DeleteCustomEvent() { bCustomEvent = false; CustomEvent = NULL; }
	void DeleteIdleEvent() { bIdleEvent = false; IdleEvent = NULL; }

	void SetRender(_bool isRender);
	void SetAlpha(_bool isAlpha) { bAlpha = isAlpha; }
	void SetGroupSync(_bool isSync) { bGroupSync = isSync; }
	void SetGroupFirst(_bool isFirst) { bFirst = isFirst; }
	
	void SetFadeIn(_bool isFadeIn) { bFadeIn = isFadeIn; }
	void SetFadeOut(_bool isFadeOut) { bFadeOut = isFadeOut; }

	void SetFadeInTime(_float fTime) { m_fFadeInTime = fTime; }
	void SetFadeOutTime(_float fTime) { m_fFadeOutTime = fTime; }

	void AddGroupList(CUIButton* pUI) { GroupList.push_back(pUI); }
	void SetRotate(_float fDegree) { m_fDegree = D3DXToRadian(fDegree); }

	void SetMultiTexIndex(_uint i) { m_iMultiTextureIndex = i; }
	void SetMultiTex(_bool b) { bUseMultiTex = b; }
public:
	void DefaultHoming();
	void DefaultIdle();
public:
	_bool GetIsHoming() { return bHoming; }
	_bool GetDefaultSync() { return bGroupSync; }
	_vec2 GetPosition() { return _vec2(m_fX, m_fY); }
	_vec2 GetSize() { return _vec2(m_fSizeX, m_fSizeY); }
	_float GetDegree() { return m_fDegree; }
public:
	map<char*, _bool>		m_mapbool;
	map<char*, _int>		m_mapint;
	map<char*, _float>		m_mapfloat;
	map<char*, _vec4>		m_mapvector;
	map<char*, LPDIRECT3DTEXTURE9> m_mapTextures;
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	vector<CTexture*>	m_vecTexCom = vector<CTexture*>();
private:
	_bool				bUseMultiTex = false;
	_uint				m_iMultiTextureIndex = 0;
	_bool				bFirst = false;
	_bool				bRender = true;
	_bool				bAlpha = true;

	_float				m_fX, m_fY, m_fSizeX, m_fSizeY;
	_uint				m_iPass = 0;
	_uint				m_iTextureIndex = 0;

	_float				m_fCurFadeTime = 0.3f;
	_float				m_fFadeInTime = 0.3f;
	_float				m_fFadeOutTime = 0.f;
	_float				m_fDegree = 0.f;
private:
	_bool				Button_Click();
	_bool				Button_Homing();

	_bool			bFadeIn = true;
	_bool			bFadeOut = true;
	_bool			bGroupSync = true;
	_bool			bClickEvent = false;
	_bool			bHomingEvent = false;
	_bool			bCustomEvent = false;
	_bool			bIdleEvent = false;
	_bool			bHoming = false;
	void(*ClickEvent)();
	void(*HomingEvent)();
	void(*CustomEvent)();

	void(*IdleEvent)();
private:
	list<CUIButton*> GroupList;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, _uint iIndex);
	HRESULT SetUp_ConstantMultiTable(LPD3DXEFFECT pEffect, _uint iIndex);
	HRESULT SetUp_ConstantTableCustom(LPD3DXEFFECT pEffect);
public:
	static CUIButton* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();
private:
	_bool isCheck = false;
public:
	void SetCheck(_bool b);
};

_END