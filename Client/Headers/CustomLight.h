#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "Transform.h"
#include "Light.h"

_BEGIN(Engine)
class CRenderer;
class CShader;
class CMesh_Static;
class CTexture;
class CCollider;
_END

_BEGIN(Client)

class CCustomLight : public CGameObject
{
private:
	explicit CCustomLight(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCustomLight(const CCustomLight& rhs);
	virtual ~CCustomLight() = default;
public:
	CLight*	GetLight() { return m_pLight; }
	void	Set_Type(D3DLIGHTTYPE type) { m_pLight->Set_Type(type); }
	void	Set_Range(_float range) { m_pLight->Set_Range(range); }

	void	Set_Diffuse(D3DXCOLOR vDiff) { m_pLight->Set_Diffuse(vDiff); }
	void	Set_Ambient(D3DXCOLOR amb) { m_pLight->Set_Ambient(amb); }
	void	Set_Specular(D3DXCOLOR spc) { m_pLight->Set_Specular(spc); }

	void	Set_Position(_vec3 vPos) { m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	m_pLight->Set_Position(vPos); }
	void	Set_Direction(_vec3 vDir) { m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vDir); 
	m_pLight->Set_Direction(vDir); }
	void	Set_Phi(_float fPhi) { m_pLight->Set_Phi(fPhi); } // �ܺο���
	void	Set_Theta(_float fTheta) { m_pLight->Set_Theta(fTheta); } // ���ο���
	void	SetRender(_bool bRender) { m_pLight->SetRender(bRender); }
	void	SetMovable(_bool bMov) { m_bMovable = bMov; }
	void	Add_ShadowCubeGroup(CGameObject* pGameObj) { m_pLight->AddObjList(pGameObj); }
	void	Ready_Render_ShadowMap() { m_pLight->Render_ShadowMap(); }
	virtual void Render_GameObject();
	void	Set_World() { m_pLight->Set_World(m_pTransformCom->Get_Matrix()); }
	//void	Cal_CamperShadow(CCollider* pCollider);
	void	Add_Light();
public:
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
private:
	CMesh_Static*	m_pLightCone = nullptr;
	CTexture*	m_pLightTexture = nullptr;
	CTransform*	m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CLight*	m_pLight = nullptr;
	_bool	m_bMovable = false;
	D3DLIGHT9	m_LightInfo;
private:
	HRESULT Ready_Light(LPDIRECT3DDEVICE9 pGraphic_Device, _vec3 vPosition, D3DLIGHTTYPE ltype);
	HRESULT Ready_GameObject();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect);
public:
	static CCustomLight* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject * Clone_GameObject() override;
	virtual void Free() override;
};

_END