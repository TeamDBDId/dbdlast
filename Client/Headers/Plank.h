#pragma once

#include "Defines.h"
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CMesh_Static;
class CCollider;
class CFrustum;
_END

_BEGIN(Client)

class CPlank final : public CGameObject
{
public:
	enum STATE { StandIdle, FallOnGround};
private:
	explicit CPlank(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CPlank(const CPlank& rhs);
	virtual ~CPlank() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
public:
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();
public:
	void SetState(STATE eState) { m_CurState = eState; m_iCurAnimation = eState; }
	void PullDownPlank() { m_bIsPullDown = !m_bIsPullDown; m_iCurAnimation = FallOnGround; }
public:
	STATE GetState() { return m_CurState; }
private:
	void State_Check();
	void Plank_Check(const _float& fTimeDelta);
	void ComunicateWithServer();
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Dynamic*		m_pMeshCom = nullptr;
	CMesh_Static*		m_pMeshColl = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	wstring m_Key;
	_int m_iOtherOption = 0;
	STATE m_OldState = StandIdle;
	STATE m_CurState = StandIdle;
	_bool m_bIsPullDown = false;
	_bool m_isFalling = false;
	_bool m_bDissolve = false;
	_float m_fDissolveTime = 0.f;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
public:
	static CPlank* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END