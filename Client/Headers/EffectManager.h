#pragma once
#include "Base.h"

_BEGIN(Engine)
class CManagement;
_END

_BEGIN(Client)
class CEffectManager : public CBase
{
	_DECLARE_SINGLETON(CEffectManager)

public:
	enum eEffect { E_Fire, E_Mist,E_Generator };
private:
	explicit CEffectManager();
	~CEffectManager() = default;

public:
	HRESULT Ready_Effect(LPDIRECT3DDEVICE9 _pGDevice);
public:
	void Make_Effect(const eEffect& _eEffect, const _vec3 & _vPos=_vec3());

private:
	void Make_Fire(const _vec3 & _vPos);
	void Make_Mist();
	void Make_Generator(const _vec3 & _vPos);
	
private:
	virtual void Free() override;


private:

	CManagement*			m_pManagement = nullptr;
};

_END