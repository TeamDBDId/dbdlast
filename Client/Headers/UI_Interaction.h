#pragma once
#include "GameObject.h"

_BEGIN(Client)

class CUI_Texture;
class CUI_Interaction : public CGameObject
{
public:
	enum Interaction {Generator};
	enum ProgressBar {Base, Bar, Texture, Text, ProgressBarEnd};
	enum SkillCheck {S_Base, S_Full, S_NoneFull, S_End, S_GridLine, S_SpaceBar, S_Text ,SkillCheckEnd};
	enum SkillCheckState {SC_NONE, SC_Ready, SC_Check};
	struct ButtonInfo
	{
		CUI_Texture* pButtonTexture = nullptr;
		CUI_Texture* pButtonText = nullptr;
		CUI_Texture* pMotionText = nullptr;
	};
private:
	explicit CUI_Interaction(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_Interaction(const CUI_Interaction& rhs);
	virtual ~CUI_Interaction() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	void	Add_Button(wstring TextureName, wstring TextName);

private:
	void	Interact_Check();
	void	Update_SkillCheck(const _float& fTimeDelta);
	void	Update_ButtonPos();
	void	Update_AddButton_Camper();
	void	Update_AddButton_Slasher();
	void	Check_CollisionObject();
	HRESULT Set_ProgressBar();
	HRESULT Set_SkillCheck();
	_bool	IsSkillCheck();

private:
	CGameObject*	m_pPlayer = nullptr;
	CGameObject*	m_pCollisionObjtect = nullptr;
	CUI_Texture*	m_ProgressBar[ProgressBarEnd];
	CUI_Texture*	m_SkillCheck[SkillCheckEnd];
	map<wstring,ButtonInfo> m_mapButton;
	_float	m_fProgress;
	_uint	m_PlayerNum = 0;
	_int	m_InteractionObject = 0;
	_float	m_fSkillCheckTime = 0.f;
	_uint	m_eSkillCheckState = SC_NONE;
public:
	static CUI_Interaction* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END