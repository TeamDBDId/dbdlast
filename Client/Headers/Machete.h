#pragma once
#include "GameObject.h"
_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Static;
class CCollider;
class CPicking;
class CFrustum;
_END


_BEGIN(Client)

class CMachete : public CGameObject
{
private:
	explicit CMachete(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CMachete(const CMachete& rhs);
	virtual ~CMachete() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	
public:
	void Set_CollState(_bool bColl) { m_isColl = bColl; }
	virtual _int Do_Coll(CGameObject* _pObj, const _vec3& _vPos = _vec3(0.f, 0.f, 0.f));
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Static*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	const _matrix*		m_pRHandMatrix = nullptr;
	_matrix				m_matParent;
	CTransform*			m_pSlasherTransform = nullptr;

	//_matrix				m_pWeaponLocal;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint& iAttributeID);
	void Approach_Slasher();
public:
	static CMachete* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END
