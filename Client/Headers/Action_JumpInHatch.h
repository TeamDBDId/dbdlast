#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CAction_JumpInHatch final : public CAction
{
public:
	explicit CAction_JumpInHatch();
	virtual ~CAction_JumpInHatch() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
private:
	_int m_iState = 0;
protected:
	virtual void Free();
};

_END