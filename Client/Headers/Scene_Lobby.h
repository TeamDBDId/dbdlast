#pragma once

#include "Defines.h"
#include "Scene.h"

_BEGIN(Client)

class CScene_Lobby final : public CScene
{
public:
	enum LobbyState { RoleSelection, OverlayMenu};
private:
	explicit CScene_Lobby(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CScene_Lobby() = default;
public:
	virtual HRESULT Ready_Scene() override;
	virtual _int Update_Scene(const _float& fTimeDelta) override;
	virtual _int LastUpdate_Scene(const _float& fTimeDelta) override;
	virtual void Render_Scene() override;
public:
	static size_t CALLBACK Lobby_Thread(LPVOID lpParam);
private:
	HANDLE hThread;
private:
	HRESULT Ready_Prototype_GameObject();
	HRESULT Ready_Prototype_Component();
	void	Update_Server();
private:
	void LobbyState_Check();
public:
	static CScene_Lobby* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
private:
	LobbyState m_eCurLobbyState = RoleSelection;
	LobbyState m_eOldLobbyState = RoleSelection;
	CGameObject* m_pCurUI = nullptr;
	_bool	IsReady = false;
	_bool	IsSlasher = false;

protected:
	virtual void Free();
};

_END