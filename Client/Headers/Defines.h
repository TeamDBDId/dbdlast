#pragma once

// Server
extern bool bStartGameServer;
extern bool bUseServer;

extern int	exPlayerNumber;
extern int	exCharacterNum;
extern char MyIPAdress[16];

extern Server_Data server_data;
extern LServer_Data lserver_data;

extern SOCKET Lobbysock;
extern SOCKET Gamesock;

extern CamperData camper_data;
extern SlasherData slasher_data;
extern Client_Data client_data;
extern LobbyData lobby_data;

extern int sendPacket;
extern LARGE_INTEGER   tickPerSecond;
extern LARGE_INTEGER   startTick, endTick;

extern LARGE_INTEGER   GtickPerSecond;
extern LARGE_INTEGER   GstartTick, GendTick;

extern bool ActiveOption;

extern double dTime;
extern double dGTime;

extern bool	bLobbyServer;
extern bool	bGameServer;

extern float fWorldTimer;

// ����
inline _vec3 operator^(_vec3 u, _vec3 v)
{
	return _vec3(u.y*v.z - u.z*v.y, -u.x*v.z + u.z*v.x, u.x*v.y - u.y*v.x);
}
// ����
inline float operator*(_vec3 u, _vec3 v)
{
	return (u.x*v.x + u.y*v.y + u.z*v.z);
}

const unsigned int g_iBackCX = 1280;
const unsigned int g_iBackCY = 720;

enum OBJECT_ID { CAMPER = 0x100, SLASHER = 0x200, CLOSET = 0x400, HATCH = 0x800, HOOK = 0x1000, PLANK = 0x2000, WINDOW = 0x4000, CHEST = 0x8000, GENERATOR = 0x10000, EXITDOOR = 0x20000, TOTEM = 0x40000, OBJECT_ID_END };
enum SCENEID {SCENE_STATIC, SCENE_LOGO, SCENE_STAGE, SCENE_LOBBY, SCENE_END};

enum DIR {FRONT,BACK,LEFT,RIGHT};
	
enum WINDOW_MODE { FULL_MODE= 0, WINDOW_MODE = 1};
extern HINSTANCE g_hInst;
extern HWND g_hWnd;

#define KEYMGR	CKeyManager::GetInstance()
#define EFFMGR	CEffectManager::GetInstance()

#define LSLASHER	1
#define CLICKBACK	6

#define _MAP_INTERVAL 800

#define NO_EVENT				0
#define EVENT					1
#define FAILED_SKILL_CHECK		10

#define WINDOWMODE				WINDOW_MODE

// SecAnim Slasher
#define	PLAYERATTACK			1
#define PICK_UP					2
#define HOOK_IN					3
#define DROP					4
#define GRAB_LOCKER				5
#define GRAB_OBSTACLES			6
#define GRAB_GENERIC_FAST		7

// SecAnim Camper
#define HOOK_BEING				1
#define BEING_HEAL				2
#define END_HEAL				5555
#define ClOSET_OCCUPIED			50