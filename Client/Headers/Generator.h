#pragma once

#include "Defines.h"
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CFrustum;
_END

_BEGIN(Client)
class CCustomLight;
class CGenerator final : public CGameObject
{
public:
	enum STATE { PulleyAdd, PistonsIdleS3, PistonsIdleS2, PistonsIdleS1, PistonsIdle, On, Idle, Failure };
private:
	explicit CGenerator(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CGenerator(const CGenerator& rhs);
	virtual ~CGenerator() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();
private:
	void State_Check();
	void Progress_Check();
	void ComunicateWithServer();
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Dynamic*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
	CCollider*			m_pLightCollider[4] = { nullptr, nullptr, nullptr, nullptr };
	CCustomLight*		m_pCustomLight[4] = { nullptr, nullptr, nullptr, nullptr };
	CCollider*			m_pColliderSpotCom = nullptr;
	CCustomLight*		m_pCustomSpotLight = nullptr;
private:
	wstring m_Key;
	_int m_iOtherOption = 0;
	STATE m_OldState = Idle;
	STATE m_CurState = Idle;

	_float m_fDeltaTime = 0.f;
	_tchar sztt[200] = L"";
private:
	void Penetration_Check();
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
public:
	static CGenerator* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END