#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CPlank;
class CAction_PullDownPlank final : public CAction
{
public:
	explicit CAction_PullDownPlank();
	virtual ~CAction_PullDownPlank() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetPlank(CGameObject* pPlank) { m_pPlank = (CPlank*)pPlank; }
private:
	_bool m_bIsInit = false;
	_vec3 m_vOriginPos;
	vector<_vec3> m_vecPos;
	_float m_fIndex = 0.f;
	_int m_iState = 0;
	CPlank* m_pPlank = nullptr;
private:
	void SetVectorPos();
protected:
	virtual void Free();
};

_END