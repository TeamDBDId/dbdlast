#pragma once
#include "GameObject.h"

_BEGIN(Client)

class CUI_Texture;
class CUI_GameState : public CGameObject
{
private:
	explicit CUI_GameState(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_GameState(const CUI_GameState& rhs);
	virtual ~CUI_GameState() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);




private:
	HRESULT Set_CamperStateUI();
	HRESULT Set_ExitState();
	HRESULT Set_BasicUI();
	void	Update_Generator();
	void	Update_CamperState();
private:
	CUI_Texture*	m_BaseUI;
	CUI_Texture*	m_pCamperState[4];
	CUI_Texture*	m_pExitState[2];
	
	_int m_iCamperNum = 4;
	_int m_iLeaveGenerator = 5;

public:
	static CUI_GameState* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END