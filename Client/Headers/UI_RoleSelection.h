#pragma once
#include "GameObject.h"

_BEGIN(Client)

class CUI_Texture;
class CUI_RoleSelection : public CGameObject
{
public:
	enum ButtonState { Touch, UnTouch, Click};
	enum Lobby1Button { NONE, CAMPER, SLASHER };
private:
	explicit CUI_RoleSelection(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_RoleSelection(const CUI_RoleSelection& rhs);
	virtual ~CUI_RoleSelection() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	
public:
	_uint	Get_ClickButton() { return m_iClickButton; }

private:
	void Mouse_Check();
	void Lobby_Check();
	void Set_Lobby();
	void Set_Background();

	void Button_L1_Camper(ButtonState State);
	void Button_L1_Slasher(ButtonState State);
	CUI_Texture* Find_UITexture(wstring UIName);
	void Delete_UI_Texture(wstring UIName);



	void Delete_All();
private:
	map<wstring, CUI_Texture*> m_UITexture;

	_uint				m_CurButton = NONE;
	_uint				m_OldButton = NONE;
	_uint				m_iClickButton = NONE;
public:
	static CUI_RoleSelection* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END