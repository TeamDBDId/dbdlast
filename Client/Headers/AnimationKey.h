#pragma once

// Animation Camper 

namespace AC {

	enum ANIMATION_CAMPER {
		Breathing,								// Locomotions
		CrawlFT,
		CrouchWalk,
		CrouchStand,
		Idle,
		Run,
		Walk,
		Walk_Slower,
		HitToCrawlBk,							// InjuredLocomotions
		HitToCrawlFk,
		Injured_CrouchIdle,						
		Injured_CrouchWalk,
		Injured_CrouchStand,
		Injured_Idle,
		Injured_Run,
		Injured_Walk,
		HookBeingRescuedIn,						// CamperInteractions
		HookBeingRescuredEnd,
		HookBeingCamperIn,
		HookBeingCamperEnd,
		ComeHere,								// Gestures
		CrouchComeHere,
		CrouchPointTo,
		InjuredComeHere,
		InjuredPointTo,
		InjuredCrouchComeHere,
		InjuredCrouchPointTo,
		PointTo,
		BeingHeal,								// Healings
		BeingHealFail,
		BeingMended,
		HealCamper,
		HealCamperFail,
		HealingSelf,
		KneelHealSelfFail,
		MendingSelf,
		JumpOverAngle60LT,						// Obstacles
		JumpOverAngle60LTFast,
		JumpOverAngle60RT,
		JumpOverAngle60RTFast,
		PullDownObjectLT,
		PullDownObjectRT,
		StandPullDownObjectLT,
		StandPullDownObjectRT,
		WalkPullDownObjectLT,
		WalkPullDownObjectRT,
		WindowVault_Fast,
		WindowVault_Mid,
		BeingKilledBySpiderIN,					// ObjectInteractions
		BeingKilledBySpiderLoop,
		BeingKilledBySpiderOut,
		ClosetFull_Entering,
		ClosetFull_Occupied,
		ClosetHide,
		ClosetHideFast,
		ClosetIdle,
		ClosetUnHide,
		ClosetUnHideFast,
		GeneratorActivation,
		GeneratorFail,
		JumpInHatch,
		LootChest,
		LootChestOpen,
		LootChestOpenOut,
		LootChestOut,
		PickItemOnGround,
		Sabotage_In,
		Sabotage_Loop,
		Spider_Reaction_In,
		Spider_Reaction_Loop,
		Spider_Reaction_Out,
		Spider_Struggle,
		Spider_StruggleToSacrifice,
		UnlockExit,
		UnlockExitIdle,
		TT_Carry_Idle,							// SlasherInteractions
		TT_Drop,
		TT_Hook_In,
		TT_Hook_Out,
		TT_Hook_SetUp,
		TT_PickUp,
		TT_PickUp_In,
		TT_Stun_Drop,
		TT_Wiggle,
		AddHitBack,								// Hits
		AddHitFront,
		AddHitLT,
		AddHitRT,
		DeathGround,
		HookedDeath,
		HookedFree,
		HookedIdle,
		HookedStruggle,
		Toolbox_IdleArmOverride,				// Toolbox
		Toolbox_RunArmOverride,
		Toolbox_WalkArmOverride,
		TT_Carry_Attack_Bow,					// SlasherInteractions
		TT_Carry_Attack_In,
		TT_Carry_Attack_Out,
		TT_Carry_Attack_Swing,
		TT_Grab_Generic_Fast,
		TT_Grab_Locker,
		TT_Grab_Obstacles_BK,
		TT_Grab_Obstacles_FT,
		CleansTotem_Loop						// Totem
	};
}

// Animation Slasher (Wraith)

namespace AW {

	enum ANIMATION_SLASHER_WRAITH
	{
		TW_Attack_Bow,				// Attacks 
		TW_Attack_Bow_FPV,								// 때리기 실패
		TW_Attack_In,
		TW_Attack_In_FPV,
		TW_Attack_Miss_Out,
		TW_Attack_Miss_Out_FPV,
		TW_Attack_Swing,
		TW_Attack_Swing_FPV,
		TW_Attack_Wipe,									// 타격후 모션
		TW_Attack_Wipe_FPV,
		TW_Drop,					// Interactions
		TW_Hook_In,
		TW_Mori01,										// 즉결처형
		TW_PickUp,
		TW_PcikUp_IN,
		TW_Bell_In,					// Locomotions
		TW_Bell_In_FPV,
		TW_Bell_Loop,
		TW_Bell_Loop_FPV,
		TW_Bell_Out,
		TW_Bell_Out_FPV,
		TW_Idle,
		TW_IdleMenu,
		TW_IdleMenu_Intterrupt01,
		TW_IdleMenu_Intterrupt02,
		TW_RunLT,
		TW_RunBKTrans,									// 빠르게
		TW_RunBK,										// 느리게
		TW_RunFTTrans,
		TW_RunFT,
		TW_RunLTTrans,
		TW_RunRTTrans,
		TW_RunRT,
		TW_Stand,
		TW_Close_Hatch,				// Obstcacles		// 개구멍 부시기
		TW_Close_Hatch_FPV,								// FPV : 1인칭 시점
		TW_Damage_Generator,
		TW_Destroy_Pallet,
		TT_Carry_Attack_BOW,		// Attack
		TT_Carry_Attack_BOW_FPV,
		TT_Carry_Attack_IN,
		TT_Carry_Attack_IN_FPV,
		TT_Carry_Attack_Out,
		TT_Carry_Attack_Out_FPV,
		TT_Carry_Attack_Swing,
		TT_Carry_Attack_Swing_FPV,
		TT_Carry_Idle,
		TT_Stun_Drop_IN,
		TT_Stun_Drop_Out,
		TT_Stun_Pallet,
		TT_WindowVault,
		TT_WindowVaultFall,
		TT_Fall,
		TT_Grab_Generic_Fast,
		TT_Grab_Generic_Fast_FPV,
		TT_Grab_Locker,
		TT_Grab_Obstacles_BK,
		TT_Grab_Obstacles_BK_FPV,
		TT_Grab_Obstacles_FT,
		TT_Grab_Obstacles_FT_FPV,
		TT_LandLight,
		TT_SearchLocker
	};
}