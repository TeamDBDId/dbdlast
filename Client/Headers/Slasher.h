#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "Transform.h"
#include "AnimationKey.h"
#include "Input_Device.h"

_BEGIN(Engine)
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CTexture;
_END

_BEGIN(Client)
class CCustomLight;
class CSlasher final : public CGameObject
{
public:
	enum STATE { IDLE, WALK, END };
	enum CHARACTER { C_WRAITH, C_END };
private:
	explicit CSlasher(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CSlasher(const CSlasher& rhs);
	virtual ~CSlasher() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
public:
	virtual _int Do_Coll(CGameObject* _pObj, const _vec3& _vPos = _vec3(0.f, 0.f, 0.f));
	void RotateY(const _float& fTimeDelta);
private:
	void CommunicationWithServer();
	void Compute_Map_Index();
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
	HRESULT Ready_Action();
public:
	const _matrix* Get_LHandMatrix() { return m_pLHandMatrix; }
	const _matrix* Get_RHandMatrix() { return m_pRHandMatrix; }
	const _matrix* Get_HeadMatrix() { return m_pHeadMatrix; }
	AW::ANIMATION_SLASHER_WRAITH Get_State() { return m_eCurState; }
public:
	void Set_State(AW::ANIMATION_SLASHER_WRAITH eState) { m_eCurState = eState; State_Check(); }
	void SetColl(_bool IsColl) { m_isColl = IsColl; }
	void IsLockKey(_bool IsLock) { m_bIsLockKey = IsLock; }
	void Set_RendHead(_bool IsRend) { m_bRendHead = IsRend; }
	void Set_Weapon(CGameObject* pWeapon) { m_pWeapon = pWeapon; }
	void Set_WeaponColl(_bool bColl);
	void Set_Carry(_bool bCarry) { m_bIsCarry = bCarry;}
	void Set_CarriedCamper(CGameObject* pCamper) {m_pCarriedCamper = pCamper;}
	CGameObject* Get_CarriedCamper() { return m_pCarriedCamper; }
private:
	void State_Check();
	void Key_Input(const _float& fTimeDelta);
	void Init_Camera();
	void Interact_Object();
	void Cal_LightPos();
	_bool Find_FPVAnim(AW::ANIMATION_SLASHER_WRAITH Anim);
	void Test();
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Dynamic*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	CGameObject*		m_pWeapon = nullptr;
	CCustomLight*		m_pCustomLight = nullptr;
private:
	_bool				m_bRendHead = true;
	_bool				m_bSkillUse = false;
	_bool				m_bIsLockKey = false;
	_bool				m_bLaitInit = false;
	_bool				m_bIsCarry = false;
	_vec3				m_vTargetPos;
	vector<_uint>		m_vecFPVAnim;
	CInput_Device*		m_pInput_Device = nullptr;
	CGameObject*		m_pCarriedCamper = nullptr;
	CGameObject*		m_pTargetOfInteraction = nullptr;
	list<CGameObject*>  m_CollObjList;
	const _matrix*		m_pCamMatrix = nullptr;
	const _matrix*		m_pRHandMatrix = nullptr;
	const _matrix*		m_pLHandMatrix = nullptr;
	const _matrix*		m_pHeadMatrix = nullptr;
private:
	AW::ANIMATION_SLASHER_WRAITH	m_eOldState = AW::TW_Idle;
	AW::ANIMATION_SLASHER_WRAITH	m_eCurState = AW::TW_Idle;
public:
	static CSlasher* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();
};

_END