#pragma once
#include "Base.h"
#include "GameObject.h"

_BEGIN(Client)
class CLoadManager : public CBase
{
	_DECLARE_SINGLETON(CLoadManager)
private:
	explicit CLoadManager();
	virtual ~CLoadManager() = default;

public:
	void	Set_GraphicDevice(LPDIRECT3DDEVICE9 pGraphic_Device) { m_pGraphic_Device = pGraphic_Device; m_pGraphic_Device->AddRef(); }
	void	LoadMapData(_uint Stage);
	void	Setup_Map_Index(CGameObject* pGameObject, const _matrix& matWorld);
	_int	Compute_Map_Index(const _vec3& vPosition);

	HRESULT Add_ReplacedName();
	HRESULT Prototype_S1_GameObject();
	HRESULT Prototype_S1_StaticMesh();
	HRESULT Prototype_S1_DynamicMesh();
	HRESULT Prototype_S1_Component();
	HRESULT Prototype_S1_Texture();
	HRESULT Prototype_S1_CollMesh();

	HRESULT Prototype_L_GameObject();
	HRESULT Prototype_L_Texture();

	_bool	Get_IsLoading() { return m_isLoading; }
	void	Set_IsLoding(_bool IsLoading) { m_isLoading = IsLoading; }
private:
	_int Find_ObjectName(wstring Key);
private:
	LPDIRECT3DDEVICE9 m_pGraphic_Device = nullptr;
	vector<wstring*>	m_vecObjectName;
	_bool				m_isLoading = true;
private:
	virtual void Free() override;

};
_END