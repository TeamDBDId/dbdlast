#pragma once

#include "Defines.h"
#include "Scene.h"
#include <dshow.h>
#include <process.h>
#include <mfplay.h>
_BEGIN(Client)

class CUI_Texture;
class CScene_Logo final : public CScene
{
private:
	explicit CScene_Logo(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CScene_Logo() = default;
public:
	virtual HRESULT Ready_Scene() override;
	virtual _int Update_Scene(const _float& fTimeDelta) override;
	virtual _int LastUpdate_Scene(const _float& fTimeDelta) override;
	virtual void Render_Scene() override;
	HRESULT End_Loading();
private:
	HRESULT Ready_Prototype_GameObject();
	HRESULT Ready_Prototype_Component();
	HRESULT Play_Video();
	void LoadEnd();
private:
	static size_t __stdcall LoadThreadFunc(LPVOID lpParam);
private:
	IGraphBuilder*			m_pGraph = nullptr;
	IMediaControl*			m_pControl = nullptr;
	IMediaEvent*			m_pEvent = nullptr;
	IVideoWindow*			m_pVideoWin = NULL;
	CUI_Texture*			m_pSkipTexture = nullptr;


	CRITICAL_SECTION	m_CritSec;
	HANDLE				m_hThread;
	_int				m_iLogoState = 0;
	
public:
	static CScene_Logo* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
protected:
	virtual void Free();

};

_END