#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "Transform.h"
#include "AnimationKey.h"

_BEGIN(Engine)
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
_END

_BEGIN(Client)
class CCustomLight;
class CCamper final : public CGameObject
{
public:
	enum CONDITION { HEALTHY, INJURED, DYING, SPECIAL, OBSTACLE, HOOKED, HOOKDEAD, BLOODDEAD, ESCAPE, END};
	enum HOOKSTAGE { EscapeAttempts, Struggle, Sacrifice, HookStage_END};
private:
	explicit CCamper(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCamper(const CCamper& rhs);
	virtual ~CCamper() = default;
public:
	const _matrix* Get_LHandMatrix() { return m_pLHandMatrix; }
	const _matrix* Get_RHandMatrix() { return m_pRHandMatrix; }
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowMap(_matrix* VP, LPD3DXEFFECT pEffect, _vec4 vLightPos); 
public:
	virtual _int Do_Coll(CGameObject* _pObj, const _vec3& _vPos = _vec3(0.f, 0.f, 0.f));
public:
	void Test();
	void Set_State(AC::ANIMATION_CAMPER eState) { m_eCurState = eState; State_Check(); }
	void Set_Index(_uint index) { m_iIndex = index; m_eObjectID = (CAMPER + index - 1); }
	_uint Get_Index() { return m_iIndex; }
	void IsLockKey(_bool IsLock) { m_bIsLockKey = IsLock; }
	_bool IsColl() { return m_isColl; }
	void SetColl(_bool IsColl) { m_isColl = IsColl; }
	void SetCurCondition(CONDITION eCondition) { m_eCurCondition = eCondition; }
	void SetOldCondition(CONDITION eCondition) { m_eOldCondition = eCondition; }
	void SetHookStage(HOOKSTAGE eHookStage) { m_eHookStage = eHookStage; }
	void SetHeal(_float fHeal) { m_fHeal = fHeal; }
	void SetDyingEnergy() { m_fDyingEnergy = m_fEnergy; }
	void SetHookedEnergy() { m_fHookEnergy = m_fEnergy; }
	void SetEnergy(_float fEnergy) { m_fEnergy = fEnergy; }
	void SetCarry(_bool bCarry) { m_bIsCarried = bCarry; }
	void SetMaxEnergyAndMaxHeal();
public:
	AC::ANIMATION_CAMPER GetState() { return m_eCurState; }
public:
	HOOKSTAGE GetHookStage() { return m_eHookStage; }
	CONDITION GetCurCondition() { return m_eCurCondition; }
	CONDITION GetOldCondition() { return m_eOldCondition; }
	_float GetHeal() { return m_fHeal; }
	_float GetMaxHeal() { return m_fMaxHeal; }
	_float GetEnergy() { return m_fEnergy; }
	_float GetMaxEnergy() { return m_fMaxEnergy; }
	_float GetDyingEnergy() { return m_fDyingEnergy; }
	_float GetHookedEnergy() { return m_fHookEnergy; }
	_bool IsCarry() { return m_bIsCarried; }
	CGameObject* GetTargetOfInteraction() { return m_pTargetOfInteraction; }
private:
	void Check_InteractionOfObject(CGameObject* pGameObject);
	void Init_Camera();
	void CommunicationWithServer();
	void Compute_Map_Index();
	void Collision_Check();
	void Check_OtherCamperState();
	void Check_SlasherCamperState();
	//void Test();
private:
	HRESULT Ready_Component();
	HRESULT Ready_Actions();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
private:
	CTransform*				m_pTransformCom = nullptr;
	CRenderer*				m_pRendererCom = nullptr;
	CShader*				m_pShaderCom = nullptr;
	CMesh_Dynamic*			m_pMeshCom = nullptr;
	CCollider*				m_pColliderCom = nullptr;
private:
	D3DXCOLOR				m_Color = D3DXCOLOR(1.f, 0.2f, 0.2f, 1.f);
	_bool					m_bPlayAnimation = true;
	_bool					LateInit = false;
	_bool					m_bIsLockKey = false;
	_bool					m_bIsInCloset = false;
	_bool					m_bIsCarried = false;
	AC::ANIMATION_CAMPER	m_eCurState;
	AC::ANIMATION_CAMPER	m_eOldState;
	CONDITION				m_eCurCondition = HEALTHY;
	CONDITION				m_eOldCondition = HEALTHY;
	HOOKSTAGE				m_eHookStage = EscapeAttempts;
	CGameObject*			m_pTargetOfInteraction = nullptr;
	list<CGameObject*>		m_CollObjList;
	_vec3					m_vTargetPos;
	_float					m_fTimeDelta = 0.f;
	_float					m_fHeal = 0.f;
	_float					m_fMaxHeal = 0.f;
	_float					m_fEnergy = 0.f;
	_float					m_fMaxEnergy = 0.f;
	_float					m_fDyingEnergy = 0.f;
	_float					m_fHookEnergy = 0.f;
	_float					m_fMoveSpeed = 1.f;
	_float					m_fAttackedTime = 0.f;
	_uint					m_iIndex = 0;
	const _matrix*			m_pMatCamera = nullptr;
	const _matrix*			m_pRHandMatrix = nullptr;
	const _matrix*			m_pLHandMatrix = nullptr;
private:
	void Key_Input(const _float& fTimeDelta);
	void State_Check();
	void Check_Attacked();
	void MoveSpeed_Check();
	void IsCarried();
public:
	static CCamper* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();
private:
	CCustomLight*		m_pCustomLight = nullptr;
};

_END