#pragma once

#include "Defines.h"
#include "Scene.h"

_BEGIN(Client)

class CScene_Stage final : public CScene
{
private:
	explicit CScene_Stage(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CScene_Stage() = default;
public:
	virtual HRESULT Ready_Scene() override;
	virtual _int Update_Scene(const _float& fTimeDelta) override;
	virtual _int LastUpdate_Scene(const _float& fTimeDelta) override;
	virtual void Render_Scene() override;
public:
	static size_t CALLBACK Game_Thread(LPVOID lpParam);
private:
	HANDLE hThread;
private:
	HRESULT Ready_LightInfo();
	HRESULT Ready_Prototype_GameObject();
	HRESULT Ready_Prototype_Component();
	HRESULT Ready_Layer_Camera(const _tchar* pLayerTag);
	HRESULT Ready_Layer_BackGround(const _tchar* pLayerTag);
	HRESULT Ready_Layer_Camper(const _tchar* pLayerTag, _uint Index);
	HRESULT Ready_Layer_Slasher(const _tchar* pLayerTag);

	HRESULT Ready_Prototype_CollMesh();
	HRESULT Ready_UI();
public:
	static CScene_Stage* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
private:
	vector<_tchar*> SceneUI;
private:
	void Check_Connection_OtherPlayers();
private:
	bool bConnectedCamper[4] = { false, false ,false ,false };
protected:
	virtual void Free();

};

_END