#pragma once
#include "GameObject.h"
_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Static;
class CCollider;
class CFrustum;
_END


_BEGIN(Client)
class CCustomLight;
class CLamp_01 : public CGameObject
{
private:
	explicit CLamp_01(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CLamp_01(const CLamp_01& rhs);
	virtual ~CLamp_01() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	CCustomLight* GetCustomLight() { return m_pCustomLight; }
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();

private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Static*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
	CCollider*			m_pLightCollider = nullptr;
private:
	CCustomLight*		m_pCustomLight = nullptr;
private:
	_float	m_Max = 0.f;
	wstring m_Key = L"";
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint& iAttributeID);
public:
	static CLamp_01* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END
