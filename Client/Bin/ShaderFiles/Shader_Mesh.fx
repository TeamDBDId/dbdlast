int		numBoneInf = 4;
matrix		g_matWorld, g_matVP;
texture		g_DiffuseTexture;
texture		g_NormalTexture;
texture		g_CubeTexture;
texture		g_AOTexture;
texture		g_MetalicTexture;
texture		g_RoughnessTexture;
texture		g_NoiseTexture;
vector		g_DiffuseColor = vector(0.7f, 0, 0, 1);
float		g_fTimeAcc;

sampler NoiseSampler = sampler_state
{
	texture = g_NoiseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler NormalSampler = sampler_state
{
	texture = g_NormalTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler CubeSampler = sampler_state
{
	texture = g_CubeTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

sampler MetalSampler = sampler_state
{
	texture = g_MetalicTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

sampler RoughSampler = sampler_state
{
	texture = g_RoughnessTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

sampler AOSampler = sampler_state
{
	texture = g_AOTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_SkinTexture;
sampler SkinSampler = sampler_state
{
	texture = g_SkinTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_LightEmissive;
sampler LESampler = sampler_state
{
	texture = g_LightEmissive;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_DissolveTexture;
sampler DissolveSampler = sampler_state
{
	texture = g_DissolveTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_DissolveEffect;
sampler DisEffectSampler = sampler_state
{
	texture = g_DissolveEffect;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

struct VS_IN
{
	float3	vPosition : POSITION;
	float3	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD;
};

struct VS_OUT_PHONG
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
};

VS_OUT_PHONG VS_MAIN_PHONG(VS_IN In)
{
	VS_OUT_PHONG			Out = (VS_OUT_PHONG)0;

	matrix		matWVP;

	matWVP = mul(g_matWorld, g_matVP);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vNormal = normalize(mul(vector(In.vNormal, 0.f), g_matWorld));
	Out.vTexUV = In.vTexUV;
	Out.vProjPos = Out.vPosition;

	return Out;
}

struct VS_IN_SKIN
{
	float3	vPosition : POSITION;
	float3	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	float4	weights : BLENDWEIGHT0;
	int4	boneIndices : BLENDINDICES0;
};

float4x4 GetSkinMatrix(int idx)
{
	float4 uv = float4(((float)((idx % 32) * 4) + 0.5f) / 128.f, ((float)((idx / 32)) + 0.5f) / 128.f, 0.0f, 0.0f);

	float4x4 mat =
	{
		tex2Dlod(SkinSampler, uv),
		tex2Dlod(SkinSampler, uv + float4(1.0f / 128.f, 0, 0, 0)),
		tex2Dlod(SkinSampler, uv + float4(2.0f / 128.f, 0, 0, 0)),
		tex2Dlod(SkinSampler, uv + float4(3.0f / 128.f, 0, 0, 0))
	};

	return mat;
}

VS_OUT_PHONG VS_MAIN_SKIN(VS_IN_SKIN In)
{
	VS_OUT_PHONG			Out = (VS_OUT_PHONG)0;

	matrix		matWVP;

	matWVP = mul(g_matWorld, g_matVP);

	Out.vTexUV = In.vTexUV;

	float lastweight = 0.f;
	int n = numBoneInf - 1.f;
	In.vNormal = normalize(In.vNormal);

	for (int i = 0; i < n; ++i)
	{
		lastweight += In.weights[i];
		Out.vPosition += In.weights[i] * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[i]));
		Out.vNormal += In.weights[i] * mul(vector(In.vNormal, 0.f), GetSkinMatrix(In.boneIndices[i]));
	}

	lastweight = 1.f - lastweight;
	Out.vPosition += lastweight * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[n]));
	Out.vNormal += lastweight * mul(vector(In.vNormal, 0.f), GetSkinMatrix(In.boneIndices[n]));

	Out.vPosition = mul(Out.vPosition, matWVP);
	//Out.vNormal = normalize(Out.vNormal);
	Out.vNormal = normalize(mul(vector(Out.vNormal.xyz, 0.f), g_matWorld));
	Out.vProjPos = Out.vPosition;

	return Out;
}

struct PS_IN_PHONG
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
};

struct PS_OUT
{
	vector	vDiffuse : COLOR0;
	vector	vNormal : COLOR1;
	vector	vDepth : COLOR2;
	vector	vRMAO : COLOR3;
};

PS_OUT PS_MAIN_PHONG(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 0.f);
	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.xyz = AO.xyz * 0.5f + 0.5f;

	AO.xyz = max(6.10352e-5, AO.xyz);
	AO.xyz > 0.04045 ? pow(AO.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : AO.xyz * (1.0 / 12.92);

	Out.vDiffuse *= AO;

	//float3 NormalX;
	//float3 NormalY;
	//float3 NormalZ = { 0,0,1 };

	//float3 Normal;
	//float3 NormalTex;

	//NormalY = normalize(Out.vNormal.xyz);
	//NormalX = normalize(cross(NormalZ, NormalY));
	//NormalZ = normalize(cross(NormalX, NormalY));
	//NormalTex = 2 * tex2D(NormalSampler, In.vTexUV).xyz - 1;

	//Normal.x = dot(NormalX, NormalTex);
	//Normal.y = dot(NormalY, NormalTex);
	//Normal.z = dot(NormalZ, NormalTex);
	//Normal = normalize(Normal);

	//Out.vNormal = vector(Normal.xyz, 0.f);

	Out.vRMAO.x = tex2D(RoughSampler, In.vTexUV);
	Out.vRMAO.y = tex2D(MetalSampler, In.vTexUV);
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);

	return Out;
}

PS_OUT PS_MAIN_PHONG_CROSS(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 0.f);
	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.xyz = AO.xyz * 0.5f + 0.5f;

	AO.xyz = max(6.10352e-5, AO.xyz);
	AO.xyz > 0.04045 ? pow(AO.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : AO.xyz * (1.0 / 12.92);

	Out.vDiffuse *= AO;

	//float3 NormalX;
	//float3 NormalY;
	//float3 NormalZ = { 0,0,1 };

	//float3 Normal;
	//float3 NormalTex;

	//NormalY = normalize(Out.vNormal.xyz);
	//NormalX = normalize(cross(NormalY, NormalZ));
	//NormalZ = normalize(cross(NormalY, NormalX));
	//NormalTex = 2 * tex2D(NormalSampler, In.vTexUV).xyz - 1;

	//Normal.x = dot(NormalX, NormalTex);
	//Normal.y = dot(NormalY, NormalTex);
	//Normal.z = dot(NormalZ, NormalTex);
	//Normal = normalize(Normal);

	//Out.vNormal = vector(Normal.xyz, 0.f);

	Out.vRMAO.x = tex2D(RoughSampler, In.vTexUV);
	Out.vRMAO.y = tex2D(MetalSampler, In.vTexUV);
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);
	Out.vRMAO.w = 1.f;

	return Out;
}

PS_OUT PS_MAIN_EMI(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);
	float  LightEmissive = 0.f;
	LightEmissive = tex2D(LESampler, In.vTexUV).x;

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	if (LightEmissive > 0.1f)
		Color.xyz *= LightEmissive * 10.f;

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, LightEmissive);
	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.xyz = AO.xyz * 0.5f + 0.5f;

	AO.xyz = max(6.10352e-5, AO.xyz);
	AO.xyz > 0.04045 ? pow(AO.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : AO.xyz * (1.0 / 12.92);

	Out.vDiffuse *= AO;
	//float3 NormalX;
	//float3 NormalY;
	//float3 NormalZ = { 0,1,0 };

	//float3 Normal;
	//float3 NormalTex;

	//NormalY = normalize(Out.vNormal.xyz);
	//NormalX = normalize(cross(NormalZ, NormalY));
	//NormalZ = normalize(cross(NormalX, NormalY));
	//NormalTex = 2 * tex2D(NormalSampler, In.vTexUV).xyz - 1;

	//Normal.x = dot(NormalX, NormalTex);
	//Normal.y = dot(NormalY, NormalTex);
	//Normal.z = dot(NormalZ, NormalTex);
	//Normal = normalize(Normal);

	//Out.vNormal = vector(Normal.xyz, 0.f);

	Out.vRMAO.x = tex2D(RoughSampler, In.vTexUV);
	Out.vRMAO.y = tex2D(MetalSampler, In.vTexUV);
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);

	return Out;
}

PS_OUT PS_MAIN_STENCIL(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = /*tex2D(DiffuseSampler, In.vTexUV);*/g_DiffuseColor;
	//float  LightEmissive = 0.f;
	//LightEmissive = tex2D(LESampler, In.vTexUV).x;

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	//if (LightEmissive > 0.1f)
	//	Color.xyz *= LightEmissive * 10.f;

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 3.f);
	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	//vector AO = tex2D(AOSampler, In.vTexUV);
	//AO.xyz = AO.xyz * 0.5f + 0.5f;

	//AO.xyz = max(6.10352e-5, AO.xyz);
	//AO.xyz > 0.04045 ? pow(AO.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : AO.xyz * (1.0 / 12.92);

	//Out.vDiffuse *= AO;
	//float3 NormalX;
	//float3 NormalY;
	//float3 NormalZ = { 0,1,0 };

	//float3 Normal;
	//float3 NormalTex;

	//NormalY = normalize(Out.vNormal.xyz);
	//NormalX = normalize(cross(NormalZ, NormalY));
	//NormalZ = normalize(cross(NormalX, NormalY));
	//NormalTex = 2 * tex2D(NormalSampler, In.vTexUV).xyz - 1;

	//Normal.x = dot(NormalX, NormalTex);
	//Normal.y = dot(NormalY, NormalTex);
	//Normal.z = dot(NormalZ, NormalTex);
	//Normal = normalize(Normal);

	//Out.vNormal = vector(Normal.xyz, 0.f);

	//Out.vRMAO.x = tex2D(RoughSampler, In.vTexUV);
	//Out.vRMAO.y = tex2D(MetalSampler, In.vTexUV);
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);

	return Out;
}

PS_OUT PS_MAIN_NOISE(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;


	vector Color = tex2D(NoiseSampler, In.vTexUV);

	//Color.xyz = max(6.10352e-5, Color.xyz);
	//Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = vector(0, 0, 0, Color.r * 0.01f);
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.f, 0.f);
	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.xyz = AO.xyz * 0.5f + 0.5f;

	AO.xyz = max(6.10352e-5, AO.xyz);
	AO.xyz > 0.04045 ? pow(AO.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : AO.xyz * (1.0 / 12.92);

	Out.vDiffuse *= AO;
	//float3 NormalX;
	//float3 NormalY;
	//float3 NormalZ = { 0,0,1 };

	//float3 Normal;
	//float3 NormalTex;

	//NormalY = normalize(Out.vNormal.xyz);
	//NormalX = normalize(cross(NormalZ, NormalY));
	//NormalZ = normalize(cross(NormalX, NormalY));
	//NormalTex = 2 * tex2D(NormalSampler, In.vTexUV).xyz - 1;

	//Normal.x = dot(NormalX, NormalTex);
	//Normal.y = dot(NormalY, NormalTex);
	//Normal.z = dot(NormalZ, NormalTex);
	//Normal = normalize(Normal);

	//Out.vNormal = vector(Normal.xyz, 0.f);

	Out.vRMAO.x = tex2D(RoughSampler, In.vTexUV);
	Out.vRMAO.y = tex2D(MetalSampler, In.vTexUV);
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);

	return Out;
}

PS_OUT PS_MAIN_SKINEDMESH(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 0.f);
	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.x = AO.x * 0.5f + 0.5f;

	AO.x = max(6.10352e-5, AO.x);
	AO.x > 0.04045 ? pow(AO.x * (1.0 / 1.055) + 0.0521327, 2.4) : AO.x * (1.0 / 12.92);

	Out.vDiffuse *= AO.x;

	//float3 NormalX;
	//float3 NormalY;
	//float3 NormalZ = { 0,0,1 };

	//float3 Normal;
	//float3 NormalTex;

	//NormalY = normalize(Out.vNormal.xyz);
	//NormalX = normalize(cross(NormalZ, NormalY));
	//NormalZ = normalize(cross(NormalX, NormalY));
	//NormalTex = 2 * tex2D(NormalSampler, In.vTexUV).xyz - 1;

	//Normal.x = dot(NormalX, NormalTex);
	//Normal.y = dot(NormalY, NormalTex);
	//Normal.z = dot(NormalZ, NormalTex);
	//Normal = normalize(Normal);

	//Out.vNormal = vector(Normal.xyz, 0.f);

	Out.vRMAO.x = AO.y;
	Out.vRMAO.y = AO.z;
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);

	return Out;
}

PS_OUT PS_MAIN_DISSOLVE(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	float DissolveTex = tex2D(DissolveSampler, In.vTexUV).r;
	vector DissolveEffect = tex2D(DisEffectSampler, In.vTexUV);
	float ClipAmount = DissolveTex - g_fTimeAcc;

	if (ClipAmount >= 0.f && ClipAmount < 0.3f)
	{
		Out.vDiffuse = DissolveEffect;
		Out.vDiffuse.xyz = max(6.10352e-5, Out.vDiffuse.xyz);
		Out.vDiffuse.xyz > 0.04045 ? pow(Out.vDiffuse.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Out.vDiffuse.xyz * (1.0 / 12.92);
		Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 0.f);
		Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
		Out.vRMAO = vector(0, 0, 0, 1);
		vector AO = tex2D(AOSampler, In.vTexUV);
		Out.vRMAO.x = AO.y;
		Out.vRMAO.y = AO.z;
		return Out;
	}

	if (ClipAmount < 0.0f)
		return Out;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 0.f);
	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.x = AO.x * 0.5f + 0.5f;

	AO.x = max(6.10352e-5, AO.x);
	AO.x > 0.04045 ? pow(AO.x * (1.0 / 1.055) + 0.0521327, 2.4) : AO.x * (1.0 / 12.92);

	Out.vDiffuse *= AO.x;

	//float3 NormalX;
	//float3 NormalY;
	//float3 NormalZ = { 0,0,1 };

	//float3 Normal;
	//float3 NormalTex;

	//NormalY = normalize(Out.vNormal.xyz);
	//NormalX = normalize(cross(NormalZ, NormalY));
	//NormalZ = normalize(cross(NormalX, NormalY));
	//NormalTex = 2 * tex2D(NormalSampler, In.vTexUV).xyz - 1;

	//Normal.x = dot(NormalX, NormalTex);
	//Normal.y = dot(NormalY, NormalTex);
	//Normal.z = dot(NormalZ, NormalTex);
	//Normal = normalize(Normal);

	//Out.vNormal = vector(Normal.xyz, 0.f);

	Out.vRMAO.x = AO.y;
	Out.vRMAO.y = AO.z;

	return Out;
}

technique	DefaultDevice
{
	pass Phong // 0
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		CullMode = ccw;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG();
	}

	pass CrossBuffer // 1
	{
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x1f;

		CullMode = none;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG_CROSS();
	}

	pass SKIN // 2
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_SKINEDMESH();
	}

	pass EMI // 3
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_EMI();
	}

	pass SKINSTENCIL // 4
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		StencilEnable = true;
		StencilMask = 0x11;
		StencilFunc = always;
		
		VertexShader = compile vs_3_0 VS_MAIN_SKIN();	
		PixelShader = compile ps_3_0 PS_MAIN_PHONG();

		STENCILPASS = REPLACE;
		ZWriteEnable = false;

		AlphaTestEnable = false;
		AlphaBlendEnable = false;
	}

	pass SKINPHANTOM // 5
	{	
		//CullMode = cw;
		//ZEnable = true;
		//ZWriteEnable = false;

		//AlphaTestEnable = false;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		//AlphaFunc = Greater;
		//AlphaRef = 0x0f;

		//StencilEnable = true;		

		ZFUNC = GREATER;

		STENCILMASK = 0x11;
		STENCILFUNC = EQUAL;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_STENCIL();

		STENCILPASS = decr;
		//ZFUNC = less;
		//StencilEnable = false;
	}

	pass Noise // 6
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_NOISE();
	}

	pass Dissolve // 7
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_DISSOLVE();
	}

	pass DissolveStatic // 8
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_DISSOLVE();
	}

	pass WEAPON // 9
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_SKINEDMESH();
	}
}