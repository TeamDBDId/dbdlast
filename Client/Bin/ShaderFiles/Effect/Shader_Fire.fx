

// 컨스턴트 테이블
matrix		g_matWorld, g_matView, g_matProj;
vector		g_vColor;
//
//int			g_iColumn;
//int			g_iRow;

float		g_fTime;



texture		g_DepthTexture;
sampler DepthSampler = sampler_state
{
	texture = g_DepthTexture;	
};


texture		g_Tex0;
sampler Tex0Sampler = sampler_state
{
	texture = g_Tex0;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};
texture		g_Tex1;
sampler Tex1Sampler = sampler_state
{
	texture = g_Tex1;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};
texture		g_Tex2;
sampler Tex2Sampler = sampler_state
{
	texture = g_Tex2;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

struct VS_OUT
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;	
	vector	vProjPos : TEXCOORD1;
	float2	vSubUV : TEXCOORD2;
	//float2	vTexUV2 : TEXCOORD3;
};


VS_OUT VS_Default(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);
	
	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;
	Out.vSubUV = In.vTexUV;// +float2(0.f, g_fTime);

	Out.vProjPos = Out.vPosition;







	return Out;
}


struct PS_IN
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	float2	vSubUV : TEXCOORD2;
};

struct PS_OUT
{
	vector	vColor : COLOR;
};



PS_OUT PS_Core(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vTex1Color = tex2D(Tex1Sampler,float2( In.vSubUV.x, In.vSubUV.y+g_fTime));
	vector	vTex0Color = tex2D(Tex0Sampler, saturate(In.vTexUV+ vTex1Color.r-0.1f));
	
	vector	vTex2Color = tex2D(Tex2Sampler, In.vTexUV);

	vTex0Color.xyz = max(6.10352e-5, vTex0Color.xyz);
	vTex0Color.xyz > 0.04045 ? pow(vTex0Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : vTex0Color.xyz * (1.0 / 12.92);
	vTex1Color.xyz = max(6.10352e-5, vTex1Color.xyz);
	vTex1Color.xyz > 0.04045 ? pow(vTex1Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : vTex1Color.xyz * (1.0 / 12.92);

	vector final = vector(0, 0, 0, 0);
	vector col = vector(0, 0, 0, 0);

	for (int x = -6; x <= 6; ++x)
	{
		//vTex1Color = tex2D(Tex1Sampler, float2(In.vSubUV.x, In.vSubUV.y + g_fTime));
		float2 T = saturate(In.vTexUV + vTex1Color.r - 0.1f);
		T.x += (4.f * x) / 1024.f;
		final = tex2D(Tex0Sampler, T) * exp((-x*x) / 12.f);
		final.xyz = max(6.10352e-5, final.xyz);
		final.xyz > 0.04045 ? pow(final.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : final.xyz * (1.0 / 12.92);
		col.xyz += final.xyz;
		col.a = final.a * 10.f;

		//final = lerp(final, col, 0.5f);
		//final.xyz = max(6.10352e-5, final.xyz);
		//final.xyz > 0.04045 ? pow(final.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : final.xyz * (1.0 / 12.92);
	}
	col.xyz /= 12;

	for (int x = -3; x <= 3; ++x)
	{
		//vTex1Color = tex2D(Tex1Sampler, float2(In.vSubUV.x, In.vSubUV.y + g_fTime));
		float2 T = saturate(In.vTexUV + vTex1Color.r - 0.1f);
		T.y += (4.f * x) / 1024.f;
		final = tex2D(Tex0Sampler, T) * exp((-x*x) / 6.f);
		final.xyz = max(6.10352e-5, final.xyz);
		final.xyz > 0.04045 ? pow(final.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : final.xyz * (1.0 / 12.92);
		col.xyz += final.xyz;
		col.a = final.a * 10.f;
	}

	col.xyz /= 6;
	col.x *= 1.5f;
	col.xyz *= 1.5f;
	if (col.a > 1.f)
		col.a = 1.f;

	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;	

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fViewZ = vDepthInfo.g * 30000.0f;

	Out.vColor = col*vTex2Color;
	//Out.vColor = vTex0Color*vTex2Color;

	Out.vColor.a *= saturate(fViewZ - In.vProjPos.w);// *vTex1Color.r;
	
	Out.vColor.xyz = max(6.10352e-5, Out.vColor.xyz);
	Out.vColor.xyz = min(Out.vColor.xyz * 12.92, pow(max(Out.vColor.xyz, 0.00313067), 1.0 / 2.4) * 1.055 - 0.055);

	return Out;
}


PS_OUT PS_Top(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;


	vector	vTex0Color = tex2D(Tex0Sampler, In.vSubUV*0.5f);
	vector	vTex1Color = tex2D(Tex1Sampler, In.vSubUV);
	vector	vTex2Color = tex2D(Tex2Sampler, In.vTexUV);

	vTex0Color.xyz = max(6.10352e-5, vTex0Color.xyz);
	vTex0Color.xyz > 0.04045 ? pow(vTex0Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : vTex0Color.xyz * (1.0 / 12.92);
	vTex1Color.xyz = max(6.10352e-5, vTex1Color.xyz);
	vTex1Color.xyz > 0.04045 ? pow(vTex1Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : vTex1Color.xyz * (1.0 / 12.92);
	vTex2Color.xyz = max(6.10352e-5, vTex2Color.xyz);
	vTex2Color.xyz > 0.04045 ? pow(vTex2Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : vTex2Color.xyz * (1.0 / 12.92);

	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fViewZ = vDepthInfo.g * 30000.0f;


	Out.vColor = vTex0Color*1.5f;


	Out.vColor.a *= saturate(fViewZ - In.vProjPos.w)*vTex1Color.r*vTex2Color.r*1.5f;// *vTex1Color.r;

	Out.vColor.xyz = max(6.10352e-5, Out.vColor.xyz);
	Out.vColor.xyz = min(Out.vColor.xyz * 12.92, pow(max(Out.vColor.xyz, 0.00313067), 1.0 / 2.4) * 1.055 - 0.055);

	return Out;
}


// technique : 장치 지원여부에 따른 ㅅㅖ이더 선택을 가능하게 하기위해. 
technique	DefaultDevice
{
	pass Fire_Core
	{
		ZEnable = true;
		ZWriteEnable = true;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_Default();
		PixelShader = compile ps_3_0 PS_Core();
	}

	pass Fire_Top
	{
		ZEnable = true;
		ZWriteEnable = true;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_Default();
		PixelShader = compile ps_3_0 PS_Top();
	}
}

