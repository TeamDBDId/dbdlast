

// 컨스턴트 테이블
matrix		g_matWorld, g_matView, g_matProj;
vector		g_vColor;

int			g_iColumn;
int			g_iRow;

float		g_fAlpha;


texture		g_DepthTexture;
sampler DepthSampler = sampler_state
{
	texture = g_DepthTexture;	
};


texture		g_Tex0;
sampler Tex0Sampler = sampler_state
{
	texture = g_Tex0;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_Tex1;
sampler Tex1Sampler = sampler_state
{
	texture = g_Tex1;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

struct VS_OUT
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	float2	vSubUV : TEXCOORD2;
};


VS_OUT VS_Smoke(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);
	
	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;
	Out.vSubUV.x = (In.vTexUV.x + g_iRow)*0.125f;
	Out.vSubUV.y = (In.vTexUV.y + g_iColumn)*0.125f;
	Out.vProjPos = Out.vPosition;

	return Out;
}

VS_OUT VS_Flash(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;
	Out.vSubUV = float2(g_fAlpha, 0.5f);
	Out.vProjPos = Out.vPosition;

	return Out;
}


struct PS_IN
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	float2	vSubUV : TEXCOORD2;
};

struct PS_OUT
{
	vector	vColor : COLOR;
};



PS_OUT PS_Smoke(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vTex0Color = tex2D(Tex0Sampler, In.vSubUV);
	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;	

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fViewZ = vDepthInfo.g * 30000.f;

	Out.vColor = float4(1.f,1.f,1.f, vTex0Color.a*g_fAlpha);
	
	return Out;
}

PS_OUT PS_Flash(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;


	vector	vTex0Color = tex2D(Tex0Sampler, In.vTexUV);
	vector	vTex1Color = tex2D(Tex1Sampler, In.vSubUV);
	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fViewZ = vDepthInfo.g * 30000.f;

	Out.vColor = float4(vTex1Color.rgb, vTex0Color.r*0.4f);
	
	return Out;
}


technique	DefaultDevice
{
	pass Smoke
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		
		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_Smoke();
		PixelShader = compile ps_3_0 PS_Smoke();
	}

	pass Flash
	{
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_Flash();
		PixelShader = compile ps_3_0 PS_Flash();
	}
}

