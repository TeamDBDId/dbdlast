matrix		g_matWorld, g_matView, g_matProj;
float2		g_fUV;
vector		g_vColor;

texture		g_DiffuseTexture;
texture		g_BlendTexture;

float		g_fTime;
bool		g_bAlpha;

float2		g_fAngle;

sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler BlendTexture = sampler_state
{
	texture = g_BlendTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

struct VS_OUT
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

VS_OUT VS_MAIN(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;
	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);
	
	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;

	return Out;
}

struct PS_IN
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
};

struct PS_OUT
{
	vector	vColor : COLOR;
};

PS_OUT PS_MAIN(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	// 픽셀의 색을 결정하는 작업.	
	// tex2D : 텍스쳐의 정보를 담은 샘플러로부터 색을 얻어온다.
	// 1 : 텍스쳐의 정보를 담은 샘플러, 2 : 유브이좌표.
	In.vTexUV = In.vTexUV*	g_fUV;
	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_Fade_Normal(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_UI(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	if (g_bAlpha)
		vDiffuseColor.a = 0.5f;

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_UIFRAME(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	vDiffuseColor.a *= 1.f;

	if (In.vTexUV.x <= (0.5f + g_fTime) && In.vTexUV.x >= (0.5f - g_fTime))
		vDiffuseColor.a *= 0.f;
	if(In.vTexUV.y <= (0.5f + g_fTime) && In.vTexUV.y >= (0.5f - g_fTime))
		vDiffuseColor.a *= 0.f;

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_Check(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	vDiffuseColor.gb = 0.f;
	vDiffuseColor.r = 1.f;
	
	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_Fade(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	vDiffuseColor.a = vDiffuseColor.a * g_fTime;

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_CUSTOMCOLOR(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	vDiffuseColor.rgb = g_vColor.rgb;
	vDiffuseColor.a *= g_vColor.a;

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_ANGLE(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);


	float3  Line = { In.vTexUV.x- 0.5f, In.vTexUV.y - 0.5f, 0.f};
	float3	BaseLine = { 0.f,-1.f,0.f };
	
	float Angle = (acos(dot(normalize(Line), normalize(BaseLine))));
	
	if (In.vTexUV.x < 0.5f)
		Angle = radians(360) - Angle;

	
	if (Angle >g_fAngle.x && Angle < g_fAngle.y)
		Out.vColor = vDiffuseColor;
	else
		Out.vColor.a = 0.f;

	return Out;
}

PS_OUT PS_MAIN_BASEUI(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);
	vDiffuseColor.a *= g_fTime;
	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_UI_FRAME(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);
	float TexAlpha = 0.f;

	if (In.vTexUV.x >= (0.5f - g_fTime*0.5f) && In.vTexUV.x <= (0.5f + g_fTime*0.5f))
		TexAlpha = 1.f;
	if (In.vTexUV.y >= (0.5f - g_fTime*0.5f) && In.vTexUV.y <= (0.5f + g_fTime*0.5f))
		TexAlpha = 1.f;
	vDiffuseColor.a *= TexAlpha;

	Out.vColor = vDiffuseColor;

	return Out;
}


technique	DefaultDevice
{
	pass Default_Rendering
	{
		ZEnable = true;
		ZWriteEnable = true;
		AlphaTestEnable = false;
		AlphaBlendEnable = false;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}

	pass Color_Rendering
	{	
		ZEnable = true;
		ZWriteEnable = true;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}

	pass UI_Rendering
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;
		//AlphaRef = 0x01;
		//AlphaFunc = Greater;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_UI();
	}

	pass UIFrame_Rendering
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		
		AlphaTestEnable = false;

		//AlphaRef = 0x01f;
		//AlphaFunc = Greater;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_UIFRAME();
	}

	pass Check_Rendering
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		//AlphaRef = 0x0f;
		//AlphaFunc = Greater;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_Check();
	}

	pass Fade_Rendering
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		//AlphaTestEnable = true;

		//AlphaRef = 0x0f;
		//AlphaFunc = Greater;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_Fade();
	}

	pass Normal_Rendering
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		//AlphaRef = 0x01f;
		//AlphaFunc = Greater;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_Fade_Normal();
	}

	pass CustomColor_Rendering
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		//AlphaRef = 0x0f;
		//AlphaFunc = Greater;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_CUSTOMCOLOR();
	}

	pass UI_UV_Rendering
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}

	pass UI_Angle_Rendering
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_ANGLE();

	}

	pass UI_Base_Rendering
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_BASEUI();
	}
	pass UI_Base_Rendering
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_UI_FRAME();
	}
	
}


//
//셰이더 비긴
//
//패스 시작(0)
//객체 렌더링
//패스 끝
//
//패스 시작(1)
//객체 렌더링
//패스 끝
//
//
//셰이더 엔드