matrix		g_matWorld, g_matVP;
texture		g_DiffuseTexture;
texture		g_NormalTexture;
texture		g_CubeTexture;
texture		g_AOTexture;
texture		g_MetalicTexture;
texture		g_RoughnessTexture;
texture		g_NoiseTexture;
vector		g_DiffuseColor = vector(0.7f, 0, 0, 1);

sampler NoiseSampler = sampler_state
{
	texture = g_NoiseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler NormalSampler = sampler_state
{
	texture = g_NormalTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler CubeSampler = sampler_state
{
	texture = g_CubeTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

sampler MetalSampler = sampler_state
{
	texture = g_MetalicTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

sampler RoughSampler = sampler_state
{
	texture = g_RoughnessTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

sampler AOSampler = sampler_state
{
	texture = g_AOTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_SkinTexture;
sampler SkinSampler = sampler_state
{
	texture = g_SkinTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_LightEmissive;
sampler LESampler = sampler_state
{
	texture = g_LightEmissive;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

struct VS_IN
{
	float3	vPosition	: POSITION;
	float3	vNormal		: NORMAL;
	float2	vTexUV		: TEXCOORD0;
	float3	vRight		: TEXCOORD1;
	float3	vUp			: TEXCOORD2;
	float3	vLook		: TEXCOORD3;
	float3	vPos		: TEXCOORD4;
};

struct VS_OUT_PHONG
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
};

VS_OUT_PHONG VS_MAIN_PHONG(VS_IN In)
{
	VS_OUT_PHONG			Out = (VS_OUT_PHONG)0;

	matrix		matW, matWVP;

	matW = float4x4(float4(In.vRight, 0.f), float4(In.vUp, 0.f), float4(In.vLook, 0.f), float4(In.vPos, 1.f));
	matWVP = mul(matW, g_matVP);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vNormal = normalize(mul(vector(In.vNormal, 0.f), matW));
	Out.vTexUV = In.vTexUV;
	Out.vProjPos = Out.vPosition;

	return Out;
}


struct PS_IN_PHONG
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
};

struct PS_OUT
{
	vector	vDiffuse : COLOR0;
	vector	vNormal : COLOR1;
	vector	vDepth : COLOR2;
	vector	vRMAO : COLOR3;
};

PS_OUT PS_MAIN_PHONG(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 0.f);
	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.xyz = AO.xyz * 0.5f + 0.5f;

	AO.xyz = max(6.10352e-5, AO.xyz);
	AO.xyz > 0.04045 ? pow(AO.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : AO.xyz * (1.0 / 12.92);

	Out.vDiffuse *= AO;
	//float3 NormalX;
	//float3 NormalY;
	//float3 NormalZ = { 0,0,1 };

	//float3 Normal;
	//float3 NormalTex;

	//NormalY = normalize(In.vNormal.xyz);
	//NormalX = normalize(cross(NormalY, NormalZ));
	//NormalZ = normalize(cross(NormalY, NormalX));
	//NormalTex = 2 * tex2D(NormalSampler, In.vTexUV).xyz - 1;

	//Normal.x = dot(NormalX, NormalTex);
	//Normal.y = dot(NormalY, NormalTex);
	//Normal.z = dot(NormalZ, NormalTex);
	//Normal = normalize(Normal);

	//Out.vNormal = vector(Normal, 0.f);

	Out.vRMAO.x = tex2D(RoughSampler, In.vTexUV);
	Out.vRMAO.y = tex2D(MetalSampler, In.vTexUV);
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);

	return Out;
}

PS_OUT PS_MAIN_PHONG_CROSS(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	Out.vDiffuse = tex2D(DiffuseSampler, In.vTexUV);
	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 500.0f, 0.0f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.xyz = AO.xyz * 0.5f + 0.5f;

	AO.xyz = max(6.10352e-5, AO.xyz);
	AO.xyz > 0.04045 ? pow(AO.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : AO.xyz * (1.0 / 12.92);

	Out.vDiffuse *= AO;
	//float3 NormalX;
	//float3 NormalY;
	//float3 NormalZ = { 0,0,1 };

	//float3 Normal;
	//float3 NormalTex;

	//NormalY = normalize(Out.vNormal.xyz);
	//NormalX = normalize(cross(NormalY, NormalZ));
	//NormalZ = normalize(cross(NormalY, NormalX));
	//NormalTex = 2 * tex2D(NormalSampler, In.vTexUV).xyz - 1;

	//Normal.x = dot(NormalX, NormalTex);
	//Normal.y = dot(NormalY, NormalTex);
	//Normal.z = dot(NormalZ, NormalTex);
	//Normal = normalize(Normal);

	//Out.vNormal = vector(Normal.xyz, 0.f);

	Out.vRMAO.x = tex2D(RoughSampler, In.vTexUV);
	Out.vRMAO.y = tex2D(MetalSampler, In.vTexUV);
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);
	Out.vRMAO.w = 1.f;

	return Out;
}

technique	DefaultDevice
{
	pass Phong // �ȼ��ܿ��� �� ����.
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		CullMode = ccw;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG();
	}

	pass CrossBuffer
	{
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x1f;

		CullMode = none;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG_CROSS();
	}
}