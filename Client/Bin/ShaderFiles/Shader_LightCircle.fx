
matrix		g_matWVP;
texture		g_DiffuseTexture;

sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

struct VS_OUT
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
};

VS_OUT VS_MAIN(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	Out.vPosition = mul(vector(In.vPosition, 1.f), g_matWVP);
	Out.vTexUV = In.vTexUV;
	Out.vProjPos = Out.vPosition;

	return Out;
}

struct PS_IN
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;	
	vector	vProjPos : TEXCOORD1;
};

struct PS_OUT
{
	vector	vDiffuse : COLOR0;
};

PS_OUT PS_MAIN(PS_IN In)
{
	PS_OUT		Out = (PS_OUT)0;

	float fAlpha = tex2D(DiffuseSampler, In.vTexUV).r;

	fAlpha *= 0.3f;
	Out.vDiffuse = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, fAlpha, 1.f);

	return Out;
}

technique	DefaultDevice
{
	pass Light // �ȼ��ܿ��� �� ����.
	{
		ZEnable = true;
		ZWriteEnable = true;

		AlphaBlendEnable = true;

		AlphaTestEnable = false;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}
}