#pragma once
#include "Base.h"
#include "GameObject.h"

_BEGIN(Client)
class CLoadManager : public CBase
{
	_DECLARE_SINGLETON(CLoadManager)
private:
	explicit CLoadManager();
	virtual ~CLoadManager() = default;

public:
	void	Set_GraphicDevice(LPDIRECT3DDEVICE9 pGraphic_Device) { m_pGraphic_Device = pGraphic_Device; m_pGraphic_Device->AddRef(); }
	void	LoadMapData(_uint Stage);

	HRESULT Add_ReplacedName();
	HRESULT Prototype_S1_GameObject();
	HRESULT Prototype_S1_StaticMesh();
	HRESULT Prototype_S1_DynamicMesh();
private:
	_int Find_ObjectName(wstring Key);
private:
	vector<wstring*> m_vecObjectName;
	LPDIRECT3DDEVICE9 m_pGraphic_Device = nullptr;
private:
	virtual void Free() override;

};
_END