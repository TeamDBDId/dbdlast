#pragma once
#include "GameObject.h"
#include "Defines.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CBuffer_RcTex;
class CPicking;
class CTexture;
_END

_BEGIN(Client)

class CPickingTerrain : public CGameObject
{
public:
	explicit CPickingTerrain(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CPickingTerrain(const CPickingTerrain& rhs);
	virtual ~CPickingTerrain() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();

private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CPicking*			m_pPickingCom = nullptr;
private:
	HRESULT Ready_Component();
public:
	static CPickingTerrain* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();
};

_END