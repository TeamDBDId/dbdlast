#include "stdafx.h"
#include "..\Headers\ToolManager.h"
#include "Defines.h"

_USING(Client)
_IMPLEMENT_SINGLETON(CToolManager)



CToolManager::CToolManager()
{
}


void CToolManager::AddPickingPosition(CGameObject * Object, _vec4 vPickingPoint)
{
	POINT		ptMouse;
	GetCursorPos(&ptMouse);
	ScreenToClient(g_hWnd, &ptMouse);
	m_MousePoint = ptMouse;

	if (Object == m_pPickingTerrain)
		m_vPickingPoint = vPickingPoint;
	else if (m_vPickingPoint.w == -1 || vPickingPoint.w < m_vPickingPoint.w)
	{
		m_vPickingPoint = vPickingPoint;
		m_pPickingObject = Object;
	}
}

void CToolManager::Free()
{
}
