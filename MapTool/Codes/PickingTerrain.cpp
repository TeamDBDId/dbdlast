#include "stdafx.h"
#include "..\Headers\PickingTerrain.h"
#include "Management.h"
#include "ToolManager.h"

_USING(Client)

CPickingTerrain::CPickingTerrain(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CPickingTerrain::CPickingTerrain(const CPickingTerrain & rhs)
	: CGameObject(rhs)
{
}

HRESULT CPickingTerrain::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CPickingTerrain::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	CToolManager* pToolManager = GET_INSTANCE(CToolManager);
	pToolManager->AddRef();
	pToolManager->SetPickingTerrain(this);

	m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &_vec3(1000000.f, 0.f, 0.f));
	m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &_vec3(0.f, 1000000.f, 0.f));
	m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &_vec3(0.f, 0.f, 1.f));
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(30000.f, 0.f, 500000.f));

	m_pTransformCom->SetUp_RotationX(D3DXToRadian(90));
	Safe_Release(pToolManager);

	return NOERROR;
}

_int CPickingTerrain::Update_GameObject(const _float & fTimeDelta)
{
	GET_INSTANCE(CToolManager)->ReSetPickingObject();
	if (nullptr != m_pPickingCom)
		m_pPickingCom->Update_Ray();
	_bool bIsPick = false;
	_vec4 vPickingPoint;
	bIsPick = m_pBufferCom->Picking_ToBuffer(&vPickingPoint, m_pTransformCom, m_pPickingCom);

	if (bIsPick)
		GET_INSTANCE(CToolManager)->AddPickingPosition(this, vPickingPoint);
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

_int CPickingTerrain::LastUpdate_GameObject(const _float & fTimeDelta)
{
	return _int();
}

void CPickingTerrain::Render_GameObject()
{
	//if (nullptr == m_pBufferCom)
	//	return;

	//m_pGraphic_Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);

	///*m_pGraphic_Device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	//m_pGraphic_Device->SetRenderState(D3DRS_ALPHAREF, 100);
	//m_pGraphic_Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);	*/

	//m_pTransformCom->SetUp_OnGraphicDev();

	//m_pBufferCom->Render_VIBuffer();

	////m_pGraphic_Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	//m_pGraphic_Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
}

HRESULT CPickingTerrain::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	// For.Com_Picking
	m_pPickingCom = (CPicking*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Picking");
	if (FAILED(Add_Component(L"Com_Picking", m_pPickingCom)))
		return E_FAIL;



	Safe_Release(pManagement);
	return NOERROR;
}

CPickingTerrain * CPickingTerrain::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CPickingTerrain*	pInstance = new CPickingTerrain(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CPickingTerrain Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CPickingTerrain::Clone_GameObject()
{
	CGameObject*	pInstance = new CPickingTerrain(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CGameObject Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CPickingTerrain::Free()
{
	Safe_Release(m_pPickingCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}
