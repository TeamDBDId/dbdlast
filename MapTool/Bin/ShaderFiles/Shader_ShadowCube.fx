matrix	g_matW;
matrix	g_matVP;
vector	g_vLightPos;

struct VS_IN
{
	float3	vPosition : POSITION;
};

struct VS_OUT
{
	vector	vPosition : POSITION0;
	float3	vLight : TEXCOORD0;
};

VS_OUT VS_MAIN(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	float4x4 matWVP;
	matWVP = mul(g_matW, g_matVP);

	vector vPos = mul(vector(In.vPosition, 1.f), g_matW);
	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);

	Out.vLight = (g_vLightPos.xyz - vPos.xyz);

	return Out;
}

struct PS_IN
{
	vector	vPosition : POSITION0;
	float3	vLight : TEXCOORD0;
};

struct PS_OUT
{
	vector		Tex : COLOR0;
};


PS_OUT PS_MAIN(PS_IN In)
{
	PS_OUT		Out = (PS_OUT)0;

	Out.Tex.rgba = length(In.vLight) + 6.f;

	return Out;
}

technique	Tech0
{
	pass Cube
	{
		ZEnable = true;
		ZWriteEnable = true;
		COLORWRITEENABLE = red;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();

		COLORWRITEENABLE = red | green | blue | alpha;
	}
}