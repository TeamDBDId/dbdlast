matrix		g_matWorld, g_matView, g_matProj;
texture		g_DiffuseTexture;
texture		g_NormalTexture;
texture		g_CubeTexture;

sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler NormalSampler = sampler_state
{
	texture = g_NormalTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler CubeSampler = sampler_state
{
	texture = g_CubeTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

struct VS_IN
{
	float3	vPosition	: POSITION;
	float3	vNormal		: NORMAL;
	float2	vTexUV		: TEXCOORD0;	
	float3	vRight		: TEXCOORD1;
	float3	vUp		: TEXCOORD2;
	float3	vLook		: TEXCOORD3;
	float3	vPos		: TEXCOORD4;
};

struct VS_OUT_PHONG
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
};

VS_OUT_PHONG VS_MAIN_PHONG(VS_IN In)
{
	VS_OUT_PHONG			Out = (VS_OUT_PHONG)0;

	matrix		matW, matWV, matWVP;

	matW = matrix(vector(In.vRight, 0.f), vector(In.vUp, 0.f), vector(In.vLook, 0.f), vector(In.vPos, 1.f));
	matWV = mul(matW, g_matView);
	matWVP = mul(matWV, g_matProj);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vNormal = normalize(mul(vector(In.vNormal, 0.f), matW));
	Out.vTexUV = In.vTexUV;
	Out.vProjPos = Out.vPosition;	

	return Out;
}


struct PS_IN_PHONG
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;	
	vector	vProjPos : TEXCOORD1;
};

struct PS_OUT
{
	vector	vDiffuse : COLOR0;
	vector	vNormal : COLOR1;
	vector	vDepth : COLOR2;
};

PS_OUT PS_MAIN_PHONG(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	Out.vDiffuse = tex2D(DiffuseSampler, In.vTexUV);
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 50000.0f, 0.f, 0.f);

	float3 NormalX;
	float3 NormalY;
	float3 NormalZ = { 0,0,1 };

	float3 Normal;
	float3 NormalTex;

	NormalY = normalize(In.vNormal.xyz);
	NormalX = normalize(cross(NormalY, NormalZ));
	NormalZ = normalize(cross(NormalY, NormalX));
	NormalTex = 2 * tex2D(NormalSampler, In.vTexUV).xyz - 1;

	Normal.x = dot(NormalX, NormalTex);
	Normal.y = dot(NormalY, NormalTex);
	Normal.z = dot(NormalZ, NormalTex);
	Normal = normalize(Normal);

	Out.vNormal = vector(Normal, 0.f);

	return Out;
}

PS_OUT PS_MAIN_PHONG_CROSS(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	Out.vDiffuse = tex2D(DiffuseSampler, In.vTexUV);	
	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 500.0f, 0.f, 0.f);

	return Out;
}

technique	DefaultDevice
{
	pass Phong // �ȼ��ܿ��� �� ����.
	{	
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		CullMode = ccw;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG();
	}

	pass CrossBuffer 
	{
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x1f;

		CullMode = none;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG_CROSS();
	}
}