
//vector			g_vScreenLightPos = vector(0, 0, 0, 0);

texture			g_DiffuseTexture;

sampler	DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture			g_ShadeTexture;

sampler	ShadeSampler = sampler_state
{
	texture = g_ShadeTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};


texture			g_SpecularTexture;

sampler	SpecularSampler = sampler_state
{
	texture = g_SpecularTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture			g_RefTexture;

sampler	RefSampler = sampler_state
{
	texture = g_RefTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture			g_LightTexture;

sampler	UserMapSampler = sampler_state
{
	texture = g_LightTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};


struct PS_IN
{
	vector		vPosition : POSITION;
	float2		vTexUV : TEXCOORD0;
};

struct PS_OUT
{
	vector		vColor : COLOR0;
};

float4 LightShaft(float2 texCoord, float2 vScreenLPos)
{
	float decay = 0.97815;
	float exposure = 0.92;
	float density = 0.966;
	float weight = 0.58767;
	int NUM_SAMPLES = 30;
	float2 tc = texCoord;
	float2 deltaTexCoord = (tc - vScreenLPos.xy);
	deltaTexCoord *= 1.0f / NUM_SAMPLES * density;
	float illuminationDecay = 1.0;
	float4 color = tex2D(UserMapSampler, tc)*0.4;
	color.w = 1.f;
	for (int i = 0; i < NUM_SAMPLES; i++)
	{
		tc -= deltaTexCoord;
		float4 samp = tex2D(UserMapSampler, tc)*0.4;
		samp.w = 1.f;
		samp *= illuminationDecay * weight;
		color += samp;
		illuminationDecay *= decay;
	}

	return float4((float3(color.r,color.g,color.b) * exposure),1);
}

PS_OUT PS_MAIN(PS_IN In)
{
	PS_OUT			Out = (PS_OUT)0;

	vector			vRef = tex2D(RefSampler, In.vTexUV);
	vector			vDiffuse = /*pow(*/tex2D(DiffuseSampler, In.vTexUV)/*, 2.2f)*/;
	vector			vShade = tex2D(ShadeSampler, In.vTexUV);
	vector			vSpecular = tex2D(SpecularSampler, In.vTexUV);	

	vector vColor = vector(0,0,0,1);
	float vFog = vSpecular.y;
	//if (vFog < 0.0001f)
	//{
	//	Out.vColor = vector(0.4f, 0.4f, 0.4f, 1.f);
	//	return Out;
	//}



	//float2 vVelosity = vSpecular.zw;

	//In.vTexUV += vVelosity;
	//for (int i = 1; i <6; ++i, In.vTexUV += vVelosity)
	//{
	//	float4 currentColor = tex2D(ShadeSampler, In.vTexUV) * tex2D(DiffuseSampler, In.vTexUV) /*+ tex2D(RefSampler, In.vTexUV);*/;
	//	vColor += currentColor;
	//}

	//float4 finalColor = vColor / 6.f;
	float4 final = vDiffuse;
	float vDir = vSpecular.x;


	float4 col;
	//if (vDir > 0.01f)
	//{
		for (int x = -6; x <= 6; ++x)
		{
			float2 T = In.vTexUV;
			T.x += (2.f * x) / 1024.f;
			col += tex2D(DiffuseSampler, /*float4(T.x, T.y, 0, 0)*/T) * exp((-x*x) / 12.f);
			final = lerp(final, col, /*(1.f - factor) * (1.f - factor)*/ vDir * 0.002f);
		}
		for (int x = -3; x <= 3; ++x)
		{
			float2 T = In.vTexUV;
			T.y += (2.f * x) / 1024.f;
			col += tex2D(DiffuseSampler, /*float4(T.x, T.y, 0, 0)*/T) * exp((-x*x) / 12.f);
			final = lerp(final, col, /*(1.f - factor) * (1.f - factor)*/ vDir * 0.002f);
		}

		if (final.x > 0.8f && final.y > 0.8f && final.z > 0.8f)
		{
			for (int x = -14; x <= 14; ++x)
			{
				float2 T = In.vTexUV;
				T.x += (2.f * x) / 1024.f;
				col += tex2D(DiffuseSampler, /*float4(T.x, T.y, 0, 0)*/T) * exp((-x*x) / 12.f);
				final = lerp(final, col, /*(1.f - factor) * (1.f - factor)*/ 0.5f);
			}
			for (int x = -7; x <= 7; ++x)
			{
				float2 T = In.vTexUV;
				T.y += (2.f * x) / 1024.f;
				col += tex2D(DiffuseSampler, /*float4(T.x, T.y, 0, 0)*/T) * exp((-x*x) / 12.f);
				final = lerp(final, col, /*(1.f - factor) * (1.f - factor)*/ 0.5f);
			}
		}

		//if (vRef.w > 0.02f)
		//{
		//	//for (int x = -6; x <= 6; ++x)
		//	//{
		//	//	float2 T = In.vTexUV;
		//	//	T.x += (25.f * x * vRef.w) / 1024.f;
		//	//	col += tex2D(DiffuseSampler, /*float4(T.x, T.y, 0, 0)*/T) * exp((-x*x) / 12.f);
		//	//	final = lerp(final, col, /*(1.f - factor) * (1.f - factor)*/ vDir * 0.02f);
		//	//}
		//	final = vector(1, 0, 0, 1);
		//}

	//}
	//else
	//	final = vDiffuse;



	vColor = final * vShade + vRef;

	vColor = vFog * vColor + (1.f - vFog) *  (vector(0.0323f, 0.0272f, 0.0252f, 1.f));

	//col /= 12.f;

	//float factor = max(((0.5f - length(vDir)) / 0.5f), 0.f);
	//float factor = /*((vDir * 1.9f)*//* * (length(vDir) * 1.8f);*/ vDir;
	//Out.vColor = lerp(finalColor, col, /*(1.f - factor) * (1.f - factor)*/factor);

	//}
	//else
	
	//Out.vColor = finalColor;

	//if (g_vScreenLightPos.x > 0.f/* && g_vScreenLightPos.z < 0.f*/ && g_vScreenLightPos.x < 1280.f
	//	&& g_vScreenLightPos.y > 0.f && g_vScreenLightPos.y < 720.f)
	//{
	//vector ShaftColor = LightShaft(In.vTexUV, g_vScreenLightPos.xy);
	//if (ShaftColor.x > 0.2f)
	//{
	//	vector	vShaft = vColor * 1.1f + ShaftColor;

	//	vColor = vShaft;
	//}

	//}
	//else if (g_vScreenLightPos.x > 0.f && g_vScreenLightPos.z > 0.f)
	//{
	//	vColor += LightShaft(In.vTexUV, g_vScreenLightPos.xy);
	//	vColor += LightShaft(In.vTexUV, g_vScreenLightPos.zw);
	//	vColor *= 0.5f;
	//}

	vColor.xyz = max(6.10352e-5, vColor.xyz);
	vColor.xyz = min(vColor.xyz * 12.92, pow(max(vColor.xyz, 0.00313067), 1.0 / 2.4) * 1.055 - 0.055);
	//float Luminance = dot(RGB, float3(0.2125f, 0.7154f, 0.0721f));
	vColor.a = 1.f;
	Out.vColor = /*pow(vColor, 1.f/2.2f);*/vColor;

	return Out;
}

technique	DefaultDevice
{
	pass Render_Blend
	{
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0;

		ZEnable = false;
		ZWriteEnable = false;

		VertexShader = NULL;
		PixelShader = compile ps_3_0 PS_MAIN();
	}
}


