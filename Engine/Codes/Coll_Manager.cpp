#pragma once

#include "Coll_Manager.h"

#include "GameObject.h"
#include "Collider.h"
#include "Transform.h"

#include "KeyManager.h"

_USING(Engine)
_IMPLEMENT_SINGLETON(CColl_Manager)

CColl_Manager::CColl_Manager()
{
}


HRESULT CColl_Manager::Add_CollGroup(eCollGroup _eGroup, CGameObject * _pObj, _bool* _bool)
{

	COL* col = new COL;

	col->pObj = _pObj;
	col->pCollider = _pObj->Get_ComponentPointer(L"Com_Collider");


	_uint iIdx = 0;
	if (C_PLAYER == _eGroup || C_WOOD == _eGroup|| C_EXIT == _eGroup||C_WEAPON== _eGroup)
		iIdx = 0;
	else
		iIdx = _pObj->Get_PosIdx();


	if (C_PLANK == _eGroup)
	{
		col->pIsFall = _bool;
	}




	if (MAXIDX <= iIdx)
		_MSG_BOX("CColl_Manager Idx Fail");


	m_CollList[iIdx][_eGroup].push_back(col);





	return NOERROR;
}



HRESULT CColl_Manager::Erase_Obj(eCollGroup _eGroup, CGameObject * _pObj)
{

	_uint iIdx = 0;
	if (C_PLAYER == _eGroup || C_WOOD == _eGroup|| C_EXIT == _eGroup)
		iIdx = 0;
	else
		iIdx = _pObj->Get_PosIdx();


	for (auto& iter = m_CollList[iIdx][_eGroup].begin(); iter != m_CollList[iIdx][_eGroup].end();)
	{
		if (*iter != nullptr)
		{

			if ((*iter)->pObj == _pObj)
			{
				Safe_Delete(*iter);
				iter = m_CollList[iIdx][_eGroup].erase(iter);
				return NOERROR;
			}
			else
				iter++;

		}
	}




	return NOERROR;
}

HRESULT CColl_Manager::Coll(const _float& _fTime)
{

	if (true == ((CGameObject*)m_CollList[0][C_PLAYER][0]->pObj)->Get_IsColl())
	{
		Coll_Player_Player();
		Coll_Player_Wood();
		Coll_Player_ExitBuilding();
		Coll_Player_LAY_IdxCheck();
		Coll_Player_HOUSEHUT_IdxCheck(_fTime);
		Coll_Player_Interation_IdxCheck();
		Coll_Player_Weapon();
	}
	

	
	

	
	

	
	

	return NOERROR;
}




void CColl_Manager::Coll_Player_LAY_IdxCheck()
{

	_uint iPlayerIdx = ((CGameObject*)m_CollList[0][C_PLAYER][0]->pObj)->Get_PosIdx();

	_uint iObjIdx = iPlayerIdx - 15;





	for (size_t i = 0; i < 3; ++i)
	{
		for (size_t j = 0; j < 3; ++j)
		{

			if (0 <= iObjIdx&&iObjIdx < 196)//유효한인덱스일경우
			{
				Coll_Player_LAY(iObjIdx);
			
			}
			iObjIdx++;
		}
		iObjIdx += 11;
	}


}

void CColl_Manager::Coll_Player_LAY(const _uint& _iIdx)
{
	COL* pPlayer = m_CollList[0][C_PLAYER][0];

		
	if (m_CollList[_iIdx][C_LAY].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;



	for (auto& pObj : m_CollList[_iIdx][C_LAY])
	{
		if (((CCollider*)pObj->pCollider)->Collision_Sphere(((CCollider*)pPlayer->pCollider)))
		{
			CTransform* pPlayerTrans = (CTransform*)((CGameObject*)pPlayer->pObj)->Get_ComponentPointer(L"Com_Transform");

			_vec3 vGap = _vec3(0.f, 0.f, 0.f);

			((CCollider*)pObj->pCollider)->Collision_LAY(pPlayerTrans, &vGap);
			//((CCollider*)pObj->pCollider)->Collision_LAY_Sub(pPlayerTrans, &vGap);
			pPlayerTrans->Move_V3(vGap);
		}
	}


}

void CColl_Manager::Coll_Player_HOUSEHUT_IdxCheck(const _float& _fTime)
{
	CGameObject* pPlayer = (CGameObject*)m_CollList[0][C_PLAYER][0]->pObj;
	_uint iPlayerIdx = pPlayer->Get_PosIdx();
	CTransform* pPlayerTrans = (CTransform*)pPlayer->Get_ComponentPointer(L"Com_Transform");

	
	if ((78 <= iPlayerIdx&&iPlayerIdx <= 81) ||
		(92 <= iPlayerIdx&&iPlayerIdx <= 95) ||
		(106 <= iPlayerIdx&&iPlayerIdx <= 109) ||
		(121 <= iPlayerIdx&&iPlayerIdx <= 123))
	{
		Coll_Player_HOUSEHUT(_fTime, C_HOUSE,94);
		Coll_Player_HOUSEHUT(_fTime, C_TERRAIN, 108);

		if (pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION)->y < 0)
		{
			Coll_Player_HOUSEHUT(_fTime, C_UNDER, 94);

		}


	}
	else
	if ((155 <= iPlayerIdx&&iPlayerIdx <= 156) ||
		(169 <= iPlayerIdx&&iPlayerIdx <= 170))
	{
		Coll_Player_HOUSEHUT(_fTime, C_HUT,155);
	}
	else if(69== iPlayerIdx|| 70 == iPlayerIdx || 83 == iPlayerIdx || 84== iPlayerIdx)
	{
		Coll_Player_ExitDoor(83);
		Coll_Player_HOUSEHUT(_fTime, C_HUT, 84);
	}
	else if (139 == iPlayerIdx || 140 == iPlayerIdx || 153 == iPlayerIdx || 154 == iPlayerIdx)
	{
		Coll_Player_ExitDoor(154);
		Coll_Player_HOUSEHUT(_fTime, C_HUT, 140);
	}


}


void CColl_Manager::Coll_Player_HOUSEHUT(const _float& _fTime, const eCollGroup& _eGroup, const _uint& _iIdx)
{

	COL* pPlayer = m_CollList[0][C_PLAYER][0];
	COL* pObj = m_CollList[_iIdx][_eGroup][0];

	CGameObject* pPlayerObj = (CGameObject*)pPlayer->pObj;

	CTransform* pPlayerTrans = (CTransform*)pPlayerObj->Get_ComponentPointer(L"Com_Transform");
	

	_vec3 vGap = _vec3(0.f, 0.f, 0.f);


	if (C_HOUSE == _eGroup|| C_UNDER == _eGroup)
	{
		((CCollider*)pObj->pCollider)->Collision_LAY(pPlayerTrans, &vGap);
		//((CCollider*)pObj->pCollider)->Collision_LAY_Sub(pPlayerTrans, &vGap);
	}
	else if (C_HUT == _eGroup)
	{
		_vec3 vPos= *pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION);
		if (vPos.y <= 0.f)
		{
			vPos.y = 0.f;

			pPlayerTrans->Set_StateInfo(CTransform::STATE_POSITION,&vPos);

			pPlayerTrans->Set_Acc(0.f);
		}
	}
	

	_bool isFall = false;

	if (((CCollider*)pObj->pCollider)->Collision_LAY_Terr(pPlayerTrans,&isFall))
	{	
		pPlayerTrans->Set_Acc(0.f);

		/*if (true == isFall)
		{
			pPlayerObj->Set_IsFall(isFall);
		}
		else
		{
			if (C_HOUSE == _eGroup || C_UNDER == _eGroup)
				pPlayerObj->Set_IsFall(isFall);
		}*/

		
	}
	else
	{
	/*	if (C_HOUSE == _eGroup || C_UNDER == _eGroup)
			pPlayerObj->Set_IsFall(isFall);*/

		pPlayerTrans->Move_Gravity(_fTime);
	}

	pPlayerObj->Set_IsFall(pPlayerTrans->Get_fAcc() < -100.f);
	pPlayerTrans->Move_V3(vGap);
	

}

void CColl_Manager::Coll_Player_Wood()
{

	COL* pPlayer = m_CollList[0][C_PLAYER][0];
	COL* pObj = m_CollList[0][C_WOOD][0];


	
	CTransform* pPlayerTrans = (CTransform*)((CGameObject*)pPlayer->pObj)->Get_ComponentPointer(L"Com_Transform");

	_vec3 vGap = _vec3(0.f, 0.f, 0.f);
	_bool isColl = false;


	if (((CCollider*)pObj->pCollider)->Collision_Sphere(((CCollider*)pPlayer->pCollider)))
	{
		((CCollider*)pObj->pCollider)->Collision_LAY(pPlayerTrans, &vGap);
		//((CCollider*)pObj->pCollider)->Collision_LAY_Sub(pPlayerTrans, &vGap);
		pPlayerTrans->Move_V3(vGap);
			
	}
}

void CColl_Manager::Coll_Player_ExitBuilding()
{
	COL* pPlayer = m_CollList[0][C_PLAYER][0];
	CTransform* pPlayerTrans = (CTransform*)((CGameObject*)pPlayer->pObj)->Get_ComponentPointer(L"Com_Transform");

	for (auto& pObj : m_CollList[0][C_EXIT])
	{

		_vec3 vGap = _vec3(0.f, 0.f, 0.f);


		((CCollider*)pObj->pCollider)->Collision_LAY(pPlayerTrans, &vGap);
		//((CCollider*)pObj->pCollider)->Collision_LAY_Sub(pPlayerTrans, &vGap);
		pPlayerTrans->Move_V3(vGap);


	}

	


	

	
	

	

	
}

void CColl_Manager::Coll_Player_ExitDoor(const _uint& _iIdx)
{
	COL* pObj = m_CollList[_iIdx][C_EXITDOOR][0];

	

	COL* pPlayer = m_CollList[0][C_PLAYER][0];
	


	CTransform* pPlayerTrans = (CTransform*)((CGameObject*)pPlayer->pObj)->Get_ComponentPointer(L"Com_Transform");


	_vec3 vGap = _vec3(0.f, 0.f, 0.f);


	((CCollider*)pObj->pCollider)->Collision_LAY(pPlayerTrans, &vGap);
//	((CCollider*)pObj->pCollider)->Collision_LAY_Sub(pPlayerTrans, &vGap);
	pPlayerTrans->Move_V3(vGap);

}

void CColl_Manager::Coll_Player_Player()
{
	if (0==m_CollList[0][C_PLAYER].size()) //해당인덱스의 오브젝트가 없으면 리턴
		return;



	COL* pPlayer = m_CollList[0][C_PLAYER][0];	//나
	CGameObject* pPlayerObj = (CGameObject*)pPlayer->pObj;

	if (false == pPlayerObj->Get_IsColl())
		return;

	for (auto& pObj : m_CollList[0][C_PLAYER])//다른플레이어들
	{
		if (pPlayer == pObj)//나랑,나랑은 충돌안함
			continue;


		if (false == ((CGameObject*)pObj->pObj)->Get_IsColl())
			continue;

		_vec3 vGap = _vec3(0.f, 0.f, 0.f);



		if (((CCollider*)pObj->pCollider)->Collision_Player(((CCollider*)pPlayer->pCollider),&vGap))
		{
			CTransform* pPlayerTrans = (CTransform*)pPlayerObj->Get_ComponentPointer(L"Com_Transform");

			if (KEYMGR->KeyPressing(DIK_W)|| KEYMGR->KeyPressing(DIK_S)|| KEYMGR->KeyPressing(DIK_A)|| KEYMGR->KeyPressing(DIK_D))
				pPlayerTrans->Move_V3(vGap);
			_vec3 vPos = *pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION);
			pPlayerObj->Do_Coll((CGameObject*)pObj->pObj, vPos);
		}
	}


}



void CColl_Manager::Coll_Player_Interation_IdxCheck()
{

	_uint iPlayerIdx = ((CGameObject*)m_CollList[0][C_PLAYER][0]->pObj)->Get_PosIdx();

	_uint iObjIdx = iPlayerIdx - 15;





	for (size_t i = 0; i < 3; ++i)
	{
		for (size_t j = 0; j < 3; ++j)
		{

			if (0 <= iObjIdx&&iObjIdx < 196)//유효한인덱스일경우
			{
				Coll_Player_Interation(iObjIdx, C_CHEST);
				Coll_Player_Interation(iObjIdx, C_HOOK);
				Coll_Player_Interation(iObjIdx, C_CLOSET);

				Coll_Player_Interation(iObjIdx, C_GENER);
				Coll_Player_Interation(iObjIdx, C_WINDOW);
				Coll_Player_Interation(iObjIdx, C_EXITDOOR);

				Coll_Player_Interation(iObjIdx, C_HATCH);
				Coll_Player_Interation(iObjIdx, C_TOTEM);

				Coll_Player_Interation(iObjIdx, C_PLANK);
				//추후 오브젝트

			}
			iObjIdx++;
		}
		iObjIdx += 11;
	}

}

void CColl_Manager::Coll_Player_Interation(const _uint& _iIdx, const eCollGroup& _eCollGroup)
{
	COL* pPlayer = m_CollList[0][C_PLAYER][0];
	CTransform* pPlayerTrans = (CTransform*)((CGameObject*)pPlayer->pObj)->Get_ComponentPointer(L"Com_Transform");

	if (m_CollList[_iIdx][_eCollGroup].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;


	_float fDist = 0.f;

	for (auto& pObj : m_CollList[_iIdx][_eCollGroup])
	{
		if (C_HOOK == _eCollGroup)
		{
			_vec3 vGap = _vec3(0.f, 0.f, 0.f);
			((CCollider*)pObj->pCollider)->Collision_LAY(pPlayerTrans, &vGap);
			pPlayerTrans->Move_V3(vGap);
		}
		else if (C_PLANK == _eCollGroup)
		{
			CGameObject* pInterObj = (CGameObject*)pObj->pObj;
			if (true == pInterObj->Get_IsFall())//넘어졌다면
			{
				_vec3 vGap = _vec3(0.f, 0.f, 0.f);
				((CCollider*)pObj->pCollider)->Collision_LAY(pPlayerTrans, &vGap, true);
				pPlayerTrans->Move_V3(vGap);
			}
			else//안넘어졌으면
			{
				_vec3 vGap = _vec3(0.f, 0.f, 0.f);
				((CCollider*)pObj->pCollider)->Collision_LAY(pPlayerTrans, &vGap);
				pPlayerTrans->Move_V3(vGap);
			}


		}


		

		if (((CCollider*)pObj->pCollider)->Collision_Sphere(((CCollider*)pPlayer->pCollider), &fDist))
		{


			CGameObject* pPlayerObj = (CGameObject*)pPlayer->pObj;
			CGameObject* pInterObj = (CGameObject*)pObj->pObj;
			CTransform* pObjTrans = (CTransform*)pInterObj->Get_ComponentPointer(L"Com_Transform");

			_float fDist = 1.f;
			if (pPlayerObj->GetID() & 0x200)//슬레셔일떄믄 1.5배
				fDist = 1.5f;
			

			if (false == pInterObj->Get_IsColl())
				continue;

			if (C_EXITDOOR != _eCollGroup&&C_WINDOW != _eCollGroup&&C_HATCH!= _eCollGroup&&C_TOTEM != _eCollGroup&&C_PLANK!= _eCollGroup&&C_HOOK!= _eCollGroup)
			{
				//이동관련
				_vec3 vGap = _vec3(0.f, 0.f, 0.f);
				((CCollider*)pObj->pCollider)->Collision_LAY(pPlayerTrans, &vGap);
				pPlayerTrans->Move_V3(vGap);
			}
			
	
			

			

			
			const _vec3& vPlayerPos = *pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION);
			const _vec3& vObjPos = *pObjTrans->Get_StateInfo(CTransform::STATE_POSITION);
			_vec3 vPos;
			_float minLength = 10000.f;
			switch (_eCollGroup)
			{
			case C_TOTEM:
				vPos = vObjPos;
				pPlayerObj->Do_Coll(pInterObj, vPos);
				break;
			case C_CHEST:
			case C_CLOSET:
			
				vPos = vObjPos+pInterObj->Get_InterPos(0)*fDist;
				pPlayerObj->Do_Coll(pInterObj, vPos);
				break;
			case C_HOOK:
				vPos = vObjPos + pInterObj->Get_InterPos(0)*fDist*0.1f;
				pPlayerObj->Do_Coll(pInterObj, vPos);
				break;
			case C_GENER:

				for (size_t i = 0; i < 4; ++i)
				{
					_vec3 vLength = vPlayerPos - (vObjPos+pInterObj->Get_InterPos(i));
					if (D3DXVec3Length(&vLength) < minLength)
					{
						minLength = D3DXVec3Length(&vLength);
						vPos = vObjPos+pInterObj->Get_InterPos(i)*fDist;
					}
						
				}
				
				pPlayerObj->Do_Coll(pInterObj, vPos);
				break;
			case C_WINDOW:
				for (size_t i = 0; i < 2; ++i)
				{
					_vec3 vLength = vPlayerPos - (vObjPos+pInterObj->Get_InterPos(i));
					if (D3DXVec3Length(&vLength) < minLength)
					{
						minLength = D3DXVec3Length(&vLength);
						vPos = vObjPos+pInterObj->Get_InterPos(i)*fDist;
					}

				}
				pPlayerObj->Do_Coll(pInterObj, vPos);
				break;
			case C_EXITDOOR:
			case C_HATCH:
				vPos = ((CCollider*)pObj->pCollider)->Get_Center();
				pPlayerObj->Do_Coll(pInterObj, vPos);
				break;
			case C_PLANK:
				//if()
				_bool isColl = false;

				if (KEYMGR->KeyPressing(DIK_LSHIFT))//쉬프트를 누르고있을때는
				{
					if (true == pInterObj->Get_IsFall())//넘어졌다면
						isColl = true;
					else//안넘어졌다면
					{
						_vec3 vLook = *pPlayerTrans->Get_StateInfo(CTransform::STATE_LOOK);
						D3DXVec3Normalize(&vLook, &vLook);

						_vec3 vDir = ((CCollider*)pObj->pCollider)->Get_Center() - *pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION);
						D3DXVec3Normalize(&vDir, &vDir);

						if (D3DXVec3Dot(&vLook, &vDir)<0)//뒤에있을때만
							isColl = true;
					}
				}
				else//안누르고 있을때는  
					isColl = true;
				
				if (true == isColl)
				{
					for (size_t i = 0; i < 2; ++i)
					{
						_vec3 vLength = vPlayerPos - (vObjPos + pInterObj->Get_InterPos(i));
						if (D3DXVec3Length(&vLength) < minLength)
						{
							minLength = D3DXVec3Length(&vLength);
							vPos = vObjPos + pInterObj->Get_InterPos(i)*fDist;
						}

					}
					pPlayerObj->Do_Coll(pInterObj, vPos);
				}

				break;
			}
			




//			((CGameObject*)pPlayer->pObj)->Do_Coll((CGameObject*)pObj->pObj,vPos);
		}
	}



}

void CColl_Manager::Coll_Player_Weapon()
{
	if (0 == m_CollList[0][C_WEAPON].size())
		return;

	COL* pPlayer = m_CollList[0][C_PLAYER][0];
	COL* pWeapon = m_CollList[0][C_WEAPON][0];
	CGameObject* pPlayerObj = (CGameObject*)pPlayer->pObj;
	CGameObject* pWeaponObj = (CGameObject*)pWeapon->pObj;


	if (false == pWeaponObj->Get_IsColl())
		return;


	if (pPlayerObj->GetID() & 0x200)//내가 슬래셔일떄
	{


		for (auto& pObj : m_CollList[0][C_PLAYER])//모든플래이어를돌면서
		{
			if (pPlayer == pObj)
				continue;
			

			if (((CCollider*)pWeapon->pCollider)->Collision_Sphere(((CCollider*)pObj->pCollider)))
			{
				CGameObject* pCamperObj = (CGameObject*)pObj->pObj;
				pCamperObj->Do_Coll(pWeaponObj);
				pWeaponObj->Do_Coll(pCamperObj);
			}


		}


	
	}


	






	



	

}



void CColl_Manager::Free()
{

	for (size_t i = 0; i < MAXIDX; i++)
	{

		for (size_t j = 0; j < C_END; j++)
		{
			if (m_CollList[i][j].size() <= 0)
				continue;

			for (auto& pCol : m_CollList[i][j])
			{
				Safe_Delete(pCol);
			}

			m_CollList[i][j].clear();
		}

	}

}

