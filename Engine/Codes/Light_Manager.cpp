#include "..\Headers\Light_Manager.h"
#include "Light.h"

_IMPLEMENT_SINGLETON(CLight_Manager)

CLight_Manager::CLight_Manager()
{
}

D3DLIGHT9 * CLight_Manager::Get_LightInfo(const _uint & iIndex)
{
	if (m_LightList.size() <= iIndex)
		return nullptr;

	auto	iter = m_LightList.begin();

	for (size_t i = 0; i < iIndex; ++i)
		++iter;	

	return (*iter)->Get_LightInfo();
}

_vec3 CLight_Manager::GetProjPos(const _uint & iIndex)
{
	if (m_LightList.size() <= iIndex)
		return _vec3(0,0,0);

	auto	iter = m_LightList.begin();

	for (size_t i = 0; i < iIndex; ++i)
		++iter;

	return (*iter)->GetProjPos();
}

HRESULT CLight_Manager::Add_LightInfo(LPDIRECT3DDEVICE9 pGraphic_Device, const D3DLIGHT9 & LightInfo)
{
	CLight*			pLight = CLight::Create(pGraphic_Device, LightInfo);
	if (nullptr == pLight)
		return E_FAIL;

	m_LightList.push_back(pLight);

	return NOERROR;
}

CLight * CLight_Manager::Add_LightInfo2(LPDIRECT3DDEVICE9 pGraphic_Device, const D3DLIGHT9 & LightInfo)
{
	CLight*			pLight = CLight::Create(pGraphic_Device, LightInfo);
	if (nullptr == pLight)
		return nullptr;

	m_LightList.push_back(pLight);

	return pLight;
}

void CLight_Manager::Render_Light(LPD3DXEFFECT pEffect)
{
	for (auto& pLight : m_LightList)
		pLight->Render_Light(pEffect);
}

void CLight_Manager::Ready_ShadowMap(CLight * pLight)
{
	for (auto Light : m_LightList)
		if (pLight == Light)
			Light->Render_ShadowMap();
}

//void CLight_Manager::Render_DynamicShadow()
//{
//	for (auto Light : m_LightList)
//		Light->Render_DynamicShadowMap();
//}

void CLight_Manager::Free()
{
	for (auto& pLight : m_LightList)
		Safe_Release(pLight);
	m_LightList.clear();
}
