#include "..\Headers\Mesh_Dynamic.h"
#include "HierarchyLoader.h"
#include "AnimationCtrl.h"
#include "MeshTexture.h"
CMesh_Dynamic::CMesh_Dynamic(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CComponent(pGraphic_Device)
{

}

CMesh_Dynamic::CMesh_Dynamic(const CMesh_Dynamic & rhs)
	: CComponent(rhs)
	, m_vecMeshContainer(rhs.m_vecMeshContainer)
	, m_pLoader(rhs.m_pLoader)
	, m_pRootFrame(rhs.m_pRootFrame)
	, m_matPivot(rhs.m_matPivot)
	, m_pAnimationCtrl(rhs.m_pAnimationCtrl->Clone())
{

	m_pLoader->AddRef();
}

_matrix CMesh_Dynamic::Get_LocalTransform(const _uint & iContainerIdx) const
{
	_matrix			matResult;
	D3DXMatrixIdentity(&matResult);

	if (m_vecMeshContainer.size() <= iContainerIdx)
		return matResult;

	_matrix			matScale, matTrans;

	D3DXMatrixScaling(&matScale, m_vecMeshContainer[iContainerIdx]->vMax.x - m_vecMeshContainer[iContainerIdx]->vMin.x, m_vecMeshContainer[iContainerIdx]->vMax.y - m_vecMeshContainer[iContainerIdx]->vMin.y, m_vecMeshContainer[iContainerIdx]->vMax.z - m_vecMeshContainer[iContainerIdx]->vMin.z);

	D3DXMatrixTranslation(&matTrans, (m_vecMeshContainer[iContainerIdx]->vMax.x + m_vecMeshContainer[iContainerIdx]->vMin.x) * 0.5f, (m_vecMeshContainer[iContainerIdx]->vMax.y + m_vecMeshContainer[iContainerIdx]->vMin.y) * 0.5f, (m_vecMeshContainer[iContainerIdx]->vMax.z + m_vecMeshContainer[iContainerIdx]->vMin.z) * 0.5f);

	return matResult = matScale * matTrans;
}

const _matrix * CMesh_Dynamic::Find_Frame(const char * pFrameName)
{
	D3DXFRAME_DERIVED*	pFindFrame = ((D3DXFRAME_DERIVED*)D3DXFrameFind(m_pRootFrame, pFrameName));

	if (nullptr != pFindFrame)
		return &pFindFrame->CombinedTransformationMatrix;
	else
	{
		pFindFrame = ((D3DXFRAME_DERIVED*)D3DXFrameFind(m_pServeRootFrame, pFrameName));
		return &pFindFrame->CombinedTransformationMatrix;
	}
}

_bool CMesh_Dynamic::IsOverTime(const _float& fCorrectionValue)
{
	return m_pAnimationCtrl->IsOverTime(fCorrectionValue);
}

HRESULT CMesh_Dynamic::Ready_Mesh_Dynamic(const _tchar * pFilePath, const _tchar * pFileName)

{
	LPD3DXMESH		pMesh = nullptr;

	_tchar			szFullPath[MAX_PATH] = L"";

	lstrcpy(szFullPath, pFilePath);
	lstrcat(szFullPath, pFileName);

	m_pLoader = CHierarchyLoader::Create(m_pGraphic_Device, pFilePath);
	if (nullptr == m_pLoader)
		return E_FAIL;

	LPD3DXANIMATIONCONTROLLER			pAniCtrl = nullptr;

	if (FAILED(D3DXLoadMeshHierarchyFromX(szFullPath, D3DXMESH_MANAGED, m_pGraphic_Device, m_pLoader, nullptr, &m_pRootFrame, &pAniCtrl)))
		return E_FAIL;

	if (nullptr != pAniCtrl)
	{
		m_pAnimationCtrl = CAnimationCtrl::Create(pAniCtrl);

		if (nullptr == m_pAnimationCtrl)
			return E_FAIL;
	}

	Safe_Release(pAniCtrl);

	D3DXMatrixRotationY(&m_matPivot, D3DXToRadian(180.f));

	Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED*)m_pRootFrame, &m_matPivot);
	SetUp_CombinedMatrixPointer((D3DXFRAME_DERIVED*)m_pRootFrame);

	return NOERROR;
}

HRESULT CMesh_Dynamic::Ready_Clone_Mesh_Dynamic(void * pArg)
{
	if (pArg == nullptr)
		return NOERROR;

	OBJECT* eObject = (OBJECT*)&pArg;
	m_eObejct = *eObject;
	if (*eObject == CAMPER)
	{
		m_pServeRootName = "joint_TorsoC_01";
		m_pServeAnimationCtrl = m_pAnimationCtrl->Clone();
		Separate_Bone((D3DXFRAME_DERIVED*)m_pRootFrame);
	}
	else if (*eObject == SLASHER)
	{
		m_pServeRootName = "joint_TorsoA_01";
		m_pServeAnimationCtrl = m_pAnimationCtrl->Clone();
		Separate_Bone((D3DXFRAME_DERIVED*)m_pRootFrame);
	}

	return NOERROR;
}

_bool CMesh_Dynamic::Update_Skinning(const _uint& iMeshContainerIdx, const _uint& iAttributeID)
{
	if (m_vecMeshContainer.size() < iMeshContainerIdx ||
		m_vecMeshContainer[iMeshContainerIdx]->NumMaterials <= iAttributeID)
		return false;

	D3DXMESHCONTAINER_DERIVED*	pMeshContainer = m_vecMeshContainer[iMeshContainerIdx];
	if (nullptr == pMeshContainer)
		return false;

	if (pMeshContainer->pSkinInfo == NULL)
	{
		pMeshContainer->MeshData.pMesh->DrawSubset(iAttributeID);
		return true;
	}


	if (nullptr == pMeshContainer->pBoneCombinationBuf)
		return false;

	_uint Vindex = pMeshContainer->dwNumFrames;

	LPD3DXBONECOMBINATION pBoneComb = reinterpret_cast<LPD3DXBONECOMBINATION>(pMeshContainer->pBoneCombinationBuf->GetBufferPointer());

	for (_uint iPaletteEntry = 0; iPaletteEntry < Vindex; ++iPaletteEntry)
	{
		int iMatIndex = pBoneComb[iAttributeID].BoneId[iPaletteEntry];
		if (iMatIndex != UINT_MAX)
		{
			D3DXMatrixMultiply(&pMeshContainer->pRenderingMatrices[iPaletteEntry]
				, &pMeshContainer->pOffsetMatrices[iMatIndex], pMeshContainer->ppCombinedTransformationMatrices[iMatIndex]);
		}
	}
	return false;
}

void CMesh_Dynamic::Render_Mesh(const _uint& iMeshContainerIdx, const _uint& iAttributeID)
{
	if (m_vecMeshContainer.size() <= iMeshContainerIdx ||
		m_vecMeshContainer[iMeshContainerIdx]->NumMaterials <= iAttributeID)
		return;

	D3DXMESHCONTAINER_DERIVED*	pMeshContainer = m_vecMeshContainer[iMeshContainerIdx];
	if (nullptr == pMeshContainer)
		return;

	pMeshContainer->MeshData.pMesh->DrawSubset(iAttributeID);
}

HRESULT CMesh_Dynamic::Set_AnimationSet(const _uint& iAnimationID, _bool bIsServe)
{
	return m_pAnimationCtrl->Set_AnimationSet(iAnimationID);
}
HRESULT CMesh_Dynamic::Set_NoBleningAnimationSet(const _uint & iAnimationID)
{
	return m_pAnimationCtrl->Set_NoBleningAnimationSet(iAnimationID);
}
HRESULT CMesh_Dynamic::Set_ServeAnimationSet(const _uint & iAnimationID)
{
	return m_pServeAnimationCtrl->Set_AnimationSet(iAnimationID);
}
void CMesh_Dynamic::Play_Animation(const _float& fTimeDelta)
{

	m_pAnimationCtrl->Play_Animation(fTimeDelta);

	D3DXMatrixRotationY(&m_matPivot, D3DXToRadian(180.f));

	Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED*)m_pRootFrame, &m_matPivot);
}

void CMesh_Dynamic::Play_ServeAnimation(const _float & fTimeDelta)
{
	m_pServeAnimationCtrl->Play_Animation(fTimeDelta);
	Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED*)m_pServeRootFrame, &((D3DXFRAME_DERIVED*)m_pConnectFrame)->CombinedTransformationMatrix);
}

void CMesh_Dynamic::Setup_AnimationTime(const _uint& Track, const _float & fFixingTime)
{
	m_pAnimationCtrl->Setup_AnimationTime(Track, fFixingTime);
}

_uint CMesh_Dynamic::Get_CurAnimation()
{
	return m_pAnimationCtrl->Get_CurAnimation();
}
void CMesh_Dynamic::Update_CombinedTransformationMatrices(D3DXFRAME_DERIVED * pFrame, const _matrix* pParentMatrix)
{
	pFrame->CombinedTransformationMatrix = pFrame->TransformationMatrix * *pParentMatrix;

	if (nullptr != pFrame->pFrameSibling)
		Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED *)pFrame->pFrameSibling, pParentMatrix);

	if (nullptr != pFrame->pFrameFirstChild)
		Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED *)pFrame->pFrameFirstChild, &pFrame->CombinedTransformationMatrix);
}

HRESULT CMesh_Dynamic::SetUp_CombinedMatrixPointer(D3DXFRAME_DERIVED * pFrame)
{
	if (nullptr != pFrame->pMeshContainer)
	{
		D3DXMESHCONTAINER_DERIVED* pMeshContainer = (D3DXMESHCONTAINER_DERIVED*)pFrame->pMeshContainer;

		m_vecMeshContainer.push_back(pMeshContainer);

		for (_ulong i = 0; i < pMeshContainer->dwNumFrames; ++i)
		{
			const char* pBoneName = pMeshContainer->pSkinInfo->GetBoneName(i);
			if (nullptr == pBoneName)
				return E_FAIL;

			D3DXFRAME_DERIVED*	pFindFrame = (D3DXFRAME_DERIVED*)D3DXFrameFind(m_pRootFrame, pBoneName);
			if (nullptr == pFindFrame)
				return E_FAIL;

			pMeshContainer->ppCombinedTransformationMatrices[i] = &pFindFrame->CombinedTransformationMatrix;
		}
	}

	HRESULT			hr = 0;

	if (nullptr != pFrame->pFrameSibling)
	{
		if (FAILED(SetUp_CombinedMatrixPointer((D3DXFRAME_DERIVED *)pFrame->pFrameSibling)))
			return E_FAIL;
	}


	if (nullptr != pFrame->pFrameFirstChild)
	{
		if (FAILED(SetUp_CombinedMatrixPointer((D3DXFRAME_DERIVED *)pFrame->pFrameFirstChild)))
			return E_FAIL;
	}

	return NOERROR;
}

void CMesh_Dynamic::Separate_Bone(D3DXFRAME_DERIVED * pFrame)
{
	if (m_eObejct == CAMPER)
	{
		//if (!strcmp(pFrame->Name, m_pServeRootName))
		//{
		//	m_pConnectFrame = pFrame;
		//	m_pServeRootFrame = pFrame->pFrameFirstChild;
		//	pFrame->pFrameFirstChild = m_pServeRootFrame->pFrameSibling;
		//	m_pServeRootFrame->pFrameSibling = nullptr;
		//}
	}

	//
	//if (nullptr != pFrame->pFrameFirstChild &&
	//	!strcmp(pFrame->pFrameFirstChild->Name, m_pServeRootName))
	//{
	//	m_pConnectFrame = pFrame;
	//	m_pServeRootFrame = pFrame->pFrameFirstChild;
	//	pFrame->pFrameFirstChild = m_pServeRootFrame->pFrameSibling;
	//	m_pServeRootFrame->pFrameSibling = nullptr;
	//}

	if (nullptr != pFrame->pFrameSibling)
		Separate_Bone((D3DXFRAME_DERIVED *)pFrame->pFrameSibling);

	if (nullptr != pFrame->pFrameFirstChild)
		Separate_Bone((D3DXFRAME_DERIVED *)pFrame->pFrameFirstChild);
}

CMesh_Dynamic * CMesh_Dynamic::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _tchar* pFilePath, const _tchar* pFileName)
{
	CMesh_Dynamic*	pInstance = new CMesh_Dynamic(pGraphic_Device);

	if (FAILED(pInstance->Ready_Mesh_Dynamic(pFilePath, pFileName)))
	{
		MessageBox(0, L"CMesh_Dynamic Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CMesh_Dynamic::Clone_Component(void* pArg)
{
	CMesh_Dynamic* pInstance = new CMesh_Dynamic(*this);

	if (FAILED(pInstance->Ready_Clone_Mesh_Dynamic(pArg)))
	{
		_MSG_BOX("CMesh_Dynamic Cloned Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CMesh_Dynamic::Free()
{
	if (m_pServeRootName != nullptr)
	{
		if (m_pServeRootFrame != nullptr)
		{
			m_pServeRootFrame->pFrameSibling = m_pConnectFrame->pFrameFirstChild;
			m_pConnectFrame->pFrameFirstChild = m_pServeRootFrame;
		}
	}

	if (false == m_isClone)
	{
		m_pLoader->DestroyFrame(m_pRootFrame);

		for (auto& pMeshContainer : m_vecMeshContainer)
		{
			m_pLoader->DestroyMeshContainer(pMeshContainer);
		}
	}

	Safe_Release(m_pAnimationCtrl);
	Safe_Release(m_pServeAnimationCtrl);

	m_vecMeshContainer.clear();

	Safe_Release(m_pLoader);

	CComponent::Free();
}

