#include "..\Headers\Action_Manager.h"
#include "Action.h"

_IMPLEMENT_SINGLETON(CAction_Manager)

CAction_Manager::CAction_Manager()
{
}

HRESULT CAction_Manager::Add_Action(const _tchar * pActionTag, CAction* pAction)
{
	if (nullptr == pAction)
		return E_FAIL;

	if (nullptr != Find_Action(pActionTag))
		return E_FAIL;

	auto IsExist = m_mapActions.insert(MAPACTIONS::value_type(pActionTag, pAction));
	if (!IsExist.second)
		IsExist.first->second = pAction;

	return NOERROR;
}

HRESULT CAction_Manager::Set_Action(CGameObject* pGameObejct, const _tchar * pActionTag, const float & fTimeOfDuration)
{
	if (nullptr == pGameObejct)
		return E_FAIL;

	CAction* pAction = Find_Action(pActionTag);
	if (nullptr == pAction)
		return E_FAIL;

	pAction->m_fCurrentTime = 0.f;
	pAction->m_fDuration_Time = fTimeOfDuration;
	pAction->m_pGameObject = pGameObejct;

	pAction->Ready_Action();
	lstrcpy(m_pCurActionTag, pActionTag);

	return NOERROR;
}

HRESULT CAction_Manager::Remove_Action(const _tchar * pActionTag)
{
	if (m_mapActions.empty())
		return E_FAIL;

	auto iter = find_if(m_mapActions.begin(), m_mapActions.end(), CFinder_Tag(pActionTag));
	if (iter == m_mapActions.end())
		return E_FAIL;

	Safe_Release(iter->second);

	return NOERROR;
}
 
CAction * CAction_Manager::Find_Action(const _tchar * pActionTag)
{
	auto iter = find_if(m_mapActions.begin(), m_mapActions.end(), CFinder_Tag(pActionTag));

	if (iter == m_mapActions.end())
		return nullptr;

	return iter->second;
}

_bool CAction_Manager::Update_Actions(const _float & fTimeDelta)
{
	if (m_mapActions.empty())
		return false;

	for (auto pAction : m_mapActions)
	{
		if (CAction::END_ACTION == pAction.second->Update_Action(fTimeDelta))
		{
			pAction.second->End_Action();
			lstrcpy(m_pCurActionTag, L"");
		}	
	}

	return true;
}

HRESULT CAction_Manager::Assert_EndAllAction()
{
	for (auto& Pair : m_mapActions)
	{
		if (Pair.second->m_bIsPlaying == true)
			Pair.second->End_Action();
	}

	return NOERROR;
}

_bool CAction_Manager::IsPlaying_Action(_tchar* pActionTag)
{
	if (!lstrcmp(pActionTag, m_pCurActionTag))
		return true;

	return false;
}

void CAction_Manager::Free()
{
	for (auto& Pair : m_mapActions)
		Safe_Release(Pair.second);

	m_mapActions.clear();
}
