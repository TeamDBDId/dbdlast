#include "..\Headers\AnimationCtrl.h"

CAnimationCtrl::CAnimationCtrl()
{
}

HRESULT CAnimationCtrl::Ready_Animation(LPD3DXANIMATIONCONTROLLER pAniCtrl)
{
	m_pAniCtrl = pAniCtrl;
	m_pAniCtrl->AddRef();

	return NOERROR;
}

HRESULT CAnimationCtrl::Clone_Animation(const CAnimationCtrl & rhs)
{
	if (FAILED(rhs.m_pAniCtrl->CloneAnimationController(rhs.m_pAniCtrl->GetMaxNumAnimationOutputs(), rhs.m_pAniCtrl->GetMaxNumAnimationSets(), rhs.m_pAniCtrl->GetMaxNumTracks(), rhs.m_pAniCtrl->GetMaxNumEvents(), &m_pAniCtrl)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CAnimationCtrl::Set_AnimationSet(const _uint & iAnimationID)
{
	if (m_iOldAnimationIdx == iAnimationID)
		return NOERROR;

	LPD3DXANIMATIONSET		pAniSet = nullptr;

	m_pAniCtrl->GetAnimationSet(iAnimationID, &pAniSet);

	if (nullptr == pAniSet)
		return E_FAIL;

	m_Period = pAniSet->GetPeriod();

	m_iNewTrack = m_iCurrentTrack == 0 ? 1 : 0;

	if (FAILED(m_pAniCtrl->SetTrackAnimationSet(m_iNewTrack, pAniSet)))
		return E_FAIL;

	Safe_Release(pAniSet);

	m_pAniCtrl->UnkeyAllTrackEvents(m_iCurrentTrack);
	m_pAniCtrl->UnkeyAllTrackEvents(m_iNewTrack);

	m_pAniCtrl->KeyTrackEnable(m_iCurrentTrack, FALSE, m_TimeAcc + 0.25);
	m_pAniCtrl->KeyTrackSpeed(m_iCurrentTrack, 1.f, m_TimeAcc, 0.25, D3DXTRANSITION_LINEAR);
	m_pAniCtrl->KeyTrackWeight(m_iCurrentTrack, 0.1f, m_TimeAcc, 0.25, D3DXTRANSITION_LINEAR);

	m_pAniCtrl->SetTrackEnable(m_iNewTrack, TRUE);
	m_pAniCtrl->KeyTrackSpeed(m_iNewTrack, 1.f, m_TimeAcc, 0.25, D3DXTRANSITION_LINEAR);
	m_pAniCtrl->KeyTrackWeight(m_iNewTrack, 0.9f, m_TimeAcc, 0.25, D3DXTRANSITION_LINEAR);

	m_pAniCtrl->SetTrackPosition(m_iNewTrack, 0.0);
	m_pAniCtrl->ResetTime();
	m_TimeAcc = 0.0;

	m_iCurrentTrack = m_iNewTrack;
	m_iOldAnimationIdx = iAnimationID;

	return NOERROR;
}

HRESULT CAnimationCtrl::Set_NoBleningAnimationSet(const _uint & iAnimationID)
{
	if (m_iOldAnimationIdx == iAnimationID)
		return NOERROR;

	LPD3DXANIMATIONSET		pAniSet = nullptr;

	m_pAniCtrl->GetAnimationSet(iAnimationID, &pAniSet);

	if (nullptr == pAniSet)
		return E_FAIL;

	m_Period = pAniSet->GetPeriod();

	m_iNewTrack = m_iCurrentTrack == 0 ? 1 : 0;

	if (FAILED(m_pAniCtrl->SetTrackAnimationSet(m_iNewTrack, pAniSet)))
		return E_FAIL;

	Safe_Release(pAniSet);

	m_pAniCtrl->UnkeyAllTrackEvents(m_iCurrentTrack);
	m_pAniCtrl->UnkeyAllTrackEvents(m_iNewTrack);

	m_pAniCtrl->SetTrackEnable(m_iCurrentTrack, FALSE);
	//m_pAniCtrl->KeyTrackEnable(m_iCurrentTrack, FALSE, 0.999);
	//m_pAniCtrl->KeyTrackSpeed(m_iCurrentTrack, 1.f, m_TimeAcc, 0.999, D3DXTRANSITION_LINEAR);
	//m_pAniCtrl->KeyTrackWeight(m_iCurrentTrack, 0.0001f, m_TimeAcc, 0.999, D3DXTRANSITION_LINEAR);

	m_pAniCtrl->SetTrackEnable(m_iNewTrack, TRUE);
	//m_pAniCtrl->KeyTrackSpeed(m_iNewTrack, 1.f, m_TimeAcc, 0.001, D3DXTRANSITION_LINEAR);
	//m_pAniCtrl->KeyTrackWeight(m_iNewTrack, 0.9999f, m_TimeAcc, 0.001, D3DXTRANSITION_LINEAR);

	m_pAniCtrl->SetTrackPosition(m_iNewTrack, 0.0);
	m_pAniCtrl->ResetTime();
	m_TimeAcc = 0.0;
	m_iCurrentTrack = m_iNewTrack;
	m_iOldAnimationIdx = iAnimationID;

	return NOERROR;
}



void CAnimationCtrl::Play_Animation(const _float & fTimeDelta)
{
	m_pAniCtrl->AdvanceTime(fTimeDelta, nullptr);

	m_TimeAcc += fTimeDelta;

}

void CAnimationCtrl::Setup_AnimationTime(const _uint& Track,const _float & fFixingTime)
{
	m_pAniCtrl->SetTrackPosition(Track, 0.0);
	m_pAniCtrl->AdvanceTime(fFixingTime, nullptr);
}

_bool CAnimationCtrl::IsOverTime(const _float & fCorrectValue)
{
	if (m_TimeAcc - (m_Period - fCorrectValue) > 0.001f)
		return true;

	return false;
}

CAnimationCtrl * CAnimationCtrl::Create(LPD3DXANIMATIONCONTROLLER pAniCtrl)
{
	CAnimationCtrl*	pInstance = new CAnimationCtrl();

	if (FAILED(pInstance->Ready_Animation(pAniCtrl)))
	{
		MessageBox(0, L"pAniCtrl Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;

}

CAnimationCtrl * CAnimationCtrl::Clone()
{
	CAnimationCtrl*	pInstance = new CAnimationCtrl();

	if (FAILED(pInstance->Clone_Animation(*this)))
	{
		MessageBox(0, L"pAniCtrl Cloned Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;

}

void CAnimationCtrl::Free()
{

	Safe_Release(m_pAniCtrl);
}
