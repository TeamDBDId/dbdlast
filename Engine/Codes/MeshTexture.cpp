#include "MeshTexture.h"

_IMPLEMENT_SINGLETON(CMeshTexture)

CMeshTexture::CMeshTexture()
{
}


HRESULT CMeshTexture::AddTextureEX(LPDIRECT3DDEVICE9 pGraphic_Deivce, LPCWSTR szFilePath, LPCWSTR szFileName)
{
	wstring FileName = szFileName;
	wstring FilePath = szFilePath;
	FilePath = FilePath + FileName;
	LPDIRECT3DTEXTURE9		pTexture = nullptr;
	D3DXIMAGE_INFO ImgInfo;

	if (FAILED(D3DXCreateTextureFromFileEx(pGraphic_Deivce, FilePath.c_str(), 0, 0, 0, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, &ImgInfo, NULL, &pTexture)))
		return E_FAIL;
	m_mapTexture.insert(make_pair(FileName, pTexture));
	m_mapTextureSize.insert(make_pair(FileName, _vec2((_float)ImgInfo.Width, (_float)ImgInfo.Height)));
	return NOERROR;
}

HRESULT CMeshTexture::AddTexture(LPDIRECT3DDEVICE9 pGraphic_Device, LPCWSTR szFilePath, LPCWSTR szFileName, LPDIRECT3DTEXTURE9* pTexture)
{
	wstring FileName = szFileName;
	wstring FilePath = szFilePath;
	Find_ReplacedName(&FileName);
	
	FilePath = FilePath + FileName;
	*pTexture = Find_Texture(FileName);
	if (*pTexture == nullptr)
	{
		if (FAILED(D3DXCreateTextureFromFile(pGraphic_Device, FilePath.c_str(), pTexture)))
			return E_FAIL;
		m_mapTexture.insert(make_pair(FileName, *pTexture));
	}
	return NOERROR;
}

LPDIRECT3DTEXTURE9 CMeshTexture::Find_Texture(wstring FileName)
{
	auto& iter = m_mapTexture.find(FileName);
	if (iter == m_mapTexture.end())
		return nullptr;

	return iter->second;
}

_vec2 * CMeshTexture::Find_TextureSize(wstring FileName)
{
	auto& iter = m_mapTextureSize.find(FileName);
	if (iter == m_mapTextureSize.end())
		return nullptr;

	return &iter->second;
}

_bool CMeshTexture::Find_ReplacedName(wstring * FileName)
{
	auto& iter = m_mapReplacedName.find(*FileName);
	if (iter == m_mapReplacedName.end())
		return false;
	*FileName = iter->second;

	return true;
}

void CMeshTexture::Add_ReplacedName(const _tchar* Name, const _tchar* ReplacedName)
{
	wstring wstrName = Name;
	wstring wstrReplacedName = ReplacedName;
	m_mapReplacedName.insert({ wstrName,wstrReplacedName });
}

void CMeshTexture::Free()
{
	for (auto& iter : m_mapTexture)
		Safe_Release(iter.second);
	m_mapTexture.clear();
}