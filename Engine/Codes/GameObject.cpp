#include "..\Headers\GameObject.h"
#include "Component.h"
#include "Action.h"

CGameObject::CGameObject(LPDIRECT3DDEVICE9 pGraphic_Device)
	: m_pGraphic_Device(pGraphic_Device)
{
	m_eObjectID = 0;
	m_pGraphic_Device->AddRef();
}

CGameObject::CGameObject(const CGameObject & rhs)
	: m_pGraphic_Device(rhs.m_pGraphic_Device)
	, m_eObjectID(rhs.m_eObjectID)
{
	m_isDead = false;
	m_pGraphic_Device->AddRef();
}

CComponent * CGameObject::Get_ComponentPointer(const _tchar * pComponentTag)
{
	CComponent*	pComponent = Find_Component(pComponentTag);
	if (nullptr == pComponent)
		return nullptr;

	return pComponent;
}

HRESULT CGameObject::Set_Action(const _tchar * pActionTag, const float & fTimeDuration)
{
	CAction* pAction = Find_Action(pActionTag);
	if (nullptr == pAction)
		return E_FAIL;

	pAction->m_fCurrentTime = 0.f;
	pAction->m_fDuration_Time = fTimeDuration;
	pAction->m_pGameObject = this;

	pAction->Ready_Action();

	return NOERROR;
}

CAction * CGameObject::Find_Action(const _tchar * pActionTag)
{
	auto iter = find_if(m_mapActions.begin(), m_mapActions.end(), CFinder_Tag(pActionTag));

	if (iter == m_mapActions.end())
		return nullptr;

	return iter->second;
}

_bool CGameObject::Update_Actions(const _float & fTimeDelta)
{
	if (m_mapActions.empty())
		return false;

	for (auto pAction : m_mapActions)
	{
		if (CAction::END_ACTION == pAction.second->Update_Action(fTimeDelta))
			pAction.second->End_Action();
	}

	return true;
}

HRESULT CGameObject::Assert_EndAllAction()
{
	for (auto& Pair : m_mapActions)
	{
		if (Pair.second->m_bIsPlaying == true)
			Pair.second->End_Action();
	}

	return NOERROR;
}

HRESULT CGameObject::Ready_GameObject()
{
	return NOERROR;
}

_int CGameObject::Update_GameObject(const _float & fTimeDelta)
{
	return _int();
}

_int CGameObject::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (bIsDead)
		return DEAD_OBJ;

	return _int();
}

void CGameObject::Render_GameObject()
{
}

_int CGameObject::Do_Coll(CGameObject* _pObj, const _vec3& _vPos)
{
	return _int();
}

const _vec3&  CGameObject::Get_InterPos(const _uint & _iDir)
{
	return m_vPosArr[_iDir];

	// TODO: 여기에 반환 구문을 삽입합니다.
}


HRESULT CGameObject::Add_Component(const _tchar * pComponentTag, CComponent * pComponent)
{
	if (nullptr == pComponent)
		return E_FAIL;

	if (nullptr != Find_Component(pComponentTag))
		return E_FAIL;

	m_mapComponent.insert(MAPCOMPONENT::value_type(pComponentTag, pComponent));

	pComponent->AddRef();

	return NOERROR;
}

CComponent * CGameObject::Find_Component(const _tchar * pComponentTag)
{
	auto iter = find_if(m_mapComponent.begin(), m_mapComponent.end(), CFinder_Tag(pComponentTag));

	if (iter == m_mapComponent.end())
		return nullptr;

	return iter->second;
}

HRESULT CGameObject::Compute_CameraDistance(const _vec3* pWorldPos)
{
	_matrix		matView, matRot;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);

	D3DXMatrixInverse(&matView, nullptr, &matView);

	_vec3		vCamPosition, vCamLook, vLength, vCross;
	memcpy(&vCamPosition, &matView.m[3][0], sizeof(_vec3));
	
	D3DXMatrixRotationY(&matRot, D3DXToRadian(90));
	matView = matRot * matView;
	memcpy(&vCamLook, &matView.m[2][0], sizeof(_vec3));

	vLength = vCamPosition - *pWorldPos;
	m_fCameraDistance = D3DXVec3Length(&vLength);

	D3DXVec3Cross(&vCross, &vLength, &vCamLook);
	
	if (vCross.y < 0)
	{
		//D3DXVec3Normalize(&vLength, &vLength);
		//D3DXVec3Normalize(&vCamLook, &vCamLook);
		//_float fAngle = D3DXVec3Dot(&vLength, &vCamLook);
		//fAngle = acos(fAngle);
		//if(fAngle <= D3DXToRadian(30.f) || fAngle >= D3DXToRadian(-30.f))
		m_isCameraOut = true;
		return NOERROR;
	}
	
	m_isCameraOut = false;
	return NOERROR;
}

void CGameObject::Free()
{
	for (auto& Pair : m_mapActions)
		Safe_Release(Pair.second);

	m_mapActions.clear();

	if(nullptr!= m_vPosArr)
		Safe_Delete_Array(m_vPosArr);

	for (auto& Pair : m_mapComponent)
		Safe_Release(Pair.second);
	m_mapComponent.clear();

	Safe_Release(m_pGraphic_Device);
	Safe_Release(m_pSkinTextures);
}
