#include "..\Headers\Collider.h"
#include "Shader.h"
#include "Picking.h"

#include "Management.h"
#include "Mesh_Static.h"

#include "Transform.h"

CCollider::CCollider(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CComponent(pGraphic_Device)
{
	
}

CCollider::CCollider(const CCollider& rhs)
	: CComponent(rhs)
	, m_eType(rhs.m_eType)
	, m_pShader(rhs.m_pShader)
	, m_isColl(rhs.m_isColl)
	, m_pOBB(rhs.m_pOBB)
{
	m_pShader->AddRef();

	rhs.m_pCollider->CloneMeshFVF(rhs.m_pCollider->GetOptions(), rhs.m_pCollider->GetFVF(), rhs.m_pGraphic_Device, &m_pCollider);
}

const _vec3 CCollider::Get_Center()
{
	_vec3 vCenter;

	_matrix		matTransform, matWorld;

	if (nullptr == m_ColliderInfo.pBoneMatrix)
		matTransform = *m_ColliderInfo.pWorldMatrix;
	else
		matTransform = *m_ColliderInfo.pBoneMatrix * *m_ColliderInfo.pWorldMatrix;

	if (nullptr != m_ColliderInfo.pParentMatrix)
		matTransform = matTransform * *m_ColliderInfo.pParentMatrix;

	matWorld = m_ColliderInfo.matLocalTransform * matTransform;

	memcpy(&vCenter, &matWorld.m[3][0], sizeof(_vec3));

	return vCenter;
}

HRESULT CCollider::Ready_Collider(TYPE eType)
{
	HRESULT		hr = 0;

	m_eType = eType;

	switch (eType)
	{
	case TYPE_BOX:
		hr = Ready_BoundingBox();
		break;
	case TYPE_SPHERE:
		hr = Ready_BoundingSphere();
		break;
	}

	if (FAILED(hr))
		return E_FAIL;

	m_pShader = CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Collider.fx");
	if (nullptr == m_pShader)
		return E_FAIL;

	return NOERROR;
}

HRESULT CCollider::Ready_Clone_Collider(void * pArg)
{
	// 로컬영역내에서의 변환.
	COLLIDERINFO*			pColliderInfo = (COLLIDERINFO*)pArg;

	m_ColliderInfo = *pColliderInfo;

	m_eBoxType = pColliderInfo->eBoxType;


	
	void*			pVertices = nullptr;	

	_uint			iVertexSize = D3DXGetFVFVertexSize(m_pCollider->GetFVF());

	m_pCollider->LockVertexBuffer(0, &pVertices);

	_ulong			dwNumVertices = m_pCollider->GetNumVertices();

	for (size_t i = 0; i < dwNumVertices; ++i)		
	{
		D3DXVec3TransformCoord((_vec3*)((_byte*)pVertices + (i * iVertexSize)), (_vec3*)((_byte*)pVertices + (i * iVertexSize)), &pColliderInfo->matLocalTransform);
	}

	// 로컬영역상의 민, 맥스포인트. 
	D3DXComputeBoundingBox((_vec3*)pVertices, dwNumVertices, iVertexSize, &m_vMin, &m_vMax);

	m_pCollider->UnlockVertexBuffer();



	if (BOXTYPE_OBB == m_eBoxType)
	{
		m_pOBB = new OBB;
		ZeroMemory(m_pOBB, sizeof(OBB));

		m_pOBB->vPoint[0] = _vec3(m_vMin.x, m_vMax.y, m_vMin.z);
		m_pOBB->vPoint[1] = _vec3(m_vMax.x, m_vMax.y, m_vMin.z);
		m_pOBB->vPoint[2] = _vec3(m_vMax.x, m_vMin.y, m_vMin.z);
		m_pOBB->vPoint[3] = _vec3(m_vMin.x, m_vMin.y, m_vMin.z);

		m_pOBB->vPoint[4] = _vec3(m_vMin.x, m_vMax.y, m_vMax.z);
		m_pOBB->vPoint[5] = _vec3(m_vMax.x, m_vMax.y, m_vMax.z);
		m_pOBB->vPoint[6] = _vec3(m_vMax.x, m_vMin.y, m_vMax.z);
		m_pOBB->vPoint[7] = _vec3(m_vMin.x, m_vMin.y, m_vMax.z);
	}








	return NOERROR;
}

HRESULT CCollider::Ready_Clone_ColliderSphere(void * _pArg)
{
	// 로컬영역내에서의 변환.
	COLLIDERINFO*			pColliderInfo = (COLLIDERINFO*)_pArg;

	m_ColliderInfo = *pColliderInfo;

	_float fMax = max(max(m_ColliderInfo.matLocalTransform._11, m_ColliderInfo.matLocalTransform._22), m_ColliderInfo.matLocalTransform._33);
	m_ColliderInfo.matLocalTransform._11 = m_ColliderInfo.matLocalTransform._22 = m_ColliderInfo.matLocalTransform._33 = fMax;

	void*			pVtx = nullptr;

	_uint			iVtxSize = D3DXGetFVFVertexSize(m_pCollider->GetFVF());
	 
	m_pCollider->LockVertexBuffer(0, &pVtx);

	_ulong			dwNumVtx = m_pCollider->GetNumVertices();

	for (size_t i = 0; i < dwNumVtx; ++i)
	{
		D3DXVec3TransformCoord((_vec3*)((_byte*)pVtx + (i * iVtxSize)), (_vec3*)((_byte*)pVtx + (i * iVtxSize)), &m_ColliderInfo.matLocalTransform);
	}

	D3DXComputeBoundingSphere((_vec3*)pVtx, dwNumVtx, iVtxSize, &m_vCenter, &m_fRad);


	m_pCollider->UnlockVertexBuffer();


	return NOERROR;
}



HRESULT CCollider::Ready_HullMesh(const _uint& _iScene, const _tchar* _pHull)
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();


	_tchar hullTag[256];
	lstrcpy(hullTag, _pHull);
	lstrcat(hullTag,L"_HULL");


	m_pHull = (CMesh_Static*)pManagement->Clone_Component(_iScene, hullTag);
	if (nullptr == m_pHull)
		return E_FAIL;

	m_isHullLoad = true;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CCollider::Ready_NaviMesh(const _uint & _iScene, const _tchar * _pHull)
{

	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();


	_tchar hullTag[256];
	lstrcpy(hullTag, _pHull);
	lstrcat(hullTag, L"_NAVI");


	m_pNavi = (CMesh_Static*)pManagement->Clone_Component(_iScene, hullTag);
	if (nullptr == m_pNavi)
		return E_FAIL;

	m_isNaviLoad = true;

	Safe_Release(pManagement);
	return NOERROR;
}


HRESULT CCollider::Ready_RealMesh(CMesh_Static * _pRealMesh)
{

	m_pReal = (CMesh_Static*)_pRealMesh;
	

	return NOERROR;
}


_bool CCollider::Collision_Sphere(const CCollider * _pTargetCollider,_float* _pOut)
{
	_matrix		mat = Compute_WorldTransform();
	_matrix		matTarget = _pTargetCollider->Compute_WorldTransform();

	_vec3		vCenter;// = (m_vMax + m_vMin)*0.5f;
	_vec3		vTargetCenter = _pTargetCollider->m_vCenter;
	//_float		fBulletRad = 3.f;

	D3DXVec3TransformCoord(&vCenter, &m_vCenter, &mat);
	D3DXVec3TransformCoord(&vTargetCenter, &vTargetCenter, &matTarget);

	//_float		fBulletRad = (_pTargetCollider->m_vMax.x - _pTargetCollider->m_vMin.x)*0.5f;


	m_isColl = false;

	_float fDist = D3DXVec3Length(&(vCenter - vTargetCenter));
	_float fSumRad = D3DXVec3Length((_vec3*)&mat.m[0][0])*m_fRad
					+ D3DXVec3Length((_vec3*)&matTarget.m[0][0])*_pTargetCollider->m_fRad;

	if (fDist < fSumRad)
	{
		if (nullptr != _pOut)
			*_pOut = m_fRad;

		return m_isColl = true;
	}
	else
		return false;
}



//_bool CCollider::Collision_AABB(const CCollider * pTargetCollider, _vec3* _pGap)
//{
//	// 콜라이더들이 가지고 있는 로컬영역상의 min, max를 각 콜라이더의 월드스페이스변환을 위한 행렬. 
//	_matrix		SourMatrix = Compute_WorldTransform();
//	_matrix		DestMatrix = pTargetCollider->Compute_WorldTransform();
//
//	_vec3		vSourMin, vSourMax;
//	_vec3		vDestMin, vDestMax;
//
//	D3DXVec3TransformCoord(&vSourMin, &m_vMin, &SourMatrix);
//	D3DXVec3TransformCoord(&vSourMax, &m_vMax, &SourMatrix);
//
//	D3DXVec3TransformCoord(&vDestMin, &pTargetCollider->m_vMin, &DestMatrix);
//	D3DXVec3TransformCoord(&vDestMax, &pTargetCollider->m_vMax, &DestMatrix);
//
//	m_isColl = false;
//
//	_vec3		vGap;
//
//	if ((vGap.x = min(vSourMax.x, vDestMax.x) - max(vSourMin.x, vDestMin.x)) < 0.f)
//		return false;
//	if ((vGap.z = min(vSourMax.z, vDestMax.z) - max(vSourMin.z, vDestMin.z)) < 0.f)
//		return false;
//
//
//	m_isColl = true;
//
//	if (nullptr == _pGap)
//		return true;
//
//	_float fMin = min(vGap.x,vGap.z);
//	if (3.f < fMin)
//	{
//		if (vGap.x == fMin)
//		{
//			if (vSourMax.x == max(vSourMax.x, vDestMax.x))
//				_pGap->x = -vGap.x;
//			else
//				_pGap->x = vGap.x;
//		}
//		else if (vGap.z == fMin)
//		{
//			if (vSourMax.z == max(vSourMax.z, vDestMax.z))
//				_pGap->z = -vGap.z;
//			else
//				_pGap->z = vGap.z;
//		}
//	}
//
//
//
//	return _bool(true);
//}


//
//_bool CCollider::Collision_OBB(const CCollider * pTargetCollider)
//{	
//	const OBB*	pTargetOBB = pTargetCollider->Get_OBBDescPointer();
//	if (nullptr == pTargetOBB)
//		return false;
//
//
//	
//
//
//
//	m_isColl = false;
//
//	OBB			tOBB[2];
//	ZeroMemory(tOBB, sizeof(OBB) * 2);
//
//	for (size_t i = 0; i < 8; ++i)
//	{
//		D3DXVec3TransformCoord(&tOBB[0].vPoint[i], &m_pOBB->vPoint[i], &Compute_WorldTransform());
//		D3DXVec3TransformCoord(&tOBB[1].vPoint[i], &pTargetOBB->vPoint[i], &pTargetCollider->Compute_WorldTransform());
//	}
//
//	tOBB[0].vCenter = (tOBB[0].vPoint[0] + tOBB[0].vPoint[6]) * 0.5f;
//	tOBB[1].vCenter = (tOBB[1].vPoint[0] + tOBB[1].vPoint[6]) * 0.5f;
//
//	for (size_t i = 0; i < 2; ++i)
//	{
//		Compute_AlignAxis(&tOBB[i]);
//		Compute_ProjAxis(&tOBB[i]);
//	}
//
//	_vec3	vAlignAxis[9];
//
//	for (size_t i = 0; i < 3; ++i)
//	{
//		for (size_t j = 0; j < 3; ++j)
//		{
//			_uint		iIndex = i * 3 + j;
//			D3DXVec3Cross(&vAlignAxis[iIndex], &tOBB[0].vAlignAxis[i], &tOBB[1].vAlignAxis[j]);
//		}	
//	}		
//	
//	// 충돌비교!
//	_float		fDistance[3];
//
//	// 중점과 중점을 잇는 벡터의 길이.
//
//	for (size_t i = 0; i < 2; ++i)
//	{
//		for (size_t j = 0; j < 3; ++j)
//		{
//			_vec3		vCenterDir = tOBB[1].vCenter - tOBB[0].vCenter;
//			fDistance[0] = fabs(D3DXVec3Dot(&vCenterDir, &tOBB[i].vAlignAxis[j]));
//
//			fDistance[1] = fabs(D3DXVec3Dot(&tOBB[0].vProjAxis[0], &tOBB[i].vAlignAxis[j])) +
//				fabs(D3DXVec3Dot(&tOBB[0].vProjAxis[1], &tOBB[i].vAlignAxis[j])) +
//				fabs(D3DXVec3Dot(&tOBB[0].vProjAxis[2], &tOBB[i].vAlignAxis[j]));
//
//			fDistance[2] = fabs(D3DXVec3Dot(&tOBB[1].vProjAxis[0], &tOBB[i].vAlignAxis[j])) +
//				fabs(D3DXVec3Dot(&tOBB[1].vProjAxis[1], &tOBB[i].vAlignAxis[j])) +
//				fabs(D3DXVec3Dot(&tOBB[1].vProjAxis[2], &tOBB[i].vAlignAxis[j]));
//
//			if (fDistance[1] + fDistance[2] < fDistance[0])
//				return false;
//
//		}
//	}
//
//
//
//	for (size_t i = 0; i < 9; ++i)
//{	
//		_vec3		vCenterDir = tOBB[1].vCenter - tOBB[0].vCenter;
//		fDistance[0] = fabs(D3DXVec3Dot(&vCenterDir, &vAlignAxis[i]));
//
//		fDistance[1] = fabs(D3DXVec3Dot(&tOBB[0].vProjAxis[0], &vAlignAxis[i])) +
//			fabs(D3DXVec3Dot(&tOBB[0].vProjAxis[1], &vAlignAxis[i])) +
//			fabs(D3DXVec3Dot(&tOBB[0].vProjAxis[2], &vAlignAxis[i]));
//
//		fDistance[2] = fabs(D3DXVec3Dot(&tOBB[1].vProjAxis[0], &vAlignAxis[i])) +
//			fabs(D3DXVec3Dot(&tOBB[1].vProjAxis[1], &vAlignAxis[i])) +
//			fabs(D3DXVec3Dot(&tOBB[1].vProjAxis[2], &vAlignAxis[i]));
//
//		if (fDistance[1] + fDistance[2] < fDistance[0])
//			return false;		
//	}
//	
//	
//	return _bool(m_isColl = true);
//}

_bool CCollider::Collision_LAY(const CTransform* _pTrans, _vec3* _pGap, const _bool& _isReal)
{
	LPD3DXMESH pMesh = nullptr;
	if (false == _isReal)
		pMesh = m_pHull->Get_Mesh();
	else
		pMesh = m_pReal->Get_Mesh();


	_vec3	vRayPos = *_pTrans->Get_StateInfo(CTransform::STATE_POSITION);
	vRayPos.y += 60.f;

	_vec3	vRayDir;
	vRayDir = *_pTrans->Get_StateInfo(CTransform::STATE_LOOK);
	D3DXVec3Normalize(&vRayDir, &vRayDir);




	_matrix matWorld = Compute_WorldTransformForMesh();
	_matrix matWorldInv;
	D3DXMatrixInverse(&matWorldInv, nullptr, &matWorld);

	D3DXVec3TransformCoord(&vRayPos, &vRayPos, &matWorldInv);
	D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matWorldInv);

	_bool coll = false;
	_matrix matRot;
	D3DXMatrixRotationY(&matRot, D3DXToRadian(30.f));
	for (size_t i = 0; i < 12; ++i)
	{
		_float		fDist = 0.f;
		BOOL		isColl = FALSE;
		DWORD		dwFaceIdx = 0;

		
		D3DXIntersect(pMesh, &vRayPos, &vRayDir, &isColl, &dwFaceIdx, nullptr, nullptr, &fDist, nullptr, nullptr);
		

		if (TRUE == isColl&&fDist < 40.f)
		{
			_vec3 vDir;
			D3DXVec3TransformNormal(&vDir, &vRayDir, &matWorld);
			D3DXVec3Normalize(&vDir, &vDir);

			_vec3 vNormal = -Cal_Normal(pMesh, dwFaceIdx);
			D3DXVec3TransformNormal(&vNormal, &vNormal, &matWorld);
			vDir = -vNormal*cosf(acosf(D3DXVec3Dot(&vDir, &vNormal)));
			fDist = 40.f - fDist;
			*_pGap += vDir*fDist;
			coll = true;
		}

		D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matRot);
	}

	if (false == coll)
		return false;
	
	

	_pGap->y = 0.f;
	return true;
}

_bool CCollider::Collision_LAY_Terr(CTransform * _pTrans,_bool* _pIsFall)
{
	_vec3	vRayDir = _vec3(0.f, -1.f, 0.f);
	_vec3	vRayPos = *_pTrans->Get_StateInfo(CTransform::STATE_POSITION);


	vRayPos.y += 50.f;




	_matrix matWorld = Compute_WorldTransformForMesh();
	_matrix matWorldInv;
	D3DXMatrixInverse(&matWorldInv, nullptr, &matWorld);

	D3DXVec3TransformCoord(&vRayPos, &vRayPos, &matWorldInv);
	D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matWorldInv);

	_float fDist = 0.f;

	LPD3DXMESH pMesh = nullptr;

	
	pMesh = m_pNavi->Get_Mesh();


	BOOL		isColl = FALSE;

	DWORD dwFaceIdx = 0;

	D3DXIntersect(pMesh, &vRayPos, &vRayDir, &isColl, &dwFaceIdx, nullptr, nullptr, &fDist, nullptr, nullptr);



	if (false == isColl)
	{
		*_pIsFall = false;
		return false;
	}
		
	

	if (55.f < fDist)
	{
		*_pIsFall = true;
		return false;
	}
		

	D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matWorld);
	D3DXVec3Normalize(&vRayDir, &vRayDir);


	fDist = fDist*1.12f - 50.f;


	_vec3 vPos = *_pTrans->Get_StateInfo(CTransform::STATE_POSITION) +vRayDir*fDist;
	_pTrans->Set_StateInfo(CTransform::STATE_POSITION, &vPos);


	return true;
}

_bool CCollider::Collision_Player(CCollider * _pTargetCollider, _vec3 * _pGap)
{

	_matrix		mat = Compute_WorldTransform();
	_matrix		matTarget = _pTargetCollider->Compute_WorldTransform();

	_vec3		vCenter;// = (m_vMax + m_vMin)*0.5f;
	_vec3		vTargetCenter = _pTargetCollider->m_vCenter;
	//_float		fBulletRad = 3.f;

	D3DXVec3TransformCoord(&vCenter, &m_vCenter, &mat);
	D3DXVec3TransformCoord(&vTargetCenter, &vTargetCenter, &matTarget);

	//_float		fBulletRad = (_pTargetCollider->m_vMax.x - _pTargetCollider->m_vMin.x)*0.5f;


	m_isColl = false;

	_vec3 vDir = vCenter - vTargetCenter;
	_float fDist = D3DXVec3Length(&vDir)- 60.f;

	if (0.f<fDist)
		return false;
	else
	{
		D3DXVec3Normalize(&vDir, &vDir);
		vDir *= fDist;
		vDir.y = 0.f;
		*_pGap = vDir;
		return m_isColl = true;
	}


}


_bool CCollider::Picking_ToCollider(_vec4 * pOut, const CPicking * pPickingCom)
{
	_vec3	vRayPivot = pPickingCom->Get_MouseRayPivot();
	_vec3	vRay = pPickingCom->Get_MouseRay();

	_matrix	matWorldInv = Compute_WorldTransform();
	D3DXMatrixInverse(&matWorldInv, nullptr, &matWorldInv);


	// 로컬영역상의 피봇과 레이를 구한다.
	D3DXVec3TransformCoord(&vRayPivot, &vRayPivot, &matWorldInv);
	D3DXVec3TransformNormal(&vRay, &vRay, &matWorldInv);


	_float		fU, fV, fDist;

	BOOL		isPick = FALSE;

	D3DXIntersect(m_pCollider, &vRayPivot, &vRay, &isPick, nullptr, &fU, &fV, &fDist, nullptr, nullptr);
	if (isPick)
	{
		_vec3 Pos = pPickingCom->Get_MouseRayPivot() + (pPickingCom->Get_MouseRay()* fDist);
		*pOut = _vec4(Pos.x, Pos.y, Pos.z, fDist);
	}

	return _bool(isPick != 0);
}

void CCollider::Render_Collider()
{
	if (!m_fRender)
		return;
	if (nullptr == m_pShader)
		return;

	LPD3DXEFFECT pEffect = m_pShader->Get_EffectHandle();

	if (nullptr == pEffect)
		return;

	_matrix		matTransform = Compute_WorldTransform();
	
	pEffect->SetMatrix("g_matWorld", &matTransform);

	_matrix		matView, matProj;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	pEffect->SetMatrix("g_matView", &matView);
	pEffect->SetMatrix("g_matProj", &matProj);

	_vec4		vColor;

	vColor = m_isColl == true ? _vec4(1.f, 0.f, 0.f, 1.f) : _vec4(0.f, 1.f, 0.f, 1.f);

	pEffect->SetVector("g_vColor", &vColor);


	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(0);

	m_pCollider->DrawSubset(0);

	pEffect->EndPass();
	pEffect->End();
}

 _vec3 CCollider::Cal_Normal(const LPD3DXMESH & pMesh, const DWORD & dwNumFaces)
{



	LPDIRECT3DVERTEXBUFFER9 pVB;
	LPDIRECT3DINDEXBUFFER9 pIB;

	pMesh->GetVertexBuffer(&pVB);
	pMesh->GetIndexBuffer(&pIB);

	WORD* pIndices;
	VTXNORTEX* pVertices;

	pIB->Lock(0, 0, (void**)&pIndices, 0);
	pVB->Lock(0, 0, (void**)&pVertices, 0);

	D3DXVECTOR3 v0 = pVertices[pIndices[3 * dwNumFaces + 0]].vPosition;
	D3DXVECTOR3 v1 = pVertices[pIndices[3 * dwNumFaces + 1]].vPosition;
	D3DXVECTOR3 v2 = pVertices[pIndices[3 * dwNumFaces + 2]].vPosition;

	D3DXVECTOR3 u = v1 - v0;
	D3DXVECTOR3 v = v2 - v0;

	D3DXVECTOR3 vOut;

	D3DXVec3Cross(&vOut, &u, &v);
	D3DXVec3Normalize(&vOut, &vOut);

	pVB->Unlock();
	pIB->Unlock();
	Safe_Release(pVB);
	Safe_Release(pIB);

	return vOut;
}

HRESULT CCollider::Ready_BoundingBox()
{
	if (FAILED(D3DXCreateBox(m_pGraphic_Device, 1.f, 1.f, 1.f, &m_pCollider, &m_pAdjacency)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CCollider::Ready_BoundingSphere()
{
	if (FAILED(D3DXCreateSphere(m_pGraphic_Device, 0.5f, 10, 10, &m_pCollider, &m_pAdjacency)))
		return E_FAIL;

	return NOERROR;
}

_matrix CCollider::Remove_Rotation(_matrix matTransform) const
{
	_vec3			vRight(1.f, 0.f, 0.f), vUp(0.f, 1.f, 0.f), vLook(0.f, 0.f, 1.f);

	vRight *= D3DXVec3Length((_vec3*)&matTransform.m[0][0]);
	vUp *= D3DXVec3Length((_vec3*)&matTransform.m[1][0]);
	vLook *= D3DXVec3Length((_vec3*)&matTransform.m[2][0]);

	memcpy(&matTransform.m[0][0], &vRight, sizeof(_vec3));
	memcpy(&matTransform.m[1][0], &vUp, sizeof(_vec3));
	memcpy(&matTransform.m[2][0], &vLook, sizeof(_vec3));

	return _matrix(matTransform);
}

_matrix CCollider::Compute_WorldTransform() const
{
	_matrix		matTransform;

	if (nullptr == m_ColliderInfo.pBoneMatrix)
		matTransform = *m_ColliderInfo.pWorldMatrix;
	else
		matTransform = *m_ColliderInfo.pBoneMatrix * *m_ColliderInfo.pWorldMatrix;

	if (nullptr != m_ColliderInfo.pParentMatrix)	
		matTransform = matTransform * *m_ColliderInfo.pParentMatrix;

	if (BOXTYPE_AABB == m_ColliderInfo.eBoxType)
		matTransform = Remove_Rotation(matTransform);


	return _matrix(matTransform);
}

_matrix CCollider::Compute_WorldTransformForMesh() const
{
	_matrix		matTransform;

	if (nullptr == m_ColliderInfo.pBoneMatrix)
		matTransform = *m_ColliderInfo.pWorldMatrix;
	else
		matTransform = *m_ColliderInfo.pBoneMatrix * *m_ColliderInfo.pWorldMatrix;

	if (nullptr != m_ColliderInfo.pParentMatrix)
		matTransform = matTransform * *m_ColliderInfo.pParentMatrix;


	return _matrix(matTransform);
}

void CCollider::Compute_AlignAxis(OBB * pOBB)
{
	pOBB->vAlignAxis[0] = pOBB->vPoint[2] - pOBB->vPoint[3];
	pOBB->vAlignAxis[1] = pOBB->vPoint[0] - pOBB->vPoint[3];
	pOBB->vAlignAxis[2] = pOBB->vPoint[7] - pOBB->vPoint[3];

	for (size_t i = 0; i < 3; ++i)
	{
		D3DXVec3Normalize(&pOBB->vAlignAxis[i], &pOBB->vAlignAxis[i]);

	}
}

void CCollider::Compute_ProjAxis(OBB * pOBB)
{
	pOBB->vProjAxis[0] = (pOBB->vPoint[5] + pOBB->vPoint[2]) * 0.5f - pOBB->vCenter;
	pOBB->vProjAxis[1] = (pOBB->vPoint[5] + pOBB->vPoint[0]) * 0.5f - pOBB->vCenter;
	pOBB->vProjAxis[2] = (pOBB->vPoint[5] + pOBB->vPoint[7]) * 0.5f - pOBB->vCenter;
}


CCollider * CCollider::Create(LPDIRECT3DDEVICE9 pGraphic_Device, TYPE eType)
{
	CCollider*	pInstance = new CCollider(pGraphic_Device);

	if (FAILED(pInstance->Ready_Collider(eType)))
	{
		MessageBox(0, L"CCollider Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CCollider::Clone_Component(void* pArg)
{
	CCollider*	pCollider = new CCollider(*this);



	if (m_eType == TYPE_BOX)
	{
		if (FAILED(pCollider->Ready_Clone_Collider(pArg)))
		{
			_MSG_BOX("Collider Cloned Failed");
			Safe_Release(pCollider);
		}
	}
	else if (m_eType == TYPE_SPHERE)
	{
		if (FAILED(pCollider->Ready_Clone_ColliderSphere(pArg)))
		{
			_MSG_BOX("Collider Cloned Failed");
			Safe_Release(pCollider);
		}
	}

	
	return pCollider;		
}


void CCollider::Free()
{	
	Safe_Delete_Array(m_pOBB);
	Safe_Release(m_pShader);
	Safe_Release(m_pCollider);

	if (true == m_isHullLoad)
		Safe_Release(m_pHull);

	if (true == m_isNaviLoad)
		Safe_Release(m_pNavi);

	CComponent::Free();
}
