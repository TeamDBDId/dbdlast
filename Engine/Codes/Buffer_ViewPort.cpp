#include "..\Headers\Buffer_ViewPort.h"
#include "Picking.h"
#include "Transform.h"

CBuffer_ViewPort::CBuffer_ViewPort(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CVIBuffer(pGraphic_Device)
{

}

CBuffer_ViewPort::CBuffer_ViewPort(const CBuffer_ViewPort & rhs)
	: CVIBuffer(rhs)	
	, m_pOriVertices(rhs.m_pOriVertices)
{
	

}

HRESULT CBuffer_ViewPort::Ready_VIBuffer()
{
	m_iNumVertices = 4;
	m_iStride = sizeof(VTXVIEWPORT);
	m_dwFVF = D3DFVF_XYZRHW | D3DFVF_TEX1/* | D3DFVF_TEXCOORDSIZE2(0)*/;
	m_pPosition = new _vec3[4];

	m_pOriVertices = new VTXVIEWPORT[4];

	m_iNumPolygons = 2;
	m_iPolygonSize = sizeof(POLYGON16);	
	m_eFormat = D3DFMT_INDEX16;

	if (FAILED(CVIBuffer::Ready_VIBuffer()))
		return E_FAIL;

	VTXVIEWPORT*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	pVertices[0].vPosition = _vec4(-0.5f, -0.5f, 0.f, 1.f);
	pVertices[0].vTexUV = _vec2(0.0f, 0.f);
	m_pOriVertices[0] = pVertices[0];

	pVertices[1].vPosition = _vec4(0.5f, -0.5f, 0.f, 1.f);
	pVertices[1].vTexUV = _vec2(1.f, 0.f);
	m_pOriVertices[1] = pVertices[1];

	pVertices[2].vPosition = _vec4(0.5f, 0.5f, 0.f, 1.f);
	pVertices[2].vTexUV = _vec2(1.f, 1.f);
	m_pOriVertices[2] = pVertices[2];

	pVertices[3].vPosition = _vec4(-0.5f, 0.5f, 0.f, 1.f);
	pVertices[3].vTexUV = _vec2(0.f, 1.f);	
	m_pOriVertices[3] = pVertices[3];

	//for (size_t i = 0; i < m_iNumVertices; ++i)
	//	m_pPosition[i] = pVertices[i].vPosition;	

	m_pVB->Unlock();



	
	POLYGON16*		pIndices = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	pIndices[0]._0 = 0;
	pIndices[0]._1 = 1;
	pIndices[0]._2 = 2;

	pIndices[1]._0 = 0;
	pIndices[1]._1 = 2;
	pIndices[1]._2 = 3;

	m_pIB->Unlock();


	return NOERROR;
}

void CBuffer_ViewPort::Render_VIBuffer(const _matrix* pWorldMatrix)
{
	if (FAILED(Transform_Vertices(pWorldMatrix)))
		return;

	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
	// m_pGraphic_Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, m_iNumPolygons);
}

_bool CBuffer_ViewPort::Picking_ToBuffer(_vec3 * pOut, const CTransform * pTransformCom, const CPicking * pPickingCom)
{

 	_vec3	vRayPivot = pPickingCom->Get_MouseRayPivot();
	_vec3	vRay = pPickingCom->Get_MouseRay();

	_matrix	matWorldInv = pTransformCom->Get_Matrix_Inverse();

	// 로컬영역상의 피봇과 레이를 구한다.
	D3DXVec3TransformCoord(&vRayPivot, &vRayPivot, &matWorldInv);
	D3DXVec3TransformNormal(&vRay, &vRay, &matWorldInv);

	
	_float		fU, fV, fDist;

	_bool		isPick = false;

	// 오른쪽 상단 삼각형과 충돌이냐?!
	if (TRUE == D3DXIntersectTri(&m_pPosition[1], &m_pPosition[0], &m_pPosition[2], &vRayPivot, &vRay, &fU, &fV, &fDist))
		isPick = true;

	// 왼쪽 하단 삼각형 비교.
	if(TRUE == D3DXIntersectTri(&m_pPosition[3], &m_pPosition[2], &m_pPosition[0], &vRayPivot, &vRay, &fU, &fV, &fDist))
		isPick = true;

	return isPick;
}

HRESULT CBuffer_ViewPort::Transform_Vertices(const _matrix * pWorldMatrix)
{
	VTXVIEWPORT*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	for (size_t i = 0; i < m_iNumVertices; ++i)
	{
		D3DXVec4Transform(&pVertices[i].vPosition, &m_pOriVertices[i].vPosition, pWorldMatrix);
	}	

	m_pVB->Unlock();

	return NOERROR;
}


CBuffer_ViewPort * CBuffer_ViewPort::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBuffer_ViewPort*	pInstance = new CBuffer_ViewPort(pGraphic_Device);

	if (FAILED(pInstance->Ready_VIBuffer()))
	{
		MessageBox(0, L"CBuffer_ViewPort Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;

}


CComponent * CBuffer_ViewPort::Clone_Component(void* pArg)
{
	return new CBuffer_ViewPort(*this);
}

void CBuffer_ViewPort::Free()
{
	if (false == m_isClone)
		Safe_Delete_Array(m_pOriVertices);

	CVIBuffer::Free();
}
