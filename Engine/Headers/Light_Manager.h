#pragma once

#include "Base.h"


_BEGIN(Engine)

class CLight;
class _ENGINE_DLL CLight_Manager final : public CBase
{
	_DECLARE_SINGLETON(CLight_Manager)
public:
	explicit CLight_Manager();
	virtual ~CLight_Manager() = default;
public:
	D3DLIGHT9* Get_LightInfo(const _uint& iIndex = 0);
	_vec3	GetProjPos(const _uint& iIndex = 0);
	_uint	GetSize() { return m_LightList.size(); }
public:
	HRESULT Add_LightInfo(LPDIRECT3DDEVICE9 pGraphic_Device, const D3DLIGHT9& LightInfo);
	CLight* Add_LightInfo2(LPDIRECT3DDEVICE9 pGraphic_Device, const D3DLIGHT9& LightInfo);
	void Render_Light(LPD3DXEFFECT pEffect);
	void Ready_ShadowMap(CLight* pLight);
	//void Render_DynamicShadow();
private:
	list<CLight*>			m_LightList;
	typedef list<CLight*>	LIGHTLIST;
protected:
	virtual void Free();
};

_END