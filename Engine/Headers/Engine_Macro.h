#pragma once

#define NO_COPY(CLASSNAME)									\
	private:												\
	CLASSNAME(const CLASSNAME&);							\
	CLASSNAME& operator = (const CLASSNAME&);				

#define _DECLARE_SINGLETON(CLASSNAME)						\
	NO_COPY(CLASSNAME)										\
	private:												\
	static CLASSNAME*	m_pInstance;						\
	public:													\
	static CLASSNAME*	GetInstance( void );				\
	static unsigned long DestroyInstance( void );					

#define _IMPLEMENT_SINGLETON(CLASSNAME)						\
	CLASSNAME*	CLASSNAME::m_pInstance = NULL;				\
	CLASSNAME*	CLASSNAME::GetInstance( void )	{			\
		if(NULL == m_pInstance) {							\
			m_pInstance = new CLASSNAME;					\
		}													\
		return m_pInstance;									\
	}														\
	unsigned long CLASSNAME::DestroyInstance( void ) {		\
		unsigned long dwRefCnt = 0;							\
		if(NULL != m_pInstance)	{							\
			dwRefCnt = m_pInstance->Release();				\
			if(0 == dwRefCnt) m_pInstance = NULL;			\
		}													\
		return dwRefCnt;									\
	}

#define DEAD_OBJ	1

#define _MSG_BOX(MESSAGE) MessageBox(0, TEXT(MESSAGE), TEXT("Error"), MB_OK);

#define _BEGIN(NAMESPACE) namespace NAMESPACE {

#define _END }

#define _USING(NAMESPACE) using namespace NAMESPACE;

#ifdef ENGINE_EXPORTS // 엔진프로젝트안이었다라면.

#define _ENGINE_DLL _declspec(dllexport)

#else

#define _ENGINE_DLL _declspec(dllimport)

#endif

#define KEYMGR	CKeyManager::GetInstance()

#define GET_INSTANCE(class) class::GetInstance()

#define GET_INSTANCE_MANAGEMENTR(VAL)						\
CManagement* pManagement = CManagement::GetInstance();		\
if (pManagement == nullptr)									\
	return VAL;												\
pManagement->AddRef();

#define GET_INSTANCE_MANAGEMENT								\
CManagement* pManagement = CManagement::GetInstance();		\
if (pManagement == nullptr)									\
	return;												\
pManagement->AddRef();


#define _MIN3(a,b,c) min(min(a,b),c)