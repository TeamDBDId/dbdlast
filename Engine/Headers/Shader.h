#pragma once

// 월드행렬을 보관한다.
// 객체의 기본적이 ㄴ상태를 표현한다.

#include "Component.h"

_BEGIN(Engine)

class _ENGINE_DLL CShader final : public CComponent
{
private:
	explicit CShader(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CShader(const CShader& rhs);
	virtual ~CShader() = default;	
public:
	LPD3DXEFFECT	Get_EffectHandle() const {
		return m_pEffect; }
public:
	HRESULT Ready_Shader(const _tchar* pFilePath);
private:
	LPD3DXEFFECT			m_pEffect = nullptr;
	LPD3DXBUFFER			m_pBuffer = nullptr;
public:
	static CShader* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _tchar* pFilePath);
	virtual CComponent* Clone_Component(void* pArg = nullptr);
protected:
	virtual void Free();
};

_END