#pragma once

typedef struct tagMesh_Texture
{
	LPDIRECT3DTEXTURE9			pDiffuseTexture;
	LPDIRECT3DTEXTURE9			pNormalTexture;
	LPDIRECT3DTEXTURE9			pSpecularTexture;
	LPDIRECT3DTEXTURE9			pAmbientOcclusionTexture;
	LPDIRECT3DTEXTURE9			pRoughnessTexture;
	LPDIRECT3DTEXTURE9			pHightTexture;
	LPDIRECT3DTEXTURE9			pMetallicTexture;
}MESHTEXTURE;



typedef struct tagCollObj
{
	void*			pObj;
	void*			pCollider;
	bool*			pIsFall;
}COL;




typedef struct tagSubset_Desc
{
	D3DXMATERIAL		Material;
	MESHTEXTURE			MeshTexture;
}SUBSETDESC;

typedef struct tagD3DXMeshContainer_Derived : public D3DXMESHCONTAINER
{
	LPD3DXMESH			pOriginalMesh;
	// d3dxmaterial + texture
	SUBSETDESC*			pSubSetDesc;
	unsigned long		dwNumFrames;
	D3DXMATRIX**		ppCombinedTransformationMatrices;
	D3DXMATRIX*			pOffsetMatrices;
	D3DXMATRIX*			pRenderingMatrices; // pOffset * pCombined
	D3DXVECTOR3			vMin, vMax;
	LPD3DXBUFFER		pBoneCombinationBuf;
}D3DXMESHCONTAINER_DERIVED;

typedef struct tagD3DXFrame_Derived : public D3DXFRAME
{
	D3DXMATRIX		CombinedTransformationMatrix;
}D3DXFRAME_DERIVED;

typedef struct tagCamera_Desc
{
	D3DXVECTOR3		vEye; // 카메라의 위치.In.WorldSpace
	D3DXVECTOR3		vAt; // 카메라가 바라보는 점.
	D3DXVECTOR3		vAxisY; // 내 좌표계상에서의 y축벡터의 방향.
}CAMERADESC;

typedef struct tagProjection_Desc
{
	float		fFovY; // 내 카메라의 시야 범위.
	float		fAspect; // 내 윈도우의 가로, 세로 비율.
	float		fNear;
	float		fFar;
}PROJDESC;

// 정점을 표현한 구조체.
typedef struct tagVertex_Texture
{
	D3DXVECTOR3			vPosition;
	D3DXVECTOR2			vTexUV;
}VTXTEX;

typedef struct tagVertex_Normal_Texture
{
	D3DXVECTOR3			vPosition;
	D3DXVECTOR3			vNormal;
	D3DXVECTOR2			vTexUV;
}VTXNORTEX;


typedef struct tagVertex_ViewPort
{
	D3DXVECTOR4			vPosition;
	D3DXVECTOR2			vTexUV;
}VTXVIEWPORT;


typedef struct tagVertex_Cube_Texture
{
	D3DXVECTOR3			vPosition;
	D3DXVECTOR3			vTexUV;
}VTXCUBETEX;

typedef struct tagVertex_Color
{
	D3DXVECTOR3			vPosition;	
	DWORD				dwColor;
}VTXCOL;

typedef struct tagPolygon16
{
	unsigned short		_0, _1, _2;
}POLYGON16;

typedef struct tagPolygon32
{
	unsigned long		_0, _1, _2;
}POLYGON32;

typedef struct tagRobby //Robby
{
	int		Number = -1;
	int		character = 0;

	unsigned int		Packet = 0;

	char	IPAdress[16];
	bool	bConnect = false;
	bool	Ready = false;
}LobbyData;

typedef struct tagRobbyServerData
{
	LobbyData playerdat[5];
	int		  Number[5] = {0,0,0,0,0};
	int		  PickTime = 0;
	int		  WaittingTime = 0;
	bool	  GameStart = false;
	bool	  AllReady = false;
}LServer_Data;

typedef struct tagCamperData //Camper
{
	int				Number = 0;
	bool			bConnect = false;
	bool			bLive = false;

	D3DXVECTOR3 vRight = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vUp = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vLook = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vPos = { 0.f, 0.f, 0.f };

	int				InterationObject = 0;
	int				InterationObjAnimation = 0;
	int				SecondInterationObject = 0;
	int				SecondInterationObjAnimation = 0;
	int				Character = 0;
	int				iState = 0;

	bool			bUseItem = false;
	unsigned int	iItem = 0;
	unsigned int	iCondition = 0;
	unsigned int	iHookStage = 0;
	float			fEnergy = 0.f;

	int				Packet = 0;
	float			fHeal = 0.f;

	int				Score[4] = { 0,0,0,0 };
}CamperData;

typedef struct tagSlasherData //Slasher
{
	int				Number = 5;
	bool			bConnect = false;

	unsigned int	iCharacter = 0;
	int				iState = 0;
	int				iSkill = 0;

	int				InterationObject = 0;
	int				InterationObjAnimation = 0;
	int				SecondInterationObject = 0;
	int				SecondInterationObjAnimation = 0;

	D3DXVECTOR3		vCamLook = { 0.f, 0.f, 0.f };
	int				Packet = 0;

	D3DXVECTOR3 vRight = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vUp = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vLook = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vPos = { 0.f, 0.f, 0.f };

	int				Score[4] = { 0,0,0,0 };
}SlasherData;

typedef struct tagGameData
{
	float	GRP[7];	// GeneratorRepairProgress
	float	ItemBox[10];
	int		ItemBoxAnim[10];

	float	Totem[5];
	int		Packet = 0;

	BYTE	WoodBoard[10];
	int		Locker[20];
	int		Hook[12];
	float	ExitDoor[2];
}GameData;

typedef struct tagServerData
{
	CamperData	Campers[MAX_Client];
	SlasherData	Slasher;
	GameData	Game_Data;
}Server_Data;


struct SOCKETINFO {
	OVERLAPPED overlapped;
	SOCKET sock;
	char buf[BUFSIZE + 1];
	bool Incoming_data;
	WSABUF wsabuf;
};

struct SOCKETINFOGAME {
	OVERLAPPED overlapped;
	SOCKET sock;
	char buf[BUFSIZE + 1];
	bool Incoming_data;
	WSABUF wsabuf;
};

typedef struct tagClientData {
	int client_imei = -1;
	int Number = -1;
	int Character = -1;
}Client_Data;

typedef struct {
	SOCKET hClntSock;
	SOCKADDR_IN clntAddr;
	int client_imei = 0;
} PER_HANDLE_DATA, *LPPER_HANDLE_DATA;

typedef struct InstancingData {
	D3DXVECTOR3 vRight;
	D3DXVECTOR3 vUp;
	D3DXVECTOR3 vLook;
	D3DXVECTOR3 vPosition;
}InstancingData;

typedef struct IndexData {
	D3DXVECTOR3 vPosition;
	D3DXVECTOR3 vNormal;
	D3DXVECTOR2 vTexCoord;
}IndexData;

const D3DVERTEXELEMENT9 g_INDEXDATA[] =
{
	{ 0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
	{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,   0 },
	{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
	D3DDECL_END()
};

const D3DVERTEXELEMENT9 g_INSTANCEDATA[] =
{
	{ 1, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
	{ 1, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
	{ 1, 24, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
	{ 1, 36, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 4 },
	D3DDECL_END()
};

const D3DVERTEXELEMENT9 g_VertexDecl[] =
{
	{ 0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
	{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,   0 },
	{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },	
	{ 1, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
	{ 1, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
	{ 1, 24, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
	{ 1, 36, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 4 },
	D3DDECL_END()
};