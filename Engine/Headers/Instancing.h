#pragma once
#include "Base.h"
#include "Mesh_Static.h"

_BEGIN(Engine)
class CInstancing : public CBase
{
	struct tagInstancing
	{
		vector<_matrix>	vecMatrix;
		CMesh_Static*	Mesh_Static;
	};
private:
	CInstancing(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CInstancing() = default;

private:
	HRESULT Ready_Instrancing();

public:
	void Add_Instancing(CMesh_Static* pMesh_Static, const _matrix& matWorld);
	void Rendering_Instancing();


private:
	void Setup_Instancing(vector<_matrix> vecMatrix);
private:
	typedef vector<_matrix>	vecMatrix;
	map <LPD3DXMESH, tagInstancing>		m_mapInstancing;
private:
	D3DXATTRIBUTERANGE*				m_pAttributeTable = nullptr;
	LPDIRECT3DDEVICE9				m_pGraphic_Device = nullptr;
	LPDIRECT3DVERTEXBUFFER9			m_pInstancingVB = nullptr;
	LPDIRECT3DVERTEXDECLARATION9	m_pVertexDecl = nullptr;
	LPD3DXEFFECT					m_pEffect = nullptr;
public:
	static CInstancing* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
protected:
	virtual void Free() override;
};

_END