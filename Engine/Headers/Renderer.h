#pragma once

// 렌더링해야할 객체들을 모아서 관리한다.(왜? 3d에서 객체들을 그리는 순서가 중요하다)
// 모을때도 그리는 순서대로 모은다.순서대로 그린다.

#include "Component.h"

_BEGIN(Engine)

class CShader;
class CTarget_Manager;
class CGameObject;
class CInstancing;
class CMesh_Static;
class _ENGINE_DLL CRenderer final : public CComponent
{
public:
	enum RENDERGROUP { RENDER_PRIORITY, RENDER_NONEALPHA, RENDER_ALPHA, RENDER_UI, RENDER_END};
private:
	explicit CRenderer(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CRenderer() = default;	
public:
	HRESULT Ready_Renderer();
	HRESULT Add_RenderGroup(RENDERGROUP eGroup, CGameObject* pGameObject);
	HRESULT Add_CubeGroup(CGameObject* pGameObject);
	HRESULT Add_StemGroup(CGameObject* pGameObject);
	HRESULT Render_RenderGroup();
	CShader* GetCubeEffectHandle() { return m_pShader_Cube; }
	void	Add_Instancing(CMesh_Static* pMeshCom, const _matrix& matWorld);
	void	SetScreenPos(_vec3 scr) { m_vScrPos = scr; }
	HRESULT SetUp_OnShaderFromTarget(LPD3DXEFFECT pEffect, _tchar* TargetName, const char* pConstantName);
private:
	CTarget_Manager*			m_pTarget_Manager = nullptr;
	CShader*					m_pShader_LightAcc = nullptr;
	CShader*					m_pShader_Blend = nullptr;
	CShader*					m_pShader_Cube = nullptr;
private:
	list<CGameObject*>			m_RenderList[RENDER_END];
	typedef list<CGameObject*>	OBJECTLIST;
private:	// Cube
	//list<CGameObject*>			m_StemmapList = list<CGameObject*>();
	list<CGameObject*>			m_CubemapList = list<CGameObject*>();
	LPDIRECT3DSURFACE9			pSrf = nullptr;
	_int						m_iCount = 0;
	LPD3DXRenderToEnvMap		m_pRndEnv = nullptr;
	LPDIRECT3DCUBETEXTURE9		m_pTxCbm = nullptr;
private:	// MotionBlur
	_matrix						m_matPreView = _matrix();
	CInstancing*				m_pInstancing = nullptr;
	_vec3						m_vScrPos;
private:
	LPDIRECT3DVERTEXBUFFER9		m_pVB = nullptr;
	LPDIRECT3DINDEXBUFFER9		m_pIB = nullptr;
private:
	void Render_Priority();
	void Render_NoneAlpha();
	void Render_Alpha();
	void Render_UI();
private:
	void Render_LightStem();
	void Render_Deferred();
	void Render_LightAcc();
	void Render_Blend();
private:
	void T_SetupCubeViewMatrix(_matrix* pmtViw, DWORD dwFace);
	void Render_CubeMap(_matrix* View, _matrix* Proj);
	void Render_EnvMap();
public: 
	static CRenderer* Create(LPDIRECT3DDEVICE9 pGraphic_Device); // 원형객체 생성을 위한.
	virtual CComponent* Clone_Component(void* pArg = nullptr); // 복제될 객체를 생성하기 위한.
protected:
	virtual void Free();
};

_END