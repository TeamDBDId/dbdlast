#pragma once

#include "Base.h"

_BEGIN(Engine)

class CLine;
class CCell final : public CBase
{
public:
	enum POINT {POINT_A, POINT_B, POINT_C, POINT_END};
	enum NEIGHBOR {NEIGHBOR_AB, NEIGHBOR_BC, NEIGHBOR_CA, NEIGHBOR_END};
	enum LINE {LINE_AB, LINE_BC, LINE_CA, LINE_END};
private:
	explicit CCell(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CCell() = default;
public:
	const _vec3* Get_Point(POINT ePoint) const {
		return &m_vPoint[ePoint]; }
	const CCell* Get_Neighbor(NEIGHBOR eNeighbor) const {
		return m_pNeighbor[eNeighbor]; }

	_uint Get_CellIndex() const {
		return m_iIndex; }
public:
	void Set_Neighbor(NEIGHBOR eNeighbor, CCell* pCell) {
		pCell->AddRef(); m_pNeighbor[eNeighbor] = pCell; }

public:
	HRESULT Ready_Cell(const _vec3* pPointA, const _vec3* pPointB, const _vec3* pPointC, const _uint& iIndex);
	_bool is_InCell(_vec3 vMovedPos, LINE* pOutLine);
	_bool Compare_Point(const _vec3* pPointA, const _vec3* pPointB);
	void Clear_Neighbor();
	void Render_Cell();
private:
	LPDIRECT3DDEVICE9	m_pGraphic_Device = nullptr;
	LPD3DXLINE			m_pLineSDK = nullptr;
	_vec3				m_vPoint[POINT_END];
	_uint				m_iIndex = 0;
private:
	CCell*				m_pNeighbor[NEIGHBOR_END] = { nullptr };
	CLine*				m_pLine[LINE_END];

public:
	static CCell* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _vec3* pPointA, const _vec3* pPointB, const _vec3* pPointC, const _uint& iIndex);
protected:
	virtual void Free();
};

_END