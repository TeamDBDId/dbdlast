#pragma once

// 키보드, 마우스, 게임패드를 초기화한다.
// 위 입력장치의 입력상태를 조사한다.

#include "Base.h"

_BEGIN(Engine)

class _ENGINE_DLL CInput_Device final : public CBase
{
	_DECLARE_SINGLETON(CInput_Device)
public:
	enum MOUSEBUTTON {DIM_LBUTTON, DIM_RBUTTON, DIM_WHEEL, DIM_XBUTTON};
	enum MOUSEMOVE {DIM_X, DIM_Y, DIM_Z};

private:
	explicit CInput_Device();
	virtual ~CInput_Device() = default;
public:
	HRESULT Ready_Input_Device(HINSTANCE hInst, HWND hWnd);
	// 현재 키보드와 마우스의 상태를 얻어온다.
	void SetUp_InputState();
public:
	_byte Get_DIKeyState(_ubyte byKeyID) {
		return m_KeyState[byKeyID];  }
	_byte Get_DIMouseState(_ubyte byKeyID) {
		return m_MouseState.rgbButtons[byKeyID]; }
	LONG Get_DIMouseMove(_ubyte byKeyID) {
		return *((long*)&m_MouseState + byKeyID);	}
	
	
private:
	LPDIRECTINPUT8			m_pSDK = nullptr; // 입력장치를 생성하는 기능을 가진 객체다.
	LPDIRECTINPUTDEVICE8	m_pKeyBoard = nullptr;
	LPDIRECTINPUTDEVICE8	m_pMouse = nullptr;	
private:
	_byte					m_KeyState[256];
	DIMOUSESTATE			m_MouseState;
private:
	HRESULT Ready_KeyBoard(HWND hWnd);
	HRESULT Ready_Mouse(HWND hWnd);
protected:
	virtual void Free();
};

_END