#pragma once

#include "Base.h"


_BEGIN(Engine)
class CGameObject;
class _ENGINE_DLL CColl_Manager final : public CBase
{
	_DECLARE_SINGLETON(CColl_Manager)

public:

	enum eCollGroup {C_PLAYER,C_AABB,C_LAY,C_WOOD,C_CHEST,C_HOOK,C_GENER,C_CLOSET,C_WINDOW,C_EXITDOOR,C_HOUSE,C_HUT,C_TERRAIN,C_UNDER,C_PLANK,C_HATCH,C_TOTEM,C_EXIT,C_WEAPON,C_END	};
private:
	explicit CColl_Manager();
	virtual ~CColl_Manager() = default;

public:
	HRESULT Add_CollGroup(eCollGroup _eGroup, CGameObject* _pObj, _bool* _bool = nullptr);
	HRESULT	Erase_Obj(eCollGroup _eGroup, CGameObject* _pObj);
	HRESULT Coll(const _float& _fTime);
	
private:
	void Coll_Player_LAY_IdxCheck();
	void Coll_Player_LAY(const _uint& _iIdx);

	void Coll_Player_HOUSEHUT_IdxCheck(const _float& _fTime);
	void Coll_Player_HOUSEHUT(const _float& _fTime,const eCollGroup& _eGroup, const _uint& _iIdx);

	void Coll_Player_Wood();
	void Coll_Player_ExitBuilding();
	void Coll_Player_ExitDoor(const _uint& _iIdx);
	void Coll_Player_Player();

	void Coll_Player_Interation_IdxCheck();
	void Coll_Player_Interation(const _uint& _iIdx, const eCollGroup& _eCollGroup);

	void Coll_Player_Weapon();

private:
	const _uint		MAXIDX = 196;
	vector<COL*>	m_CollList[196][C_END];



protected:
	virtual void Free(void);
};

_END