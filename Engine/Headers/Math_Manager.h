#pragma once
#include "Engine_Defines.h"

_BEGIN(Engine)
class _ENGINE_DLL Math_Manager
{
public:
	Math_Manager();
	~Math_Manager() = default;

public:
	static _int CalRandIntFromTo(const _int& _iFrom, const _int& _iTo);
	static const _float& CalRandFloatFromTo(const _float& _fFrom, const _float& _fTo);

	static _bool CalBoolPer(const _float& _fPer);
	static _bool IsRectIn(const _vec2& MousePos, const _vec4& Rect);
};


_END