#pragma once

// 1. 객체들을 모아서 구분한다.
// 2. 객체들의 업데이트를 수행한다.
// 3. 객체들의 렌더링을 수행한다.(xxxxxxxxxxxxxx)

#include "Base.h"
#include "GameObject.h"

_BEGIN(Engine)

class CLayer final : public CBase
{
private:
	explicit CLayer();
	virtual ~CLayer() = default;
public:
	CComponent* Get_ComponentPointer(const _tchar* pComponentTag, const _uint& iIndex);
public:
	HRESULT Add_Object(CGameObject* pGameObject);
	HRESULT Ready_Layer();
	_int Update_Object(const _float& fTimeDelta);
	_int LastUpdate_Object(const _float& fTimeDelta);	

public:
	list<CGameObject*>& Get_ObjList() { return m_ObjectList; }
private:
	list<CGameObject*>			m_ObjectList;
	typedef list<CGameObject*>	OBJECTLIST;
public:
	list<CGameObject*> & GetGameObjectList();
	CGameObject* Get_GameObject(const _uint& iIndex);
public:
	static CLayer*	Create();
protected:
	virtual void Free();
};

_END