#pragma once

#include "Component.h"

_BEGIN(Engine)

class CHierarchyLoader;
class CAnimationCtrl;
class _ENGINE_DLL CMesh_Dynamic final : public CComponent
{
public:
	enum OBJECT { OTHER, CAMPER, SLASHER};
private:
	explicit CMesh_Dynamic(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CMesh_Dynamic(const CMesh_Dynamic& rhs);
	virtual ~CMesh_Dynamic() = default;
public:
	_matrix Get_LocalTransform(const _uint& iContainerIdx) const;
	_uint Get_NumMeshContainer() const {
		return m_vecMeshContainer.size();
	}
	const _matrix* Find_Frame(const char* pFrameName);
	const D3DXMESHCONTAINER_DERIVED* Get_MeshContainer(const _uint& iIndex) const {
		return m_vecMeshContainer[iIndex];
	}
	_bool	IsOverTime(const _float& fCorrectionValue);
public:
	HRESULT Ready_Mesh_Dynamic(const _tchar* pFilePath, const _tchar* pFileName);
	HRESULT Ready_Clone_Mesh_Dynamic(void* pArg);
	_bool Update_Skinning(const _uint& iMeshContainerIdx, const _uint& iAttributeID);
	void Render_Mesh(const _uint& iMeshContainerIdx, const _uint& iAttributeID);
	HRESULT Set_AnimationSet(const _uint& iAnimationID, _bool bIsServe = false); // 특정 애니메이션 동작을 준비시킨다.
	HRESULT Set_NoBleningAnimationSet(const _uint& iAnimationID);
	HRESULT Set_ServeAnimationSet(const _uint& iAnimationID);
	void Play_Animation(const _float& fTimeDelta); // 애니메이션을 재생시키낟.
	void Play_ServeAnimation(const _float& fTimeDelta);
	void  Setup_AnimationTime(const _uint& Track, const _float& fFixingTime);
	_uint Get_CurAnimation();
private:
	CHierarchyLoader*	m_pLoader = nullptr;
	D3DXFRAME*			m_pRootFrame = nullptr;
	D3DXFRAME*			m_pConnectFrame = nullptr;
	D3DXFRAME*			m_pServeRootFrame = nullptr;
	_matrix				m_matPivot;
	CAnimationCtrl*		m_pAnimationCtrl = nullptr;
	_uint				m_CurAnimation = 0;
	const char*			m_pServeRootName = nullptr;
	CAnimationCtrl*		m_pServeAnimationCtrl = nullptr;
	_bool				m_bIsServeAnimation = false;
	OBJECT				m_eObejct = OTHER;
private:
	vector<D3DXMESHCONTAINER_DERIVED*>			m_vecMeshContainer;
	typedef vector<D3DXMESHCONTAINER_DERIVED*>	VECMESHCONTAINER;
public:
	void Update_CombinedTransformationMatrices(D3DXFRAME_DERIVED*	pFrame, const _matrix* pParentMatrix);
	HRESULT SetUp_CombinedMatrixPointer(D3DXFRAME_DERIVED*	pFrame);
	void Separate_Bone(D3DXFRAME_DERIVED*	pFrame);
public:
	static CMesh_Dynamic* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _tchar* pFilePath, const _tchar* pFileName);
	virtual CComponent* Clone_Component(void* pArg = nullptr);
protected:
	virtual void Free();
};

_END